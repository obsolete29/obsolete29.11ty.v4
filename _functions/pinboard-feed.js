let Parser = require('rss-parser'),
    parser = new Parser(),
    fs = require('fs'),
    file = '/home/obsolete29/Notes/2-Writing/index.md';

(async () => {

  let feed = await parser.parseURL('https://feeds.pinboard.in/rss/secret:c0b516feebde1ead703f/u:obsolete29/t:goodmorningcomputer/unread/');
  let heading = `## Things I found interesting...\n\nHere are some things I found interesting around the web this week.\n\n`
  let content = heading
  feed.items.forEach(item => {
    let title = item.title.replace('[toread]', '');
    let newContent = `### [${title.trim()}](${item.link})\n${item.content}\n\n`;
    content += newContent
  });
  // console.log(content);
    // append data to a file
    fs.appendFile(file, content, (err) => {
        if (err) {
            throw err;
        }
        console.log("File is updated.");
    });
})();