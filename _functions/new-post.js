const fs = require('fs');
      path = require('path');

let files = ['post.yml','index.md'];
let srcDir = '/home/obsolete29/Writing/Templates';
let destDir = '/home/obsolete29/Writing/Drafts';

files.forEach(file => {
    let src = path.join(srcDir, file);
    let dest = path.join(destDir, file);
    fs.copyFileSync(src, dest);
});
