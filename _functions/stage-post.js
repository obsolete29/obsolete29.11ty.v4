// Dependencies
const fs = require('fs'),
      path = require('path'),
      os = require('os'),
      fm = require('front-matter'),
      slugify = require('slugify'),
      { DateTime } = require('luxon'),
      now = DateTime.now(),
      YAML = require('json-to-pretty-yaml'),
      urljoin = require('url-join');

// init directory vars
const homeDir = os.homedir(),
      publishYearDir = now.year.toString(),
      draftDir = path.join(homeDir, '/Writing/Drafts'),
      rootPublishDir = path.join(homeDir, '/Projects/obsolete29.11ty.v4/src/posts', publishYearDir),
      tempFile = path.join(homeDir, '/Projects/obsolete29.11ty.v4/_temp', 'metadata.json'),
      postRootUrl = 'https://obsolete29.com/posts/';

// get front matter file and contents
function getFrontMatter(dir) {
  const yamlFile = fs.readdirSync(dir).filter(function(e){
      return path.extname(e).toLowerCase() === '.yml'
    });
    const frontMatterFile = path.join(draftDir, yamlFile[0])
    const frontMatterContent = fm(fs.readFileSync(frontMatterFile,'utf8'));
    frontMatterContent.attributes.date = now.toISO()
    return frontMatterContent.attributes
}
const frontMatter = getFrontMatter(draftDir);
const newFolderName = slugify(frontMatter.title, {lower:true})

// get content file and contents
const contentFile = fs.readdirSync(draftDir).filter(function(e){
  return path.extname(e).toLowerCase() === '.md'
});
const fullContentFile = path.join(draftDir, contentFile[0]);
const postContent = fs.readFileSync(fullContentFile,'utf8');

// create new publish dir

const fullFolderName = path.join(rootPublishDir, newFolderName);
if (!fs.existsSync(fullFolderName)){
  fs.mkdirSync(fullFolderName);
}

// write new file
const newContent = `---\n${YAML.stringify(frontMatter)}---\n${postContent}`;
const newContentFileName = path.join(fullFolderName, 'index.md');
fs.writeFileSync(newContentFileName, newContent);

// save social metadata
function saveSocialMeta(frontmatter) {
  let meta = Object.assign( {}, frontmatter );
  delete meta.tags;
  delete meta.desc;
  delete meta.date;
  let postDateSlug = now.toFormat('yyyy/MM/dd');
  meta.socialUrl = urljoin(postRootUrl, postDateSlug, newFolderName);
  let metaContent = JSON.stringify(meta)
  fs.writeFileSync(tempFile, metaContent);
  console.log(metaContent);
}
saveSocialMeta(frontMatter);

