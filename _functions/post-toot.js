const fs = require('fs');
const path = require('path');
const os = require('os');
const config = require('config');
const api = config.get('api');
const https = require('https')

const homeDir = os.homedir()
const tempFile = path.join(homeDir, '/Projects/obsolete29.11ty.v4/_temp', 'metadata.json');

function getJSONMetadata(file) {
  let metadata = JSON.parse(fs.readFileSync(file,'utf8'))

  function getSocialTitle(frontmatter) {
    let title = 'New blog post --> '
    let socialTitle = `${title} ${frontmatter.title}\n`
    return socialTitle
  }
  let socialTitle = getSocialTitle(metadata);

  function getSocialTags(frontmatter) {
    let string = "";
    let tags = frontmatter.socialTags;
    for(let i = 0; i < tags.length; i++){
        let tag = `#${tags[i]} `;
        string = string.concat(tag);
    }
    string = string.concat('\n');
    return string;
  }
  socialTags = getSocialTags(metadata);

  function getSocialUrl(frontmatter) {
    let url = frontmatter.socialUrl;
    return url;
  }
  socialUrl = getSocialUrl(metadata);

  let status = `${socialTitle}\n${socialTags}\n${socialUrl}`
  return status
}
let newPost = getJSONMetadata(tempFile);

const data = JSON.stringify({
  status: newPost
});

const options = {
  hostname: 'indieweb.social',
  port: 443,
  path: '/api/v1/statuses',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Content-Length': data.length,
    'Authorization': `Bearer ${api.key}`
  }
}

const req = https.request(options, res => {
  console.log(`statusCode: ${res.statusCode}`)

  res.on('data', d => {
    process.stdout.write(d)
  })
})

req.on('error', error => {
  console.error(error)
})

req.write(data)
req.end()
