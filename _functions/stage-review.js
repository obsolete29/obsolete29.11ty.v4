const fs = require('fs');
const path = require('path');
const draftDir = path.resolve('/home/obsolete29/Notes/2-Writing/');
const draftArchiveDir = path.resolve('/home/obsolete29/Notes/Archive');
const fm = require('front-matter');
const slugify = require('slugify');
const { DateTime } = require('luxon');
const now = DateTime.now();
const YAML = require('json-to-pretty-yaml');
const rootPublishDir = '/home/obsolete29/Projects/obsolete29.11ty.v4/src/'
const rootPublishDirYear = '/2022/'

// get front matter file and contents
const yamlFile = fs.readdirSync(draftDir).filter(function(e){
    return path.extname(e).toLowerCase() === '.yml'
});
const frontMatterFile = draftDir + '/' + yamlFile[0]
const files = [yamlFile]
const frontMatterContent = fm(fs.readFileSync(frontMatterFile,'utf8'));
const frontMatter = frontMatterContent.attributes
const newFolderName = slugify(frontMatter.title, {lower:true})
frontMatter.date = now.toISO()
const publishDir = frontMatter.publishDir
function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}
const titlePrefix = capitalize(frontMatter.tags[0])

function getContentHeader(frontmatter) {
    function getCoverImage(frontmatter) {
        return `{% myImage "cover.jpg", "Title cover for {{ ${frontmatter.title} }}." %}\n`
    }
    const coverImage = getCoverImage(frontMatter);
    
    function getTitle(frontmatter) {
        if (frontmatter.series) {
            return `[${frontmatter.title}](${frontmatter.titleurl}) (${frontmatter.series})`
        }
        else {
            return `[${frontmatter.title}](${frontmatter.titleurl})`
        }
    }
    const contentTitle = getTitle(frontMatter)
    
    function getAuthor(frontmatter) {
        if (frontmatter.tags[0] == "read") {
            return '[' + frontmatter.author + '](' + frontmatter.authorurl + ')'
        }
        else {
            return "None"
        }
    }
    const contentAuthor = getAuthor(frontMatter);
    
    function getRating(frontmatter) {
        return frontmatter.rating
    }
    const contentRating = getRating(frontMatter);
    if (frontmatter.tags[0] == "read") {
        return `${coverImage}\n- ${contentTitle}\n- ${contentAuthor}\n- ${contentRating}\n`;
    } else {
        return `${coverImage}\n- ${contentTitle}\n- ${contentRating}\n`;
    }
}

const contentHeader = getContentHeader(frontMatter)

// clean up frontMatter
frontMatter.title = titlePrefix + ' ' + frontMatter.title;
delete frontMatter.publishDir
delete frontMatter.series
delete frontMatter.author
delete frontMatter.authorurl
delete frontMatter.titleurl

// get content file and contents
const contentFile = fs.readdirSync(draftDir).filter(function(e){
    return path.extname(e).toLowerCase() === '.md'
  });
const fullContentFile = draftDir + '/' + contentFile[0]
files.push(contentFile)
const postContent = fs.readFileSync(fullContentFile,'utf8');

// create new publish dir
const fullFolderName = rootPublishDir + publishDir + rootPublishDirYear + newFolderName
const fullImageFolderName = fullFolderName + '/images'
fs.mkdirSync(fullFolderName)
fs.mkdirSync(fullImageFolderName)

// write new file
const newContent = `---\n${YAML.stringify(frontMatter)}---\n${contentHeader}\n${postContent}`;
const newContentFileName = fullFolderName + '/index.md'
fs.writeFileSync(newContentFileName, newContent)

// copy cover.jpg
const imageFile = fs.readdirSync(draftDir).filter(function(e){
    return path.extname(e).toLowerCase() === '.jpg'
});
const coverImageName = '/cover.jpg'
const coverImageSrcDir = draftDir + '/' + imageFile[0];
files.push(imageFile);
const coverImageDestinationDir = fullImageFolderName + coverImageName
fs.copyFileSync(coverImageSrcDir, coverImageDestinationDir)

// archive source files
files.forEach(function (fileName) {
    let currentPath = path.join('/home/obsolete29/Notes/2-Writing/', fileName[0]);
    const fullFolderName = '/home/obsolete29/Notes/Archive/2022/' + newFolderName
    if (!fs.existsSync(fullFolderName)){
        fs.mkdirSync(fullFolderName);
    }
    let destinationPath = path.join(fullFolderName, fileName[0]);
    fs.rename(currentPath, destinationPath, function (err) {
      if (err) {
          throw err
      } else {
          console.log("Successfully moved the file!");
      }
    });
  });