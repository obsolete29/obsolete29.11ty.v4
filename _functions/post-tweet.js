const   Twit = require('twit');
        config = require('config');
        twitterAPI = config.get('twitter');
        fs = require('fs');
        path = require('path');
        os = require('os');
        config = require('config');
        homeDir = os.homedir()
        tempFile = path.join(homeDir, '/Projects/obsolete29.11ty.v4/_temp', 'metadata.json');

const T = new Twit({
  consumer_key:         twitterAPI.consumer_key,
  consumer_secret:      twitterAPI.consumer_secret,
  access_token:         twitterAPI.access_token,
  access_token_secret:  twitterAPI.access_token_secret,
  timeout_ms:           60*1000,  // optional HTTP request timeout to apply to all requests.
  strictSSL:            true,     // optional - requires SSL certificates to be valid.
});

function getJSONMetadata(file) {
    let metadata = JSON.parse(fs.readFileSync(file,'utf8'))
  
    function getSocialTitle(frontmatter) {
      let title = 'New blog post --> '
      let socialTitle = `${title} ${frontmatter.title}\n`
      return socialTitle
    }
    let socialTitle = getSocialTitle(metadata);
  
    function getSocialTags(frontmatter) {
      let string = "";
      let tags = frontmatter.socialTags;
      for(let i = 0; i < tags.length; i++){
          let tag = `#${tags[i]} `;
          string = string.concat(tag);
      }
      string = string.concat('\n');
      return string;
    }
    socialTags = getSocialTags(metadata);
  
    function getSocialUrl(frontmatter) {
      let url = frontmatter.socialUrl;
      return url;
    }
    socialUrl = getSocialUrl(metadata);
  
    let status = `${socialTitle}\n${socialTags}\n${socialUrl}`
    return status
}
let newPost = getJSONMetadata(tempFile);

T.post('statuses/update', { status: newPost }, function(err, data, response) {
  console.log(response.status)
})
