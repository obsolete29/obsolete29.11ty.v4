const localDir = "../obsolete29v4";
const CleanCSS = require("clean-css");
const slugify = require("slugify");
const { DateTime } = require('luxon');
const Image = require("@11ty/eleventy-img");
const pluginRss = require("@11ty/eleventy-plugin-rss");
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");

async function imageShortcode(src, alt, sizes = "100vw") {
  // let sourcePath = (src.startsWith('./src/posts/')) ? src : './src/posts/' + this.page.fileSlug + '/images/' + src;
  let sourceDir = this.page.inputPath.replace('index.md', '')
  let sourcePath = sourceDir + 'images/' + src;

  let metadata = await Image(sourcePath, {
    widths: [360, 651, 778],
    formats: ["webp", "jpeg"],
    urlPath: "/images/",
    outputDir: localDir + "/images/"
  });

  let imageAttributes = {
    alt,
    sizes,
    loading: "lazy",
    decoding: "async",
  };

  // You bet we throw an error on missing alt in `imageAttributes` (alt="" works okay)
  return Image.generateHTML(metadata, imageAttributes, {
    whitespaceMode: "inline"
  });
};

async function defaultImageShortcode(src, alt, sizes) {
  let metadata = await Image(src, {
    widths: [360, 651, 778],
    formats: ["webp", "jpeg"],
    urlPath: "/images/",
    outputDir: localDir + "/images/"
  });

  let imageAttributes = {
    alt,
    sizes,
    loading: "lazy",
    decoding: "async",
  };

  // You bet we throw an error on missing alt in `imageAttributes` (alt="" works okay)
  return Image.generateHTML(metadata, imageAttributes, {
    whitespaceMode: "inline"
  });
}
module.exports = function(eleventyConfig) {
  eleventyConfig.addPassthroughCopy({ "src/assets/favicon": "/" });
  eleventyConfig.addFilter("cssmin", function(code) {
    return new CleanCSS({}).minify(code).styles;
  });
  eleventyConfig.addFilter("slug", (input) => {
    const options = {
      replacement: "-",
      remove: /[#&,+()$~%.'":*?!<>{}]/g,
      lower: true
    };
    return slugify(input, options);
  });
  eleventyConfig.addFilter("date", function(dateIn) {
    return DateTime.fromJSDate(dateIn, {zone: 'utc'}).toFormat('yyyy/MM/dd');
  });
  eleventyConfig.addFilter("dateformat", function(dateIn) {
    return DateTime.fromISO(dateIn, {zone: 'utc'}).toFormat('MMMM d, yyyy');
  });
  eleventyConfig.addNunjucksAsyncShortcode("myImage", imageShortcode);
  eleventyConfig.addNunjucksAsyncShortcode("image", defaultImageShortcode);
  eleventyConfig.addPlugin(pluginRss);
  eleventyConfig.addPlugin(syntaxHighlight);
  return {
    dir: {
      input: "src",
      output: localDir
    },
    markdownTemplateEngine: "njk"
  };
};