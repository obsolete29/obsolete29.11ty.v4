---
title: Follow Mike
permalink: /follow/
---
Thanks for your interest in following my posts! 

## RSS

- [Michael Harley's Blog](/feed/feed.xml) - This is my traditional RSS feed with my blog posts.
- [Michael Harley's Reviews](/feed/reviews.xml) - I log the movies, tv series and books I consume on my [reviews page](/reviews/). This is the RSS feed if you're interested in such things.

## Email

If you don't know what RSS is or just prefer an email notification about new posts you can subscribe to new post notifications by clicking the link.

