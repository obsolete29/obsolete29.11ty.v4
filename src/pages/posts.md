---
title: All Posts
permalink: /posts/
---
{% for post in collections.posts | reverse %}
<p><span class="date">{{ post.data.date | dateformat }}</span><a href={{ post.url }}>{{ post.data.title }}</a></p>
{% endfor %}