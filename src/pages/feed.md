---
title: RSS Feeds
permalink: /feed/
---
Thanks for your interest in following my RSS feeds! Here is what I'm currently publishing.

- [Michael Harley's Blog](/feed/feed.xml) - This is my traditional RSS feed with my blog posts.
- [Michael Harley's Reviews](/feed/reviews.xml) - I log the movies, tv series and books I consume on my [reviews page](/reviews/). This is the RSS feed if you're interested in such things.