---
title: Things I use
desc: This is my list of hardware and software that I am currently using.
permalink: /uses/
lastUpdated: '2022-07-31T09:22:55.000-05:00'
---
Here is my list of hardware and software that I use to do my things.

## Desktop Hardware
- [System76 Meerkat](https://system76.com/desktops/meerkat) mini (meer5)
- OS: [Pop!_OS](https://pop.system76.com/)
- Monitor: HP 345495 Business Z27n G2 27" LED LCD Monitor - 16:9-5 ms GTG 
- Keyboard: System76 [Launch Configurable Keyboard](https://system76.com/accessories/launch) with Kailh Box Jade Switches.
- Mouse: Logitech MX Master 3 Advanced Wireless Mouse - Graphite
- Speakers: Mackie Studio Monitor, Black w/green trim, 3-inch (CR3)
- Microphone: AKG Pro Audio Lyra Ultra-HD, Four Capsule, Multi-Capture Mode, USB-C Condenser Microphone
- Camera: Logitech - C922 Pro Stream Webcam

<figure>
  {% image "src/assets/images/uses/desk01.jpg", "Wood computer desk with RGB lights and a desktop computer displayed.", "(min-width: 30em) 50vw, 100vw" %}
  <figcaption>Here is command central.</figcaption>
</figure>

## Laptop
- MacBook Pro (Retina, 13-inch, Early 2015)
- OS: [Pop!_OS](https://pop.system76.com/)

## Software
- [Firefox](https://www.mozilla.org/en-US/firefox/new/)
- [KeePassXC](https://keepassxc.org/)
- [Feedbin](https://feedbin.com) #rss
- [Signal](https://signal.org/)
- [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox) #music
- [Obsidian](https://obsidian.md/) #notes
- [Syncthing](https://syncthing.net/)
- [VSCodium](https://vscodium.com/)
- [Freetube](https://freetubeapp.io/)
- [Typora](https://typora.io/)

## Mobile
- Pixel 4a
- [CalyxOS](https://calyxos.org/)
- Firefox
- [K-9 Mail](https://f-droid.org/en/packages/com.fsck.k9/)
- [KeePassDX](https://f-droid.org/en/packages/com.kunzisoft.keepass.libre/)
- [Aegis](https://f-droid.org/en/packages/com.beemdevelopment.aegis/)
- Signal

## Hosting
- [Hover](https://www.hover.com/) #dns
- [Capsul](https://capsul.org/) #vps
- Fastmail #email #main

## Other Hardware 
- [Synology DS220+](https://www.synology.com/en-us/products/DS220+) #nas
- [Ubiquity Dream Machine Pro](https://store.ui.com/collections/unifi-network-unifi-os-consoles/products/udm-pro)
- KVM: TESmart 4-Port HDMI KVM Switch
- Kobo eReader

## Deprecated Hardware
- iPhone SE (2nd gen)
- Apple Watch (4th gen)