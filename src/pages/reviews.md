---
title: All Reviews
permalink: /reviews/
---
This is where I log and review the media I consume, including books, tv shows and movies. 

{% for post in collections.reviews | reverse %}
<p><span class="date">{{ post.data.date | dateformat }}</span><a href={{ post.url }}>{{ post.data.title }}</a><span class="stars">{{ post.data.rating }}</span></p>
{% endfor %}