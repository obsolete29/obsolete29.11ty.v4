---
title: Contact
permalink: /contact/
lastUpdated: "2022-07-30T20:20:04.000-05:00"
---
The best way contact me is email: <a href="mailto:{{ site.author.email }}">{{ site.author.email }}</a>