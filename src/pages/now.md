---
title: What I'm doing right now
permalink: /now/
date: '2022-01-15T05:23:06.364-06:00'
lastUpdated: '2022-01-15T05:57:30.713-06:00'
---
It's ground hog day at my house. Every day feels like a repeat of the previous. I wear lounge clothes full time so there's no demarcation of the days and the COVID march continues.

## Personal

I've enjoyed tinkering around on my personal web site. Either iterating through designs or writing blog posts about tech topics. Rachelle and I are watching Ozark together. She seems to always have some project around the house. Moving the TV here or there. Painting this or that. I'm going through fits and starts of reading and I'm hoping to read some books that are outside my normal wheel house of military science fiction.

## Professional

I'm back at the USDA as a SharePoint Developer, Practice Lead. I feel like I've leveled up my skills with Power Apps and Power Automate in the past six to eight months and I find a lot of satisfaction in the work I'm doing with the team. I believe I've had a positive impact and I will endeavor to continue.

I finished my first six month contract at Asurion, also doing SharePoint Development. I was doing that full time for the first four months but missed working as part of a team so I finished the project on a part time basis. 

I'm continuing my part time work at Asurion with another group with another part time role. I've really enoyed working in these part time roles because it gives me opportunities to solve different problems than I'm solving at USDA. It's great experience and I feel myself leveling up at a rapid pace.

_This “Now” page is inspired by [Derek Sivers’ nownownow project](https://www.jakekorth.com/now/)._