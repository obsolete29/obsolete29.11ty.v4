---
title: About
permalink: /about/
---
By day, I work as a SharePoint Developer specializing in Power Apps, Power Automate and PowerShell to solve business problems. Let's connect on [LinkedIn](https://www.linkedin.com/in/mharleydev/) if that's your jam.

By night, I've been teaching myself web development and I've fallen into [Jamstack](https://jamstack.org/) because of the static site generator ([Eleventy](https://11ty.dev)) I use to build this site. As I'm typing these words, I'm interested in learning more javascript and node.

Lately, I've been embracing [IndieWeb](https://indieweb.org/), which for me means being purposeful about my online identity and the content I'm producing, such as it is. I've also become passionate about online privacy and resisting surveillance capitalism, which lines up nicely with the IndieWeb movement. If I'm owning my own site and content, then it's harder for the online tech giants to track me across the web. Instead of writing tweet chains on twitter, I'll post my thoughts on my blog. Instead of using Goodreads to track my book reviews, I want to make an effort to prefer posting those ratings and reviews on my site first.

My passions go through peaks and valleys but I also enjoy reading, foosball, hiking, riding bicycles (bike packing!), grilling and my cats.

Nearly 1 million years ago, I signed up for an online message board related to the small rural town I lived in at the time. I was into this industrial metal band called [Fear Factory](https://fearfactory.com/). They had a recently released album called Obsolete. I was twenty-nine years old. Add all that up and you get obsolete29.

<figure>
  {% image "src/assets/images/about/mike-hat.jpeg", "Selfie of Mike with a big beard, glasses and a large knit hat.", "(min-width: 30em) 50vw, 100vw" %}
  <figcaption>Look at this dang fancy hat - January 22nd 2022</figcaption>
</figure>