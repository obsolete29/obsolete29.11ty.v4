---
title: "A quick note on contactless payments"
date: "2022-05-22T07:05:59.300-05:00"
tags:
  - "contactlesspayments"
  - "apple"
socialTags:
  - "GoodMorning"
---
I receive the [Recommendo newsletter](https://www.getrevue.co/profile/Recomendo/issues/exit-scam-yestheory-contactless-payments-1180023?utm_campaign=Issue&utm_content=view_in_browser&utm_medium=email&utm_source=Recomendo) and they had a note about contactless payments that I really needed to comment on.

> I’m traveling in England and no stores seem to want to use cash anymore. Everyone uses [Apple Pay](https://support.apple.com/en-us/HT204506?utm_campaign=Recomendo&utm_medium=email&utm_source=Revue newsletter) even for the smallest purchase. Contactless payment made by hovering  your phone near a device is rapidly becoming common all around the  world, US included. I was immensely surprised how easy it was to hook my credit card up to my iPhone to make payments. Took 30 seconds, and no  new accounts, no bank, no wallet, just my usual credit card. Now it’s  Apple Pay all the time for me. — KK

You don't need a phone to use contactless payments. Most credit cards have NFC chips built right into them that allows you to wave it over the payment terminal to complete the transaction.

Be wary of tying yourself more closely to any particular tech ecosystem imo. Big banks are shitty enough all on their own. No need to insert a big tech giant between you and your banking relationships.