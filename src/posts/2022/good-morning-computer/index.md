---
title: "Good morning computer 23feb22"
date: "2022-02-23T06:34:23.008-06:00"
tags:
  - "goodmorningcomputer"
socialTags:
  - "GoodMorning"
---
I just finished writing the auto-post functionality of my publishing script for my blog posts. It's super hacky and I don't really understand all the node async/promise stuff but my script works and I'm pretty happy about that. I feel like I'm leveling up quickly and I'll soon be to the point where I'm ready to read and understand some books on the topic.

At work I've taken on the role of Product Lead on the SharePoint Development team. I'll also start taking on some scrum master responsibilities for the SharePoint projects. I'll coach the team up and help manage the technical aspects of the projects and just try really hard to make everyone be the best they can be. I'm both excited and anxious about the new role.

I'm reading and really enjoying [Project Hail Mary](https://www.andyweirauthor.com/) by Andy Weir.

This post is inspired by Warren Ellis and his [good morning computer](https://warrenellis.ltd/mc/commitment-to-the-ritual-morning-computer/) posts.