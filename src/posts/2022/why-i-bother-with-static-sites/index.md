---
title: "Why I bother with static sites"
desc: "Many people in my feed seem to be frustrated by running a static site. Here is why I persist."
date: "2022-03-10T06:18:45.854-06:00"
tags:
  - "blogging"
  - "eleventy"
socialTags:
  - "Blogging"
  - "Eleventy"
  - "StaticSites"
---
Ru Singh recently [blogged about her frustrations with static site generators](https://rusingh.com/why-bother-with-static-sites/) so I wanted to reflect and journal about my reasons for using one.

I'm attracted to static sites because of the simplicity of the idea. The entirety of my website exists as text and image files right on my computer. I have the maximum amount of flexibility and I can migrate to any number of hosting providers and I can do that very easily  I'm currently hosting my site on a VPS with [Capsul.org](https://capsul.org) but I can just as easily move to some sort of managed provider like GitHub Pages or one of the clones. I put a lot value in that flexibility and simplicity in serving pages.

I'm sure some sort of law about complexity has been identified but it seems there comes a point where complexity can no longer be reduced, only shifted and that's how I feel about the publishing part of static site generators. Yes, my site now exists as static files but the publishing process sure is more complex than a solution like Wordpress. I have to come up with some sort of filing system for the source posts. The writing experience is just not as a good in VS Code. And heaven help you if you want to make posts with *images*. 

When I read about [other people's workflow](https://kevq.uk/how-i-manage-jekyll-content/) for managing these things, I just cringe because I know exactly the problems they're solving for. I've cobbled together a few node scripts to help me with [my publishing workflow](/posts/2022/02/21/automating-some-of-the-publishing-steps-for-my-eleventy-blog/) and I use the excellent [image plugin for Eleventy](https://www.11ty.dev/docs/plugins/image/) to handle the image optimization bits but yeah, I understand Ru's frustrations. The publishing workflow is objectively harder and more complex with a static site generator than it is with Wordpress.

I've tried very hard to keep my dependancies at a bare minimum. As I've mentioned previously, I do not use GitHub pages or any equivalent to publish my content via a CI/CD pipeline. I don't use Vercel or Netlify. I build my site locally on my own computer then rsync the site to my VPS. I don't use any sort of CSS package (framework?) like Tailwind, Sass, PostCSS. I built my base.njk template and my style.css file with my bare hands. I plan to continue to iterate and improve on my publishing scripts as they're barely at a proof of concept right now.

So that's it. That's why I like and use static sites and some of the ways I try to mitigate the publishing complexities. 