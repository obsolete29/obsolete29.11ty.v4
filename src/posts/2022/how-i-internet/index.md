---
title: "How I Internet"
desc: "Inspired by Cassey Lottman, I am going to share a bit about how I spend my time online."
date: "2022-11-07T06:46:06.771-06:00"
tags:
  - "Uses"
socialTags:
  - "Uses"
---
Inspired by Cassey Lottman's, [My Internet](https://www.cassey.dev/posts/2022-11-05-my-internet/), I'm going to share a bit about how I spend my time online. I'm going to change my format up just a smidge though. Here is what I do, usually in order of priority.

## Mastodon

Nearly always, the first thing I reach for in the morning after checking text messages and email is Mastodon. This is my primary and only social media. I've been using it as my primary social media for over a year now and I think it's pretty much the perfect social media for me: Microblogging, FOSS and decentralized (federated decentralized, not block chain decentralized). If you have a Mastodon account, [let's be Internet friends](https://indieweb.social/@obsolete29).

On desktop, I access Mastodon via the web client. On mobile, I'm about half Mastodon web client and half Tusky. More and more I'm using Tusky.

I do not have Facebook, Instagram, Twitter or TikTok. It's not that I just use those platforms less than Mastodon, I literally deleted my accounts or just never had one (TikTok).

## Feedbin (RSS)

Nearly anytime I'm picking up my phone to do some scrolling, I'll check Mastodon and the #Always category on my reader. I've been a bit haphazard about how I've organized the feeds I follow, but if someone publishes a personal blog in their bio, I'll usually make the effort to follow the RSS feed and stick them in my #Bulk category. If I'm liking a person's blog posts, they can get bumped to #Always. High volume feeds usually get unfollowed after it's obvious.

Here are some general things I follow:

- Ars Technica - I'm a subscriber and I get full article text in the RSS feed. It's great.
- NY Times > Technology - I'm also a NY Times subscriber but they don't publish any sort of special feed. :(
- Lots of personal blogs - Like I said, if you publish a personal site or blog, I'll probably plonk the URL into Feedbin to see if it'll easily pull in a RSS feed.
- Lots of author blogs - I always seek out RSS feeds of my favorite authors. This is pretty hit and miss. Seems only about half the authors I like and want to follow seem to even have a real personal website and of those, a published RSS feed is spotty. Which sucks!
- Newsletters - Feedbin has a feature where you get a special email address that you can use to subscribe to newsletters with. This way, the few newsletters I follow come to my RSS reader and not my email. I'm not really a huge newsletter fan but the couple I follow come here!

On both desktop and mobile, I access Feedbin directly via the browser and find the experience to be pretty nice. No special client software needed. :) 

## Traditional media online

I want to support journalism. I think it's important so I put my money where my mouth is by subscribing. In the flow of my Internetting, going to one of these websites are usually a third level effort and I usually don't get this far.

- Ars Technica - I've been a member of Ars Technica since 1999. Back when they published articles about overclocking Intel Celeron chips to crazy speeds and QuakeWorld Team Fortress was a thing. I'm a paying member and really enjoy the full text RSS articles and the nice feeling I get by supporting something I like with money.
- NY Times - Traditional media and I seem to read a lot of Times articles. At least once a day, I'll scroll the home page article headlines.
- Washington Post - Ditto, though I don't come here to read quite as often as the Times. I might browse the Washington Post front page articles once a week.

Ok that's it and thanks for reading! How do you Internet?
