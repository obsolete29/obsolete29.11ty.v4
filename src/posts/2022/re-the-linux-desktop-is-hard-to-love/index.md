---
title: "Re: The Linux Desktop is Hard to Love"
desc: "Comparing Linux and Apple is a mistake as they are different approaches."
date: "2022-07-17T05:55:16.351-05:00"
tags:
  - "apple"
  - "linux"
socialTags:
  - "Apple"
  - "Linux"
---
I've seen some discussions about the "Linux Desktop" in the circles I follow recently. Brad even made a whole [blog post](https://tdarb.org/blog/linux-love.html) about it. I'm a [recovering Apple user](/posts/2021/08/06/i-am-breaking-up-with-apple/) and I've been using Linux as my full time daily driver about a year so I have some opinions about this. I wanted to use Brad's post as an opportunity to respond.

> If I had to summarize in a word what Linux lacks compared to macOS it would be: *cohesion*.

I think it's unfair to compare macOS with "Linux" as a whole. Apple is the richest company in the history of the world. They hire armies of the best developers, designers, project mangers, and computer scientists. Of *course* they can make a product that's more consistent overall. They're so committed to this idea that they created their own computer chips! By comparison, Linux is developed by a heard of cats all going in different directions, doing their own thing for their own reasons that runs mostly on used hardware.

MacOS is objectively a better user experience. Their hardware is best in class. Their products work seamlessly together. Pairing AirPods to a new iPhone or having your contacts automatically synced between your iPhone and Mac is *very nice*.

Despite all that, Linux still aligns better with my principles, ideals and goals. I'm willing to use products that are only 90% as a good in order to operate in a manner that feels truer to those principles.

Apple's plans to [scan user devices for banned content](https://www.theregister.com/2021/08/05/apple_csam_scanning/) was the main thing that made me realize that Apple wasn't really the company I thought they were. In short, it made me feel like the devices I paid Apple a lot of money for, were just another end point on their cloud infrastructure. Once the scanning capability is deployed to handsets, it's just a policy decision that prevents abuse by the company. It seems unlikely to me that China would be unable to compel Apple to do it's bidding for example.

I feet that having a technology mono culture and being totally dependent on a single company for technology needs is a vulnerability. Before last summer, I was allll in on Apple. Phone, computers, watch, headphones, tv streaming device, smart assistants. What would have happened if my Apple ID was somehow compromised or disabled?

I think Apple only cares about privacy insofar as it's their marketing strategy. I think Apple is actively hostile to interoperability, competition and open standards. I don't really care that contacts sync automatically and seamless to all other Apple devices. I'll continue to grumble on Mastodon about my frustrations of doing that simple task with Linux and Google-free Android but at least I feel like I'm in more control of technology. I'm so glad that Linux gives me the opportunity to do so.
