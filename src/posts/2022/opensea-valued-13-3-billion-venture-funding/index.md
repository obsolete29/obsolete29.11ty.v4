---
title: OpenSea valued at $13.3 billion in new round of venture funding.
desc: Tich people are dumb.
date: '2022-01-05T06:58:50.032-06:00'
tags:
  - nft
  - blockchain
slug: opensea-valued-13-3-billion-venture-funding
---
> OpenSea, one of the most talked about blockchain start-ups in Silicon Valley, said on Tuesday that it had raised $300 million in new venture capital, making it the latest company to cash in on a rush to fund cryptocurrency start-ups.
>
> <cite>[NYTimes](https://www.nytimes.com/2022/01/04/business/opensea-13-billion-valuation-venture-funding.html)</cite>

Rachelle and I recently listened to [The Dropout](https://abcaudio.com/podcasts/the-dropout/) which is the story of how Elizabeth Holmes did some fraud on investors based pretty much on straight up lies. She had some idea, not based in reality and for some reason people wanted to believe in a 19 year old Stanford dropout. Everyone seemed to think she was the next Steve Jobs and they all wanted to be in on the ground floor.

That's what I thought of when I read that OpenSea received a new round of funding and is now somehow magically valued at $13 billion. There's all this hype, based pretty much on nothing and that seems to make rich people really super excited. You see, people with money think that having and getting money makes them *really* smart. They like to drive around in exotics cars, hustling, sleeping 4 hours a night, wearing the same outfit everyday, drinking Soylent, feeling important and trying to get more money. But none of that means shit.

The problem is that people are going to see that OpenSea got new funding and read the reports that they're valued at $13 billion. That by itself will make the idea of NFTs credible to lots of people. Do not be fooled dear reader. NFTs are bullshit and it doesn't matter how much OpenSea is valued. 

People like Elizabeth Holmes, must have a force of will and ability to divorce themselves from reality because they're able to talk old guys with lots of money into investing in their vision. Once you get one big name investor, then everyone else wants to be in the cool kid club too. OpenSea has an advantage over Holmes though as they're just dealing in straight up hype, digital tokens and such. They don't have to make a medical device that can do the thing they're claiming it will to do. 

Don't get caught up in the hype folks. I know we're all excited about new technology and we want to be in on the ground floor of the cool new thing but I still think NFTs are nothing but hype.

