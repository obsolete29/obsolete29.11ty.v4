---
title: "Good morning computer, 07Mar22"
date: "2022-03-07T05:53:52.490-06:00"
tags:
  - "goodmorningcomputer"
socialTags:
  - "GoodMorning"
---
Good morning computer,

My weekend was better than average. One of my favorite pre-pandemic bars finally reopened. Rachelle and I went with our friend Whitney to hang out and check out the remodeled space. The patio has been upgraded with an actual covered section, instead of just those tarp sail things but I think overall the place lost some of it's character. 

I [finished reading](/reviews/2022/03/06/read-better-off-dead/) the latest Jack Reacher novel. These are not great works of literature but still fun to read.

Here's a link round up of things I found interesting or enjoyed reading.

* [Attackers can force Amazon Echos to hack themselves with self-issued commands](https://arstechnica.com/information-technology/2022/03/attackers-can-force-amazon-echos-to-hack-themselves-with-self-issued-commands/)
* [Bye iOS, hello Android](https://chrislachance.com/bye-ios-hello-android/)
* [I now have a QR code tattoo!](https://astrid.tech/2022/03/03/0/qr-tattoo-result/)
* [An Open Letter To Epic Games Regarding Bandcamp](https://blog.grappling.ca/h1an-open-letter-to-epic-games-regarding-bandcamp-h1)
* [Thoughts On Markdown](https://www.smashingmagazine.com/2022/02/thoughts-on-markdown/)
* [Write plain text files](https://sive.rs/plaintext)

