---
title: Say hello to obsolete29.com v4!
desc: Eleventy came out with v1.0 so I rebuilt my site to celebrate.
date: '2022-01-16T12:30:19.344-06:00'
tags:
  - obsolete29
  - obsolete29v4
  - eleventy
---
[Eleventy](https://www.11ty.dev/) recently released version 1.0 so I decided to celebrate by rebuilding my site with the new version. I was also especially looking forward to the new [disk cache](https://www.11ty.dev/docs/plugins/image/#disk-cache-new-in-image-1.0.0) feature that was released in the new version of [eleventy-img](https://www.11ty.dev/docs/plugins/image/). The biggest breaking change that I was on the lookout for was changes to the built in slugify function. Instead of chancing it, I decided to just port over the custom slugify function that I was using in v3.

The site continues to be fully static with no tracking or analytics at all. This web site is so static that I don't even bother with CI/CD pipelines to build the site on the fly with a git push. I build the site on my local computer then use rsync to copy the files up to my VPS web host. Dead simple.

I've been leaning harder and harder into the monochrome, minimalists styles and this design continues in that direction.  I increased the font size a bit to 1.15em but otherwise I'm still using basic system fonts. No dependency on Google fonts for me. (laughs in web0)

When I [changed my site host from GitHub to Capsul](/posts/2021/09/05/my-blog-is-now-hosted-on-capsul-as-a-vps/), I downloaded the Eleventy build source files to my local system and never bothered looking for another repository to host them. Since I don't bother with CI/CD to publish my site, it wasn't a priority to host them anywhere.

With version 4 of the site, I decided to use Codeberg because they're a *community-driven, non-profit software development platform* and those are all the right words in my mind. If it's your jam, check out the repository: [obsolete29.11ty.v4](https://codeberg.org/obsolete29/obsolete29.11ty.v4)

Many other bloggers avoid redesigning and developing their sites because they feel it gets in the way of *producing content*. I don't feel that way at all though. I enjoy doing the development part of having a site. My goal is to *have fun*. Sometimes that's producing content but sometimes that's writing HTML, CSS, JavaScript and figuring out how to make Eleventy do a thing. 

Overall, I'm pretty happy with how the site has turned out. I need to figure out why the favicon isn't displaying for me but I've read those are notoriously hard to reset in caches.

Thanks for reading. :) 