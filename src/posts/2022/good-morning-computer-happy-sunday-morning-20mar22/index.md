---
title: "Good morning computer, happy Sunday morning 20mar22"
date: "2022-03-20T09:11:49.275-05:00"
tags:
  - "goodmorningcomputer"
socialTags:
  - "GoodMorning"
---
Good morning computer,

I have a concert tonight. I know, a concert on a school night... who am I? I'm seeing [Monolord](https://monolord.bandcamp.com/) at Hi Tone in Memphis. I'm super stoked. I'm staying the night in Memphis and will just drive back in the morning, in time for work.

Work has still been pretty stressful. I'm juggling too many balls and still taking on too much responsibility. Shifting my thinking just takes a little time but I feel I'm making some progress.

The Asurion project is just starting to ramp up so not much to report there. Still looking forward to building this little application and workflow.

## Things I found interesting...

Here are some things I found interesting around the web this week.

### [Five Books](https://fivebooks.com/)

>We ask experts to recommend the five best books in their subject and explain their selection in an interview.
>
>This site has an archive of more than one thousand seven hundred  interviews, or eight thousand book recommendations. We publish at least  two new interviews per week.



### [Texas and other states want to punish fossil fuel divestment](https://www.npr.org/2022/03/16/1086764072/texas-and-other-states-want-to-boycott-fossil-fuel-divestment-blackrock-climate)

>To see companies taking that position, I really felt like this was an anti-Texas narrative," says Jason Isaac. 
>
>Isaac heads the Texas Public Policy Foundation, an influential think  tank that opposes efforts to fight climate change and receives millions  of dollars from fossil fuel interests.

This is the worst of tribal, partisan, identity politics. picard-rubs-forehead.gif



### [‘Mayday acknowledged’: Sydney Airport emergency after ‘flight control malfunction’](https://www.smh.com.au/national/nsw/mayday-acknowledged-sydney-airport-emergency-after-flight-control-malfunction-20220313-p5a48q.html)

> A jet pilot issued a mayday and aborted the final approach to Sydney  Airport over Botany Bay on Saturday night after telling air traffic  controllers the plane was experiencing flight control issues and  required emergency assistance.

I think there are lots of applications for autonomous vehicle applications. Freight flights are one of those applications that I'm still surprised we've not seen much news about. Most accidents are caused by human error and that's something a computer can correct. One metric that isn't captured is how many accidents are adverted because of the problem solving skills of a human. In this instance, where a flap was malfunctioning and they couldn't slow the plane down to the proper speed, would a computer just throw an error code and crashed? I think there would need to be a system where the computer could alert air traffic control and a human could take over the flight control systems, especially during take off and landing operations.



### [Google “hijacked millions of customers and orders” from restaurants, lawsuit says](Google “hijacked millions of customers and orders” from restaurants, lawsuit says)

> Google is being sued by a Florida restaurant group alleging that the  tech company has been setting up unauthorized pages to capture food  orders rather than directing them to the restaurant’s own site.

Just from my casual reading on the web, this is a common practice for services like UberEats, DoorDash, and Yelp.



### [Including RSS Content in your Eleventy Site](https://www.raymondcamden.com/2022/03/08/including-rss-content-in-your-eleventy-site)

> Before I begin, this post is *not* about generating an RSS page with Eleventy. If you need to do that, check the [plugin](https://www.11ty.dev/docs/plugins/rss/) that makes it (mostly) trivial to do. This post is about *consuming* RSS for your Eleventy site. I've got a page here ([About](https://www.raymondcamden.com/about)) where I track my external articles and books. At work, we use Medium to host our [blog](https://medium.com/adobetech) and I've been publishing there as part of my job. I was curious how I could get that content on my About page as well.



### [Git branching for small teams](https://victoria.dev/blog/git-branching-for-small-teams/)

> Here’s a practice I use personally and encourage within my open source projects and any small teams I run for work. I’ve seen major elements of it presented under a few different names: Short-Lived Feature Branch flow, GitHub flow (not to be confused with GitFlow), and Feature Branch Workflow are some. Having implemented features I like from all of these with different teams over the years, I’ll describe the resulting process that I’ve found works best for small teams of about 5-12 people.



### [Be anonymous](https://kg.dev/thoughts/be-anonymous)

> Ultimately, anonymity comes down to one thing: Control. You should  educate yourself on data privacy and make sure that you know what data  you're sharing and what is possibly out there. For example, did you know that when you send a photo on iMessage, the chances are that the  location of that photo is in the metadata? All you have to do is save  that photo and swipe up. Now, you have the exact geolocation, the  capture time, and the camera type. Or that plenty of you have a profile  on Whitepages with your phone number and address freely available?
>
>  Once you have all of that figured out, go on about your life and share  whatever you're comfortable sharing. As long as you feel in control,  your anonymity status doesn't really matter.