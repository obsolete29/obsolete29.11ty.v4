---
title: "Power Automate Get Member Email Addresses of a SharePoint Online Group"
desc: "Here is a quick walk through on how I user Power Automate Flow to get member emails from a SharePoint Online group."
date: '2022-03-26T12:31:57.749-05:00'
tags:
  - power-plateform
  - power-automate
  - m365
  - o365
  - sharepoint-online
---
Using Power Automate to send email to the members of a SharePoint Online group is something I have to do on just about every project. I can't quite remember the details since we don't do this as often as needed to memorize it. This is a log to myself so when I have to do this again in two months, I can reference this post to remind myself. Plus, if someone else should stumble upon this post, perhaps they will get some value from my clear explanation without the one million bouncing, moving ads that seem to infect many of the sites that publish SharePoint/Power Platform guides.

## Scenario

There are many reasons a Power Platform developer may need to send emails to all members of a SharePoint Online group. Power Automate does not have any native hooks to do this so we must tap into the SharePoint REST API to get all members of a group. I use Solutions so I can call child flows as I generally have several different Flows where I need to perform this functionality. Solutions and child Flows are pretty close to PowerShell functions. Build it once then call the functionality from whichever Flow you need.

## Solution

We're going to use the Send an HTTP request to SharePoint action in Power Automate to access the SharePoint Online REST API.

{% myImage "image1.png", "Screenshot of some Power Automate actions." %}

### Trigger

Since I'm building a child flow to use as a function for multiple other flows, my trigger is *Manually trigger a flow* with the group name as the only input.

### Send an HTTP request to SharePoint

Next, let's use add a *Send an HTTP request to SharePoint* action.

Uri: `_api/web/sitegroups/getbyname('YOUR-GROUP-NAME-HERE'/users`

Headers:

```json
{
  "accept ": "application/json;odata=verbose",
  "content-type": "application/json;odata=verbose"
}
```

{% myImage "image2.png", "Screenshot of some Power Automate actions." %}

### Select

Next, add a *Select* action.

From: `body('GetUsersFromSharePointGroupREST')?['d']?['results']`

Map: `item()?['Email']`

### Join

Now let's add a *Join* action.

From: `body('SelectEmailAddressField')`

Join with: `, ` 

(Note: ^^ This is a simple comma!)

### Respond to a PowerApp or flow

Finally, we *Respond to a PowerApp or flow* and return the email addresses back to the parent flow.

<figure>
  {% myImage "image3.png", "Screenshot of some Power Automate actions." %}
  <figcaption>The full flow!</figcaption>
</figure>

## Conclusion

Now I have a flow that I can call as a child flow and serves the same functionality of a PowerShell function in that we can reuse it across multiple other flows in our solution.

Future me, you're welcome.