---
title: "Hello to all the new Mastodon users"
desc: "Here are some of my fav people to follow on Mastodon."
date: "2022-04-26T06:22:49.010-05:00"
tags:
  - "mastodon"
  - "fediverse"
socialTags:
  - "Welcome"
---
Elon Musk buying Twitter will not be news to anyone reading this post probably so I won't belabor the point other than to say, that was the nudge I needed to go ahead and delete my accounts. That seems to also be the nudge a lot of people need to try out Mastodon so hi, hello and welcome!

I hope everyone coming over to give it a try can give it an honest try. While the two microblogging platforms have similarities, I think people are better served to think of Mastodon as it's own thing and not just "not Twitter". 

I remember really struggling to find interesting people to follow when I first started and that was probably the hardest part for my own transition. So, I want to share some of my favorite people to follow on Mastodon. In chronological order:

- Tim Chambers - https://indieweb.social/@tchambers
- Micah Lee - https://mastodon.social/@micahflee
- Ru - https://fosstodon.org/@celia
- Laura Kalbag - https://mastodon.laurakalbag.com/@laura
- Nolan - https://toot.cafe/@nolan
- Autumn - https://triangletoot.party/@autumn
- Tom Zijlstra - https://m.tzyl.nl/@ton
- Kev Quirk - https://fosstodon.org/@kev
- Charles Stanhope - https://social.coop/@cstanhope
- Chris Wiegman - https://mastodon.chriswiegman.com/@chris
- Jan-Lukas Else - https://fosstodon.org/@jle
- Mike Rockwell - https://libertynode.net/@mike
- Chris Were - https://linuxrocks.online/@ChrisWere
- Hund - https://fosstodon.org/@hund
- Nicholas - https://smallcamp.art/@ndanes
- Christina - https://mastodon.sdf.org/@cosullivan
- Adam Piggott - https://fosstodon.org/@proactiveservices
- Lars Wirzenlus - https://toot.liw.fi/@liw
- Ruben Schade - https://bsd.network/@rubenerd
- Sneak - https://s.sneak.berlin/@sneak
- Matt - https://writing.exchange/@matt
- Karen Sandler - https://mastodon.technology/@o0karen0o
- Wouter Groenveld - https://chat.brainbaking.com/@wouter
- tdarb - https://fosstodon.org/@tdarb
- Erin Bee - https://gorgon.city/@erinbee
- zabbeer - https://fosstodon.org/@zabbeer
- Silvia Maggi - https://indieweb.social/@silviamaggi
- Minutes to Midnight - https://indieweb.social/@m2m
- SaraMG - https://mastodon.technology/@saramg
