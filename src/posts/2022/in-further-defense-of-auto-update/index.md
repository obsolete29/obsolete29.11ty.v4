---
title: In further defense of auto-update
date: '2022-01-07T08:15:54.605-06:00'
tags:
  - security
slug: in-further-defense-of-auto-update
---
I've been working in IT for over twenty years now. Most of that time has been in the role of sysadmin. Even though my job title now has developer in it, in my heart of hearts, I'm still just an ole sysadmin. I've managed racks of servers that served tens of thousands of people. One of the most important activities that I did as a sysadmin was make sure security updates were applied to those servers in a timely manner. Having servers hacked or infiltrated by worms, script kiddies, or ransomware was the thing that gave me the most heartburn (you-had-one-job.jpg). Those worms and script kiddies often use known vulnerabilities as entry points into their target networks. In my mind, installing security updates was *critical*. 

That was my frame of mind when I wrote [please do not buy a WiFi router that doesn't autoupdate](https://obsolete29.com/posts/2022/01/02/please-do-not-buy-a-wifi-router-thata-doesnt-autoupdate/) a few days ago. That post generated a couple people commenting that auto-updates are actually a bad idea and bad for privacy. I'm not sure if this is a case of me being stuck in an old mindset or these people are missing the forest for the trees. I wanted to take a few minutes to respond to their call outs.

Ali Reza Hayati wrote a [blog post](https://alirezahayati.com/2022/01/03/auto-update-is-a-bad-idea/) in response to mine. You should go read the post but his basic point is we should all be fully engaged with the software we use and read all the changes that new software is applying to our systems prior to installing it. A company could apply an update to your system that installs privacy invasive features and backdoors for example. 

If I don't trust a company/project/developer not to install major updates that will add privacy invasive features or backdoors via their auto-update mechanism then I don't trust them enough to use their software to begin with. Auto-updates enables software developers the capability so they *could* install a backdoor or some other privacy invasive feature but running out-of-date software on a connected device *does* make you vulnerable to drive-by hacks and you *will* get hacked eventually.

I think it boils down to your own threat model. Are you more concerned that a backdoor could be installed via auto-update of the software you use or are you more concerned about being hacked by malicious code that uses a known vulnerability? My threat model and experience says the later is a bigger threat to my data. Maybe your threat model or requirements are different than mine.

Ali's approach is the sensible and responsible thing to do and in the ideal world everyone would follow that path. I just don't think many people are willing to read every change log of every software update for all the software on their devices and then manually install said software updates in a timely manner. People are going to forget they turned updates off or they're just not going to think about it.

My auto-updates are turned on. I recommend that all my friends and family turn theirs on too. 

Note: If you're a person who may be the target of [APT](https://en.wikipedia.org/wiki/Advanced_persistent_threat) actors, then none of this is for you. In fact, why are you even on the internet right now reading this?! You're probably already hacked!! 