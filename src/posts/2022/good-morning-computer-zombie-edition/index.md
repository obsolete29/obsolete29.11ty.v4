---
title: "Good morning computer, zombie edition"
date: "2022-05-08T07:34:58.819-05:00"
tags:
  - "goodmorningcomputer"
socialTags:
  - "GoodMorning"
  - "LinkSharing"
---
A few weeks ago I wrote that I was discontinuing my *good morning computer* posts but I think I've changed my mind. I do really enjoy sharing interesting links from around the web so I intend to post them via this format, once every week or two. Additionally, I will use these posts to do a little light reflecting on my week... a bit of online journal if you will. Without further ado, let's get to it. ;)

## Things I found interesting...

Here are some things I found interesting around the web this week.

### [How and Why I Stopped Buying New Laptops | LOW←TECH MAGAZINE](https://solar.lowtechmagazine.com/2020/12/how-and-why-i-stopped-buying-new-laptops.html)
> As a freelance journalist – or an office worker if you wish – I have always believed that I should regularly buy a new laptop. But older machines offer more quality for much less money. 

New to me but I really enjoyed this article and feel inspired to replicate.

### [RSS Feed Best Practises](https://kevincox.ca/2022/05/06/rss-feed-best-practices/)
> These are some technical tips for publishing a blog. These have nothing to do with good content, just how to share that content. The recommendations are roughly in order of importance and have rationale for why they are that important.

I love RSS!

### [Gear from Netgear, Linksys, and 200 others has unpatched DNS poisoning flaw](https://arstechnica.com/?p=1852022)
> Hardware and software makers are scrambling to determine if their wares suffer from a critical vulnerability recently discovered in third-party code libraries used by hundreds of vendors, including Netgear, Linksys, Axis, and the Gentoo embedded Linux distribution.

### [Botnet that hid for 18 months boasted some of the coolest tradecraft ever](https://arstechnica.com/?p=1851778)
> It’s not the kind of security discovery that happens often. A previously unknown hacker group used a novel backdoor, top-notch trade craft, and software engineering to create an espionage botnet that was largely invisible in many victim networks.

Man I really love reading these types of write ups.

### [A human-readable RSS feed with Jekyll](https://minutestomidnight.co.uk/blog/build-a-human-readable-rss-with-jekyll/)
>  I love RSS. However, by using a third-party plugin to automatically generate the feed, I didn’t pay attention to how it could be improved. An occasional discussion on Mastodon prompted me to a change.

I feel inspired to join the cool kid club and have seen other's comment the same. I really enjoy how we all are kind of evolving our websites as a group.

### [Every little bit helps: How to pick the least eco-hostile laptop | Ars Technica](https://arstechnica.com/gadgets/2022/04/the-quick-and-dirty-guide-to-picking-a-quasi-eco-friendly-laptop/)
> Laptops are generally bad for the environment—but some are less bad than others.

### [Increasing the surface area of blogging](https://tomcritchlow.com/2022/04/21/new-rss/)
> RSS is kind of an invisible technology. People call RSS dead because you can’t see it. There’s no feed, no login, no analytics. RSS feels subsurface.

What a good post. Really enjoyed it.

### [Using Windows after 15 years on Linux · duncanlock.net](https://duncanlock.net/blog/2022/04/06/using-windows-after-15-years-on-linux/)
> I’ve been using Linux exclusively for ~15 yrs. I’ve recently started a fantastic new job – the only wrinkle was that it came with a Windows 10 laptop.
> 
> This is my first time using Windows after a 15-year break. This is how it’s been going.

### [Walmart will train scores of warehouse workers as truckers, starting at up to $110,000](https://www.npr.org/2022/04/07/1091517201/walmart-trucker-training-program-salary-increase)
> Walmart, based in Bentonville, Arkansas, also said it is raising pay for its 12,000 truck drivers. The starting range for new drivers will now between $95,000 and $110,000, according to Walmart spokeswoman Anne Hatfield. The retailer said that $87,500 had been the average that new truck drivers could make in their first year.

### [Chrome’s “Topics” advertising system is here, whether you want it or not](https://arstechnica.com/?p=1844743)
> Google is on a quest to kill the third-party web cookie, which is often used by advertisers to track users for targeted ads. Unlike other browser companies like Apple and Mozilla, which block third-party cookies outright, Google is one of the world's largest advertising companies. It doesn't want to kill the third-party cookie without first protecting its primary revenue source. Google seems to view user tracking as a mandatory part of Internet usage, and instead of third-party cookies, it wants to build a user-tracking system directly into its Chrome browser. Google's eye-roll-inducing name for this advertising system is the "Privacy Sandbox," and on Thursday, the company released its latest tracking solution in Chrome's nightly "Canary" builds.

Don't forget, Google is first and foremost an advertising company.

### [How did a hacker steal over $600 million from a crypto gaming blockchain?](https://arstechnica.com/?p=1844301)
> Axie Infinity developer Sky Mavis announced Tuesday a massive breach of its Ronin cryptocurrency sidechain. An attacker used "hacked private keys" to break through Ronin's validator network, Sky Mavis says, transferring 173,600 ethereum (worth approximately $594 million at current rates) and $25.5 million in USDC stablecoin as part of one of the largest breaches in the history of cryptocurrency.

### [Apple plans to build its own financial infrastructure for payments and lending](https://arstechnica.com/?p=1844608)
> Apple has long held to a philosophy of controlling as much of the user experience—and its own pipeline—as possible, believing this method offers the dual benefits of better experiences for customers and a bigger slice of the revenue pie for Apple. That control also means Apple can be less affected by surprises or failures from external partners.

Sure, I think at first they did it for better for experiences but now I think the primary motivator is making it harder to escape their walled garden.

### [Bitcoin’s massive energy use faces $5M shame campaign from environmental groups](https://arstechnica.com/?p=1844176)
> A new pressure campaign, called Change the Code Not the Climate, launched Tuesday, and it’s seeking to encourage bitcoin luminaries like Elon Musk, Jack Dorsey, and Abby Johnson, CEO of Fidelity Investments, to push for changes that would slash bitcoin’s energy use. 

### [Daily-driving a Mac, one year later | The Cat Fox Life](https://catfox.life/2022/03/26/daily-driving-a-mac-one-year-later/)
> It has been about a year since I published Really leaving the Linux desktop behind. This marks the first year I’ve used Mac OS as my primary computing environment since 2014. Now, I want to summarise my thoughts and feelings on using the Apple ecosystem as my primary platform – good and bad.

