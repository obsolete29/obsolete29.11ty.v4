---
title: "A quick note on my good morning computer posts"
date: "2022-04-15T05:48:04.977-05:00"
tags:
  - "goodmorningcomputer"
socialTags:
  - "GoodMorning"
---
If you follow my blog, perhaps you noticed that I was experimenting with a new type of post. With the "good morning computer" posts, I was trying to do a weekly check in, to journal about my week and share the interesting links from that week. 

I do not believe I will continue with those posts. While I enjoyed sharing interesting links, I'm not sure anyone is really interested in reading about my week at work and how stressed and busy I feel. Plus, when other bloggers post those weekly or monthly check in posts, I rarely find them interesting.

I do want to continue with the link sharing on topics I'm interested in, perhaps with a few comments, but I'm unsure exactly how to proceed with that. I've experimented with a couple different formats and haven't been entirely happy with any of them.

