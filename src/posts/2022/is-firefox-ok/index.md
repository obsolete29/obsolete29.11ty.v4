---
title: "Is Firefox OK?"
date: "2022-02-19T08:47:17.203-06:00"
tags:
  - "browsers"
  - "firefox"
---
There's lots of browser discussions in my feeds lately. I even blogged about [my own frustrations with Firefox](/posts/2022/02/15/on-being-frustrated-with-the-current-state-of-browsers/). Just wanted to comment a few things from [this Ars Technica article](https://arstechnica.com/gadgets/2022/02/is-firefox-ok/):

> However, Mozilla and Firefox acknowledge that for its long-term future  it needs to diversify the ways it makes money. These efforts have ramped up since 2019. The company owns read-it-later service Pocket, which  includes a paid premium subscription service. It has also launched two  similar VPN-style products that people can subscribe to. And the company is pushing more into advertising as well, placing ads on new tabs that  are opened in the Firefox browser.

Can someone help me understand this paragraph? Why not just focus on running the non-profit organization to support the Firefox browser? *Surely* they could raise the money to run a team to do the Firefox development? Firefox is their flagship product. Let VPN people do VPN. Let the read-later people do the read-later services. Are they really saying that they needed to branch out into these other things in order to fund the team that does the browser development? I sincerely feel like I'm missing some context.

> “Once lost, users hardly come back until there's a compelling reason,  and what would that compelling reason be?” says Bart Willemsen, a VP  analyst focusing on privacy at Gartner. Willemsen says he has been a  Firefox user since its earliest days. “I think Firefox really has a  challenge to find a unique position—not only in marketing statements,  but in their absolute product—and go in one direction,” he says.

OK yes, great! Be the privacy browser for non-nation state actors. Be the privacy browser for non-dark web people. Be the FOSS browser for the people and by the people. This will not resonate with TikTok people but I just feel there's a not insignificant percentage of people who are really interested in this type of browser.

> For Deckelmann, making Firefox more personalized is key. She says this  includes trying to increase the browser’s functionality to fit in with  people being online more. “It’s almost impossible now for people to  manage all this information,” Deckelmann says. For instance, last year  Firefox revamped its homepage to allow people to pick up previously  abandoned searches and unfinished articles. It redesigned its Android  app and added features from its password manager to the [Firefox app](https://blog.mozilla.org/en/mozilla/news/news-from-firefox-focus-and-firefox-on-mobile/). Mozilla has also been focusing on partnerships, including recently working with Facebook parent company Meta to push for [more privacy-focused advertising](https://blog.mozilla.org/en/mozilla/privacy-preserving-attribution-for-advertising/).

No. 😑

This whole line of thinking predicates that they're in competition with for profit companies for market share. I think they have to shift their thinking. Are Wikipedia directors fighting for market share against for profit-encyclopedia companies? Do they concern themselves with the market share of Encyclopædia Britannica? I don't know, maybe they do but it seems unlikely.

> Deckelmann says Firefox is likely to continue looking for ways to keep  personalizing people’s online browsing. “I'm not sure that what's going  to come out of that is going to be what people traditionally expect from a browser, but the intention will always be to put people first,” she  says. Just this week, Firefox announced a partnership with Disney—linked to a new Pixar film—that involves changing the color of the browser and ads to win subscriptions to Disney+. The deal speaks both to Firefox’s  personalization push and the strange roads its search for revenue  streams can lead down.

Hate. I'm repeating myself but just be the best privacy browser. Don't alienate your existing users, who actually care about their privacy, in order to try to appeal to TikTok people who care about changing the color of their browser. 