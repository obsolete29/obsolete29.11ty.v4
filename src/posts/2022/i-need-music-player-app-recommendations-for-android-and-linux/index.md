---
title: "I need music player app recommendations for Android and Linux"
desc: "I want to sync playlists between the two. How?"
date: "2022-11-15T05:41:07.022-06:00"
tags:
  - "music"
  - "android"
  - "linux"
socialTags:
  - "Music"
  - "Android"
  - "Linux"
  - "CalyxOS"
---
Rachelle laughs at me because I make my digital life harder with some of the choices I've made. One of those is [my decision to abstain from music streaming services](https://obsolete29.com/posts/2022/01/30/lets-make-collecting-an-mp3-library-popular-again/). That's right dear reader, I purchase music and download the MP3's onto my computer. I then use Syncthing to keep the music folder on my phone synced up with the music folder on my computer. This setup generally works pretty well. I sometimes have to jiggle a Syncthing client here or there but I'm pretty happy with how everything works overall. Plus this all works over my local network. No cloud servers. No accounts on other people's networks. Pretty straight forward.

The challenge I'm facing is syncing playlists between my Android phone and my Linux computer. **What do you use or can you recommend anything**? In the perfect world, creating, modifying, and deleting playlists on one would automatically sync those changes to the other via Syncthing, over my local network. The Android application would be available on the F-Droid store and the Linux application would be available in the standard repositories. I'm not really interested in running something like Nextcloud.
