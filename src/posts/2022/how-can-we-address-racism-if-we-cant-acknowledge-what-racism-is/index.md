---
title: How can we address racism if we can't acknowledge what racism is?
desc: I'm using MLK day as an opportunity reflect on my thoughts about discussions around race
date: '2022-01-17T10:23:24.297-06:00'
tags:
  - politics
  - race
---
I feel like our country is bad at talking about race. When I speak to my conservative friends about race, it's seems we're not even talking about the same thing and we can't land on a mutually agreed upon definition of racism. I think this perfectly describes what's going on in our country.

My anecdotal experience is that conservatives prefer a very narrow understanding and usage of the word. They mean literal hatred and an inability of a person to  so much as endure to be in the presence of another race. They mean literal and overt acts of intimidation and threats of violence like burning a cross in the yard of a black family. They mean literal and overt violence against non-white people. Since they have non-white friends, do not burn crosses in yards and have never lynched anyone, they're not racist. Suggesting otherwise is offensive to them.

Liberals on the other hand take a very broad understanding and usage of the word. Since slavery was a big part of the building of our nation and we fought a civil war over it, there are many, unconscious bias' built into the fabric of our society and it permeates the very systems that run our country. Redlining and Jim Crow are often cited as examples of these built in, systemic bias'.

How do we have constructive conservations in this country if we can't even agree on what racism is? How do we try to address systemic bias if we can't acknowledge that there *is* systemic bias? Not only is there no systemic bias, according to many conservatives, it's offensive to even teach history about racism in primary school because all it does is tell white kids that they should feel ashamed for being white. 

This state of affairs is maddening for me and I don't think there's really anything to do about it. At a personal level, I'll continue to try to treat and think of my conservative friends as individuals, worthy of respect and decency. I will try to resist thinking of conservatives as a monolithic block of people who are represented by the idiots interviewed by Jordan Klepper. 

I'm pessimistic about our ability to change the current systems and attitudes until white people become a voting minority. Happy MLK day I guess?