---
title: "On the Twitter migration and the Fediverse"
desc: "I have thoughts about the Twitter migration thing that's happening on Mastodon. After reading Hugh's essay on the topic, I feel inspired to scribble a few down."
date: "2022-11-13T05:43:15.775-06:00"
tags:
  - "twitter"
  - "fediverse"
socialTags:
  - "TwitterMigration"
  - "Fediverse"
---
I have thoughts about the Twitter migration thing that's happening on Mastodon. After reading [Hugh's essay](https://www.hughrundle.net/home-invasion/) on the topic, I feel inspired to scribble a few down.

When I first came to Mastodon, I definitely noticed a different vibe. Content warnings were neat and novel. People were talking about lots of things I like. People engaged with me *way* more than they ever did on Twitter. I felt like I had found my tribe.

With the Twitter migration people coming to Mastodon en masse, I started to see many people I knew from Twitter. Not personal friends or anything but certainly people I followed. After following some of these people on Mastodon, I noticed a shift in the tone and tenor of my timeline. In short, it felt like Twitter, and not in a good way. I did not miss the Twitter scolds telling everyone what to do, how we're supposed to act and how we're supposed to think. I did not miss the constant focus on micro-aggressions and identity politics. With the Twitter people coming over, I've started to see stuff like [this](https://social.overlappingmagisteria.org/@pearlbear/109326209520496565) and [this](https://hci.social/@cfiesler/109326159923712368) again. Ugh.

It seems a reasonable hypothesis that users who signed up for Mastodon prior to Elon buying Twitter, did so for some principled reasons. Maybe they didn't feel safe on Twitter. Maybe they were tired of corporate social media. Maybe they were looking for something different and cool. It seems that the Twitter users are migrating over not because of some principled, internal reasons but because everyone else was doing it. Lots of people in their timeline is saying Elon is a fascist, and all my friends are moving so I gotta move too! Or something. Many of these people don't care about decentralization, FOSS, or federated social networks. Or if they do, those were certainly not the reasons they're coming to Mastodon now.

I'm happy that so many people are interested in Mastodon. I think Mastodon has much better tools, concepts and approaches compared to the corporate, ad-driven based approach like Twitter and Facebook. I want to be welcoming to these people but I think Hugh's analogy of the small party is fitting. My hope is that we're noticing a vocal minority and that most of the new people are just trying their best to figure things out and fit it.

Overall, I think more people using Mastodon is a net positive, even if they're doing so to be part of the cool kids club.
