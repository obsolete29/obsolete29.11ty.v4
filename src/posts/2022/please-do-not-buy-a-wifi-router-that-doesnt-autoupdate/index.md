---
title: Please do not buy a WiFi router that doesn't autoupdate
desc: We need to give our WiFi routers more considering than a jumped up toaster oven.
date: '2022-01-02T07:21:07.699-06:00'
tags:
  - wifi
  - security
slug: please-do-not-buy-a-wifi-router-that-doesnt-autoupdate
---
I feel it's safe to say that most people don't think about their WiFi router much. I bet the majority are fine getting the [Wirecutter budget recommendation](https://www.nytimes.com/wirecutter/reviews/best-wi-fi-router/) which is $50. They fiddle around with getting it setup and stuff it under a shelf, bed or just wherever their cable modem is located and then probably never think about it again until it doesn't work.

Since I'm a weirdo, I think about WiFi routers all the time and how poor they seem to be as a group. I did my best Picard-rubs-forehead impression when I saw [Netgear leaves vulnerabilities unpatched in Nighthawk router](https://www.bleepingcomputer.com/news/security/netgear-leaves-vulnerabilities-unpatched-in-nighthawk-router/) in my feeds.

It bugs me so much that Wirecutter doesn't seem to have *the ability to auto update firmware which is enabled by default* on their list of criteria. Consumer class WiFi routers are a target for botnet operators because they're so damn easy to take over. As a class of devices, they're not well supported, fall out of support quickly, and since many do not auto update, they're often running on a version of firmware that have known vulnerabilities.

When you're in the market for a new WiFi router, please check to make sure the stupid thing has the ability to auto update itself. The Wirecutter guide has a [router setup and network maintenance](https://www.nytimes.com/wirecutter/reviews/best-wi-fi-router/#router-setup-and-network-maintenance) section that looks good to my eye. They even link to the [WiFi guide](https://decentsecurity.com/#/routerwifi-configuration/) over at Decent Security, which I've recommended in the past.

Let's consider our WiFi routers as critical infrastructure, with importance closer to our main computing device instead of thinking of them in the same class as a jumped up toaster.