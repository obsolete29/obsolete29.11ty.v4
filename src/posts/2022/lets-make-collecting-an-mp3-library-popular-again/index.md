---
title: Lets make collecting an MP3 library popular again
desc: 'If you are switching streaming services, why not just invest in your MP3 music collection? Let''s make that shit cool again.'
date: '2022-01-30T12:56:26.881-06:00'
tags:
  - music
  - spotify
---
Dear reader,

I'm writing to you as [Spotify is in the news again](https://arstechnica.com/gaming/2022/01/neil-young-threatens-to-pull-songs-from-spotify-over-joe-rogan-podcast/), this time about Joe Rogan. There are already many articles on the web about how to [change streaming services](https://www.patreon.com/posts/how-to-break-up-61760779). But I want to make the case for breaking up with *streaming music* instead. You may be asking yourself, *What the fuck is this guy even talking about? People can own music?!* Yes dear reader, we can own music.

It's true, having nearly the entire catalog of modern music at your finger tips is super convenient, but there are many problems with streaming services.

A big problem with streaming services is that most artists are paid very little when their music is consumed on these platforms. The music *industry* is doing better than ever but most *artists* get little money from people listening to their music on a streaming platform. Buying a CD directly from the artists is the best way to support the bands you love.

Streaming services are big tech capitalists and and they have a fiduciary responsibility to maximize profits for shareholders. It's no longer enough to grow their subscriber base. Now they're using their apps to collect data on their users which is then sold to the data aggregating companies. I wrote about it, [Opting out of Spotify](/posts/2021/08/17/opting-out-of-spotify/). If you don't have their apps on your devices or have accounts with the services, they can't sell your information to third parties.

You're beholden to an intermediary when you subscribe to a streaming service. Sure, most of the time it's great having access to the entire catalog of modern music but what happens when you don't? What about Neal Young fans who subscribe to Spotify right now? They're just shit out of luck is what. If we build our own MP3 music libraries, we're unaffected when a label or artist has a disagreement with one of the streaming services.

The streaming services purposefully make it hard to switch. Since streaming services are big tech capitalists, they employ all the big tech, wall garden tactics of Apple and the like. You can't easily export your playlists. You have to use 3rd party services and then the migration isn't great. Like the previous point, if you have your MP3 collection and you've been curating your playlists locally, then this is work that you won't have to do again. The streaming services can no longer use your playlists as customer retention leverage. 

So what happens when your new streaming service does something you don't like? What happens if Tidal or Deezer goes out of business? What happens if Apple does some shit you don't like or your favorite artist boycotts the platform? Buying music from artists is the best way to support them. If you're switching now, why not just go ahead and invest in your MP3 music collection? Let's make that shit cool again.