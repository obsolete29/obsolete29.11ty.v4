---
title: "Welp, my XMPP experiment is over"
desc: "I tried using XMPP for 6 months but have now shut down my server. Here are my thoughts."
date: "2022-07-30T06:52:51.796-05:00"
tags:
  - "xmpp"
socialTags:
  - "XMPP"
---
After trying out XMPP for the last year or so, I've decided to shut down the VPS server running my instance of Snikket. 

I used the Snikket client on Android and Dino on Linux. The chat experience with these clients felt cruder than other chat clients like Signal or iMessage. There's no built in message reacts. The image sharing features just seemed not great. The overall impression I had was the chat experience is stuck in the past and is not feature rich.

When I left the Apple ecosystem and asked people chat with me on Signal, many did and I felt comfortable asking them because Signal is a reasonable facsimile to iMessage. I never felt comfortable asking my normie friends and family to join me on my XMPP server. 

Ultimately, it just ended up being my friend David and I on my server chatting. And even though I published my XMPP address in my Mastodon profile and my website, I never had any conversations with anyone else that went beyond "Oh hi this is neat." Spending resources to run a VPS to chat with my friend David just no longer seemed worth it to me. We've switched over to using Signal.
