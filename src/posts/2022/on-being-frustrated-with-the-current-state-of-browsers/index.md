---
title: On being frustrated with the current state of browsers
desc: I feel pretty frustrated by the lack of a broswer that isn't working on an ad network
date: '2022-02-15T06:28:52.054-06:00'
tags:
  - browsers
  - firefox
---
This will be old news for most people reading these words, but the [recent blog post](https://blog.mozilla.org/en/mozilla/privacy-preserving-attribution-for-advertising/) from Mozilla about "private ads" has me irritated and down about the current state of browsers:

> For the last few months we have been working with a team from Meta  (formerly Facebook) on a new proposal that aims to enable conversion  measurement – or attribution – for advertising called Interoperable  Private Attribution, or IPA.

I just hate this. I don't want anything to do with Facebook. I certainly don't want the people making the browser I use, working with Facebook to *enable conversion measurement* as I surf the web. Notice that nice bit of marketing there by including the word *Private*. To quote William Goldman, "You keep using that word. I do not think it means what you think it means.”

I started to look around for other alternatives and I notice that nearly every single main stream browser is working on some version of IPA and I don't mean the delicious beer. Google has [FloC](https://developer.chrome.com/docs/privacy-sandbox/floc/), or whatever comes after it. Safari has [Private Click Measurement](https://webkit.org/blog/11529/introducing-private-click-measurement-pcm/). Brave has [Brave-Ads](https://brave.com/brave-ads/).

[Tor Browser](https://www.torproject.org/) seems to have the right intention and spirit but the use of the Tor Network makes some sites inaccessible. Additionally, Tor is **so** focused on security and privacy that it feels actively hostile to the person trying to use the browser as a daily driver. If I want to identify myself to Fastmail, and save the login cookie so I don't have to fish out my phone for 2fa every morning, then give me the option of doing that.

[IceCat Browser](https://www.gnu.org/software/gnuzilla/) looked promising at first but it doesn't seem to be well maintained. I wasn't able to make it display the login screen for Fastmail or my Mastodon instance.

Has the Internet just become an ad delivery network? Is there so much money sloshing around from ad people that software companies/teams doing browser development are simply unable to pass it up? They have to take the money because it's so much, but everyone seems to recognize people don't want to be served ads, or tracked across the web. The browser developer organizations recognize the concerns of users but really all they're willing to do about it is use the word *private* in the title of their ad tracking platform.

Maybe I'm being too naive, rigid or unrealistic but I don't want to be served ads at all. I'm sorry that businesses have built their websites using an ad-based model but that's not my problem. Offer me a subscription service, and if it's valuable enough to me then I'll pay for a subscription. I subscribe to Ars Technica, NYTimes, Washington Post, Wired Magazine to name a few.

I feel the frustration and despair setting in about the lack of choices that meet my requirements and I just need to let it go. It's like being mad that rain is wet. While I'm very distrustful of the Mozilla executives and do not believe they actually buy into the principles they say Mozilla stands far, they're still the best choice in my mind. I wish they would narrow their scope by focusing on Firefox instead of trying to be concerned with the health of the whole Internet.