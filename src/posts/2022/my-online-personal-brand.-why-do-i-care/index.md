---
title: "My online personal brand. Why do I care?"
desc: "I find myself struggling with my online personal brand. Why do i even care?"
date: "2022-09-20T07:24:29.523-05:00"
tags:
  - "obsolete29"
  - "blogging"
socialTags:
  - "Blogging"
---
I don't know what my blog's personal brand is. Why do I even care about what my personal brand is? Why can't I just write and post the things I want to post without worrying about this? My mind keeps coming back to it though so I thought I'd try to write about it to help organize my thoughts.

For the past year I've been on a journey to break up with big tech so I've had a lot of passion about privacy, FOSS, self-hosting, security and technology in general. Most of the things I've written about have been about those topics.

Lately, I feel myself wanting to write about US politics. I've considered myself to be a liberal progressive but I think there are some big topics that I don't buy into the group think. Everything isn't racist. We need more funding and better training for our police, not less funding and training. The US is still a force for good in the world. I believe in free speech. The answer to speech we don't like is more speech. Speech isn't violence. 

Pre-COVID, I took the occasional bicycle trip, hiking adventure or vacation and enjoyed documenting those trips here as well.

Finally, I work as a SharePoint Developer and enjoy recording solutions to the problems I'm solving... especially the problems that I struggled to figure out or seemed to be insufficiently documented on the web. 

Those are four, distinct brands in my mind and I'm struggling to come up with a way to present that to an audience that may be interested in following any of the one things. Going from a post where I'm expressing views on free speech, to how to send email to a SharePoint group using Power Automate feels pretty jarring in my mind.

I think I'm stumbling on the thing I always stumble on and that's the idea that I have an "audience". What do I want my online identity to be?
