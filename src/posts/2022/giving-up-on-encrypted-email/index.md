---
title: Giving up on encrypted email
desc: PGP is little more than performative LARP. I'm no longer going to mess with it.
date: '2022-01-19T06:14:22.088-06:00'
tags:
  - obsolete29
  - email
  - gpg
  - encryption
lastmod: '2022-01-19T06:14:25.967-06:00'
---
I consider myself to be a privacy and security advocate. I'm also into a lot of "old" tech that makes up the web; web0 if you will. I like blogs and think every human should have one. I like RSS, I like web sites that serve HTML/CSS only and look like a readme.txt file from 1998. I actually like email and believe it to be superior to sending Discord/Slack/Teams messages. My interest in [GnuPG](https://gnupg.org/) was inevitable. 

I've made a few attempts at trying to understand and use it. When I searched the public key repositories, I saw and vaguely remembered the certificate that expired in 2013 with my name on it. Many Serious People seem to use it and display their public encryption keys on their websites. Examples: [Jeffery Paul](https://sneak.berlin/), [TheFuzz](https://thefuzzstone.github.io/), [Bernie Innocenti](https://www.codewiz.org/wiki/ContactInformation), [Richard Stallman](https://stallman.org/gpg.html). I wanted to signal my membership in the cool kid club so I followed some starter guides, generated my own key pair and published my public encryption key on my own contact page.

When [The PGP Problem](https://latacora.micro.blog/2019/07/16/the-pgp-problem.html) rolled through my feed, it took me a few days to decide to revoke my certificate and remove the public key from my contact page. A [more recent post](https://latacora.micro.blog/2020/02/19/stop-using-encrypted.html) from [Latacora](https://latacora.micro.blog/archive/) perfectly captures it:

> Email is unsafe and cannot be made safe. The tools we have today to  encrypt email are badly flawed. Even if those flaws were fixed, email  would remain unsafe. Its problems cannot plausibly be mitigated. Avoid  encrypted email.
>
> Technologists hate this argument. Few of them specialize in  cryptography or privacy, but all of them are interested in it, and many  of them tinker with encrypted email tools.
>
> Most email encryption on the Internet is performative, done as a  status signal or show of solidarity. Ordinary people don’t exchange  email messages that any powerful adversary would bother to read, and for those people, encrypted email is [LARP](https://www.google.com/search?q=larp) security. It doesn’t matter whether or not these emails are safe, which is why they’re encrypted so shoddily.

I actually agree with all that. I've only ever exchanged encrypted emails once in the past 5 years and that was just a couple months ago with another infosec hobbyist. Ninety-five percent of my email usage is emailing bloggers about their blog posts or receiving emails about my blog posts. I still intend to use email but I'm just going to consider it in the clear communications, closer to a blog post comment or a post on my web site than a secure piece of communication. 

So in the spirit of [removing my social sharing preview images](/posts/2021/10/18/updated-social-sharing-open-graph-protocol-images-for-my-personal-site/), I'm no longer going to bother with publishing a public key. Instead, I'll publish a Signal number as a secure communications option. Publishing a Signal number is also performative virtue signaling for me but at least it actually works well at being secure.