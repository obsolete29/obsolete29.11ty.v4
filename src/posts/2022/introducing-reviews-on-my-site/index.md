---
title: Introducing reviews on my site
desc: I want to use my site to log the media I consume.
date: '2022-02-07T06:25:18.308-06:00'
tags:
  - obsolete29
  - blog
---
Just a quick note for anyone who might be interested in such things, but I've added a [Reviews](/reviews/) section to my site. I also created a new [RSS feed](/feed/reviews.xml) for the reviews as not to clutter up the posts feed.

Instead of using sites like Goodreads and Letterboxd to log and rate the books, shows and movies I consume, I want to use my own site to do that. I believe I've come up with a pretty good soluion that works for me.