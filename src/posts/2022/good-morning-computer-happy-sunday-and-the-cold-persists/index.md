---
title: "Good morning computer, happy Sunday and the cold persists"
date: "2022-03-27T11:33:47.226-05:00"
tags:
  - "goodmorningcomputer"
socialTags:
  - "GoodMorning"
---
Good morning computer,

The cold is persisting here in Nashville and it's making me grumpy. Yes, more grumpy than usual. 😑

I added another skull tattoo to my skull leg on Friday. [Skout](https://www.safehousetattoo.com/artists/skout) made a really cool, weird skull on my inner knee. Still more color to put in but lots is done already and I'm super stoked on it. It hurt like a son of a bitch though. I don't have many color pieces on my body but going over already raw places with more color is super not fun. Do not recommend. Ha.

We're heading over this evening to check out an art show at Safe House Tattoo and dinner after. I'm pumped.

Work at the primary job continues to evolve. We're looking for a junior/mid level Power Platform developer. If you know anyone who may be interested, please send them my way. It's a challenging, fast paced job but I get a lot of enjoyment from it and we have an opportunity to shape processes and be involved in developing a strong team. Plus, this person gets to work with me! Job posting coming soon, I'm sure.

Finally, I saw that [Harry Mack is in Nashville](https://www.youtube.com/watch?v=qBWmC8nmw6g) this weekend. How cool?

## Things I found interesting...

Here are some things I found interesting around the web this week.

### [The latest marketing tactic on LinkedIn: AI-generated faces : NPR](https://www.npr.org/2022/03/27/1088140809/fake-linkedin-profiles)
> That chance message launched DiResta and her colleague Josh Goldstein at the Stanford Internet Observatory on an investigation that uncovered more than 1,000 LinkedIn profiles using what appear to be faces created by artificial intelligence.

Well yeah. Microsoft and LinkedIn strike me as the most fake of the fake with synthetic engagement. Microsoft is teaching GPT-3 how to create low-code solutions ffs.

Y'all are sleeping on Microsoft, they're the worst.



### [Apple could soon turn the iPhone into a recurring subscription service](https://arstechnica.com/?p=1843337)
> Apple is working on a way for users to acquire iPhones as part of a subscrption service, according to reporting from Bloomberg. The service could launch as soon as this year, but it could also arrive in early 2023.



### [KeePassXC 2.7.0 is a Massive Upgrade with Tags, Improved Auto-Type, and Windows Hello Support](https://news.itsfoss.com/keepassxc-2-7-0-release/)
> KeePassXC, the open-source password manager, has just got a major upgrade, one that includes some new exciting features and improvements



### [Google routinely hides emails from litigation by CCing attorneys, DOJ alleges | Ars Technica](https://arstechnica.com/tech-policy/2022/03/google-routinely-hides-emails-from-litigation-by-ccing-attorneys-doj-alleges/)
> The US Department of Justice and 14 state attorneys general yesterday asked a federal judge to sanction Google for misusing attorney-client privilege to hide emails from litigation.



### [D.C. sues Grubhub for allegedly using deceptive trade practices : NPR](https://www.npr.org/2022/03/22/1087988691/washington-dc-attorney-general-grubhub-food-delivery-lawsuit)
> For starters, he said, Grubhub lists more than 1,000 "partner restaurants" available for delivery in D.C. that had no contracts with Grubhub, and added the restaurants to the platform's directory without the owners' consent.



### [Digital feudalism and freedom · boucek.me](https://www.boucek.me/blog/digital-feudalism/)
> My problem with this concept started with my iPad Air. At some point Apple has decided that this perfectly functional piece of hardware is no longer supported. At the beginning, it didn’t mean much, just that the latest iOS wasn’t available for it. But then slowly other apps started following the suite, including also security updates. At that point I realised that I was basically subscribing to Apple for hardware I was supposed to regularly buy in exchange for the possibility to access my own data.

Yep. Perfectly summarizes my realization as well. My Pixel 4a with CalyxOS might not be as shiny or fancy as the latest iPhone but I feel like it's mine and not just another end point on the Apple cloud infrastructure. 

I have my data on my local devices and network. It's mine.

