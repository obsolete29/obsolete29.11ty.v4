---
title: "Good morning computer, checking in 24jul22"
date: "2022-07-24T09:46:04.085-05:00"
tags:
  - "goodmorningcomputer"
socialTags:
  - "GoodMorning"
---
Good morning computer. Happy Sunday. I don't have any interesting links to share today. Instead, I'm just checking in to let everyone know I'm still here. 

At work, we have a big project trying to complete and it's been stressful and time consuming. All of my energy has been focused there for a few weeks now. I'm optimistic this upcoming week will be the last week I must pour all of my energy and focus into it.

Additionally, I'm excited and motivated about working on our development project process at work. That is, our project intake "hopper", how projects are scheduled, documented, worked and closed. We're developing our team based developer guides and standards, we're figuring out how to work on projects as a team and not just individual developers. We're pretty much bootstrapping these processes from nothing as nearly all the previous efforts have been from a project management perspective, reporting metrics and accomplishments up the chain of the command. There's been zero effort on the technical/developer side. Luckily, our manager/project manager is open to me leading and developing these processes. It feels good to feel like I'm making a meaningful difference on the team.

At home, I've just been reading a few fun books when I'm too tired to focus and do work. Rachelle and I visited Richmond VA a few weeks ago to explore the city as we're thinking about moving there next spring. We also have our wedding in Las Vegas coming up in September! That's still 6-7 weeks away but I know I'll blink and it'll be time to pack.

To wrap this up, I could use a sabbatical that lasts a few weeks. Maybe it's near a beach and I get to read, take walks, write and recharge my batteries. I feel like I'm really drawing from deep reserves. 
