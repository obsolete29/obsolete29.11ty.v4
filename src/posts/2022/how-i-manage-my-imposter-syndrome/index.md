---
title: "How I manage my imposter syndrome"
desc: "Imposter syndrome is a real thing and here are some techniques I use to manage mine"
date: "2022-05-02T07:21:47.095-05:00"
tags:
  - "career"
socialTags:
  - "GoodMorning"
  - "NewBlogPost"
---
Kev posted [The Expert vs the Impostor](https://kevq.uk/the-expert-vs-the-impostor/) and I just wanted to write a few thoughts down in response.

Like Kev, my impostor syndrome is a big giant demon that haunts me. I'm a high school drop out and I only completed a single year of community college. I was not a good student. My first IT job sent me to get my MCSE on Windows NT 4.0 when I was promoted to be the "Network Administrator" from PC Support. It was a boot camp style situation, where I went to training after working a full, normal day.

Other than that bit of certification, I don't have much in the way of real formal training. I read and learn on the job. A buddy of mine at a previous job was a developer in one of this previous lives and he taught me many development concepts while I was first learning PowerShell. I would show him the code that I'd copied from the Internet and hacked on, then he would then tell me topics to go research. Example: "Oh, you want an array for that instead of a list of variables." So then I would go off on the Internet reading and learning about arrays in PowerShell.

I'm always afraid that other developers and IT professionals with proper computer science degrees are going to know and understand something at a level that I just can't know by on the job training or experience. I disarm those feelings of being an impostor by being honest and open about the things I don't know. I also try really hard to model that behavior, in the open with our team of developers so that other's are encouraged to speak up or reach out if something isn't clear or they need help on a topic/concept.

So like Kev, I think I'm an expert at knowing what I don't know. I'm fond of this Mark Twain quote: *It's not what we don't know that gets us in trouble. It's what we know for sure that just ain't so* 
