---
title: "Rachelle and I got married in Las Vegas"
desc: "A quick write up about our elopment in Las Vegas."
date: "2022-11-02T05:58:22.907-05:00"
tags:
  - "life"
---
There was no Elvis but there was plenty of fun.

We used [Elope in Las Vegas](https://www.elopeinlasvegas.com/) to handle all the details. We just had to get our marriage license and show up at the right place at the right time. Easy peasy. 10 out of 10. Would recommend. We stayed downtown at [Circa](https://www.circalasvegas.com/), ate lots of good food, drank lots of expensive drinks, did a [walking tour of Vegas](https://www.viator.com/tours/Las-Vegas/Fremont-Street-History-Walking-Tour/d684-6756FCCWT), and [visited Hoover dam](https://www.viator.com/tours/Las-Vegas/Hoover-Dam-Mini-Tour/d684-65770P2). 

Overall, it was a fun and enjoyable trip but we both decided we probably don't need to ever go back. Please enjoy these photos.

<figure>
  {% myImage "vegas09.jpg", "The Las Vegas sign." %}
  <figcaption>The obligatory Las Vegas sign photo. There was a crazy long line. We took this photo as we zoomed by on a tour bus. Much better.</figcaption>
</figure>

<figure>
  {% myImage "vegas06.jpg", "Mike and Rachelle posing with carnival girls for a photo." %}
  <figcaption>First day in Vegas, first 10 minutes of walking out of Circa on Freemont Street. This photo cost a $40 "tip".</figcaption>
</figure>

<figure>
  {% myImage "vegas07.jpg", "Mike and Rachelle posing in front of the Las Vegas marriage license office." %}
  <figcaption>We were nearly first in line but it was obvious this place gets super busy.</figcaption>
</figure>

<figure>
  {% myImage "vegas01.jpg", "Mike and Rachelle posing." %}
  {% myImage "vegas03.jpg", "Mike and Rachelle posing." %}
  {% myImage "vegas04.jpg", "Mike and Rachelle posing." %}
  {% myImage "vegas05.jpg", "Mike and Rachelle posing." %}
  {% myImage "vegas02.jpg", "Mike and Rachelle posing." %}
  <figcaption>A few of my fav photos for the cermony..</figcaption>
</figure>

<figure>
  {% myImage "vegas13.jpg", "Mike and Rachelle posing." %}
  {% myImage "vegas11.jpg", "Mike and Rachelle holding hands, showing a dice and triple 7 tattoos." %}
  <figcaption>Why yes, we did get Vegas tattoos. :D</figcaption>
</figure>

<figure>
  {% myImage "vegas12.jpg", "Mike and Rachelle posing." %}
  {% myImage "vegas08.jpg", "Mike and Rachelle posing." %}
  {% myImage "vegas10.jpg", "Mike and Rachelle posing." %}
  {% myImage "vegas14.jpg", "Mike and Rachelle posing." %}
  {% myImage "vegas17.jpg", "Hoover dam." %}

  <figcaption>And a few misc photo from us wandering around doing tourist stuff.</figcaption>
</figure>