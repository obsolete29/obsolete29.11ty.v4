---
title: "Good morning computer, cold and snowy in Nashville"
date: "2022-03-12T07:08:46.102-06:00"
tags:
  - "goodmorningcomputer"
socialTags:
  - "GoodMorning"
---
Good morning computer. Here in Nashville it's 26 degrees Fahrenheit with 3-4 inches of snow on the back deck. I'm definitely ready for spring but this happens every year. Hopefully this is old man weather's last parting shot.

I had a very stressful and busy week at work. A couple days, I literally had back to back meetings for several hours. It's very hard to get anything done when it's so many meetings. I feel extra pressure with the new Practice Lead role as well. I'm trying to figure out how to adjust so I can sustain a certain level of output so I don't burn out. I'm trying to make some adjustments mentally with my own expectations and I really need to practice how to say no to new requests.

Additionally, I've picked up a part time contracting role with Asurion. I'm building a little project management application for a change management group. I'm using SharePoint Online, Power Apps and Power Automate. It'll be a fun little project and I'm really looking forward to diving in.

## Around the web

Here is some interesting content that came across my feed this week.

### [Spotify just works — Apple Music does not.](https://rusingh.com/spotify-just-works-apple-music-does-not/)

> How do I feel ethically about this choice? I am not privileged enough to inconvenience myself for the greater cause. Pick your battles. I have  mine, and fair pay music streaming is not one of them. It’s the only way to make meaningful change.

Oof. Kind of hate this attitude but to each their own I suppose. Most people prioritize convenience over everything else. It's why the tech giants will probably always be the tech giants. We're unwilling to go without, even for something as *privileged* as streaming digital music.

### [Amazon lied about using seller data, lawmakers say, urging DOJ investigation](https://arstechnica.com/tech-policy/2022/03/us-lawmakers-seek-criminal-probe-of-amazon-for-lying-about-use-of-seller-data/)

> Amazon lied to Congress about its use of third-party seller data, the House Judiciary Committee said today. In a [letter](https://judiciary.house.gov/uploadedfiles/hjc_referral_--_amazon.pdf) to the Department of Justice, the committee chairs asked prosecutors to investigate the company for criminal obstruction of Congress.
>
> “Amazon lied through a senior executive’s sworn testimony that Amazon did not use *any* of the troves of data it had collected on its third-party sellers to  compete with them,” the letter says (emphasis in the original).

### [Bitcoin Was Made for This Moment. So Why Isn’t It Booming?](https://www.nytimes.com/2022/03/11/technology/bitcoin-ukraine-russia-roose.html)

> Long hailed by its advocates as a safe hedge during uncertain times, Bitcoin’s value has fallen steadily in recent weeks.

### [Brave takes on the creepy websites that override your privacy settings](https://arstechnica.com/information-technology/2022/03/brave-has-a-plan-to-stymie-websites-that-override-your-privacy-settings/)

> Some websites just can't take "no" for an answer. Instead of  respecting visitors' choice to block third-party cookies—the identifiers that track browsing activity as a user moves from site to site—they  find sneaky ways to bypass those settings. Now, makers of the Brave  browser are taking action.
>
> Earlier this week, Brave Nightly—the testing and development version  of the browser—rolled out a feature that's designed to prevent what's  known as bounce tracking. The new feature, known as unlinkable bouncing, will roll out for general release in Brave version 1.37 slated for  March 29.

### [System76 Launch review: Linux-friendly keyboard with a USB hub](https://arstechnica.com/gadgets/2022/03/system76-launch-review-linux-friendly-keyboard-with-a-usb-hub/)

> The Launch is System76's [first mechanical keyboard](https://system76.com/accessories/launch), but it could be the last keyboard you need. With hot-swappable  mechanical switches, legends that won't fade, a durable build, and a  pair of detachable cables, this tenkeyless board can evolve with you.

I have one of these and I really love it. I've only ever used one other mechanical keyboard so I don't have much to compare it against. My hands were tired for the first week or so of typing on this thing but now I don't even notice. Love.

### [Biden to issue executive order on cryptocurrency](https://nypost.com/2022/03/08/biden-to-issue-executive-order-on-cryptocurrency/)

> President Biden is expected to sign an executive order on  cryptocurrency this week that will mark the first step toward regulating how digital currency is traded.
>
> The move comes as administration officials have raised concerns in  recent weeks about Russia’s use of cryptocurrency to evade the impact of crushing sanctions in response to its invasion of Ukraine. The  sanctions have sent the ruble to historic lows and have closed the  country’s stock market.

### [Coinbase blocks over 25,000 Russian-linked crypto addresses](https://www.bleepingcomputer.com/news/security/coinbase-blocks-over-25-000-russian-linked-crypto-addresses/)

> Coinbase, one of the most popular cryptocurrency exchange platforms,  announced today that it's blocking access to more than 25,000 blockchain addresses linked to Russian individuals and entities.

I think we're seeing just how inadequate blockchain is at accomplishing it's going of being distributed. You still have a big player able to consolidate and then affect the thing.

### [Go truck yourself](https://world.hey.com/dhh/go-truck-yourself-a14306d9)

> The Canadian truckers have now for almost two weeks faced [insults](https://www.cbc.ca/news/politics/convoy-covid-vaccines-ottawa-black-parliamentary-1.6338720), [threats](https://www.theguardian.com/world/2022/feb/02/truck-convoy-blockade-canada-covid), and [slander](https://twitter.com/TrueNorthCentre/status/1487465754668810240) to [protest the country's new vaccine mandates](https://www.youtube.com/watch?v=5FBwPjtBMXc). With a persistence that is driving some progressives both inside and  outside of Canada absolutely batty. How dare these truckers continue to  exercise their basic political rights to freedom of assembly,  association, and speech?!

Despite my own personal feelings about Hansson, I think he's making a pretty great point.