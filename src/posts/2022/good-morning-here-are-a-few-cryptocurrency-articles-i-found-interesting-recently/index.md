---
title: "Good morning - here are a few cryptocurrency articles I found interesting recently"
date: "2022-02-25T06:39:46.922-06:00"
tags:
  - "goodmorningcomputer"
  - "cryptocurrency"
socialTags:
  - "GoodMorning"
  - "CryptoCurrency"
---
Good morning and Happy Friday.

Here are a few cryptocurrency articles that I found interesting. Maybe you will too.

- [Bitcoin miners revived a dying coal plant – then CO2 emissions soared](https://www.theguardian.com/technology/2022/feb/18/bitcoin-miners-revive-fossil-fuel-plant-co2-emissions-soared)
- [Russia Could Use Cryptocurrency to Mitigate U.S. Sanctions](https://www.nytimes.com/2022/02/23/business/russia-sanctions-cryptocurrency.html)
- [How the U.S. benefits when China turns its back on Bitcoin](https://www.npr.org/2022/02/24/1081252187/bitcoin-cryptocurrency-china-us)