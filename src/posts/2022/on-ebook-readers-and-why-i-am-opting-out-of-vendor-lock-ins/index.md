---
title: "On ebook readers and why I am opting out of vendor lock-ins"
desc: "A detailed look into why bad data retention practices and vendor lock in no longer works for me."
date: "2022-03-09T05:47:47.352-06:00"
tags:
  - "ebooks"
  - "reading"
socialTags:
  - "Reading"
  - "eBooks"
  - "Kindle"
---
I love to read and consider myself to be an avid reader. The smell of books instantly transports me to my teenage years when I was first discovering all the fantasy books that still make up a core of my internal identity. I'm nearly 50 now but I still read a lot. I read less fantasy these days and more science fiction but reading is one of my favorite pastimes. 

I was an early adopter of ebook readers and I believe they provide a better reading experience. Sure, you have to sometimes charge your book but having your entire library at your finger tips, on a single device is quite amazing to me. Additionally, I can buy a new book and start reading within minutes. As I've aged, the ole eyes aren't what they used to be. I actually need my glasses to read these days but on an ebook reader, I can increase the font size and screen brightness which helps tremendously.  Finally, I really love being able to long press on a word right on the screen to get the definition.

Despite the real benefits and advantages of using an ebook reader, I've decided to no longer purchase or consume books in that manner. My problem with the current ebook reader landscape is that books are tied closely to a device's walled garden. Many books (most?) have DRM and there aren't easy, official ways to transfer books between devices of different manufacturers. 

I've had some version of a Kindle for over a decade so most of my ebooks were purchased from Amazon. As far as I know, there's no officially supported way to port the books I've purchased from my Amazon/Kindle account over to a different device like my computer or a device from a different manufacturer. So short of doing some hacking, the books I've purchased to read on my Kindle are held hostage by Amazon.

My newest ebook reading device is a Kobo Clara HD and it's very similar to an Amazon Paper White. It's a really great reading experience as well but I've found that Kobo has many of the same practices as Amazon plus most book publishers insist on DRM with their books so I'm not free to use my own books however I like. It turns out that just being Not Amazon isn't really enough for me.

I feel myself wanting to move away from the tendrils and machinations of big tech. I don't want to be locked into a specific vendor to consume content that I've paid for. I have some books locked up at Amazon, a few at Kobo and even some at Apple as I tried out iBooks. I hate how big tech uses walled garden, data retention tactics on me. Little do they know, the harder they try to retrain me with shitty, anti-consumer practices, the more likely I am to hit the eject button on the whole enterprise.

I'm not against electronic books or readers, I just wish they worked more like MP3s. I purchase some MP3s from my favorite artist, I download some files to my computer then I consume them however I like, on whichever device I like, as much as a like, completely unrestricted, thank you please. Some light web searching seems to indicate that downloading the ebooks from the various places and using something like [Calbre](https://calibre-ebook.com/download) to remove the DRM could be an option at some point, more on that soon I think.

So what's the solution for now? I've been purchasing paper books from my local book store. Rachelle bought me a book light that clips onto the book (thanks babe!) and I have some 2x readers. I'll just read books the old fashioned way. If my local book store pisses me off for some reason then I'll just order books from a different book store. No vendor lock in. No shitty walled garden tactics. Just people selling books to book lovers. That sounds perfect to me.