---
title: "Police start arresting protesters to end the siege of Canada's capital"
desc: "I'm so tired of COVID and protesters. Please go away."
date: "2022-02-18T12:13:21.248-06:00"
tags:
  - "covid"
---
[NPR Posted](https://www.npr.org/2022/02/18/1081716448/police-arrest-ottawa-protesters):

> OTTAWA, Ontario — Police began arresting protesters and towing away  trucks Friday in a bid to break the three-week, traffic-snarling siege  of Canada's capital by hundreds of truckers angry over the country's  COVID-19 restrictions.
>
> "Freedom was never free," said trucker Kevin Homaund, of Montreal. "So  what if they put the handcuffs on us and they put us in jail?" 

I honestly don't understand the world view of these people. Or maybe I understand fine but just disagree that their freedom overrides the public good. Is this just more partisan politics? A football for the entrenched sides to fight over. If you're conservative then you're against mandates and in favor of freedom. If you're liberal then you're in favor of vaccines and the public good. Is it really just simple tribalism?

What really frustrates me about our response to COVID is our half-assed measures because there's no political will to really do anything. We're kind of doing some COVID measures. We're kind of trying to move on with our lives and live in this new reality. The constant mixed messaging is exhausting for me and doing a little bit of this and little bit of that just seems like the worst of both worlds.

I'm tired of people shouting on social media that there's a pandemic, and we shouldn't be doing anything. I'm tired of the mandate protesters and their bullshit. I'm just really tired of it.

Sorry about the rant.