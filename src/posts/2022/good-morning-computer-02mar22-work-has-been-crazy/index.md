---
title: "Good morning computer 02Mar22 - Work has been crazy"
date: "2022-03-02T05:22:12.577-06:00"
tags:
  - "goodmorningcomputer"
socialTags:
  - "GoodMorning"
---
I'm having a lot of fun building a pretty complicated Power App at work this last week. I'm learning a lot about UI/UX, repeatable patterns and how to present a complicated workflow on the screen so that it's intuitive for someone trying to interact with it. I'm going a bit too hard on it though as a couple of my other duties have just been on pause while I focused on this. Yesterday evening I hit a wall with brain power and energy and just had to step away. It's as if I mentally bonked and just couldn't brain anymore until I rested.