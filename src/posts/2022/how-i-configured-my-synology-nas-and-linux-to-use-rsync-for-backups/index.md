---
title: "How I configured my Synology NAS and Linux to use rsync for backups"
desc: "There are many guides on the web about this topic but many are different and contain unnecessary steps."
date: "2022-04-30T11:03:25.615-05:00"
tags:
  - "rsync"
  - "synology"
  - "backup"
socialTags:
  - "Synology"
  - "Rsync"
  - "Linux"
---
I've only used Linux as my daily driver for about a year so I still consider myself a newbie. I don't know about you, but when I'm learning new technologies, there's a fair bit of searching the web and experimenting. Some of that stuff works but some of it doesn't. 

That's how I setup my Synology NAS to work with rsync and my Linux desktop the first time around. I eventually got it to work but I didn't do a good job of documenting how I did it. Plus, I always had this nagging sense that something I tried has misconfigured something that will allow a hostile actor to access my stuff. I finally took the time to reset my Synology so I could setup the NAS and feel confident only the required services are enabled.

## Use case

My use case is I have a Linux PC and I want to use rsync to backup my files to my NAS. My NAS is a [Synology DS220+](https://www.synology.com/en-us/products/DS220+) with two [Seagate IronWolf 4TB](https://www.amazon.com/gp/product/B07H289S79) drives setup in a RAID 1 configuration. My PC is a [System76 Meercat](https://system76.com/desktops/meerkat) running PopOS.

## Synology NAS

First, I need to enable SSH and rsync on the NAS. I use my web browser to navigate to the IP address of my NAS on port 5000 (http://192.168.1.69:5000/) and log in.

Navigate to Control Panel -> Terminal & SNMP then check the *Enable SSH service* check box. Select apply.

Navigate to Control Panel -> File Services. Select the rsync tab and select the *Enable rsync service* check box. Select apply.

Let's test out ssh by opening a terminal window on the local computer and trying to ssh to our NAS: `ssh username@192.168.1.69`. When I did this on a freshly setup NAS, I received the following error message after logging in: *Could not chdir to home directory /var/services/homes/username: No such file or directory*. 

I think it's probably OK to ignore the error message but that would bother me and brings to light an internal conflict. If I enable user home services on the NAS, does that increase the attack surface?! In the end, the desire for no error message won out. I enabled user home services on my NAS by going here: Control panel -> User & Group -> Advanced -> Enable user home services. Now when I ssh into my NAS, there's no error message. Very nice.

When I enabled the rsync service, a new share called NetBackup was automatically created so I think I'll just use that as my backup destination. My computer hostname is cleverly named *workstation* so I'm going to create a new folder called *workstation* as a subfolder of the NetBackup share.

## Linux

My goal is to do automated backups using cron jobs so I want to add my ssh key to the NAS so I'm not prompted for a password when I run the rsync command.

Open a terminal window and issue the following command: `ssh-copy-id username@192.168.1.69`. I'm running these commands on my desktop and you may need to generate ssh keys if you haven't previously done so on yours.

Now when I ssh to the NAS, I'm logged in using ssh keys, without being prompted for a password.

Here is the bash script that I use to perform my backups:

```bash
# init
today=$(date +"%Y%m%d")

# backup crontab
crontab -l > cron-backup.txt

# rsync to nas
rsync -avuz --delete --log-file=$HOME/Backup/${today}-backup.log -e ssh ~/{.config,.mozilla,.ssh,Backup,Desktop,Documents,Music,Pictures,Projects} username@192.168.1.69:/volume1/NetBackup/$(hostname)
```

There's one detail that I struggled to understand about this command so I wanted to document this for anyone else who may be having the same issue.

When I first started building my script, I was trying to rsync to the network share by using this path as my destination: `username@192.168.1.69::NetBackup/$(hostname)`. I had this working prior to reloading the NAS but I think that's because I enabled a manual rsync account under Navigate to Control Panel -> File Services -> rsync. This time around, I didn't want to enable the additional service; I wanted to use the username and password I use to log into the NAS web interface.

I was able to make this destination path work for me: `username@192.168.1.69:/volume1/NetBackup/$(hostname)`. I kiiiind of understand the difference here because if I ssh into my NAS and execute `ls /`, I can see there is a folder called volume1 and then I can see there's a folder called NetBackup under that. I presume there's some Linux file that I have to go edit and put in exceptions or allowances to allow my account to rsync directly to the SMB file share.

Let's setup a cron job to perform our backup automatically. Here is my output from `crontab -e`:

```bash
SHELL=/bin/bash

0 21 * * * cd /home/username/Backup && bash backup.sh
```

I've read that cron executes jobs with a limited environment so I'm setting the SHELL statement to make sure it all works OK. This runs my backup daily at 9 PM local time.

## Conclusion

So that's it! My computer now uses rsync to perform a backup to my Synology NAS and it does it automatically, once a day.

My buddy also has a Synology NAS and we're doing remote backups to each other's NAS so I do have off-site backups. I'd like to document that process as well so that is in the works!

Here are some resources I used while getting this setup:

- [Enable and Connecting to Rsync on a Synology NAS](https://yt.artemislena.eu/watch?v=PIKUeosYcG0) 
- [How to Use rsync on Synology NAS](https://linuxhint.com/use-rsync-synology-nas/) 
- [How do I back up data from a Linux device to my Synology NAS via rsync?](https://kb.synology.com/en-my/DSM/tutorial/How_to_back_up_Linux_computer_to_Synology_NAS)
- [Getting Started with Cron Job in the Linux Server: A Complete Tutorial for Beginner](https://towardsdatascience.com/create-your-first-cronjob-on-the-linux-server-74e2fdc76e78)

Thanks for reading!
