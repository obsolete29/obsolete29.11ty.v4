---
title: "On boycotts and why that's the wrong framing for me"
date: "2022-03-05T08:10:33.220-06:00"
tags:
  - "boycotts"
  - "spotify"
socialTags:
  - "Boycotts"
  - "DeleteSpotify"
---
[The Futility and Possibility of Tech Boycotts](https://chriswiegman.com/2022/03/the-futility-and-possibility-of-tech-boycotts/):

> Yes, these boycotts make me feel better and in that there is some solace. The problem is, as a solution to the problems that need to be addressed, they’re worthless without further action.

I [deleted my Spotify account ](/posts/2021/08/17/opting-out-of-spotify/) last August in favor of [rebuilding my MP3 library](/posts/2022/01/30/lets-make-collecting-an-mp3-library-popular-again/). It's been years since I've had a Facebook account. I [no longer use an iPhone for my mobile](/posts/2021/08/06/i-am-breaking-up-with-apple/) nor do I use macOS. I have not spent money at [Chick-fil-A](https://en.wikipedia.org/wiki/Chick-fil-A_and_LGBT_people) in over a decade. I do not consider these choices as *boycotts* nor am I trying to address societal problems.

Webster [defines boycott as](https://www.merriam-webster.com/dictionary/boycott), *to engage in a concerted refusal to have dealings with (a person, a store, an  organization, etc.) usually to express disapproval or to force acceptance of certain conditions*. I'm not trying to *force acceptance of certain conditions* because that seems impossible. I'm just a single person and I can't control other people. I can't force Spotify to be a better company nor can I force other people to avoid using Spotify. I can only control myself by following my own truth and values. These companies do not align with values I find important so I don't use their products. 

I think my advice to people trying to navigate this topic, and life in general, is to identify the values you think are important. Then you can apply those values to the choices you're making, external to the flavor of the day hash tags, boycott campaigns and even your own internal feelings (h/t to my therapist). 

Good luck!

P.S. #DeleteSpotify ;)

