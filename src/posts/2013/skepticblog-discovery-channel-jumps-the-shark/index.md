---
title: "Skepticblog » Discovery Channel jumps the shark"
date: "2013-08-07"
tags: 
  - "science"
---

Ain't that the truth.

> But since they are purely commercial channels that are all trying to appeal to the 18-31 year-old male audience that advertisers craves, they’ve all gravitated to have almost the same kind of programming: junk reality shows about hillbillies and truckers and storage locker vultures, with occasional bright lights like Mythbusters or River Monsters.

via [Skepticblog » Discovery Channel jumps the shark](http://www.skepticblog.org/2013/08/07/discovery-channel-jumps-the-shark/).
