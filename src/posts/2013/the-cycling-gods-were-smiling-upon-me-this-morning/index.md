---
title: "The cycling gods were smiling upon me this morning"
date: "2013-11-05"
tags: 
  - "cycling"
  - commuting
---

\[caption id="" align="alignnone" width="800"\][![The bike ride in this morning was pretty rewarding.](images/10669319746_f613463b1f_c.jpg "The bike ride in this morning was pretty rewarding.")](http://www.flickr.com/photos/michael_harley/10669319746/ "The bike ride in this morning was pretty rewarding. by Michael.Harley, on Flickr") The bike ride in this morning was pretty rewarding.\[/caption\]

It was 39 degrees this morning as I set out on my commute into work. As a reward for my perseverance, the cycling gods rewarded me with this sight.
