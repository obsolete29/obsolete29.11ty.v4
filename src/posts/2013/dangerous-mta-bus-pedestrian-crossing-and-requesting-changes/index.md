---
title: "Dangerous MTA bus pedestrian crossing and requesting changes"
date: "2013-10-01"
tags: 
  - "travel"
---

Short version: How does a person ask the MTA or the city for improvements to a bus stop / pedestrian crossing?

Long Version: I like riding the MTA as much as possible so I always make an effort to do so when it makes sense. My daughter and grandson (saying that word makes feel so old, and stay off my lawn) came to visit us this weekend. We went to the zoo and ate out and showed them both around Nashville a little.

On Sunday we decided to take the MTA down to Centennial park to play and run around and to do the things you do at Centennial park with a 3 year old. MTA #5 goes straight by Centennial so it's an easy and straight ride, with no transfers. Coming back, we were a little pressed for time so we decided to get off across the street from our road and cross the street instead of riding around the loop in Bellevue and back to the right side of the street. In the screenshot below, we got off at the spot marked _bus stop_. [Here](http://goo.gl/maps/1DlQV) is a Google Maps link of the intersection.

\[caption id="" align="alignnone" width="500"\][![Dangerous bus intersection](images/10037009356_963afe2db8.jpg)](http://www.flickr.com/photos/michael_harley/10037009356/ "Dangerous bus intersection by Michael.Harley, on Flickr") Frogger!\[/caption\]

\[caption id="" align="alignnone" width="500"\][![Bus stop and crosswalk.](images/10036950866_382247496d.jpg)](http://www.flickr.com/photos/michael_harley/10036950866/ "Bus stop and crosswalk. by Michael.Harley, on Flickr") Picture after we've crossed the street.\[/caption\]

Our group (three adults, two teens and a three year old) existed the bus on the shoulder. There's not much room so we're standing in this over grown area, hoping that a car comes to trigger a light change. After a few minutes of no car coming to trigger the light change, there was a break in the traffic on 70 and we just ran across. The line of sight for traffic coming from Bellevue on 70 isn't very far here and cars whiz by pretty fast, 55/60 mph so it was a stressful situation for me.

There's a bus stop located here but there's no marked crosswalk or way for a pedestrian to request to cross. Why locate a bus stop here if the planners hadn't planned for people to cross the street ? A person exiting bus isn't going to scale the rock bluff; they're going to cross the street to the neighborhood.

So I'd like to ask for improvements here. I'd like for there to be a marked crosswalk and a way for somebody waiting at that bus stop to request a light change but I don't even know how to start.  Who do I ask for that?
