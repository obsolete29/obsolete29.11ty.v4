---
title: "Trip Report: Cumberland Trail, Laurel Falls"
date: "2013-01-29"
tags: 
  - "40b440"
  - "hiking"
  - trip-report
---

On Saturday Jen and I hiked the [Laurel Falls](http://www.cumberlandtrail.org/laurel-snow.html) section of the [Cumberland Trail](http://www.cumberlandtrail.org/). This was a pretty short hike as we only went to Laurel Falls plus Jen brought her DSLR so it ended up being more a photography hike than anything; we surely weren't breaking any land speed records.

## Directions

The directions on the Cumberland Trail website were pretty good so no complaints there.

## The Hike

This was a leisurely hike with Jen taking lots of photos. We just took our time and enjoyed the scenery and Jen got to experiment with her camera settings and we ended up having a real good time. There were lots of people on the trail including two hiking clubs and various other people.

[Track (1-26-13, 9-31-39AM) at EveryTrail](http://www.everytrail.com/view_trip.php?trip_id=1994593) 

<iframe src="http://www.everytrail.com/iframe2.php?trip_id=1994593&amp;width=400&amp;height=300" height="400" width="500" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>

\[caption id="" align="alignnone" width="333"\][![Laurel Falls](images/8420677356_4f342b7187.jpg "Laurel Falls")](http://www.flickr.com/photos/pamperedjen76/8420677356/ "Hiking Laurel-Snow by _harleysangel_, on Flickr") Laurel Falls\[/caption\]

\[caption id="" align="alignnone" width="333"\][![Hiking Laurel-Snow](images/8420767694_9438a3d610.jpg)](http://www.flickr.com/photos/pamperedjen76/8420767694/ "Hiking Laurel-Snow by _harleysangel_, on Flickr") This was me most of the hike... waiting for Jen to catch up after taking some photos. :P\[/caption\]

[![Hiking Laurel-Snow](images/8419683061_d6efd07abc.jpg)](http://www.flickr.com/photos/pamperedjen76/8419683061/ "Hiking Laurel-Snow by _harleysangel_, on Flickr")

[![Hiking Laurel-Snow](images/8419668041_8f5605616a.jpg)](http://www.flickr.com/photos/pamperedjen76/8419668041/ "Hiking Laurel-Snow by _harleysangel_, on Flickr")

[![Hiking Laurel-Snow](images/8419652351_428a5ff89c.jpg)](http://www.flickr.com/photos/pamperedjen76/8419652351/ "Hiking Laurel-Snow by _harleysangel_, on Flickr")

[![Hiking Laurel-Snow](images/8420741248_09a774e330.jpg)](http://www.flickr.com/photos/pamperedjen76/8420741248/ "Hiking Laurel-Snow by _harleysangel_, on Flickr")

[![Hiking Laurel-Snow](images/8420736466_d6fedd30bd.jpg)](http://www.flickr.com/photos/pamperedjen76/8420736466/ "Hiking Laurel-Snow by _harleysangel_, on Flickr")

\[caption id="" align="alignnone" width="500"\][![Hiking Laurel-Snow](images/8420669982_9a380ed7bf.jpg)](http://www.flickr.com/photos/pamperedjen76/8420669982/ "Hiking Laurel-Snow by _harleysangel_, on Flickr") We made hot chocolate on the trail! Ha ha.\[/caption\]

## Resources

- [Flickr Photo Set](http://www.flickr.com/photos/pamperedjen76/sets/72157632637943783/)
- [RunKeeper](http://runkeeper.com/user/obsolete29/activity/144704679?&tripIdBase36=2e5iuf)
