---
title: "Life..."
date: "2013-07-10"
tags: 
  - "life"
---

My life these past 3-4 weeks has been crazy and I don't know how to write about it. It's not just that I don't know _how_ to write about it but I _can't_ write about some parts of it. I think I need a therapist.

We moved to Nashville yesterday. My drive into work this morning was twelve minutes and it was awesome. I slept in an hour later and got to work thirty minutes earlier. This afternoon, I'll get home nearly two hours earlier than before. If there is an afterlife, I'm pretty sure this would be my heaven.

Jen's surgery is Friday. It's a big scary thing and I'm afraid. I think everything will OK but it's just this process we have to go through. I also feel guilty for feeling anything other than support. I'm not the sick person and the expectation is that I'm supposed to be supportive and loving. I am supporting and loving but I also feel other things.

Life is hard sometimes.
