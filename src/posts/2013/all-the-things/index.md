---
title: "All the things..."
date: "2013-06-19"
tags: 
  - "life"
---

[![pineapple](images/9082993955_6f33528fc4_m.jpg)](http://www.flickr.com/photos/michael_harley/9082993955/ "pineapple by Michael.Harley, on Flickr")

I have all this crap in my head. I'd like for it to flow out of my head, through my arms, fingers, keyboard and onto this screen but I can't get the words right. I can't figure out how to express the things to convey how I'm feeling.

Stress. Anger. Fear. Sadness. Frustration.

I try to live in the present. I try to just put one foot in front of the other and think about the problem that's directly in front of me but I'm always living in the future, always thinking about future problems or the life I want.

I'm constantly having to remind myself to take life in small chunks. A friend of mine always thinks of pineapple chunks when she hears the word chunks so here is a picture of a pineapple.
