---
title: 'Percy Warner, chores and unpacking oh my'
date: '2013-07-22'
tags:
  - journal
  - life
---

I can really tell the boys are back as the chore load has gone up, especially in the kitchen. Ugh.

We bought another TV for H from craigslist as his broke during the move. On the way, we saw [this](http://www.newschannel5.com/story/22895637/i-440-forced-closed-after-fatal-crash). :(

We've had our most productive day unpacking. Both kid bedrooms look like bedrooms now but now the hall is piled full until we can get our storage unit; hopefully that's Monday.

I got back into the swing of things with the weekly shopping (thanks [Plan to Eat](http://www.plantoeat.com/)). Saw this this as I was leaving Kroger:

{% myImage "20130721-222804.jpg", "sorry no alt text" %}
 
I went over to Percy Warner for a [walk](http://runkeeper.com/user/obsolete29/activity/212537733).

Weird day emotionally.
