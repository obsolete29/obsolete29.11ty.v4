---
title: "And so it goes"
date: "2013-07-20"
tags: 
  - "life"
---

[![20130719-222717.jpg](images/20130719-222717.jpg)](http://michaelharley.me/blog/wp-content/uploads/2013/07/20130719-222717.jpg) TGIF.

It turns out that work is only 5.1 miles from the apartment and today it took me exactly 8 minutes to get there. That will never get old.

Today wasn't terribly productive at work. Worked on some documentation, helped a couple users and then spent the rest of the day researching bike commuting and premium Wordpress themes. Also managed to get Butch Walker [tickets](http://butchwalker.com/post/55799239202/so-heres-the-deal-these-shows-go-on-sale).

Jen needed a break from sitting around so after work she tagged along as I ran errands; insurance, bank, Target and then pizza at NY Pie. I hope she didn't over do it.

I've eaten like a pig lately and I've struggled with my motivation to workout (the heat isn't helping), tho I did manage to go up to the apartment fitness center to ride the stationary bike and use a couple of the machines this evening. I'm going to run a couple miles in the morning; maybe I won't stroke out.

We're finally getting Internet tomorrow. It only took Comcast a week to get a service tech to come out.

Jen seems to feeling better everyday tho she's lonely, bored and tired. Here is a picture of Nürse Ben Stiller.
