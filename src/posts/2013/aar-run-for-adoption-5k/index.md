---
title: "AAR: Run For Adoption 5K"
date: "2013-03-04"
tags: 
  - "40b440"
  - "fitness"
  - running
---

Completing 5 “races” is the second item on My 40 Before 40 list. On Saturday May 2nd, I ran my 2nd 5K, the [Run for Adoption 5K](http://jeremyjyoung.wordpress.com/run-for-adoption-5k/ "Run For Adoption 5K") in Murfressboro, TN. This was a really small event compared to my first 5K. Plus it was cold and snowy so I bet even fewer people showed up.

I didn't follow any specific training plan other than running as much as I could. In the weeks leading up to the 5K, I logged 31.18 miles which I felt pretty good about.

I finished the run in 35:28 according to [RunKeeper](http://runkeeper.com/user/obsolete29/activity/153480132). I was expecting to best the time of 38:38 that I posted for the Mayors 5K Challenge back in November. I would have been happy with 37:00 and would have been totally stoked with 36:30 so to even best _that_ by over a minute was pretty surprising! The course was pretty flat and my normal running routes have a good amount of hills. I actually ran the whole course without stopping to walk!

I felt pretty OK during the run tho I never really felt like I got into the zone 100%. I went out kind of fast and then started feeling anxious about how my legs were feeling. After about the first mile, I was able to calm myself and pace myself better. Compared to others, I'm really slow... I was so slow that I got beat by a mom pushing a stroller! I'm pretty sure see was a cyborg or perhaps an ironmom.

My next 5K is [Petey's 5k Face-Off](http://petersonforparkinsons.org/Events/Peteys_5k_FaceOff "Petey's 5k Face-Off") on April 6th. This will be my first certified and chip timed course. I don't have any specific training plan for the coming month other than run as much as possible. I intend to run 4 times a week. I don't know what the course profile is but if it's flat, I hope to finish in 34:45.

\[caption id="" align="alignnone" width="375"\][![Mike at the Run for Adoption 5k](images/8527494693_f580594650.jpg "Mike at the Run for Adoption 5k")](http://www.flickr.com/photos/michael_harley/8527494693/ "Mike at the Run for Adoption 5k by Michael.Harley, on Flickr") Me before the run!\[/caption\]

\[caption id="" align="alignnone" width="500"\][![Cyborg mom with stroller](images/8527549841_bf96a95254.jpg)](http://www.flickr.com/photos/michael_harley/8527549841/ "Cyborg mom with stroller by Michael.Harley, on Flickr") You can see the cyborg mom in this picture. I think she had power assist on her stroller.\[/caption\]
