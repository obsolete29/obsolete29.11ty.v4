---
title: "Good Riddance 2013"
date: "2013-12-31"
tags: 
  - "year-in-review"
---

This has been the hardest and most stressful year of my life. I'm so glad 2013 is over; good riddance.

I'm not going to do resolutions this year other than to say I want to do no harm, I want to be kind to people, I want to bike, run and hike, I want to learn new things, go see new places and have new experiences.

## I read books

[Goodreads](https://www.goodreads.com/review/list/1009942-michael-harley?read_at=2013&view=covers#) says I read 28 books.

## I blogged stuff

The blog got 3,761 hits in 2013.

## I moved my body

According to [RunKeeper](http://runkeeper.com/user/obsolete29/), I ran 145 miles, walked 35 miles, biked 932 miles, and hiked 31 miles in 2013.

## I listened to [music](http://www.last.fm/user/obsolete29).

{% myImage "11670319886_f46dc5c055_o.png", "opartists by Michael.Harley, on Flickr" %}

{% myImage "11670320086_0b8a50b39d_o.png", "toptracks by Michael.Harley, on Flickr" %}

## Photo year in review

<figure>
  {% myImage "8371897720_1bac9731a3_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>January: Totaled Jeep gets hauled off for scrap</figcaption>
</figure>

<figure>
  {% myImage "8401507379_876d70d918.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>January: Jen and I were invited to hang at a cabin near the Smokies by our friend David.</figcaption>
</figure>

<figure>
  {% myImage "8436386232_b89dc11cf0_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>February: Carter! Snow!</figcaption>
</figure>

<figure>
  {% myImage "8527494693_f580594650_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>March: I ran a 5k!</figcaption>
</figure>

<figure>
  {% myImage "8528319167_e08fa42c3f_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>March: This guy...</figcaption>
</figure>

<figure>
  {% myImage "8546388730_68cf500553.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>March: Butch Walker! First!</figcaption>
</figure>

<figure>
  {% myImage "8624766654_2fabfc6f14.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>April: Omgerd! Anerther furve k!</figcaption>
</figure>

<figure>
  {% myImage "8632445819_2de20e90a2_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>April: BIRTHDAY!!</figcaption>
</figure>

<figure>
  {% myImage "8712316298_728ac3dc7b_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>May: I drank tea!</figcaption>
</figure>

<figure>
  {% myImage "9109053287_a6ef61ce51.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>June: I went hiking!</figcaption>
</figure>

<figure>
  {% myImage "9177923300_7c78052a3d.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>June: And the Alamo!\</figcaption>
</figure>

<figure>
  {% myImage "9237041852_40a47e8409.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>July: South Haven</figcaption>
</figure>

<figure>
  {% myImage "9237114882_d946c35789_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>July: Pretty Lake </figcaption>
</figure>

<figure>
  {% myImage "9325948085_c01db8a572_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>July: Surgery</figcaption>
</figure>

<figure>
  {% myImage "9591394783_fa388f5ea2_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>August: Got my bike!</figcaption>
</figure>

<figure>
  {% myImage "11527458276_ba5ec3c9bb_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>August: Chemo started </figcaption>
</figure>

<figure>
  {% myImage "10006159636_0c599d44db_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>September: Carter came to visit us in Nashville</figcaption>
</figure>

<figure>
  {% myImage "10000335553_33303694f7_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>September: ...and asked lots of questions</figcaption>
</figure>

<figure>
  {% myImage "9991587326_0be172211e_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>September: ...and really liked the zoo! </figcaption>
</figure>

<figure>
  {% myImage "10177772776_a3255c83ed_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>October: I biked... </figcaption>
</figure>

<figure>
  {% myImage "10320608833_de833683fa_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>October: ...and explored</figcaption>
</figure>

<figure>
  {% myImage "10561677225_7679c6a1c8_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>October: ..and more chemo</figcaption>
</figure>

<figure>
  {% myImage "11069154595_f0ed05b859_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>November: I went camping for the first time ever.</figcaption>
</figure>

<figure>
  {% myImage "11120050633_87cc635c4d_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption> November: and biked around Nashville more.</figcaption>
</figure>

<figure>
  {% myImage "10669319746_f613463b1f_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption> This happened too.</figcaption>
</figure>

<figure>
  {% myImage "11527439603_746c0ef207_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>December: My brother graduated from TTU </figcaption>
</figure>

<figure>
  {% myImage "11627510033_6384757de2_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>December: Last chemo treatment!</figcaption>
</figure>

<figure>
  {% myImage "11628046756_06dea37aed_n.jpg", "These images were imported. Sorry no alt text." %}
  <figcaption>December: Christmas!</figcaption>
</figure>
