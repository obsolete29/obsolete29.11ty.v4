---
title: "Trip Report: Savage Gulf, Stone Door -> BGC -> BGR Trail"
date: "2012-08-13"
tags: 
  - "hiking"
  - trip-reports
  - stone-door
  - savage-gulf
---
{% myImage "7770477176_d023ac83e3_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

In preparation for our [Mt LeConte](http://www.lecontelodge.com/) hike on August 25th, we hiked in the Savage Gulf area on Saturday. We also met up with our new Twitter friend, Amber, her friend Chris and Amber's daughter Madison for the first time. They'll be joining us on our LeConte hike on the 25th too!

**Directions**

My friend David showed me where this was initially so I don't have a website to link to. Here are the details tho:

Stone Door Ranger Station 1183 Stonedoor Rd Beersheba Springs, TN 37305

[Here](https://plus.google.com/117003498819325417299/about?gl=us&hl=en) is the link on Google Maps.

There are clean bathrooms and a water fountain right at the trail head. The first 1/2 mile is paved up to the first outlook but then it's a regular trail the rest of the way.

**The Hike**

The route we took was Stone Door Ranger station to Stone Door. It's only 1 mile from the trail head to the Stone Door. It's a really cool natural feature and it's right here in our back yard. After descending from Stone Door via the big Gulf Creek trail, the trail follows along the river before ascending to a camp ground and following the Big Gulf Rim trail back to Stone Door. We had planned to see Ranger Falls but it had rained hard the night before and what's normally a dry river bed of boulders was [way up](http://www.flickr.com/photos/pamperedjen76/7770485828/) and we couldn't cross.

There was also a few fresh blow downs on the trail. [One](http://www.flickr.com/photos/pamperedjen76/7770506462/) in particular seemed to be from the night before and was somewhat difficult to circumnavigate because of where it was located on the hillside. We had to climb a pretty steep and muddy hill, cross over and then slide back down to the trail.

{% myImage "7770508800_b999755821_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

This is a hike we've done a few times before, it's one of my favorite hikes and it's about an hour from my house. I would consider this hike on the upper end of moderate because of the distance (9.5 miles), rough terrain and the elevation gain (2264' total). Even though I've done this hike 3 separate times, I'm somewhat surprised each time at how steep the climb out of the gulf is. This was Jen's first training hike in preparation for LeConte and she was pretty [pooped](http://www.flickr.com/photos/pamperedjen76/7762698176/).

Resources:

- [RunKeeper Activity Report](http://runkeeper.com/user/obsolete29/activity/108900513)
- [Flickr Set](http://www.flickr.com/photos/pamperedjen76/sets/72157631036477756/)
