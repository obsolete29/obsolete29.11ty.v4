---
title: "30 Day Personal Fitness Challenge"
date: "2013-08-05"
tags: 
  - "fitness"
  - "life"
---

These past months have been pretty stressful. That stress has made it easy for me to skip workouts and shovel all the food into my face hole. As a result, I've gained about 12 lbs during that time.

I still want to be a happy, healthy, active person so this is me trying to dig in my heels to stop the slide into fat guy land. As part of that, I'm going to do a 30 day fitness challenge. During my challenge, I'm going to do these things to make myself accountable:

First, I'm going to weigh myself and I'm going to post that on the Internet. This morning, I was 280.9 lbs. I'll post this every Monday.

Second, I'm going to move my body everyday. This morning was a [short run](http://rnkpr.com/a3n1p9i) but other activities can be walking or hitting the fitness center here in the apartment complex. I'm saving my pennies so I can purchase a bike for biking to work so I'll have about an hour of cardio built into my day once I start that.

Third, I'm going to track my calories everyday... even on bad days when I've eaten all the things and I'm way way over my calorie goal. I use [myfitnesspal](http://www.myfitnesspal.com/food/diary/obsolete29) to do that. I know there are some thoughts that perhaps calorie tracking isn't the best way to lose weight but I know this method works for me and it makes me think about what I'm shoveling into my mouth.

The goal is for all of this to translate into me feeling better about myself, feeling healthier and for the scales to agree with how I'm feeling. Wish me luck.
