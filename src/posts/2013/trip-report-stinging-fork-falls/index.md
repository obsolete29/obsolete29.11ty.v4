---
title: "Trip Report: Stinging Fork Falls"
date: "2012-07-21"
tags: 
  - "hiking"
  - trip-report
  - cumberland-trail
---
<figure>
  {% myImage "7616198780_0279d96be5_m.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Stinging Fork Falls</figcaption>
</figure>

As part of my [40 Before 40](http://michaelharley.me/blog/2012/my-40-before-40/ "My 40 Before 40"), I'm trying to hike every section of the [Cumberland Trail](http://www.cumberlandtrail.org/ "Cumberland Trail"). Today's hike was [Stinging Fork Falls](http://www.cumberlandtrail.org/stinging_fork.html "Stinging Fork Falls") down near Spring City, TN. The trail head was easy to find using the directions from the Cumberland Trail site.

I started later than I normally like because I was up late last night goofing around on the Internet. Plus, I've been on this hike before and I knew it was pretty short. While it was a short distance, there are some steep sections of the trail that you'll want to take your time with. The fact that I got a later start than normal, plus the humidity made for a really sweaty hike. There was only a couple minor blow downs across the trail and those were easy to navigate. It's rained a fair amount the past couple weeks so the falls was pretty decent. All in all, an easy hike with a decent pay off at the end. I would still rate the hike as moderate though because of how steep the one section is.

I'm planning to hike up to [LeConte lodge](http://www.lecontelodge.com/) via [Alum Cave trail](http://www.lecontelodge.com/about/hiking-trails/) on August 25th so I'm going to be ramping up the miles over the next few weeks.

Resources:

- [Photo Set](http://www.flickr.com/photos/pamperedjen76/sets/72157630682416754/)
- [RunKeeper Report](http://runkeeper.com/user/obsolete29/activity/103534752)

https://www.youtube.com/watch?v=P0OzmXN4p14
