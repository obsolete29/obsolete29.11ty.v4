---
title: "Dorking around with Nashville transit"
date: "2013-08-10"
tags: 
  - "journal"
  - "life"
  - "travel"
---

Anybody who knows me knows that I'm into transit and I want to bike around and all those hippie things that hippies do. Be green, save the earth, carbon foot prints etc. I've always been into the _idea_ of transit and urban biking at least. I've often wondered how different the idea and reality is.

Since we've moved to Nashville, I've had a few opportunities to experiment with the [Nashville MTA](http://www.nashvillemta.org/) and so far the experience hasn't been terrible.

Our friends organize a happy hour thing ([HOH Nation](http://www.hohnation.com/)) every Friday night so I thought that would be a great reason to ride the bus down into the city.

Our first experience didn't start out so great as we had to wait nearly 40 minutes for the bus to come. I have no idea what the hold up was but the rest of the experience was fine.

The second experience was today and was much better. We only had to wait 8 minutes! According to the bus schedule, the bus comes by every 20 minutes so we just lucked up there. The ride down to mid-town seemed pretty quick tho I didn't think to time it. We caught the last bus heading back at 9:58pm and it was exactly 20 minutes later that we were stepping off the bus. According to Waze, it would take us 17 minutes to drive to Winners. That's not bad for the bus!

Admittedly, Winners is on the same bus line as our apartment so it's a simple, straight shot and we don't have to transfer. Tomorrow we're going to take the bus to the [Tomato Arts Festival](http://tomatoartfest.com/) and we plan to bus... with the kids and we'll have to transfer. This will be a more robust test.

Once we learn the schedule of the bus, we can time our arrival at the at the stop better as to minimize the waiting.

One thing I don't like is the buses stop running kind of early. 10pm isn't that late for a Friday night so we would have to get a taxi home or call Taxi Tyler for a pick up if we wanted to stay later. They stop running even earlier on the weekends. Something like 7pm according to the printed schedule. Bummer. Another thing I'm not too fond of is the bus stop situation. The two in front of our apartment complex are just small blue signs on posts. The one across the street barely has any room to stand to cross the street back to the apartment nor is there a marked pedestrian cross walk. It's seems unsafe to cross the road there, especially after 10pm at night.

So far the results are mixed tho overall I'm positive on the experiences. I can't wait to get my bike.
