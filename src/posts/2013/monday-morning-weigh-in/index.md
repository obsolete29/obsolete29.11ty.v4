---
title: "Monday Morning Weigh In"
date: "2013-08-19"
tags: 
  - "fitness"
---

I got off to a rocky start to my fitness challenge but I'm on track now, I think.

My weight this morning: 276 lbs

That's 4.9 lbs in two weeks. The cycling is helping a lot.
