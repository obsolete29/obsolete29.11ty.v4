---
title: "Catching up #journal"
date: "2013-07-28"
tags: 
  - "journal"
---

Friday was the first day of Titans training camp that was open to the public so of course we went. My [brother](http://twitter.com/tgo31) and I also went yesterday and yeah, [this](http://www.usatoday.com/story/sports/nfl/titans/2013/07/27/tennessee-titans-training-camp-fight/2592915/) happened right in front of us and it was awesome. Jen and I are going today too as it's the first day of full pads. Yay for living in Nashville! :D

Last night Jen and I went to see [Oh No Fiasco](http://ohnofiasco.com/) at The Rutledge. It was great and [Lindsey](https://twitter.com/LindseyStamey) is awesome. Really. #DrVodka was present but well behaved.

My plan for today was to go figure out the MTA. I was going to ride the bus down to Centennial and then maybe bike around using the b-cycles but then I remembered, we still weren't unpacked. So, we moved stuff to storage and hung pictures instead. MTA and biking next weekend!

Jen is feeling better and better everyday. It's easy to forget that she just had major surgery just two weeks ago.
