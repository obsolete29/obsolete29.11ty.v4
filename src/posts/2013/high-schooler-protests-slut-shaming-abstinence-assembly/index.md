---
title: "High Schooler Protests 'Slut-Shaming' Abstinence Assembly"
date: "2013-04-18"
tags: 
  - "life"
  - "sex-education"
---

[High Schooler Protests 'Slut-Shaming' Abstinence Assembly](http://thinkprogress.org/health/2013/04/17/1883121/west-virginia-abstinence-assembly/?mobile=nc).

Here's a fun quote:

> GW Principal George Aulenbacher, on the other hand, didn’t see anything wrong with hosting Stenzel. “The only way to guarantee safety is abstinence. Sometimes, that can be a touchy topic, but I was not offended by her,” he told the West Virginia Gazette last week.

I often wonder why these people don't abstain from other dangerous activities like driving, swimming and riding motorcycles. After all, the only way to guarantee safety is abstinence.

Everything we do in life has risks and we all do a risk analysis before performing those activities. Can we minimize the dangers of an activity while still doing it? Yes we can.
