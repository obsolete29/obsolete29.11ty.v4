---
title: "How to hang 2 pictures on a wall evenly"
date: "2013-08-05"
tags: 
  - "handy-man"
  - how-to
---

We're still unpacking and getting settled after our move to Nashville. On Saturday we worked a few hours hanging pictures and unpacking more boxes.

I feel kind of dumb in retrospect but I just couldn't figure out how to hang two pictures on a wall evenly. I couldn't figure out the math to make the space between edge of the wall and the space between the pictures the same. I could just eyeball it but I couldn't verify it with math. Ugh. My mind kept wanting to divide the wall into thirds, measure and hang but when you do it that way, the space between the pictures is less than the space between the picture and the edge of the wall. Here are the math steps to figure it out. I'll use my specific measurements as examples.

1. Measure your wall. Our wall was 98 inches.
2. Measure the width of your pictures and add that together. We had two pictures of the same size and they were 21.5 inches wide so 21.5 + 21.5 = 43 inches.
3. Subtract the width of your pictures in step 2 from the width of your wall. 98 - 43 = 55. We now know that we have 55 inches of space that's not covered by a picture.
4. Now divide the number of spaces into the amount of free space from step 3 (55 inches). Since there are two pictures, we have three spaces. So 55 / 3 = 18.3.
5. The edge of the pictures should be placed 18 1/3 inches apart.
6. Since our pictures are 21.5 inches wide, I just divided 21.5 / 2 to find the center point on the frame (10 3/4 inches) and added that to the amount of space determined in step 4 so 18.3 + 10.75 = 29.05 inches. So I measured 29 inches from the left of my wall and made a mark. I did the same thing from the right edge of the wall and placed my hangers and hung my picture frames and they were evenly spaced. Yay OCD brain!

I wish I had paid better attention in grade school math.
