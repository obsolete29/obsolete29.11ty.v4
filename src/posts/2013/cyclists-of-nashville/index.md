---
title: "Cyclists of Nashville"
date: "2013-09-28"
tags: 
  - "cycling"
---

Here are some bikes and cyclists I saw today as I was out and about.

\[caption id="" align="alignnone" width="500"\][![Bikes of Nashville](images/9975412556_a588682b7f.jpg)](http://www.flickr.com/photos/michael_harley/9975412556/ "Bikes of Nashville by Michael.Harley, on Flickr") Doing it right! On the road, with traffic. No helmet but I'm not a helmet nazi.\[/caption\]

\[caption id="" align="alignnone" width="500"\][![Bikes of Nashville](images/9975411506_13dcfa7d4a.jpg)](http://www.flickr.com/photos/michael_harley/9975411506/ "Bikes of Nashville by Michael.Harley, on Flickr") Doing it wrong; riding on the sidewalk. :\\\[/caption\]

\[caption id="" align="alignnone" width="500"\][![Bikes of Nashville](images/9975371764_fb1373e683.jpg)](http://www.flickr.com/photos/michael_harley/9975371764/ "Bikes of Nashville by Michael.Harley, on Flickr") Using a u-lock at least!\[/caption\]

\[caption id="" align="alignnone" width="500"\][![Bikes of Nashville](images/9975370044_f04eeede8a.jpg)](http://www.flickr.com/photos/michael_harley/9975370044/ "Bikes of Nashville by Michael.Harley, on Flickr") Nice bike. Using a u-lock to secure but front wheel is quick release and not secured. It's also a dynohub so expensive! Also, nice Brooks saddle and pretty expensive; not secured.\[/caption\]

\[caption id="" align="alignnone" width="500"\][![Bikes of Nashville](images/9975407596_dd00a4b9da.jpg)](http://www.flickr.com/photos/michael_harley/9975407596/ "Bikes of Nashville by Michael.Harley, on Flickr") I approve - riding with traffic.\[/caption\]

\[caption id="" align="alignnone" width="500"\][![Bikes of Nashville](images/9975469353_b7d6a07c4f.jpg)](http://www.flickr.com/photos/michael_harley/9975469353/ "Bikes of Nashville by Michael.Harley, on Flickr") Yay!\[/caption\]

\[caption id="" align="alignnone" width="500"\][![Bikes of Nashville](images/9975335425_7e8704c3e3.jpg)](http://www.flickr.com/photos/michael_harley/9975335425/ "Bikes of Nashville by Michael.Harley, on Flickr") Work commuter!\[/caption\]

\[caption id="" align="alignnone" width="500"\][![Bikes of Nashville](images/9975362674_ee5924318e.jpg)](http://www.flickr.com/photos/michael_harley/9975362674/ "Bikes of Nashville by Michael.Harley, on Flickr") Doing it wrong. Riding on the sidewalk, against traffic. Bikes are not pedestrians.\[/caption\]

Saw lots of bikes around today.
