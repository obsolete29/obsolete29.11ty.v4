---
title: "June Check In"
date: "2013-06-04"
tags: 
  - "life"
  - jen-cancer
---

Life is stressing me out lately. There's so many big things happening right now and everything just seems so complicated. I wish things were simpler.

Jen had to have a biopsy done on her breast last week. We're getting results back this week.

We're moving to Nashville this summer before school starts back. As a result we have the house listed. I didn't realize how much stress was involved in getting and keeping a house ready for show. In the perfect world, we'd get an offer on the house today and we'd get enough money to move on. In the world I live in, we'll have to walk away from it and just scrape together the money the best way we can to move.

Tyler wrecked Jen's car that we just got her a couple months ago. It's just cosmetic damage (busted out the front headlight) mostly but still... ugh.

The car that I drive to work is on its last legs with over 225k miles on it. I'm pretty sure a wheel is going to fall off at any moment.

On the bright side, we did find new homes for all of our pets, we no longer have a toddler living in the house and all the kids are gone to visit their dad for the summer. Our house is as peaceful -- and clean -- as it's ever been.

So yeah... that's my life recently. Sorry about the rambly emo post.
