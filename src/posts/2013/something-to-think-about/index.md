---
title: "Something to think about"
date: "2013-06-11"
tags: 
  - "life"
---

I've run across a few things these past few days and they've been on my mind a lot; I felt the need to share.

"You have got to own your days & live them, each one of them every one of them, or else the years go right by & none of them belong to you." -Unknown

[![](images/9018395466_61d6625f6e.jpg)](http://www.flickr.com/photos/pamperedjen76/9018395466/)
