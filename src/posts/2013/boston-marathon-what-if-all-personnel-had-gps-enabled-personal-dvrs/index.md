---
title: "Boston Marathon: What if all personnel had gps enabled, personal dvrs?"
date: "2013-04-18"
tags: 
  - "ideas"
  - "security"
  - "technology"
---

In the wake of the bombings at the Boston Marathon I've been listening to the radio and reading about the bombings. One of the early reports described how one of the bombs was pretty close to a security guard who was scanning the crowd. If the bomber didn't look suspicious  then there's no reason for the security guard to remember the bomber at all so I'm not trying to fault the guard.

But what if all security personnel at the event wore a Google glass type device that recorded everything during their shift? What if these devices were GPS enabled and all of the video was download into a server and that data could then be datamined? For instance, show me all video from all devices that occured between 1:00pm and 3:00pm at coordinate X.

This nerdy idea has been bouncing around in my head for a few days and I hope typing it out will make it stop. ;)
