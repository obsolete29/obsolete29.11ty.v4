---
title: "In Beijing, the New IPhone Gets a Resounding 'Meh'"
date: "2013-09-12"
tags: 
  - "technology"
---

I actually agree with this:

> One employee of China Telecom (CHA), which has a partnership with Apple, declined to give her name but offered a theory on Apple’s diminished mystique in China. She says Steve Jobs was revered in China as a creative miracle. Now, she says, Apple is just an ordinary company controlled by businessmen.

via [In Beijing, the New IPhone Gets a Resounding 'Meh' - Businessweek](http://www.businessweek.com/articles/2013-09-11/in-beijing-the-new-iphone-gets-a-resounding-meh).
