---
title: 'Trying to buy a bike, help me decide'
date: '2013-08-07'
tags:
  - fitness
  - life
  - cycling
  - commuting
---

So, I want to commute to work and bike around Nashville. To get the most bang for my buck, I've been cruising Craigslist for something used. Here are the candidates, in no particular order:

1. [Trek 7.2 fx road hybrid](http://nashville.craigslist.org/bik/3976030600.html) (another one [here](http://nashville.craigslist.org/bik/3976030600.html))
2. [NEW! TREK 7.4 FX BIKE](http://nashville.craigslist.org/bik/3979886131.html)
3. [Road Bike, TREK](http://nashville.craigslist.org/bik/3983856426.html)
4. [trek 1.1](http://nashville.craigslist.org/bik/3985113018.html)

In the perfect world, I'd like to get a road bike with flat handle bars. I'm still tempted to walk into a bike shop and just let them sell me something too.
