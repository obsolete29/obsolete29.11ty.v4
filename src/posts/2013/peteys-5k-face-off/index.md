---
title: "Peteys 5k Face-Off"
date: "2013-04-18"
tags: 
  - "40b440"
  - "fitness"
  - running
---

[![Morning! Lets run and thangs.](images/8623565333_f5a7ae0ce6_n.jpg)](http://www.flickr.com/photos/michael_harley/8623565333/ "Morning! Lets run and thangs. by Michael.Harley, on Flickr")

Completing 5 “races” is the second item on My 40 Before 40 list. On Saturday April 6th, I ran my 3rd 5K, [Petey's 5k Face-Off](http://petersonforparkinsons.org/Events/Peteys_5k_FaceOff) in Hendersonville, TN. This was my first chipped and certified course.

My goal was to run it in 34:45 but in the back of my head I thought 34:00 was a possibility. I hadn't really been running as much or as consistently as I wanted leading up to the race but I was feeling real good over all.

I didn't know what the course profile looked like but it turns out that there were some hills! Gah! As soon as I saw that I thought my estimated time was out the window and this actually allowed me to relax and just run and enjoy it without the pressure of beating my old times.

The run itself was really enjoyable. I started out well and felt good. After the half way mark I really started to push myself and pick up the pace. I normally run an 11:30'ish pace if left to my own devices without trying to pace but I was right around 10:00 the whole second half and it felt pretty great. There were several hills but the training runs I do in my hood has lots of hills and some of them are pretty steep; steeper than anything on the race course and I felt like it really helped me a lot. A lot of other people in my speed group were walking on the hills but I was able to just keep on chugging... I envisioned myself as a big ole diesel engine, complete with smoke coming out of my smoke stacks.

I ended up finishing in [34:03](http://runkeeper.com/user/obsolete29/activity/164258021)! Hah! I feel pretty good about that considering the hills situation.

My next race is the [Cinco De Mayo 5k](http://www.active.com/running/nashville-tn/cinco-de-mayo-5k-10k-2013) in Nashville. The weather is going to be hotter so I'm hesitant to offer a really aggressive time estimate. I'd be happy with 33:30 but I think if I can push and get really close to 33:00.
