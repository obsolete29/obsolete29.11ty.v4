---
title: "On friendships"
date: "2013-12-31"
tags: 
  - "journal"
  - "life"
---

It's hard for a cisnormative, adult male to make new friends. I'm not sure why that is but I feel like I struggle to find what I consider to be my tribe. Where is the tribe for progressive, atheist, secular humanist, tattoo collecting, cycling, backpacking/hiking, introspective, reading, geeky people?

I read an article somewhere about introverts and while I don't consider myself an introvert exactly, what the article said about friends and friendship resonated with me. It said that introverts prefer fewer close friends to many casual friends and I found myself nodding in agreement as I read the article.

If there is a friendship scale from 1 to 10 with 1 being _w__asn't that guy's name Joe?_ and 10 being _dude, can you help me move this dead body?_, I'd say I have a good core circle of friends in the 5 range and a couple other circles in the 3-4 range. My single 8+ friend lives far away and we rarely get to hang. If I could just elevate my core group of 5 range friends to the 7 range, that'd be awesome. Instead, I recently realized that somebody that I thought of as a 7 range friend probably only considers me as a 4 range friend. Bummer.

Is it inappropriate to just inform somebody that you'd like to be BFF's? I guess I just invite people to do things? Where is the manual or Wikipedia article on how to make new BFF's? Would it be weird to create an OKC profile searching for friends only? :/

Yours truly,

Awkwardly Searching
