---
title: "Tennessee Titans selected Chance Warmack with the 10th pick"
date: "2013-04-26"
tags: 
  - "titans"
---

The Tennessee Titans [selected Chance Warmack](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=newssearch&cd=8&cad=rja&ved=0CFoQqQIoADAH&url=http%3A%2F%2Fnfl.si.com%2F2013%2F04%2F25%2Fchance-warmack-selected-no-10-by-tennessee-titans%2F&ei=Nnt6UbCyO63_4AOb9ICABg&usg=AFQjCNG-_phPmZ9M_sb9gtt7oGVfzG0Zlg&sig2=lYdaN1Z8hGId1DHoUXHjcg&bvm=bv.45645796,d.b2I) 10th overall. I'm excited... and how cool of a name is Warmack for an offensive lineman? So FFS, can we please get a consistent run game now? On paper, we have an elite offensive line with _2_ HOF offensive line coaches!

<iframe src="http://www.youtube.com/embed/GoRBxEONrIE" height="315" width="560" allowfullscreen frameborder="0"></iframe>
