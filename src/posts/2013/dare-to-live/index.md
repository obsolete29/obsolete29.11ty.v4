---
title: "Dare to live"
date: "2013-08-22"
tags: 
  - "life"
  - "qotd"
---

"Dare to live the life you have dreamed for yourself. Go forward and make your dreams come true." - Ralph Waldo Emerson
