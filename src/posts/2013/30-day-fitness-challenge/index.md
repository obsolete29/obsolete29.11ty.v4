---
title: "30 Day Fitness Challenge"
date: "2012-07-22"
tags: 
  - "fitness"
---

[Jessica](https://mobile.twitter.com/#!/jharleyxo) and I have decided to have another fitness challenge. I'm not sure if you guys remember but this will be the 2nd one that we've had and Jess won the first one.

There will be three components to this challenge.

1. myfitnesspal - We will be using the myfitnesspal app to track our calories. The goal is to be at or below the calorie goal according to the app. If we're both under for a particular day then that day is basically a draw. If we've both over, it's whoever was closest to their goal. This category was the deciding factor during the last challenge.
2. milage - we'll be using RunKeeper to track our distance walked, ran or hiked. The person with the most overall milages is winner. I easily won this category last time.
3. weightloss % - The final category is weightloss as a percentage of our body weight. Jess won this section last time.

Our first day is tomorrow. If you're interested in playing along, feel free comment!
