---
title: "Return of the Jed... err, kids"
date: "2013-07-20"
tags: 
  - "journal"
---

I [ran](http://runkeeper.com/user/obsolete29/activity/21176760) this morning. It felt good to move my body even tho it was still hot as hell. I have no idea how these people running around in the noon day sun do it.

Comcast came today and got our Internet working but there are a lot of wifi networks here so it's still a bit sketchy.

{% myImage "20130720-182856.jpg", "" %}

Here is some info about running [wifi in an apartment building](http://blogs.computerworld.com/wireless-networking/20616/wi-fi-apartment-building). In short, everybody should be running on channels 1, 6 or 11 if they're running 2.4 GHz.

We were trying to find some self storage for some of this junk we have. The self storage place down the street wants $94/month for a 5x10 unit. We paid $20 for a 6x9 in Crossville. We searched around and found a place in Brentwood for $74 but who wants to drive 25 minutes one way? We punted and will regroup on Monday.

All three boys came back today. I enjoyed the peace and quiet but the Nashville honey moon is over.

{% myImage "20130720-183529.jpg", "" %}

I changed the the shower heads to high efficiency, 1.6 gpm models. I didn't have the correct wrench so I had to go buy one. Here is a picture of me, being manly with my new pipe wrench. \*appropriate man sounds here\*

Today has been Jen's best day since the surgery. She's only taking Tylenol and ibuprofen so far and she's been able to manage her pain.
