---
title: "Nashville foosball"
desc: "I’ve been playing foosball, mostly at work for about 2 years now. I've taken to the game and when I started to pursue playing outside of work, I had some trouble finding good information on the internet about places to play foosball in Nashville."
date: "2019-10-05"
lastmod: "2019-10-05"
tags:
  - "foosball"
---
_12/24/2019 Edited to add Pins_

I’ve been playing foosball, mostly at work for about 2 years now. I've taken to the game and when I started to pursue playing outside of work, I had some trouble finding good information on the internet about places to play foosball in Nashville.

A good place to start is [The Nashville Foosball Page](https://www.facebook.com/nashfoos/) over on Facebook. Whoever runs the page, posts local foosball events, such as they are.

Below are bars that have tornado foosball tables in the Nashville area. If you know of more, please drop a comment and I'll add it to the list.

- [Pins Mechanical Co.](https://www.pinsbar.com/nashville) - This is a new place here in Nashville and they do have a Tornado table. Cons: The table isn't well lit atm though the guys have been working on trying to get something installed. Pros: Highlife is $3.50 and games are only $.50.
- [Smith & Lentz](https://goo.gl/maps/WxmYpFbC46EJu5WH9) - This is my favorite place to play foosball in Nashville right now. They have a really new foosball table and it's non-smoking inside. It does cost to play and they do not have a bill changer but the bar staff has always been very pleasant when asking them to make change to play.
- [Melrose Billiards](https://goo.gl/maps/qyAVJTfDD4ZCjZA8A) - Melrose Billiards is also a cool place as it's in my hood and in a neighborhood I frequent a lot. They have two Tornado foosball tables which are in good shape. It's smoking after 10pm so just be aware. They do have a change machine.
- [The Villager Tavern](https://goo.gl/maps/gt2JBUg4pjyBBSUw8) - Probably my least favorite place to play that still has a Tornado foosball table as it's smoking full time and the table is a little older but still in decent shape. No bill changer but bar staff has made change for me without any issues.

Some other bars that have tables that are barely worth mentioning but for the sake of completeness, I'll list them.

- [3 Crow Bar](https://goo.gl/maps/8uqyijBiQUyyL6rY6) - Tornado foosball table but in really bad shape and isn't a quality table anyway.
- [Mickey's Tavern](https://goo.gl/maps/seRqSL1gQskHZDR2A) - I think it's a Tornado foosball table but it's in bad shape as well.
- [HiFi Clyde's](https://goo.gl/maps/r7yQHA2zjbaaUDc76) - They have a few tables but they're toys and the place is usually full of tourists.
