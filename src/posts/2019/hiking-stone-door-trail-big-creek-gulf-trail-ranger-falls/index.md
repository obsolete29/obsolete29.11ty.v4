---
title: 'Hiking Stone Door Trail, Big Creek Gulf Trail, Ranger Falls'
desc: 'On July 15th 2019, Meredith and I did some hiking in Savage Gulf. This is my trip report.'
date: '2019-07-15'
lastmod: '2019-07-15'
tags:
  - hiking
  - trip-report
  - stone-door
  - savage-gulf
---
I really love the Savage Gulf area and hiking these series of trails. After [last week's disaster of an effort](/posts/hiking-fiery-gizzard-trail-raven-point/), I needed something familiar yet challenging so I could prove that I'm still a hiker. I tell you guys, last week really shook my confidence. This week, we decided to hike Stone Door Trail to Big Creek Gulf to Ranger Falls to Big Creek Rim and then back to Stone Door. Strava recorded it as a bit over 11 miles.

<iframe height="405" width="590" src="https://www.strava.com/activities/2529383972/embed/5cc5993414b8df342d821684ff02b38b0f102f7c"></iframe>

We arrived at the trailhead and had boots on trail by 7:30am. The hike through the stone door and down to the Big Creek Gulf trail was super muggy, wet and sweaty. There's a couple rock fields during the descent but they weren't wet enough to cause any problems. Meredith will tell you but I descend and negotiate rocks like an old man with sore knees but I still feel we made good time.

<figure>
  {% myImage "48277412007_c80f42838a_c.jpg", "View from the bottom of Stone Door. Man made steps raising between two very large rocks in the cliff face." %}
  <figcaption>View of stone door from the bottom.</figcaption>
</figure>

<figure>
  {% myImage "48279690702_f5409d95a5_c.jpg", "Meredith drinking water from her camel back with Mike bending over in the background tying his shoe with steam coming off his back because it's so humid." %}
  <figcaption>Steam though...</figcaption>
</figure>

Once at the bottom, we hightailed it over to Ranger falls and stopped for refueling and for a Meredith-play-in-the-water session. The water level for Ranger Falls was lower than it was the first time we came through this year but still plenty of flow and the water was good and cold.

<figure>
  {% myImage "48279848357_b289d8281e_c.jpg", "A low volume waterfall empting into a rocky pool." %}
  <figcaption>Ranger Falls</figcaption>
</figure>

<figure>
  {% myImage "48285978556_e7e6e04c7b_c.jpg", "Photo collage of Meredith playing in the waterfall." %}
  <figcaption>My normal view of water falls...</figcaption>
</figure>

<figure>
  {% myImage "48279813131_65663c2868_c.jpg", "Meredith posing with the waterfall in the background and a blue bandana on her head." %}
  <figcaption>Spooky Raccoon turned Rambo.</figcaption>
</figure>

Once we cooled off, we headed back to Big Gulf Creek trail and headed down for another mile to take our lunch and another cool off session at a nice pool and area of semi-swift water. I'm not sure if it actually has a name or even what the name of the river is there but it's the second favorite part of the hike for me. Again, Meredith played in the water and I took off my boots to cool and rest my feet. In retrospect, I think we hung out here too long as I was feeling like I could take a nap at one point.

<figure>
  {% myImage "48279493511_33f7fddb07_z.jpg", "A rapid moving stream with some white water showing with rocky banks and trees in the background." %}
  <figcaption>Lunch spot!</figcaption>
</figure>

After getting back on the trail, it wasn't too long until it's time to make the ascent up to Alum gap campground. This is the section I had been preparing and waiting for and I was determined to make a good ascent. This ascent has some grades at 30% according to Strava and it's about .5 miles to the top. On the ascent, I felt great and was ascending at a good pace that I could have kept up for a good long time.

<figure>
  {% myImage "48277680671_d6cb6edcff_z.jpg", "A view of Mike hiking up a steep section of the trail." %}
  <figcaption>And now we go up!</figcaption>
</figure>

<figure>
  {% myImage "48277680661_08645fd791_z.jpg", "Looking back down the trail." %}
  <figcaption>That's where we came from!</figcaption>
</figure>

Once back at the top, we pushed on to Big Creek Rim trail and had scenic but otherwise uneventful hike back to Stone Door Ranger Station.

<figure>
  {% myImage "48277446332_23d6015505_z.jpg", "Meredith taking a smiling selfie with Mike in the background take a break on a rock and smiling at the camera." %}
  <figcaption>Taking a fiver!</figcaption>
</figure>

<figure>
  {% myImage "48277433242_d3f987835c_z.jpg", "A view of the valley below from the top of the bluffs." %}
  <figcaption>Nice views on Big Creek Rim Trail.</figcaption>
</figure>

<figure>
  {% myImage "48277433232_e6e06fd391_z.jpg", "A view of the trail." %}
  <figcaption>Heading to the rim!</figcaption>
</figure>

<figure>
  {% myImage "48279594447_e66a94deb5_z.jpg", "Selfie of Mike looking into the camera wearing a blue running hat." %}
  <figcaption>Proof of life! I made it!</figcaption>
</figure>

Last week I tried a new hiking clothes configuration. On the right, I'm wearing hiking pants and a hiking/travel shirt. The idea in making the change was that I'd be more protected from brambles and ticks and such. On the left is the current setup and one I wore Saturday on our hike. I think the clothing situation was the single biggest thing contributing to my problems on the trail while hiking Fiery Gizzard. I still need to change the shorts again as these did cause some chaffing that I didn't notice until I was home. I'm going to try a pair of running shorts with a tights/compression type liner.

<figure>
  {% myImage "48286080971_9d8d86eedf_z.jpg", "Photo collage of Mike from two previous hikes. On the left Mike is wearing shorts a running cap and a sleevless shirt. On the right wearing a buttdown sun shirt, tilly hat and pants." %}
  <figcaption>Clothing changes!</figcaption>
</figure>
