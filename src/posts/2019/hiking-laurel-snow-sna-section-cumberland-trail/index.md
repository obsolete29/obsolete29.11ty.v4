---
title: Hiking the Laurel-Snow SNA Section of the Cumberland Trail
desc: 'On July 28th, 2019, Meredith and I hiked the Laurel-Snow SNA sectin of the Cumberland Trail. This is my trip report.'
date: '2019-07-28'
lastmod: '2019-07-28'
tags:
  - hiking
  - trip-report
  - cumberland-trail
  - laurel-snow
---
> **Edit 07/28/2019 8:49pm:** Meredith wanted me to edit this and let everyone know chiggers are bad here 😆

**Distance:** 12.97 miles  
**Elevation:** 2,177 feet  
**Difficulty:** Strenuous  
**Strava Track:** [https://www.strava.com/activities/2568385320](https://www.strava.com/activities/2568385320)  
**Full Route Name:** Cumberland Trail Laurel-Snow SNA Section: Laurel Falls, Dunn Overlook, Snow Falls, Buzzard Point.

This section of the Cumberland Trail is really one of my favorite hikes and certainly ranks up there with [Stone Door](/posts/hiking-stone-door-trail-big-creek-gulf-trail-ranger-falls-big-creek-rim/) for me so I was pleased when Meredith suggested we do this hike again and we decided to hike every bit of the trail for this section.

The trailhead for this hike is located in Dayton, TN. Both Waze and Google Maps will take you straight there without any issues if you search for Laurel Snow State Natural Area. It's about 150 miles from Nashville so we tried to get on the road by 5:30am. We managed to get boots on trail by 8am.

The first mile or so of the hike is lovely as it parallels Richland Creek which makes for a pleasant experience. The water flows fast and heavy enough that there's a nice river sound and the temp is pleasant as you're hiking along the trail.

<figure>
  {% myImage "48395360806_55b0f78376_z.jpg", "Photo of Richland Creek from the trail." %}
  <figcaption>Richland Creek</figcaption>
</figure>

The trail turns away from Richland Creek, climbs a little and then crosses Laurel Creek as you're making your way through the holler over to Laurel Falls. As we turned away from the river, the humidity started to come on so we were glistening by the time we crossed Laurel Creek to negotiate this weird little natural feature that I'm negotiating below.

<figure>
  {% myImage "48395414846_6bbee0815d_z.jpg", "Photo of Mike from behind, climbing through a narrow opening between rocks." %}
  <figcaption>Negotiating this feature thing!</figcaption>
</figure>

There's some elevation after crossing Laurel Creek and I always seem to forget about this part but we made decent time and was pleasantly surprised by nice water flow at the falls. I really expected the water level to be low since it's the end of July.

<figure>
  {% myImage "48395745407_3d5bf779f2_z.jpg", "The view of Laurel Falls from the bottom of the falls." %}
  <figcaption>Laurel Falls</figcaption>
</figure>

Meredith played in the water while I had a snack and relaxed with the view of the waterfall. After our break at the falls, we headed back down, through the holler and to Laurel Creek bridge. We took the other fork to make our way towards Dunn Overlook, Snow Falls and Buzzard Point. Before taking the other fork, we both struck a pose with a little waterfall; Meredith nailed it.

<figure>
  {% myImage "48395939092_f50e9732b0.jpg", "A two photo collage of Mike and Meredith from the same spot. Meredith is striking a pose." %}
  <figcaption>Striking a pose!</figcaption>
</figure>

After the fork you make your way by the camp site area and come to a big ole metal bridge to cross over what has now turned into Henderson Creek before beginning your assault on the mountain. And by assault, I mean walking slowly up the side of a mountain, in the humidity while trying not to have a heart attack or heat stroke. Isn't this fun? I like to say assault because it makes me feel more badass.

{% myImage "48395946872_8aca2b5454_z.jpg", "A metal bridge spanning Richland Creek." %}
{% myImage "48394783627_3e4097bf56_z.jpg", "A panoramic view of the metal bridge spanning Richland Creek from the other side." %}

After the Henderson Creek bridge, you start the climb. Strava has a segment labeled as Rogers Road Climb for this part of the trail. As we started our ascent up the mountain, I was feeling pretty good and wanted to see if we could improve on our time from when we did this on May 31. We made great time and only paused once for a quick fiver. We did this climb on May 31st in 1 hour 29 minutes. On this trip, we did it in 46 minutes. We're definitely not going to break any land speed records but we got a lot better and faster and that feels nice.

<figure>
  {% myImage "48396030182_bf1d893a92_z.jpg", "A view of the trail from the top of Rogers Road Climb. Trail goes between two large cliff faces." %}
  <figcaption>At the top of Rogers Road Climb.</figcaption>
</figure> 

At the top of the climb we took the right fork to head to Dunn Overlook. This section isn't hiked very much it seems and it's definitely spider web season. Normally, I'm ok just plowing through the webs but I get a little more jumpy when there are spiders in them \*shivers\*! This section of the trail, all the way to Snow Falls, is more overgrown than the rest of the trail and there's a fair amount of brambles, stickers and briars so that's not super fun. From Dunn Overlook you can see the top of Laurel Falls and Buzzard Point. There are some power lines here but I thought it was still pretty cool.

<figure>
  {% myImage "48394642796_a3d0e46782_z.jpg", "Panoramic view from Dunn Overlook." %}
  <figcaption>Dunn Overlook.</figcaption>
</figure>

After a quick break, we ambled our way in the general direction of Snow Falls, fighting spider webs the entire way. On the way to Snow Falls, you have to cross over Morgan Creek. The last time we did this, the water level was low enough that we could hop along on the rocks without getting wet but today the water was too high for that so we got our feet wet! When this happens, I like to tell everyone we had to _ford a river_! Again, this makes me feel more badass.

<figure>
  {% myImage "48395998266_5926fcc5fc_z.jpg", "The trail crosses through a creek with step stones to use to get across." %}
  <figcaption>We had to ford a river!</figcaption>
</figure>

We took lunch at Snow Falls because it was so much more pleasant than the last time we were here. There was lots of flowing water and the levels were pretty high. The only minor bummer about this falls is that the trail takes you to the top of the falls and this lazy, tired hiker couldn't find a way to the bottom.

<figure>
  {% myImage "48394833412_c49f6ce241_z.jpg", "The view from the top of Snow Falls which is a small falls. Less than 12 feet." %}
  {% myImage "48394812632_2d27e9e93d_z.jpg", "Another view from the top of Snow Falls  which is a small falls. Less than 12 feet." %}
  <figcaption>The top of Snow Falls.</figcaption>
</figure>

We ate food and Meredith ran around to play in the water like usual. I filtered some water for the first time and it worked great! This was such a pleasant break and it was hard to get back up and head out but by this time we were starting to talk a lot about eating Mexican food and Ubereats doesn't deliver to Snow Falls in Dayton, TN last time I checked.

<figure>
  {% myImage "48395998341_e2a2bc5904_z.jpg", "Photo from the smooth rocky middle of the wooded creek." %}
  <figcaption>This is where we took our lunch break.</figcaption>
</figure>

After leaving Snow Falls and crossing back over Morgan Creek, we backtracked to the access road that goes to Buzzard Point and took that all the way to the point. This was a nice section and is flat and we made good time over to the point.

I did want to take a moment to point something out. Both on AllTrails and on Strava (I presume they use the same underlaying map data), they show there being a trail going from Snow Falls to the access road but that trail just doesn't exist. In the screenshot below, the left screenshot is the real actual map from the CT website. The screenshot on the right is the incorrect data from AllTrails, Strava etc. The arrow points to the non-existent trail. Please plan accordingly.

<figure>
  {% myImage "48396314231_dfc16c6017_z.jpg", "A photo of a map indicating where the map is incorrect. The highlighted section doesn't exist." %}
  <figcaption>This section of the trail doesn't exist.</figcaption>
</figure>

We hustled to Buzzard Point and talked with a couple who had hiked up to the top. After snapping a few photos, we started back down the mountain with burritos on our minds.

<figure>
  {% myImage "48394750611_d31471c1a1_z.jpg", "A photo of Mike from behind, hiking over a bridge and arriving at buzzard point." %}
  {% myImage "48394890152_f485539b9b_z.jpg", "A photo of the valley below from the top of Buzzard Point." %}
  <figcaption>Buzzard Point</figcaption>
</figure>

This next part of our hike is the worst part of every hike because now we're just walking back to the car so we can get food. We made pretty good time and no doubt, the idea of eating food was driving us. We found a local Mexican place in Dayton and both ate burritos as big as our heads. It was heavenly.

This was a super great hike and I do recommend this to others. It's a long hike but also pretty rewarding. I do think it's strenuous because of the climb and because of the length. Anywho, below are some more photos.

{% myImage "48394581086_2309f8cdd0_z.jpg", "A selfie with smiling Mikd and Meredith, looking fresh before starting our hike." %}
{% myImage "48395438946_da208b16c7_z.jpg", "Mike hiking up a steep section of the trail." %}
{% myImage "48394710536_392dfac30b_z.jpg", "Mike crossing over the creek." %}
{% myImage "48394712252_8575e65917_z.jpg", "Mike sitting during our lunch break, shoes off and looking deep in throught." %}
{% myImage "48394539631_cc8b80f7f3_z.jpg", "Selfie of Meredith relaxing at the top of Snow Falls." %}
{% myImage "48395890456_cc23bb40b9_z.jpg", "Two backpacks hanging from a tree." %}