---
title: 'Hiking Greeter Falls, Big Creek Rim, Big Creek Gulf, Ranger Falls'
desc: 'Today we hiked out in one of our favorite hiking areas, the Savage Gulf State Natural Area. Here''s my trip report.'
date: '2019-08-11'
lastmod: '2019-08-11'
tags:
  - hiking
  - trip-report
  - stone-door
  - savage-gulf
---
Today we hiked out in one of our favorite hiking areas, the [Savage Gulf State Natural Area](https://www.tn.gov/environment/program-areas/na-natural-areas/natural-areas-east-region/east-region-/na-na-savage-gulf.html). Because we hike out in this area a lot, we decided to hike a little different route than normal. We started in the Greeter Falls parking area and hiked the Greeter Falls Trail --> Alum campground --> Big Creek Rim Trail --> Stone Door --> Big Creek Gulf trail --> Ranger Falls and then back to Greeter Falls trailhead.

- Distance: 12.30 miles
- Elevation: 2,465ft
- Difficulty: Strenuous
- Rating: 4 (Really liked it)
- [Strava Track](https://www.strava.com/activities/2608005988)
- [Alltrails Track](https://www.alltrails.com/explore/recording/recording-aug-10-3-55-pm--2)
- [Photos](https://flic.kr/s/aHsmG39Hiu)

## The Hike

Rain was falling as we put boots on trail at 7:30am central time. The Greeter Falls trail is a rolling, 1.5 mile trail from Greeter Falls parking lot to Alum campground. There are a couple nice overlooks right on the trail so it's pretty easy to check them out.

{% myImage "48514204941_572348e524_c.jpg", "Photo collage of Mike and Meredith selfies." %}

{% myImage "48514403957_c108c230b9_c.jpg", "View of the foggy valley from the bluffs." %}

After Alum campground, Big Creek Rim trail is ~3.5 miles of gently rolling trail. There are 3 (?) overlooks with the best ones coming first after leaving Alum campground. This section of the trail is probably the most boring section so the decision to hike our hike in the direction and order we did it was to get this section of the trail out of the way at the beginning of the hike so we'd have interesting stuff to look forward to at the end of our hike. We made good time from Alum campground to the bluffs at the top of Stone Door.

<figure>
  {% myImage "48507821591_e8c1ddaf7f_c.jpg", "Meredith sitting on the edge of a bluff." %}
  <figcaption>Spooky raccoon doing spooky raccoon stuff.</figcaption>
</figure>

<figure>
  {% myImage "48514524302_2de4e9f5d9_c.jpg", "A photo from the top of the bluffs of the valley floor below." %}
  <figcaption>View from the Stone Door bluffs.</figcaption>
</figure>

After a quick break at the bluffs, we started our descent along Big Creek Gulf trail down to the bottom of the mountain to the old river bed. We made good time down even though there are some rock fields and they were wet; nobody fell and only a few slips!

I was trying a new load out with my gear this trip. Normally, I hike with my 2 liter water bladder but this time, I decided to hike with only a 1 liter bottle and my Sawyer Squeeze water filter. The idea being, I'd just filter water once we hit some water sources. It was really hot and muggy and we were making haste to Ranger Falls so I could filter water because I ran out! As we're hiking along the old riverbed, we heard some flowing/falling water. Meredith volunteered to scamper over and fill up my new CNON water container so I could filter some water. Yay for Meredith aka, Spooky Raccoon!

Ranger Falls was amazing as usual. A difference on this trip than the last two we've done here is that we saw a lot more people. We've never saw any other people at Ranger Falls but this time we ended up seeing three.

<figure>
  {% myImage "48507136416_cb9800e737_c.jpg", "A water fall in the bright sunlight." %}
  <figcaption>Ranger Falls!</figcaption>
</figure>

Once you leave Ranger Falls and hike back out to Big Creek Gulf trail, there are some climbs that I always seem to forget about and I remembered thinking that this was more elevation than I remembered previously! Another mile down the trail takes you to another interesting place to take a break if you're so inclined and we're always inclined to relax near water when hiking.

<figure>
  {% myImage "48507284022_0c92a72949_c.jpg", "A small river with rocky shores." %}
  <figcaption>Big Creek break spot.</figcaption>
</figure>

After leaving the second spot, we hiked along next to the river with Meredith making happy noises behind me. As we hiked up and away from the river, the happy noises ceased and the groaning started from the both of us because it was getting hotter and more muggy. Eventually, the trail and river meet back up for just a few minutes before making a hard right and kicking up the side of the mountain for the final climb back up to to Alum campground.

<figure>
  {% myImage "48515144486_e671867f7c_c.jpg", "Hiking trail starting to ascend." %}
  <figcaption>Start of the climb up to Alum campground.</figcaption>
</figure>

The climb was difficult as we were doing it during the hottest part of the day - around 2:30pm. Additionally, we're making our ascent after hiking over 10 miles already. We paused frequently but ended up doing the climb in 22 minutes - only 3 minutes slower than our previous effort. I'll take it for a super hot day.

Once we climbed back up to Greeter Falls Trail and started our way back to the Greeter Falls parking lot, we were pretty exhausted. We had originally planned to go down and check out Greeter Falls, and maybe go for a dip but we were too tired and just wanted to finish and go eat some burritos. Anyhoo, below are some of my favorite photos. :)

<figure>
  {% myImage "48507934786_1a20155ee0_c.jpg", "Photo of Meredith smiling on top of a bluffs." %}
  <figcaption>Spooky Raccoon!!</figcaption>
</figure>

<figure>
  {% myImage "48514392742_6cbb9b58c9_c.jpg", "Photo of me at the top of the bluffs." %}
  <figcaption>Me at the bluffs.</figcaption>
</figure>

{% myImage "48508208787_2feef493ff_c.jpg", "Orange and black salamander." %}
{% myImage "48514909581_7b111eec99_c.jpg", "Mike hiking down the trail." %}
{% myImage "48507739262_4c4ca2b47b_c.jpg", "Delicate, teal colored fungus on a wet log." %}
{% myImage "48507733107_4ddfd5d8bd_c.jpg", "A big toad." %}


<figure>
  {% myImage "48515141986_24481c8b21_c.jpg", "Selfie of an unsmiling Meredith." %}
  <figcaption>Meredith is not pleased about the heat and the climbing 😆</figcaption>
</figure>
