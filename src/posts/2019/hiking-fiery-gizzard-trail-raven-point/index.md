---
title: Hiking Fiery Gizzard Trail to Raven Point
desc: On July 7th 2019, Meredith, Whitney and I hiked the Fiery Gizzard Trail to Raven Point. This is my trip report.
date: '2019-07-08'
lastmod: '2019-07-08'
tags:
  - hiking
  - trip-report
  - fiery-gizzard
---
<figure>
  {% myImage "48225940317_a5e3978900_z.jpg", "Selfie of Meredith, Whitney and Mike." %}
  <figcaption>The crew, pre-hike selfie!</figcaption>
</figure>

Many reviews on AllTrails mentions hiking the trail counter clockwise because of the rock fields and I'd have to agree 100% with this idea. I'm a big guy and the focus and balance required to hike on rocks for nearly 4 miles is pretty challenging. Doing this while you're fresh is best.rock fields and I'd have to agree 100% with this idea. I'm a big guy and the focus and balance required to hike on rocks for nearly 4 miles is pretty challenging. Doing this while you're fresh is best.

<figure>
  {% myImage "48225959606_7d0b4599a2_z.jpg", "Sycamore Falls empties into a pool of water in the foreground." %}
  <figcaption>Sycamore Falls.</figcaption>
</figure>

The first part of the hike was pretty lovely. The trail is scenic and goes along the river which makes for pleasant viewing and temps. The rock fields don't seem so bad at first and I was kind of wonder what the big deal was about all the reviews. It turns out that hiking on rocks for a couple hours wears me out. There's a certain amount of mental focus and balance and concentration required to do that type of hiking. It was bothering my knees and I have to pick my way carefully.

Since we were hiking along the river, I knew we had to do a big climb to get up to the top of the mountain so we could get to Raven Point. The climb up the mountain was pretty severe. Easiest the hardest and most severe climb for me this hiking season and we've hiked Stone Door down to Ranger Falls and back out and Virgin Falls.

<figure>
  {% myImage "48219519807_5d36c45598_z.jpg", "Photo of Mike on a bluff, wearing a dark shirt and pants." %}
  <figcaption>These are the new clothes I was wearing on the hike.</figcaption>
</figure>

I recently decided to try a different clothing setup - pictured above. I have been hiking in gym shorts and my sleeveless, merino wool shirt that I use for cycling. The shirt very light weight and probably the best piece of clothing I own. The reason I wanted to switch to hiking pants and a hiking/travel shirt was mostly for protection from brambles, ticks and the sun. Making this change during the middle of the summer hiking season turned out to be a huge mistake. After taking our lunch at Raven Point and heading back along the ridge, I was overheating and just couldn't cool off. It caused me to hike very slow, slowing down my hiking partners. I was feeling nauseous and even got sick a few times. The hike back took a lot longer than it ought have.

As we were getting close to the end of our hike, we started coming across swimming holes and we decided to get in the water to get cooled off and it helped tremendously. We should have done this at the very first opportunity. My take away from this is that anytime Meredith says get in the water, I'm just going to do it, no questions asked.

After our dip we started hiking the last mile or so back when the cherry on top happened. As Meredith will tell you, I roll my ankles and they do so for no good reason. Most of the time it's a little twist and awkwardly catching myself and we carry on like nothing happened. I guess I was so tired and was mid step when it happened this time that I took a tumble and started tumbling down the side of the trail embankment. It was happening in slow motion for everyone who was involved and witnessing it. Ugh.

I'd definitely rank this hike as difficult and strenuous; at least it was for me. I'm not sure how much of that was because I was experiencing some heat exhaustion. I intend to this hike again this season. I came away feeling embarrassed with my confidence shaken. I would recommend this hike to hikers as I think it's definitely some of the best hiking in TN, right there with Virgin Falls and the Stone Door trails. I'll be back Fiery Gizzard.

<iframe height="405" width="590" src="https://www.strava.com/activities/2510057396/embed/388b81cf6007ad2a91d0bb5346d2a91b45a210a8"></iframe>

<figure>
  {% myImage "48225946461_b53f3c4349_z.jpg", "A photo of a juvenile copperhead." %}
  <figcaption>Little copperhead!</figcaption>
</figure>

<figure>
  {% myImage "48226010566_8b7c5bf2be_z.jpg", "Post hike selfie of Mike, Whitney and Meredith. Everyone looks a lot less amused than before." %}
  <figcaption>Post hike selfie. Ha ha.</figcaption>
</figure>

<figure>
  {% myImage "48225989671_b9d4984391_z.jpg", "Panaramic view of the valley from Raven Point." %}
  <figcaption>Raven Point.</figcaption>
</figure>
