---
title: Cycling the C&O and GAP Trails 2015
desc: In 2015 I attemped to ride the C&O and GAP trails from Washington DC to Pittsburg PA. This is my trip report.
date: "2015-10-05"
lastmod: "2015-10-05"
tags:
  - cycling
  - cycle-touring
  - trip-report
  - co-gap-trails
---
In 2015 I attemped to ride the C&O and GAP trails from Washington DC to Pittsburg PA. This is my trip report.

- [Planning](#planning)
- [Anxious pre-trip jitters](#anxious-pre-trip-jitters)
- [Gear List](#gear-list)
  - [Camping](#camping)
  - [Kitchen](#kitchen)
  - [Clothes](#clothes)
  - [Electronics](#electronics)
  - [Toiletries](#toiletries)
  - [Misc](#misc)
- [Day 0 Arriving in DC](#day-0-arriving-in-dc)
- [Day 1 DC to Brunswick Family Camp Ground](#day-1-dc-to-brunswick-family-camp-ground)
- [Day 2 Brunswick to Fredricks State Park](#day-2-brunswick-to-fredricks-state-park)
- [Conclusion](#conclusion)

## Planning

Like [last year](/posts/cycling-the-natchez-trace/), I've decided to take a bike trip in October. This year, I've decided to ride from Washington DC to Pittsburgh PA via the [C&O Canal](http://en.wikipedia.org/wiki/Chesapeake_and_Ohio_Canal) and [Great Allegheny Passage bicycle trails](http://en.wikipedia.org/wiki/Great_Allegheny_Passage) (335 miles). I'll do this as a self supported ride, camping along the way.

The tentative plan is to use a rental car to transport myself, bike and gear from Nashville to DC on Friday, October 9th. The bicycle trip should take 6 days so that puts me in Pittsburgh on Thursday afternoon, October 15th. Jen will fly up and meet me on Thursday so we'll have a long weekend to explore Pittsburgh then drive back together on Sunday. The backup plan in case Jen can't get off from work is for me to just drive myself back with a rental car.

The two main differences that stand out to me compared to my Natchez trip is that the C&O Canal and GAP trails are non-motorized trails so I won't have to deal with cars once on the trail in DC. Second, the trails have few paved surfaces so I'll switch my tires out to 26 x 2.0 [Schwalbe Marathon Mondial tires](http://www.schwalbetires.com/bike_tires/road_tires/marathon_mondial). These have beefier tread and are wider than my normal tires and they'll serve as winter / "off road" tires.

I'm following the tenth edition TrailBook thru trip itinerary.

1. Saturday - 61 miles to Harper's Ferry
2. Sunday - 63 miles to Hancock, MD
3. Monday - 60 miles to Cumberland
4. Tuesday - 44 miles to Rockwood
5. Wednesday - 47 miles to Connellsville
6. Thursday - 60 miles to Pittsburgh, PA

The distances should hopefully give me a more leisurely pace than my Natchez trip. I do wonder how different the non-paved trails will be how that'll affect my distance and effort.

## Anxious pre-trip jitters

<figure>
  {% myImage "21636391835_f6640a3b7c_n.jpg", "Tank bicycle standing next to a sign post. Forest green bicycle with tan seat and handlebar tape." %}
  <figcaption>Tank Bicycle after her pre-trip tune up.</figcaption>
</figure>

I just realized that I'm leaving on my bike trip _next week_. I've done very little planning up until today. I went by REI and picked up most of the supplies I needed. Those supplies included some freeze dried food for breakfast and dinner but also a few shirts and a couple pair of socks, gu and trail mix. I just today had Tank Bicycle tuned up.

The original plan was to rent a car one way and just carry my bike myself. This morning I went about reserving my car only to realize that renting a car one way is much more expensive than I was expecting. One way from Nashville to DC was $375. Safe to assume that it would be comparable to rent a car one way from Pittsburgh to Nashville so you're looking at about $800 after gas. :|

So now I'm onto plan B. I'm taking Greyhound from Nashville to DC and having my bike shipped to a bike shop in DC. The ticket to DC was $65 and it's going to cost about $60 to ship the bike and another $100 to have it assembled by the bike shop there so that's $225 for one leg. If we assume a comparable amount for the return leg, we're looking at $450 round trip. The real question boils down to this: Is having to deal with a Greyhound bus trip worth $350? I'll report back my findings.

Either way, I still feel like this is going to be a good, learning experience for future trips because I can't see myself ever driving out the the west coast for instance. I'll need to know how to ship my bike, etc and now is a good as time to learn as any.

I spent some time today planning and scheduling everything so I'm feeling better than I was this morning when I was nearly having a panic attack about my lack of planning. Bus tickets purchased. Rooms reserved. Phone calls to the bike shops made. Gear post coming in the next couple days!

## Gear List

<figure>
  {% myImage "21584473898_fd2fe4af1a_z.jpg", "Photo of camping gear laid out on the floor." %}
  <figcaption>Gear!</figcaption>
</figure>

This is 95% the same as the my [gear list](/posts/cycling-the-natchez-trace/#pre-ride-gear-list-plan) for the Natchez Trace.

- [My Surly Disc Trucker](http://michaelharley.me/blog/my-bike/)
- Ortlieb Back-Roller Plus panniers
- Ortlieb Front-Roller Plus panniers
- Ortlieb Ultimate 6 M Plus handlebar bag with map case

### Camping

- Kelty TrailLogic TN2 Tent
- Big Agnes 30+ Sleeping bag
- Exped SynMat 7 M
- Packable camp pillow

### Kitchen

- JetBoil Sol w/ french press coffee attachment
- REI Spoon
- Platypus Platy (2 Liter Foldable Water Bladder)
- Lighter
- Matches
- MSR salt/pepper container
- REI "nalgene" bottle.

### Clothes

- Sea to summit garment bag M
- 2x MUSA Shorts
- 2x REI Hiking Shirts (synthetic, short sleeve)
- 1x Smartwool long sleeve shirt
- 2x cycling underwear
- cycling gloves (half finger and long finger)
- beanie
- camp clothes (underwear, shirt, shorts)
- flipflops (for camp)
- Five Ten Ascent shoes
- 2x Smartwool Phd socks
- Rain Poncho

### Electronics

- Mophie Powerstation XL
- Smaller Mophie Powerstation
- charging cables for headlight, tail light, iPhone, gopro, mophies, kindle)
- head light / tail light
- iPhone 6
- kindle
- headlamp
- gopro + mounts

### Toiletries

- dop kit
    - toothbrush
    - toothpaste
    - floss
    - deodorant
    - packable washcloth
    - soap
- toilet paper
- packable towel

### Misc

- tool kit (tubes, pump, patch kit, multitool, tire tool)
- guide book
- first aid kit (including ibu and tums)


## Day 0 Arriving in DC

Whew. I'm here and I'm pooped.

My adventure started in Nashville at the Greyhound bus station. The TL/DR version of the Greyhound leg of my journey is that the bus into Nashville was 2.5 hours late which caused me to miss a connection. I ended up getting into Washington DC at 5:30pm. That left plenty of time to walk over to [BicycleSPACE](http://www.bicyclespacedc.com/downtown) to pickup Tank Bicycle.

It turns out that my bike was damaged in shipping. It wasn't anything too serious, just bent forks. They normally charge $100 to reassemble a shipped bicycle. The cost to align the rear triangle and the front fork was $75 each but they gave me a break and only charged $75.

{% myImage "21871961336_9c5d1ff093_z.jpg", "Lincoln Memorial in the distance on a wet day." %}

The next phase was to get myself over to my motel room in one piece. I wish I had planned this part better because it was gettigg dark by the time I set off. I shipped my bicycle lights and my phone mount ahead to my room instead of carrying those parts with me so I could use them on the ride from the bike shop to the motel. My seat post was adjusted a little too high so that, combined with the fact that I was having to navigate by pulling my phone out of my short pockets plus not feeling comfortable because of my lack of good lighting, plus carrying a heavy backpack on my back, made for an anxious and somewhat uncomfortable ride.

{% myImage "21275364664_0a38235666_z.jpg", "Tank bicycle in the motel room with boxes of camping gear stacked nearby." %}

I arrived at the [Best Western](https://m.bestwestern.com/content/bestwesternmobile/en/HotelDetail.html?resortId=47133) and was relieved that the front desk guy promptly informed me that I had packages waiting for me. I hustled my bike and my boxes to the room and performed a quick inventory to make sure all the important pieces where there and they were. Jen can attest to this but I've been stressing about things I might have forgot. I was terrified that I would get here and something really important wasn't in my boxes... tent, sleeping pad, sleep bag anyone?

As I type this, I've had dinner at the restaurant next door, attached the lights and adjusted the seat on Tank Bicycle and I'm now sitting in bed, eating a chocolate brownie that I ordered to go - it's heavenly.

Before going to sleep, I'll review the guide book for tomorrow's section, review the weather for tomorrow and make sure I can find the trail marker at the very begining (it's hard to find I've read).

I'm excited to start pedaling.

## Day 1 DC to Brunswick Family Camp Ground

Today’s Distance – 58 miles Total Distance 58 miles

<iframe src="https://www.strava.com/activities/405695356/embed/fcef2c61b67b7ceb9a18b7bdaaa33fab2e0931ba" width="590" height="405" frameborder="0" scrolling="no"></iframe>

It feels so good to be back on a bike trip. :)

I read a lot about how hard it was to find the mile zero mile post for the start of the C&O path and they weren't kidding. I even studied the google map in details the night before. The thing that was throwing me off is that it felt like I was going through somebody's personal property. I wasn't going to start the trip without taking a photo with the stpuid mile post though so I stuck with it and finally found it.

{% myImage "21301197824_1788b107ab_z.jpg", "Fully loaded touring bicycle standing next to mile 0 mile marker for the C&O trail." %}

As you can see from the photo, conditions were pretty wet as it had been raining in the DC area for at least a couple days. More on that in  a minute.

The ride out of DC was pretty exciting. I was just so happy to be out on the road again. When I was started my Natchez Trace ride, it took about a half a day to get used to how the heavier bike felt. I never had that issue today as everything felt completely normal from the start. Yay muscle memory!

{% myImage "21897996286_01fb4a9fa4_z.jpg", "Loaded touring bicycle on the C&O trail running next to the tow path with water in it." %}

So the big thing about the C&O Canal Tow Path is that there are lots of locks and all the stuff that goes with said locks. There's lots of interesting things to see and for the most part, it didn't seem as monotonous to me as the Natchez Trace trip.

{% myImage "21911996222_edf9cb6a81_z.jpg", "Fast running river with heavy flow." %}

I missed my lunch stop and I was in a somewhat remote section fo the trail. By the time I got to White Ferry, it was nearly 3pm. I was at the point that if Whites Ferry didn't have something hot to eat, I was about to bust out the JetBoil and a ChiliMac meal from the packs.

After lunch, I tried to pick up my pitiful pace so I could get to camp before night fall. I ended up making it but just barely! I ended up staying at a pay camp ground instead of one of the free hiker/biker camp grouns mostly because Brunswick advertised a camp shower. It wasn't as scary as the camp shower experience on my Natchez Trace ride but it was still a camp ground camp shower. The hot didn't seem to be working but I still rinsed the mud off and got cleaned up. It's going to make a huge different sleeping tonight!

Overall, the ride was pretty good. It was a brisk, wet fall day and while that's fun at first, it was getting old at the end of the day. I saw lots of wild life: squirrels, ducks, geese, a ground hog and so many deer, I stopped turning on the GoPro to film them.

The biggest thing that stuck out in my head is how much the wet and often muddy path affected my pace. I'm not a fast cyclist anyway but the mud really slowed me down. It's fun the first few times but after the 200th time of getting caught in a rut and having to do a panic correction, it gets irritating.

So anyhoo, I'm here and I'm setup in camp. Low in the mid 40's is forecast for tonight and it's wet feeling and occasionally it's windy. I'm expecting to sleep great, tucked away in my sleeping bag and tent! Off to read about tomorrows section!

Links

- [All Photos from today](https://www.flickr.com/photos/michael_harley/albums/72157659366504896)
- [Strava Track](https://www.strava.com/activities/405695356)

## Day 2 Brunswick to Fredricks State Park

Today's Distance: 64 miles Total Distance: 122 miles

<iframe src="https://www.strava.com/activities/406563642/embed/daffada66e45943473d134bbdbb80fd4e2e83b68" width="590" height="405" frameborder="0" scrolling="no"></iframe>

First, let me talk about last night. The camp ground I stayed at last night was _right next_ to a train track. When trains fly by at about 100 yards away from a person in a tent, it wakes you up no matter how tired you are. If it wasn't the trains going by, I was rolling over or having to adjust temperature every 30-60 minutes. I was able to get some sleep but it wasn't the most restful sleep in the world. I'm very tired tonight tho and I'm expecting to sleep a lot better.

{% myImage "21764683788_50120a72ac_z.jpg", "Opened Mountain House freeze dried breakfast skillet meal with a blue camp cup of coffee." %}

Breakfast of champions and yes, having french press coffee in the woods (or a camp ground near a train track if you prefer) was pretty damn legit. It tasted great and the french press attachment did pretty ok. There were a few grounds in the 2nd cup but I thought of it more like crunchy peanut butter. ;) My plan for this morning was to get on the road, logging actual trail miles sooner than I did day 1 and I was successful at that. I was on the trail at 8:04am. I was also going to be mindful of my pace, making sure I wasn't putzing around at normal Mike Harley effort. 

{% myImage "21940453832_fb571b26d1_z.jpg", "River going over a small dam of about 8 feet." %}

This morning more of the same from yesterday. Wet roads that were equal parts bumpy and muddy and the occasional cool thing to see.

Lunch plans was Captain Benders in Sharpsburg. The app said the place was an "easy ride from the tow path". Well, I'll have you know the person who wrote those words never rode a fully loaded touring bicycle up a damn hill. There were a couple steep climbs that I had to gear all the way down in the climbing gear.

I arrived a bit early, parked my bike and waited for them to open at noon. They let me in a little early but they couldn't serve me. Whatever, thanks for letting me in out of the cold! The burger I had was decent; nothing to write home about. Also, I'm noticing the burgers up here are served with chips and fries are usually extra.

<figure>
  {% myImage "21926382636_a741ceab94_z.jpg", "Tank bicycle on her kickstand with the confederate flag flying from the building loud and proud." %}
  <figcaption>Yikes! Had they been flying this when I rode up, I most certainly would not have stopped.</figcaption>
</figure>

When I came out, I noticed that once the place opened, they put our their sign and flag pictured above. It honestly kind of blew my mind. I'd expect to see such things in the south by why here, near so many Civil War battle fields? These were Union states; it makes no sense. I asked the waitress what the deal with the Confederate flag was and her response was "Well, this is where it all happened and we're just trying to protect our freedoms" - What lady? Come again? That makes no sense. Anyway, I didn't want to push it and get any special sauce in my food or drink.

The good thing about having to climb up to get lunch was being able to coast down back to the trail!

I always dread the after lunch part of my days. It usually really sucks and today was no different. I don't know what the deal is exactly... maybe it's the fact that I just stuffed my face with meat and beer or maybe it's a mental thing where I know my count down times have all reset or a combination of both. I call it riding on the pain train because you're stuck alone in your own head with nothing but the discomfort (ouch, my hands hurt. ouch, my shoulders hurt. ouch, my legs hurt. 35 MORE MILES!?!?! /dies)

{% myImage "21764470840_2c9813b8f3_z.jpg", "Two cyclists with bright yellow cycling jerseys and helmets." %}

I pulled over to send complainy texts to Jen and Jessie, about how much bicycling sucks and I think I want to be a snowboarder instead when Rich and Sissy from South Carolina pulled up along side me riding a dang tandem! It was awesome! I'm trying to talk Jen into going on tours with me if I get us a tandem so this was perfect! We talked shop and compared notes and asked about gear and all that; it was great.

And this is where my afternoon really went downhill as I pulled off first and since I'm a dumb guy, I felt I needed to stay ahead of them. Well, if you know anything about bicycles, you also probably know tandems are faster than normal bikes because you have two dang people pedaling! So after about 20 minutes of me putting out maxium effort to stay head of them, my body blew a fuse and I bonked or something. My body let off the gas on it's own accord and they passed me and something friendly but I don't remember what they said. I was feeling jittery and shaky and cold all at the same time. Once they were out of sight, I stopped and took some water but I just couldn't shake the weird feeling. I took a couple Gu shots and ate a couple handfuls of trailmix but it was 45 minutes or so before I was over the spell.

I limped along for another hour or so hating bicycling, bicycle touring and the C&O Tow Path. As the was getting lower in the sky it occured to me that it might be after dark before I get to my planned campa site. I used my phone to see if there was a motel nearby with every intention of getting a room but the only one was 4 miles behind me and Mike Harley doesn't go backwards so that was out. The only option was pressing foreward to my original camp ground. And just like that, I had a second wind. I put some techno (dub? do kids still call it dub? maybe edm) on the iPhone and was able to maintain a faster pace for 1.5 hours of the evening than I'd been able to maintain on this trip. Hopefully that was my body finally getting the message that we're on a dang bicycle tour here; stop slacking.

As I type this, I have camp setup, I've ate my ChiliMac and I'm just about done writing this blog post. Next, chit chat with the Jenmeister then scout out tomorrows lunch and camp locations plus check the weather. Good night.

- [Flickr dump](https://www.flickr.com/photos/michael_harley/albums/72157657154417523)
- [Strava Track](https://www.strava.com/activities/406563642)

## Conclusion

Most of my friends and family know that I had to abandon my trip because of a quad strain. Having to give up on my ride really rocked my self image. That probably sounds dumb to anybody that's not me but for the past several years a core part of my identity has been as a cycle tourist. I imagine it's similar to a hipster who's beard has fallen out.

I didn't train for this ride. I knew that my milage on the bike was way down since we moved a lot closer to the office but I thought that once I got out there on the trail that I could just ride myself into shape like I was some sort of TdF pro.

The trail was harder than I was expecting _and_ I set my daily milage goals too high. I knew the trail was an unpaved trail but I thought it would be more compact than it actually was. It turned out to be muddy during the time I was trying to make my trip so the going was harder than I was expecting.

So if you combine the fact that I didn't train with a harder than expected trail, you end up with a quad injury because I was trying to push too hard to meet my daily milage goals.

I'm undecided if I'm going to try the trail again. I hate the idea that I let this trail beat me so we'll see.