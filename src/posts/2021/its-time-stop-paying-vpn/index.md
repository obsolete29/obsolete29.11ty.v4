---
title: It’s Time to Stop Paying for a VPN
desc: Brian's threat model is not everyone's threat model so be sure to keep that in mind as you read this silly article.
date: '2021-10-06T05:15:36.399-05:00'
tags:
  - privacy
  - vpn
---
Ohhh boy saw this on my morning feeds. 

> A caveat: VPNs are still great for some applications, such as in  authoritarian countries where citizens use the technology to make it  look as if they are using the internet in other locations. That helps  them access web content they cannot normally see. But as a mainstream  privacy tool, it’s no longer an ideal solution.
>
> -- <cite>[NYTimes.com](https://www.nytimes.com/2021/10/06/technology/personaltech/are-vpns-worth-it.html)</cite>

Please don't listen to Brian. His threat model seems to be privacy and security as it relates to open Wi-Fi hotspots at a coffee shop or airport and your account being hacked because someone is capturing network traffic. My threat model is to mask my IP address from the sites I'm interacting with online for the purpose of resisting online surveillance capitalism and data aggregation. 

Simple alternatives like keeping your software up-to-date, setting up online accounts with two-step verification, and using your personal hotspot do nothing to prevent data aggregators from tracking you across the web based on your IP address.

Brian does get one thing right. Be very careful about who your VPN provider is. I use [Mullvad VPN](https://mullvad.net/en/) and believe them to be reputable. Their [software is open source](https://mullvad.net/en/help/open-source/) and they have a [no logs policy](https://mullvad.net/en/help/tag/policies/). It's surprisingly fast for me and if something seems slow, I just reconnect and it picks a different server and away we go, all speedy like again.

I think I'd trust [ProtonVPN](https://protonvpn.com/) as well, even though they were [recently in the news](https://arstechnica.com/information-technology/2021/09/privacy-focused-protonmail-provided-a-users-ip-address-to-authorities/) because they had to comply with a legal order from the authorities.

What's your threat model? Does a VPN help you guard against it? Which VPN provider do you use?

--> Source: [It’s Time to Stop Paying for a VPN - The New York Times](https://www.nytimes.com/2021/10/06/technology/personaltech/are-vpns-worth-it.html)
