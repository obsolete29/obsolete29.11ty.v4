---
title: Introducing my new Weber Performer 22 inch charcoal grill
desc: We recently moved from an apartment complex into a house with a decent deck so we bought a new grill about it!
date: '2021-08-01T16:23:48.273-05:00'
lastmod: '2021-08-01T16:23:48.274-05:00'
tags:
  - grilling
  - gear
  - weber
---
We recently moved from an apartment complex into a house with a decent deck. At the apartment complex, we were not allowed to even *own* a grill. When we moved, I knew that purchasing a grill was going to be one of the first things I did.

## My grill

<figure>
  {% myImage "weber-01.jpeg", "Charcoal grill with the charcoal smoking and getting ready. Some grille corn is ready to be placed onto the grill." %}
  <figcaption>First time firing her up! Those are potato pouches on the side.</figcaption>
</figure>

Say hello to my Weber Performer grill! It's a 22" charcoal grill. I purposefully avoided getting the model that had the self starter. In my mind, it was just something else that would break a few years down the road -- I'll light my charcoal with my charcoal chimney tyvm.

In the past, I've only ever had the basic Weber kettle grill and I have to say, having the side table and the charcoal bin makes all the difference. #VeryNice

## Gas vs charcoal

I love charcoal. I love lighting it with the charcoal chimney. I love the smell of it as it's getting ready. I love sitting with my beer while it gets up to the proper heat.  I love the smokey flavor of the food. 

I've only ever cooked on a charcoal grill but when I've eaten food from a gas grill, it's fine. It's quicker to get ready I suppose but it doesn't have the flavor. I'm sorry to say but Hank Hill is wrong.

## Charcoal

[Wirecutter recommends](https://www.nytimes.com/wirecutter/reviews/best-charcoal-for-grilling/) Royal Oak and that's what I get if it's at my local Kroger. Otherwise I go with Kingsford classic. If we're going to Wal-mart for some reason, I'll get the Royal Oak though!

I plan to share some of my favorite grilling recipes -- what are some of yours? So far this season we've grilled burgers, hot dogs, chicken breast, chicken wings, bacon wrapped shrimp, and tons of vegetables. We've slow cooked some pork ribs and we even grilled some pizza and a peach pie.

Do you prefer gas or charcoal grills?

Ok, that's it for now -- thanks for eading!
