---
title: My 2021 in review
desc: This was not a great year for me but here are some recollections and photos from the year!
date: '2021-12-28T07:52:51.396-06:00'
tags:
  - year-in-review
slug: my-2021-in-review
---
Well, 2021 is in the books and as I reflect on the year as a whole, I'm going to give it a grade of C-. 

TL;DR: Rachelle and I rented a house together. I left USDA for Asurion. Didn't like it, went back to USDA and we took a family trip to Gulf Shores, Alabama and two long weekend trips to Memphis and Washington DC. 

## New house

Rachelle and I moved in together at the end of 2020 and in the spring of 2021, we rented a house together. Moving out of the townhouse I rented was nice. We have a nice back deck. We have a garage and lots of room. The house is a three bedroom with a big ole bonus room so we each of an office as we both work from home full time. This has been of the bright spots of 2021 for me.

## Work

In the summer, I left my job at USDA as a SharePoint Developer to take a consulting position at Asurion. The project sounded interesting and challenging. This was my first contractor role and it turns out that line of work just isn't for me. The technical work was fine but I missed having a team and working on processes, improving on them and the overall teamwork involved when working as a team vs a lone developer. 

Once I decided that I was ready to make a change, I reached out to my old manager to give her first dibs on coming back to USDA before searching for new opportunities. I enjoyed my time at USDA and working for my old manager and working with the team. Turns out she liked me too and she was able to bring me back on. So now, I'm back on at USDA as a SharePoint Developer, using Power Platform and O365 to solve business problems for the USDA. I'm pretty happy with it!

## Trips

Rachelle and I took a family trip with my family (daughter, mom and grandson) down to Gulf Shores Alabama to hang out at the beach. We rented a mini-van and took the 6 hour road trip down south. It was a lot of fun!

Rachelle and I only took a couple trips this year. First, we went took a long weekend [trip to Memphis](/posts/2021/08/04/our-quick-weekend-trip-to-memphis/) which was a lot of fun for both of us as neither of us visited before. Next, we took a quick trip to Washington DC just before going back to USDA. I didn't write about this one but we ended up visiting with Rachelle friends. We did manage to spend some time in DC visiting the [Holocaust Museum](https://www.ushmm.org/) and we got to spend a few hours in the [National Museum of African American History & Culture](https://nmaahc.si.edu/) but that thing is huge and I'd really like to go again and plan to spend the day.

## Expectations

When I started 2021, I had some loose plans to go on at least one, possibly two bicycle trips and to do lots of hiking in 2021. I started out in 2021 logging some miles on the bike and on the trail but somehow I lost my way. Between moving and changing jobs that stuff just kind of fell to the wayside. My mental health hasn't been great this year either. I chalk it up to the never ending pandemic, feeling like I'm not allowed to live life and the stress of moving and job switching. I'm a stress eater so I really gained some weight as a result. As I type these words, I'm the heaviest I've ever been in my life.

## Resolutions

I really hate resolutions. For me, they're just something to feel bad about when I don't stick with them. But what are some things I want to strive for and be better at in 2022? Glad you asked!
- Hiking - I love hiking so I want to hike myself into better shape. This is a lot more specific than I normally like, but I'd love to build up and hike [Mt LeConte](https://en.wikipedia.org/wiki/Mount_Le_Conte_(Tennessee)) in the Smokies.
- Cycling - I also love riding my bicycle. We live in an area now that's not bike friendly but I really want to get back on the bike. Ultimately, I'd like to plan a [bike trip](https://obsolete29.com/topics/cycling/) this year coming up. Perhaps two!

So anyhoo, here are some photo impressions of my year.

## Photos

<figure>
  {% myImage "covidlife.jpg", "Mike and Rachelle sitting on the couch. Oliver the fat cat lays on Mike." %}
  {% myImage "covid01.jpg", "I got my covid-19 vaccine sticker." %}
  <figcaption>If there is one photo that represents 2021 for me, it's the top one here. COVID life.</figcaption>
</figure>

<figure>
  {% myImage "beach01.jpg", "Mike Rachelle Jessica Linda and Carter in the van getting ready to hit the road to the beach." %}
  {% myImage "beach03.jpg", "10 year old Carter wearing blue swim tunks entering the evening ocean surf with surprised smile on his face." %}
  {% myImage "beach04.jpg", "10 year old Carter and Papa Mike flying a kite on the beach with hotels in the background." %}
  {% myImage "beach05.jpg", "Mike and Rachelle selfie on the beach with the evening sun in the background." %}
  {% myImage "beach06.jpg", "Mom Jessica and 10 year old Carter standing in the late evening surf facing the ocean." %}
  {% myImage "beach07.jpg", "10 year old Carter standing on the beach with a boogie board wearing a hat posing for the camera with the ocean in the background." %}
  {% myImage "beach08.jpg", "Morning photo of an empty beach from the condo balcony. Low heavy clouds are in the middle distance." %}
  {% myImage "beach09.jpg", "Mike and Rachelle selfie. Rachelle wears a lepard print top visor and sun glasses. Mike has the outline of his torso tattoo and wears sun glasses and a gilly hat with the bill flipped up." %}
  {% myImage "beach10.jpg", "Mom Linda sitting with Mike on the beach under a beach umbrella with coolers and other beach equipment nearby." %}
  {% myImage "beach11.jpg", "10 year old Carter in the ocean wearing a life vest and waving from the water towards the camera." %}
  {% myImage "beach12.jpg", "Photo of Mike and Rachelle on the evening beach. Mike wears a black cut off T and green shorts. Rachelle is holding a drink and wears a blue long dress." %}
  <figcaption>Family beach trip to Gulf Shores</figcaption>
</figure>

<figure>
  {% myImage "moving01.jpg", "Empty moving boxes in the living room in preparation for our move to the new house." %}
  {% myImage "moving02.jpg", "A partially loaded moving truck with a ramp on moving day." %}
  {% myImage "moving03.jpg", "The empty living room of the new house. Test green paint patches are painted on two walls. A coffee table with some painting equipment and beer cans are in the foreground." %}
  {% myImage "moving04.jpg", "Rachelles finished gallery wall. An eclectic mix of photos on a dark green wall and a plant self." %}
  <figcaption>Rachelle and I moved into a house!</figcaption>
</figure>

<figure>
  {% myImage "memphis01.jpg", "A selfie of Mike and Rachelle in Memphis." %}
  {% myImage "memphis02.jpg", "A mural of a hand holding a bottle of Budweiser in the early afternoon sun." %}
  {% myImage "memphis03.jpg", "A busy Beale Street in Memphis. Many pedestrian mingle on this warm night." %}
  <figcaption>Rachelle and I went to Memphis!</figcaption>
</figure>
<figure>
  {% myImage "grilling01.jpg", "Brand new Weber grill setup on the back porch." %}
  <figcaption>New grill. We used it a lot this year.</figcaption>
</figure>

<figure>
  {% myImage "dctrip01.jpg", "The Washington Monument on a sunny day in DC." %}
  {% myImage "dctrip02.jpg", "Rachelle viewing the walls of photos of Jewish people who were killed in the Holocaust." %}
  <figcaption>We took a short weekend trip to DC.</figcaption>
</figure>

<figure>
  {% myImage "bike01.jpg", "Bicycle handlebars are in the foreground with a park in the background." %}
  {% myImage "hiking01.jpg", "Bearded Mike wearing a green Safe House tattoo hat takes a photo in front of Abrams Falls." %}
  {% myImage "hiking02.jpg", "Foggy view of stairs going up between a cut in the rocks on a hike to Black Mountain." %}
  {% myImage "hiking03.jpg", "A full Virgin Falls with a rainbow showing in the mist of the falls." %}
  <figcaption>See? I promise I did some physical activities. Hiking and biking.</figcaption>
</figure>

<figure>
  {% myImage "cats01.jpg", "Lily and Oliver the cats lounge on the back porch." %}
  {% myImage "cats02.jpg", "Lily and Oliver the cats sitting on the ottoman." %}
  {% myImage "cats03.jpg", "Lily Oliver and Sammie the cats in a rare photo of all three. Lily is on a bookshelf. Oliver is on a chair and Sammie is in the floor." %}
  {% myImage "cats04.jpg", "Oliver and Lily the cats sleep in the reading chair in Mikes office." %}
  {% myImage "lily01.jpg", "Lily the black cat sits on the back porch rail in the afternoon with a mad look on her face." %}
  {% myImage "lily02.jpg", "Lily the black cat lays on the back porch with her fat belly showing." %}
  {% myImage "lily03.jpg", "Lily the black cat captured in a portrait mode photo with the laundry basket in the background." %}
  {% myImage "oliver01.jpg", "Oliver the black and white kitty laying on his back in a classic Oliver pose." %}
  {% myImage "oliver02.jpg", "Oliver the black and white kitty laying on a yellow chair crunched up asleep with his head laying on the arm like a little person." %}
  {% myImage "oliver03.jpg", "Oliver the black and white kitty captured in a portrait mode photo looking directly at the camera." %}
  {% myImage "sammie01.jpg", "Sammie the gray kitty prowls along the back porch railing in the early evening." %}
  <figcaption>Look at these dang cats.</figcaption>
</figure>

<figure>
  {% myImage "desk01.jpg", "A wooden desk with neat cable management and RGB lights glowing." %}
  <figcaption>Finally finished my dang desk.</figcaption>
</figure>

<figure>
  {% myImage "foosball01.jpg", "Players play foosball with intense expressions on their faces." %}
  <figcaption>Played some dang foosball too.</figcaption>
</figure>

<figure>
  {% myImage "newtattoo01.jpg", "A fresh tattoo on Mikes leg. A red reose with a small skull in the center." %}
  {% myImage "newtattoo02.jpg", "A full black and gray shark tattoo on Mikes torso." %}
  <figcaption>New tattoos too!</figcaption>
</figure>

<figure>
  {% myImage "birthdaycake.jpg", "Chocolate cake with chocolate skulls." %}
  <figcaption>I had another dang birthday. Look at this cake Rachelle made.</figcaption>
</figure>

<figure>
  {% myImage "concert.jpg", "A band on stage in a dark venue." %}
  <figcaption>I saw Whitechapel in Knoxville. First concert in two years.</figcaption>
</figure>
