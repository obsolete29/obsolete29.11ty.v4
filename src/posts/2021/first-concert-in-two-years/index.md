---
title: I went to my first concert in two years!
desc: I saw Whitechapel play at The Mill & Mine. I had a great time.
date: '2021-12-19T09:07:01.651-06:00'
tags:
  - trip-report
  - knoxville
  - whitechapel
  - concerts
  - the-mill-and-mine
slug: first-concert-in-two-years
---
I drove to Knoxville to watch [Whitechapel](https://whitechapelband.com/) play at [The Mill & Mine](https://themillandmine.com/) on Friday. I had a great time being outside of my house, drinking Miller Lite and listening to music with like minded people.

## But what about COVID

I struggled with my decision to go and I'm still struggling with my feelings about having went. On one hand, I still see people shouting *EVERYBODY STAY HOME THERE'S A DEADLY PANDEMIC* on the Internet. On the other, I'm vaccinated & boosted, and we literally never leave the house. Rachelle and I both work from home. I feel like there's this giant group assignment and very few people are actually doing the work. I think this tension about the pandemic is a contributor my [janky mental health](/posts/2021/10/19/on-languishing-and-trying-to-see-the-positives/) lately.

There was very little mask use at the venue. It was mostly staff and I think I only saw one or two concert goers who were wearing one. I'll admit, I did not wear my mask even though I brought one. I purposefully stayed away from shoulder to shoulder areas. I did not get into the pit, even though that's something I do and am interested in doing. The venue does have a large, nice outside area (near the bar!) and you can position yourself in such a way as to still see the stage, perfectly clear so I ended up spending most of my evening outside.

I know that concerts are considered a high risk activity. I also know Omicron is popping off right now. I could have done better. I should have worn my mask. But how could I get my drank on while wearing a mask. Not great Bob.

## On avoiding big tech for this trip

As I was planning my trip, I was [anticipating having to use Google Maps and Lyft for the trip](/posts/2021/12/14/example-of-why-i-find-it-hard-to-move-away-from-iphone/). After some encouragement from people, I ended up just adding the hotel I was staying at to OpenStreetMaps and using Organic Maps to navigate. There was one hiccup with the navigation: Organic Maps wanted me to turn left when no left turns were allowed at the intersection right after getting off the interstate. I've added that and a couple other things to my OpenStreetMaps TO-DO list.

Additionally, I just called a cab to when I was ready to head to the venue. It was fine. The cab was good. The driver was good. Everything was good and I didn't have to install the Lyft app or contribute to the problematic business model of the sharing economy.

## On to the show!

<figure>
  {% myImage "concert01.jpg", "A metal band plays on a stage in dimly lit venue." %}
  <figcaption>Realm at The Mill & Mine</figcaption>
</figure>

I've never been to The Mill & Mine and I have to say, I think it's a really nice venue. It's not a huge venue but it was nice and seemed newly remodeled. The outside area is *especially* nice IMO. The bathrooms were nice. The floors weren't even sticky anywhere! 

[Realm](https://realmknox.bandcamp.com/) was my favorite opener. They definitely have a doom/sludge vibe and picked up a CD to rip.

The other bands were pretty good though nothing really stood out for me specifically. Whitechapel was great of course! It's the first time I'd ever seen them and I was not disappointed. 

I made a few single serving concert fiends and we ended up walking over to a bar after the show. There was a foosball table there so I got to play some foosball too!! When one of my single serving friends started talking about wanting to fight someone, I thought that was probably my sign that it was time for me to get myself back to my room. I called the cab again, and she took me back to the room. No problems!

What a great evening. I got to hang out with a lot of like minded people. I made some single serving friends. I got to play foosball. Now let's see if I get sick and die about it. 
