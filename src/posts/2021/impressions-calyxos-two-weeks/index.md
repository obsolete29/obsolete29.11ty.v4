---
title: Impressions of CalyxOS after two weeks
desc: 'I''ve had my Pixel 4a, running CalyxOS for about two weeks now and I wanted to share my impressions at this point'
date: '2021-08-16T08:09:01.977-05:00'
lastmod: '2021-08-16T08:09:01.979-05:00'
tags:
  - mike-breaksup-with-apple
  - pixel4a
  - calyxos
  - android
---
I've had my Pixel 4a, running [Calyxos](https://calyxos.org/) for about two weeks now and I wanted to share my impressions at this point.

- I'm really missing the gesture on iOS where you can just swipe down on the Home Screen and get the search box to pop up. I used that a *lot* on my iPhone.
- The keyboard is still giving me fits. I'm not sure if it's just my big dumb fingers and I'll eventually get used to the slightly different layout or if the keyboard that comes by default just isn't very good-maybe it's a bit of both.
- I'm starting to feel like a second class citizen using this phone. I suspect this is just kind of the way of things with FOSS. Example: As a principle, I want to be the customer and not the product so it's important to me that I pay for the software I'm using if possible. I was looking at the Plume twitter client and I really wanted to avoid the ad-supported version. The developers of Plume do not offer any way to purchase the premium version of the application without using the Google Play Store. Ok fine, I can still purchase the software but then I have to use a burner g-account because it's possible the account could be banned for violating the play store terms of service. 😑 
- Using signal over iMessage has been mixed so far. I sent the message to my friends and family, asking them to pretty please use Signal. More than I thought have joined me over on Signal. The rest just ignored the message.

So yeah, this morning I'm feeling a tiny bit down about moving off Apple devices. I suspect I just need to shift my thinking. A Google-free Android phone will just have some compromises. I need to train some new brain pathways and maybe go read a book instead of doom-scrolling Twitter anyway.