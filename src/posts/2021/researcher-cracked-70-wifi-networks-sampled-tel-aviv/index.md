---
title: Researcher cracked 70% of WiFi networks sampled in Tel Aviv
desc: Good password practices are as important as ever. Update yours if you haven't done so already.
date: '2021-10-27T07:26:01.792-05:00'
tags:
  - wifi
  - passwords
  - security
---
Hey folks, be sure you're choosing a good password for your WiFi network.

> A researcher has managed to crack 70% of a 5,000 WiFi network sample in  his hometown, Tel Aviv, to prove that home networks are severely  unsecured and easy to hijack.
>
> <cite>[BleepingComputer](https://www.bleepingcomputer.com/news/security/researcher-cracked-70-percent-of-wifi-networks-sampled-in-tel-aviv/)</cite>

The researcher was able to do this because people are using weak passwords. Secure your WiFi properly. I personally like using passphrases instead of passwords. Instead of using `'Z#lJ+uK+A^H7J{j@LK%$`, I'd use `accompany.morbidity.daycare.ribcage` as my password. Passphrases are easier to type on a mobile keyboard. 

From the article:

> The research shows that most people are not setting a strong password for their WiFi networks even though they are at risk of being hacked.
>
> If your WiFi password is hacked, anyone can access your home network, change your router's settings, and potentially pivot to your personal  devices by exploiting flaws.
>
> Good passwords should be at least ten characters long and have a mix  of lower case and upper case letters and symbols and digits.

Since you're thinking about updating your WiFi password, go ahead and check out the [router configuration guide](https://decentsecurity.com/#/routerwifi-configuration/) at Decent Security. Lots of good information there.
