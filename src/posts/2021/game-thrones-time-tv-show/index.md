---
title: Game of Thrones is still the all time best TV show
desc: Rachelle and I just finished our re-watch of Game of Thrones and it's still the best TV ever made.
date: '2021-09-29T06:31:06.851-05:00'
tags:
  - watched
  - review
---
Rachelle and I decided to rewatch Game of Thrones and we just finished the final episode last night. The show holds up *extremely well* on rewatch. I think I've actually revised my opinion upwards about the show if that's even possible. My recollection on the first watch through was that the 8th season was really poorly done and I had a general distaste for the whole season. But really, it was only the last two episodes of season 8 that wasn't great and felt extremely rushed.

Game of Thrones is my #1 favorite TV show by a mile. [The Wire](/posts/2021/02/13/watched-the-wire-★★★★½/) used to rank but after rewatching GoT, it's not nearly as close. It's Game of Thrones then everything else. I have no reservations in giving this show 5 stars.

I need to think about this some but some other shows that will rank in my top 5 are Star Trek:TNG, Star Trek:VOY, Fire Fly, The Wire. Maybe The Sopranos would be at the low end of the list?

What are your favorite TV shows?
