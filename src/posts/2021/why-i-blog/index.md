---
title: Why I blog
desc: I recently read an article about building a blogging habit and I'm inspired to share my own thoughts on it.
date: '2021-07-25T12:00:28.389-05:00'
lastmod: '2021-07-25T12:00:28.391-05:00'
tags:
  - obsolete29
---
I follow Chris Wiegman on Mastodon and he wrote a post about [Building a Blogging Habit](https://chriswiegman.com/2021/07/building-a-blogging-habit/). I wanted to write about the differences between Chris's approach to blogging and my own.

## Re: Stop worrying about the site design

> First and foremost, I need to stop worrying about my actual site. In the past a single “off” link could send me off into a rabbit whole of code and tweaks. That means that, rather than writing a post, I’m writing the code for the site and never actually get to the content.

Oh boy, that quote from Chris really resonated deeply for me because I've also struggled with the inherit friction built into [Eleventy](https://11ty.dev), and I presume most static site generators. At some point, I'd love to feel like development on my site is done or is at some 1.0 version but there's always just one more thing I want to do.

I have spent some effort in trying to reduce the friction of making new posts. I think I'm at a pretty good spot atm in that regard.

## Re: Audience

> Forgetting who your audience is, or even that you have one, can really kill any motivation to write. If this was a personal journal that might be different but it isn’t. I’m a teacher at heart and I enjoy scratching that itch through my writing.

Ok, this is where he and I are taking different approaches to how we write blog posts. I don't like writing with the idea that I'm writing for an audience. That idea causes me to lock up and really not want to write anything because someone else has wrote about what I'm wanting to write about and they've written about it better. For example, someone else has ***for sure*** written about [setting a conditional variable in Javascript](/posts/setting-conditional-variable-javascript/). If I listen to the voice that's saying someone else has written about a thing, then I'd never write anything ever.

I write for myself. I'm not trying to grow my page views or blog visitors (good thing too because I have very few!). I'm not monitizing anything. I don't have any ads. I don't care about any of that. I enjoy writing about my experiences and I enjoy hacking around on my site, teaching myself HTML, CSS, Javascript, Node etc. If someone else should stumble upon something I've written and they're helped by it in some way, then that's super awesome and the idea brings me joy. If no one reads my posts, well that's ok too because I enjoyed thinking about the topic and recording my thoughts.

That's it for now. Thanks for reading!
