---
title: Read The Last Graduate - 4.5 stars
desc: this is the description field
date: '2021-12-16T11:40:16.260-06:00'
tags:
  - bookshelf
  - 2021-reading-goals
  - audio-book
  - urban-fantasy
  - naomi-novik
  - the-scholomance
slug: read-the-last-graduate
---
[The Last Graduate (The Scholomance #2)](https://www.naominovik.com/the-last-graduate/) by [Naomi Novik](https://www.naominovik.com)

{% myImage "novik-last-graduate-1.jpg", "Cover art for The Last Graduate book. Green background with gold writing. A mystical lock is displayed." %}

Rated: 4.5 of 5 stars

This is was a fun, coming of age urban fantasy novel. I really enjoyed it and would recommend. I listened on Libro.fm and the narrator was great! I'm def looking forward to the next one.
