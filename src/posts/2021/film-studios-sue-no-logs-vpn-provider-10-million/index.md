---
title: Film studios sue “no logs” VPN provider for $10 million
desc: This is is disconcerting
date: '2021-09-27T13:38:27.065-05:00'
tags:
  - privacy
  - vpn
---
> Dozens of movie production companies sued LiquidVPN this year over the VPN provider's marketing efforts that could be perceived as promoting piracy. These companies, which are now seeking $10 million in damages, claim that the "no log" policy of LiquidVPN is not a valid excuse, as the VPN provider actively chose to not keep logs.
> 
> --<cite>[Ars Technica](https://arstechnica.com/information-technology/2021/09/film-studios-sue-liquidvpn-for-10-million-but-is-it-fair/)</cite>

As a user of [Mullvad VPN](https://mullvad.net/en/), I often encouter sites that simply block VPN users. Best Buy, Lowes, Credit Karma, Etsy are a few that pop into my head without trying too hard so I wasn't at all surprised by this news. I think too many still conflate seeking privacy as doing something illegal. I'm just trying to buy the Matrix Trilogy Steelbox Bluray without that purchase being associated with me as a person with the data aggregators.

I don't know anything about law but if the studios are successful, could it set a precedent that could then require VPN provides to keep logs? We recently learned that [ProtonMail is legally obligated to enable and provide logs when requested](https://restoreprivacy.com/protonmail-logs-users/). Could we start to see laws that require VPN providers to do the same? It just feels like there's a battle to remove privacy on the internet.
