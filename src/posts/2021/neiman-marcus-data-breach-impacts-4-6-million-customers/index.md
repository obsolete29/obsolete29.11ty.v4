---
title: Neiman Marcus data breach impacts 4.6 million customers
desc: Let's use this example of a data breach to review my SOPs as they relate to security and privacy.
date: '2021-10-01T08:28:33.818-05:00'
tags:
  - privacy
  - security
  - OpSecReview
---
{{ desc }}

> Yesterday, Neiman Marcus disclosed that its 2020 data breach impacted about 4.6 million customers with Neiman Marcus online accounts. The personal information of these customers was potentially compromised during the incident. The bits of information include:
> 
> - Names, addresses, contact information usernames and passwords of Neiman Marcus online accounts
> - Payment card numbers and expiration dates (although no CVV numbers)
> - Neiman Marcus virtual gift card numbers (without PINs)
> - Security questions of Neiman Marcus online accounts
> 
> <cite>[Ars Technica](https://arstechnica.com/information-technology/2021/10/neiman-marcus-data-breach-impacts-4-6-million-customers/)</cite>

Ok so let's take these one-by-one as if my information was included in this data breach.

## Names, addresses, contact information usernames and passwords of Neiman Marcus online accounts

 - **Name**: I try to avoid using my first name, instead opting to use my first initial or first and second initial. Can I get better here though? Legally, can I use a pseudonym for online transaction? I don't want to commit fraud and I'm not trying to do that but can I have a couple pseudonyms like authors have? Idk, that's probably extreme!
 - **Address**: I could use a PO Box for a billing address so that my physical address isn't expected but that doesn't work if I'm getting items shipped to me.
 - **Usernames and passwords**: Bummer but not that big of a deal. If given the option, I always generate a random username but most vendors use email address as the username. Again, not that big of a deal for me as I also generate random and unique email addresses for each online account I have. Passwords are not a problem either as I generate unique and random passwords so this can't be used on another site.

## Payment card numbers and expiration dates

No big deal here as I use unique virtual credit cards via Privacy. I can just close the card for this vendor and generate another if I need.

## Security questions

Great example of why I treat security questions just like passwords. I use my password generator to generate unique answers to the security questions. It doesn't matter that the answers to these questions are on the dark web as they're not real. I'd just change my security questions and generate new answers. No problems.

## Conclusion

Overall, I think I'd give myself an A- on this one. I don't use a PO Box but I could. I have questions about the legality of using pseudonyms with online vendors.
