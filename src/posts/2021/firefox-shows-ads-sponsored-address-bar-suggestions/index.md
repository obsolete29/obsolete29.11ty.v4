---
title: Firefox now shows ads as sponsored address bar suggestions
desc: 'Oh man I really don''t know how to feel about this. :('
date: '2021-10-07T09:48:35.896-05:00'
tags:
  - firefox
  - mozilla
  - opensource
---
Bleh. I really dislike this. :(

> Mozilla is now showing ads in the form of sponsored Firefox contextual suggestions when U.S. users type in the URL address bar.
>
> --> <cite>[Firefox now shows ads as sponsored address bar suggestions](https://www.bleepingcomputer.com/news/security/firefox-now-shows-ads-as-sponsored-address-bar-suggestions/)</cite>

I'm not sure how else to feel about this. It's super important that the browser market has some competition with Chrome and Chromium but this just doesn't feel great. It looks like a person can opt out of it. When I looked at my settings, I was already opted in. I don't think this is any different than how DuckDuckGo shows you sponsored links based on what you type into the search box. This just feels different because it's software on my computer, even though it's technically the search box for the Internet.

I don't know. What options are there that isn't Chromium based? Or do you have a good argument about why I should just use Chromium? Or should I just opt out of this and keep it moving? How do you feel about this news?
