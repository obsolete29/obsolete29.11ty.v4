---
title: Scooter user death in Nashville shows how car centric we are
desc: Cities should be made for people not cars. Why do we have such a blind spot for how dangerous cars are?
date: '2021-10-05T05:53:34.223-05:00'
tags:
  - Pedestrians
  - Nashville
  - Walkability
  - Scooters
---
Metro Nashville PD tweeted about the death of Melinda Lovelady while riding a Lime scooter in downtown Nashville.

> BREAKING: Melinda Lovelady, 54, of Tyler, TX, was killed tonight when  she lost control of the Lime scooter she was riding in the northbound  lane of 3rd Av S @ Symphony Pl. Lovelady collided with the rear tires of a southbound moving semi tractor. She died at the scene.
>
> -- <cite>[Metro Nashville PD on Twitter 10:44 PM · Oct 3, 2021](https://twitter.com/MNPDNashville/status/1444870894690701316)</cite>

The area where the accident happened is in downtown Nashville, where lots of people are walking around and having a good time. Scooters are often used by people to zip around and sometimes those people have been drinking. Reading the comments really shows the blind spot people have for cars.

- *Those scooters need to be banned, nashville has wwaaaaaaayy to much traffic and has for years*
- *How many more have to die or get seriously hurt on these scooters before they get banned??? Serious question*
- *These scooters are a death trap. Something needs to be done.*

I'd like to point out that it's not the ***scooters*** that's dangerous. It's the interaction of the scooters with ***cars*** that's dangerous. Why was there a semi truck cruising downtown Nashville? Why are there cars allowed below 5th Ave on the weekends? Why aren't people saying *how many more people have to die or get seriously hurt by cars before they get banned??? Serious question*? Yes, people use the dumb scooters without a helmet and after drinking but *so what*? Over 36k people died in the US from car accidents but it's the *scooters* that are the dangerous menace. 

If any place in Nashville can be considered a walkable, pedestrian friendly area it's here. Cities should be made for people not cars. Can we at least make this part of Nashville for people?
