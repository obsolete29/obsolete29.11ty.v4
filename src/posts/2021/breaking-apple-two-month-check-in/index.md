---
title: Breaking up with Apple - Two Month Check in
desc: It's been just over two months since I declared I was breaking up with Apple so I wanted to check in and document my experiences so far.
date: '2021-10-10T05:58:45.467-05:00'
tags:
  - apple
  - privacy
  - mike-breaksup-with-apple
---
It's been just over two months since I declared [I was breaking up with Apple](/posts/2021/08/06/i-am-breaking-up-with-apple/) so I wanted to check in and document my experiences so far. The main two things that I did was switch from my iPhone SE to a Google Pixel running [CalyxOS](https://calyxos.org/) and switched from my 2015 MacBook Pro 13" to a [System76 Meerkat](https://system76.com/desktops/meerkat) running [Pop OS Linux](https://pop.system76.com/) as my main computer.

## Switching from the iPhone

I was prepared for [this change](/posts/2021/08/11/an-apple-fanboy-tries-out-android-on-a-pixel-4a/) to be much harder than it has been so far. The biggest thing in my mind has been is finding a good maps app. I've landed on [OsmAnd](https://osmand.net/) on my phone. It uses [OpenStreetMaps](https://www.openstreetmap.org) as it's source. It's good and pretty functional but definitely makes me feel like I'm using Waze in the early the days. It's kind of hard to find things sometimes and I can edit the maps myself when I find an error.

I didn't really realize how much I used the map app to look for businesses. We wanted to order some Mexican take out one evening from the place down the street. Normally, I'd just have popped open Apple Maps and either search "Mexican" or scrolled over to where it exists on the map, click their business and either go to their site or call them. Trying this with OsmAnd was not successful. Not only did it not find the business name when I searched, when I tabbed over to where it exists on the map, it was missing. Great opportunity to add it to the map myself but in the moment, I just wanted to order a burrito and not add something else to my crowd source citizen chore list.

It turns out that when you're a giant corporation and can spend bucket loads of money on developing a maps app, it'll be really good compared to anything the open source community can build.  OsmAnd is pretty nice but it'll never be as nice and slick as Apple or Google Maps.

The other big thing I miss is being able to swipe down on the home screen to get the search box. That's a killer iOS feature that I think is under appreciated by most. It's what allowed me to keep my home screen nearly empty of app shortcuts. This feature isn't on CalyxOS and I haven't really investigated to see if there's some option that I could install as a 3rd party app.

Here are a few other thoughts on my switch from iPhone

- **Keyboard** - I'm a lot less accurate on the Pixel than the iPhone. I think part of it is muscle memory from using a specific layout for 10 years but I think part of it is that the CalyxOS keyboard just isn't as good.
- **iMessage** - I invited my friends and family to join me on Signal and more joined me over there than I figured would! This allows me to communicate using Signal in the same way I used iMessage. Only a couple people persist with SMS texting even though I'm not iMessage any longer. I'm not good at seeing those messages because I don't look at my phone that much. Plus, I don't really like texting on my phone because of the previously mentioned lack of accuracy. I'll just be the guy who's bad at texting I guess.
- **Apple Watch** - One unexpected side affect of setting the iPhone aside is that I've also stopped wearing my Apple Watch. The two main things I miss from the watch is sleep tracking and not being able to quickly record my walks with Strava. I'll also not be able to use it to record my hikes and bike rides when I circle back around to those activities at some point so I'll need to come up with a solution, and I know there are many. I just wear a plain old Walmart special Citizen watch. It works really great at telling time and even has little glow in the dark hands. It feels retro.

## Switching from MacBook Pro

I was prepared for [this change](/posts/2021/08/31/macbook-pro-user-tries-linux-as-daily-driver/) to be harder than it has been as well. There's really nothing big that stands out for me. Sometimes things are harder because, Linux but overall it's been pretty easy to make this switch. Mostly, I miss specific applications that are available only on Mac.

The biggest thing in my mind is syncing contacts/calendar between my Pixel and my computer. I've landed on a solution using my Synology NAS but I'm not entirely happy with it so I'll be revisiting this topic. For now, it's working ok... I have to turn off my VPN on my phone because DAVx5 doesn't work even if I make an exception for it. I want a bullet proof solution that works using my local network. I'd love it if I could use Syncthing!

Another minor quibble is syncing music playlists between my phone and computer. It's fine. It works. But the play list stuff is kind of a manual process and is mostly a one way affair. There's some room for improvement here and I've been keeping my eye out for better applications.

I've loaded my MacBook Pro with Pop OS Linux and it has now replaced my iPad as my main couch surfing device.

## Conclusion

I'm pleased with how this switch has gone so far. I feel less connected, and I mean that in a good way. I don't have my watch on my wrist, buzzing me about this or that. The Apple TV app on my my devices are no longer reminding me about this new episode or that one. There's no charge notifications, no updates about steps, breathing, standing up or weekly spending or screen time. I disabled some of those notifications but I felt like some were valuable. Now that I don't have them, I realize how not important they are. I sincerely believe that convincing you that those notifications *are* important is part of the Apple (big tech?) seduction. I do not need to know that the new episode of Ted Lasso is out. I'll see it when I turn on the TV and am ready to see what's new to watch.

[Mastodon](https://indieweb.social/@obsolete29) has become my primary social media platform. Without Tweetbot (Apple only), Twitter's signal to noise ratio is a bit too unbalanced on the noise side for me. I get a ***lot*** more interaction on Mastodon than Twitter. Even though many of my real life friends and acquaintances are on Twitter, I feel like many don't see my tweets because of the algorithm. Or perhaps they're all just collectively rolling their eyes as I rant about [Amazon fridges](/posts/2021/10/08/amazon-designed-fridge-will-use-the-same-tech-as-amazon-go-stores/) (shout out to Erin!). 

Looking forward, I think there's an opportunity to reduce the HomePods. Having a voice assistant is super nice. Out of the three main ones, I still trust Apple way more than Google or Amazon. I do want to implement [Home Assistant](https://www.home-assistant.io/) where it makes sense to do so, so the scheduled automation stuff mostly. Any future home automation will be Home Assistant.

Same goes for our Apple TV for streaming. There's just no way I'd ditch the Apple TV for an Amazon fire-something-or-another. I'm unsure about Roku but they don't seem to be especially privacy minded at all. I have purchased a Blu-ray player and future movie purchases will be physical media now instead of buying them on iTunes via the Apple TV.
