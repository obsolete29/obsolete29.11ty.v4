---
title: How I'm using my Synology NAS to own my content
desc: I've just started the journey of self-hosting and divesting myself from big tech. Here are a few of the things I'm doing so far!
date: '2021-09-06T07:56:39.118-05:00'
lastmod: '2021-09-06T07:56:39.122-05:00'
tags:
  - mike-breaksup-with-apple
  - mike-yells-at-clouds
  - self-hosting
  - synology
  - nas
  - indie-web
---
My [breakup with Apple](/topics/mike-breaksup-with-apple/) has really become a breakup with big tech and a love affair with [IndieWeb](https://indieweb.org/) and [Self-Hosting](https://www.vice.com/en/article/pkb4ng/meet-the-self-hosters-taking-back-the-internet-one-server-at-a-time). I recently purchased a [Synology DS220+](https://www.synology.com/en-us/products/DS220+) with the intent of creating a little backup cloud with my friend [David](https://twitter.com/dbyrge). The idea being that since we both have Synology devices, we can backup our devices to our local NAS, then backup the local NAS to the remote device -- he backup his to mine and me backup mine to his, over the Internet. I'm able to adhere to the [3-2-1 rule](https://en.wikipedia.org/wiki/Backup#3-2-1_rule) in backup and I'm able to cancel my [Backblaze](https://www.backblaze.com/) subscription. Yay! 

In addition to backing up the files from my computer, the Synology has some self hosting capabilities that I think have a lot of potential.

## Local and offsite backups
Of course I'm using my NAS to backup my [System76 Meerkat](/posts/2021/08/31/macbook-pro-user-tries-linux-as-daily-driver/)! I've setup a cron job to run rsync to backup my files form my PC to the NAS on a nightly basis. I'm not using any sort of backup scheme for this part -- I'm just syncing my files from my PC to the NAS once a day.

It's worth pointing out that I'm using [Syncthing](https://syncthing.net/) to sync a few folders (Photos, Music, Keepass, Notes) between my [Pixel 4a with CalyxOS](/posts/2021/08/11/an-apple-fanboy-tries-out-android-on-a-pixel-4a/) and my Linux computer.

I use Synology's Hyper Backup to run a nightly backup of the synced folder and that gets sent over the Internet to my friend David's NAS. Hyper Backup has some retention policies and strikes me as a proper backup software. It keeps versions and you're able to browse the backup set and pick out a specific version and all that fun backup stuff. Neat.

## Sync contacts and calendars
As a recovering Apple user, syncing contacts and calendars between devices is just not something I ever thought about other than to make sure the little blue toggle was clicked on for those entries under iCloud settings.

Now I'm using a Pixel as my phone and Linux as my computer, I wanted to see if I could sync my device contacts and calendars locally. Enter Synology Contacts and Synology Calendar! These are just applications you can install on the NAS using the package manager that comes with it. Each of those applications support CardDAV and CalDAV respectively.

I was able to get my contacts syncing from my Pixel to the NAS without any problems by using [DAVx5](https://f-droid.org/en/packages/at.bitfire.davdroid/). I'm struggling a bit to get the calendar to work but I've not really tried very hard to troubleshoot and figure it out.

I was able to get syncing working on my Linux machine by installing [GNOME Evolution](https://en.wikipedia.org/wiki/GNOME_Evolution). It's a basic email/calendar/contacts client and I'm able to connect to the Synology's CardDAV and CalDAV instances without any problems. Evolution then syncs those items from the Synology to the contacts and calendar apps on my computer.

Once I get the calendar sync issue figured out on my phone, I'll have contacts and calendars syncing between my devices and doing that all locally. How cool is that?

## Conclusion
My requirements for self-hosting are pretty low. I'm not trying to stream media or share calendars. I'm not trying to do any sort of family sharing. I mostly just want to sync a few things between my phone and my computer and I want to backup that stuff in case my house burns down and I'd like to do all that on my local network.

What are you self-hosting? What are you interested in self-hosting? How do you sync your contacts and calendar between your android phone and Linux computer?
