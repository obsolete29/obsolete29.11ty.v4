---
title: Coffeeneuring Challenge 2021
desc: 'A challenge to ride bikes and drink coffee? Yes, thank you please.'
date: '2021-10-06T07:37:16.414-05:00'
tags:
  - cycling
  - Coffeeneuring
  - CoffeeneuringChallenge2021
---
I've decided to participate in the [Coffeeneuring Challenge 2021](https://chasingmailboxes.com/2021/10/05/coffeeneuring-challenge-2021-the-c1-edition/). Click the link to read about the "rules" but you just have to ride your bicycle about two'ish miles to seven different places for a coffee'ish type of drink, over the course of about six weeks.

I've been in a pretty bad mental funk for the past few months. I've gained a lot of weight and I've picked up some pretty unhealthy habits. I love a challenge, I love coffee and I love cycling so I'm going to use this as a reason to try to jump start a healthy habit that makes me feel a lot better about myself.

Join in if that's your jam!
