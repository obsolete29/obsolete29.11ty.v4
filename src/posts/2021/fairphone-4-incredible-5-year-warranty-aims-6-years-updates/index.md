---
title: 'Fairphone 4 has an incredible 5-year warranty, aims for 6 years of updates'
desc: This looks very interesting to me! Love the idea.
date: '2021-09-30T08:02:34.303-05:00'
tags:
  - FairPhone
---
I'm going to do some reading but what OS does it run? Quickly scanning a couple pages say *our clean version of Android* and I read *clean* to mean no Google.

What if CalyxOS and Graphene and the others could support the Fairphone?

> Fairphone is unique in the world of smartphones. It's pretty much the only company trying to build a sustainable device that isn't glued together and hostile to the repair community. Today, Fairphone is announcing a brand-new flagship: the Fairphone 4, which brings an updated design and better specs while still shipping with all the modularity you would expect.

-- [Ars Technica](https://arstechnica.com/gadgets/2021/09/fairphone-4-has-an-incredible-5-year-warranty-aims-for-6-years-of-updates/)
