---
title: Opting out of Spotify
desc: Spotify is the worst of the worst when it comes to monetization and privacy. I'm opting out.
date: '2021-08-17T08:13:41.530-05:00'
lastmod: '2021-08-17T08:13:41.531-05:00'
tags:
  - mike-yells-at-clouds
  - spotify
  - privacy
---
[Removing shows from Spotify](https://remysharp.com/2021/08/17/removing-shows-from-spotify) showed up in my RSS reader this morning. It's basically Remy showing you how he hacked the Spotify app to stop showing him show recommendations. Which reminded me that I recently saw [All the Ways Spotify Tracks You-and How to Stop It](https://www.wired.com/story/spotify-tracking-how-to-stop-it/), which itself links to the Wall Street Journal Article, [Spotify Has Big Ambitions for Ad Business](https://www.wsj.com/articles/spotify-has-big-ambitions-for-ad-business-1502964001).

For the last few years, I've tried to subscribe to the idea that I want to be the customer, not the product. That is, if I pay for the product with money, then the company doesn't need to sell my information and attention to ad companies. I'm beginning to think I was naive in that thinking because I give you this Spotify example.

Remy states in his post that he's *happily paying for Spotify, so he doesn't have to listen to ads*. Even though he's a paying customer, he's still getting these show recommendations which demonstrates that Spotify is the worst of the worst-they're taking Remy's money, collecting tons of information ***and*** showing him ads. 

> Everything you do in Spotify’s web player and desktop and mobile apps is tracked. Every tap, song start, playlist listen, search, shuffle, and pause is logged. Spotify knows that you started playing Lizzo’s “Truth Hurts” at 23:03, listened to it for one minute, then searched for “break up” and listened to the entire four hours and 52 minutes of the “ANGRY BREAKUP PLAYLIST” without any pauses.
>
> All this behavioral data can be mined by Spotify—and it can be deeply revealing. Back in 2015, when Spotify had just 15 million paying subscribers, one executive [said](https://venturebeat.com/2015/02/24/spotify-exec-we-collect-an-enormous-amount-of-data-on-what-people-are-listening-to-where-and-in-what-context/) it collects an “enormous amount of data on what people are listening to, where, and in what context. It really gives us insight into what these people are doing.”
>
> The music you listen to mirrors how you feel, who you’re with, and what you’re doing. To make the most of this, Spotify has invested heavily in data science and has even used people’s listening habits in its advertising. "Dear person in the Theater District who listened to the Hamilton Soundtrack 5,376 times this year, can you get us tickets?" read one ad [from 2017](https://www.wired.co.uk/article/spotify-tinder-netflix-advertising-customer-information-privacy).
>
> -- <cite>[Wired.com](https://www.wired.com/story/spotify-tracking-how-to-stop-it/)</cite>

I sincerely believe that developers and companies need to monetize the software and services they're providing. Additionally, I sincerely *want* to support the developers of the software I like and use. Maybe I'm naive in my thinking, but if I pay for the product or service then I should be exempt from the other forms of monetization. Instead, tech companies like Spotify want to stick it to us coming and going. That's capitalism though, but fuck Spotify and companies like them!

So what can I do about it? Nothing really... other than opt out of their awful business model. On this one, I'm feeling like going full on luddite, curmudgeon, slow-tech, yelling-at-clouds old man-mode. I'm just going to buy music and download it to my devices. I'll use some tools to sync my music around to my devices using my local network. No one is collecting analytics on me. No one is trying to figure out what type of mood I'm in, so they can serve me the right ad. Additionally, more of the money I'm spending goes to the artists instead of a shitty digital company.
