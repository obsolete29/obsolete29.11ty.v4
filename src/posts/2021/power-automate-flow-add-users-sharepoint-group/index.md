---
title: Using Power Automate Flow to add users to a SharePoint group
desc: I recently had a requirement to add users to a SharePoint group using Power Automate Flow. This is how I did it.
date: '2021-07-27T14:20:29.212-05:00'
lastmod: '2021-07-27T14:20:29.213-05:00'
tags:
  - power-platform
  - power-automate
  - o365
  - sharepoint-online
---
I recently had a requirement to add users to a SharePoint group programmatically. The client had a list of locations as a SharePoint List. A "manager" was defined for each location in the SharePoint list as a person field. Each location had a SharePoint group already created and assigned permissions to various other lists/libraries/folders. To make the process as user friendly as possible, the decision was made to allow the site owners the ability to define the manager for a location directly on the list instead of asking them to drill down into the SharePoint permissions screens to assign the permissions.

My approach for this solution is to call the [SharePoint REST API](https://docs.microsoft.com/en-us/sharepoint/dev/sp-add-ins/get-to-know-the-sharepoint-rest-service?tabs=csom) using the Power Automate action called `Send an HTTP request to SharePoint`.

> This walk thru is a simplified version of what I actually did.

## Trigger

For the purpose of this walk thru, I'm using the `When an item is created or modified` SharePoint trigger to initiate the Flow.

## Get the group ID

Ultimately, I'm going to use the `_api/web/SiteGroups(12)/users` REST API endpoint to add people to the SharePoint group. The problem is that we only know the group *name* but not the group *ID*. So how do we get the group ID?? Luckily, there is also an endpoint to look up groups by name. Using that, we can return the group ID.

### Initialize variable

The group names in this scenerio follow a naming convention that we can take advantage of. The naming convention is `Manager - ` plus the list item ID of the item in the Location list. Example: The location called Roberts Western World has a list item ID of 1. The SharePoint group associated with Roberts Western World therefore, is called `Manager - 1`. 

In Power Automate Flow, I'm adding an `Initialize variable` action with a value of `GroupName` and a type of `String`. For the value, let's use the `concat()` function. 

> Tip: When building an expression in a Flow, you can't just copy and paste some code directly into the value field. You have to click into the value field then use dynamic content and the expression tab. 

{% myImage "screen-shot-03.jpeg", "A screenshot of the Initialize variable action of the flow." %}

Here is the content of the expression:

```powershell
concat('Manager - ', triggerOutputs()?['body/ID'])
```

### Send an HTTP request to SharePoint

I now add the `Send an HTTP request to SharePoint` action in order to make the REST API call to look up the group ID by the group name. We'll return the Id value of the group so that then we can use it in the last step. There are four configuration settings that we need to define. 

#### Site Address

I hope that one is pretty obvious. :) 

#### Method

Since I'm only retriving information, I'm going to use the Get method.

#### Uri

The uri is the REST API end point and in this instance we need the `_api/web/sitegroups/GetByName` end point. In the previous step we built a variable with the target group name so we'll plug that in. Finally, we'll need to be sure the ID is returned. Here is what I landed on:

```javascript
_api/web/sitegroups/getbyname('@{variables('GroupName')}')?$select=id
```

#### Headers

> Tip: You can switch the headers box on the Power Automate Flow action to text mode by clicking the little button to the right of the headers section -- that makes it easier to copy and paste.

```json
{
  "accept ": "application/json;odata=verbose",
  "content-type": "application/json;odata=verbose"
}
```

#### Body

I'm leaving the Body blank for this step.

Here is what my function looks like fully configured:

{% myImage "screen-shot-01.jpeg", "Screenshot of a Send an HTTP request to SharePoint action in the Power Automate console." %}

## Parse JSON

The REST API call to get the site group ID returns a big block of JSON as `body`. I want to parse the JSON so I can reference the Id value directly in the next step. For that I'm going to add the Parse JSON action.

After adding the Parse JSON action, add the `body` value from the Dynamic content available for `Send an HTTP request to SharePoint` to the `Content` field.

Next, click the `Generate from sample` button. Now you'll need to paste the output from a test run of the `Send an HTTP request to SharePoint` action above. My output looked like this:

```json
{
  "d": {
    "__metadata": {
      "id": "https://obsolete29.sharepoint.com/sites/TenForward/_api/Web/SiteGroups/GetById(12)",
      "uri": "https://obsolete29.sharepoint.com/sites/TenForward/_api/Web/SiteGroups/GetById(12)",
      "type": "SP.Group"
    },
    "Id": 12
  }
}
```

To get yours, perform a test run of your Flow and look at an individual run history. Next, click on the `Send an HTTP request to SharePoint` action and scroll to the bottom and copy the contents of the `body` field. That's what you'll paste into the `Generate from sample` field.

After pasting my output into the Parse JSON action, it transformed it into this:

```json
{
    "type": "object",
    "properties": {
        "d": {
            "type": "object",
            "properties": {
                "__metadata": {
                    "type": "object",
                    "properties": {
                        "id": {
                            "type": "string"
                        },
                        "uri": {
                            "type": "string"
                        },
                        "type": {
                            "type": "string"
                        }
                    }
                },
                "Id": {
                    "type": "integer"
                }
            }
        }
    }
}
```
{% myImage "screen-shot-04.jpeg", "Screenshot of the Parse JSON action in the Power Automate console." %}

I now have the option to select `Id` when looking at the dynamic content for Parse JSON. Excellent!

## Add the user to the SharePoint group

Add another `Send an HTTP request to SharePoint` action to the flow, set the `Method` to `POST` and use the same headers as before.

### Uri

I'm now going to use the SiteGroups endpoint and I'm going to pass in the Id value from the Parse JSON action above.

```javascript
_api/web/SiteGroups(@{body('Parse_JSON')?['d']?['Id']})/users
```

### Body

Notice here that I'm passing in the Claims value of the Manager field from the SharePoint list.

```json
{
  "__metadata": {
    "type": "SP.User"
  },
  "LoginName": "@{triggerOutputs()?['body/Manager/Claims']}"
}
```

{% myImage "screen-shot-02.jpeg", "Screenshot of a Send an HTTP request to SharePoint action in the Power Automate console." %}

## Conclusion

So now when the Flow runs, it takes the person listed in the `Manager` field of the `Locations` SharePoint list, and puts that person into the corresponding group for that location. Some improvements you may consider, and this is actually what was implemented for the solution would be to run the Flow only if the `Manager` field changed.

Here is the full Flow that I built to write the demo. Please let me know if there are any questions or improvements I could make.

{% myImage "screen-shot-05.jpeg", "Screenshot of the full Power Automate Flow in the Power Automate Flow console." %}

That's it for now -- thanks for reading!