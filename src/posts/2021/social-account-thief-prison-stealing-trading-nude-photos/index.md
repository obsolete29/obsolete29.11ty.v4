---
title: 'Social account thief goes to prison for stealing, trading nude photos'
desc: 'Reason number 5,117 as to why basic account security is important.'
date: '2021-08-20T06:53:29.374-05:00'
lastmod: '2021-08-20T06:53:29.378-05:00'
tags:
  - infosec
  - hacking
  - basic-computer-security-101
  - mike-makes-comments
---
{{ desc }}

It looks like the hacker had pretty simple means for accessing other people's accounts. Most people shrug off account security because they think they have nothing of value, that someone else would be interested in. Well, how about those juicy nudes tho?

Lesson learned: Treat your security questions just like a password. At the very least, don't use the *actual* answers to your security questions.

> One of the methods used was to initiate the password reset procedure and providing the correct answers for the security questions that allowed defining a new password.

Lesson learned: Be skeptical and don't forward your one time codes. :D

> Another method described for accessing victim Snapchat accounts included texting them that they used the victim’s phone number to sign up to and they needed the ephemeral code to log in.

--> Source: [BleepingComputer](https://www.bleepingcomputer.com/news/security/social-account-thief-goes-to-prison-for-stealing-trading-nude-photos/)
