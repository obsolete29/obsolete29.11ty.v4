---
title: Log4j - challenge your work place to support OSS projects
date: '2021-12-13T06:44:53.240-06:00'
tags:
  - security
  - oss
slug: log4j-challenge-work-place-support-oss-projects
---
I follow a lot of developers and security minded people so [log4j](https://www.theverge.com/2021/12/10/22828303/log4j-library-vulnerability-log4shell-zero-day-exploit) is a topic I've been reading a lot about.

> Everyone is shitting bricks about the log4j vulnerability but no one seems to be shitting bricks about the corporate game of chicken in taking critical dependencies on OSS libraries without actually paying for the cost of their maintenance. You can't pray away the black swan
>
> <cite>[@ehashman@toot.cat](https://toot.cat/@ehashman/107435984257077113)</cite>

I've seen lots of comments similar to ehashman's. And usually, I'm right there as I think the pursuit of profits for shareholders, over the interests of people, the environment and society can lead to some problematic outcomes. Maybe I'm being a bit naive but I feel there's a less sinister answer as to why companies do not support the projects their business processes run on and that is, they just don't know. 

Developers seeing the 'looking for contributions' in npm doesn't translate into some sort of OSS support program at a big company unless a developer is trying to actively bridge that gap between engineers and management. I can see how this could get lost in the gaps without there being nefarious motivations of capitalists to maximize profits.

If you're reading these words and your company uses OSS in their development stack, I challenge you to run the idea up the chain. Start with your manager, explore and see if there is already a program at your company -- maybe there is. If there is, try to get the OSS projects that your department is using into that program. If there is no program, try to help develop some sort of program at your company. Be persistent. Research what other companies are doing to support OSS. In short, be the change. 

If your management is fully aware that the company's processes are built on OSS projects and they're refusing to support the project, well it's time to find a new company to work for IMO!
