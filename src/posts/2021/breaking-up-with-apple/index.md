---
title: I am breaking up with Apple
desc: Scanning customer phones for banned content is the opposite of taking user privacy seriously. I'm breaking up with you Apple.
date: '2021-08-06T09:01:29.347-05:00'
lastmod: '2021-08-06T09:01:29.348-05:00'
tags:
  - apple
  - privacy
  - mike-breaksup-with-apple
---
I'm heavily invested in the Apple ecosystem. I have almost every type of Apple device: iPhone, iPad, Apple Watch, MacBook Pro, CarPlay, several AppleTVs, and many HomePod minis. I'm about as heavily invested into a technology ecosystem as a person can be. The news that [Apple is about to start scanning iPhone users' devices for banned content](https://www.theregister.com/2021/08/05/apple_csam_scanning/) has me shook. 

I think a lot about consumer privacy and security--I'm sure way more than the average person. I believed Apple when they said they took privacy seriously. The incidents where [Apple refused to crack/hack phones for the FBI](https://www.nytimes.com/2016/02/20/business/justice-department-calls-apples-refusal-to-unlock-iphone-a-marketing-strategy.html) was proof of that for me. I think Apple must be under some sort of extreme pressure from governments to give backdoor access, and I think this is the first step. Or maybe I was just naive and *really* wanted to believe Apple was serious about privacy. 

They start with the worst of the worst, child porn, to get the system in place, but then it's easy to adjust the parameters later. In the US, it could be used to scan for "terrorist" imagery, for instance. In authoritarian countries like China, Turkey, Hungary, and Russia perhaps they are scanning photos and texts for anti-government speech? 

As recently as six months ago, I wouldn't have thought this was possible, but [Apple has compromised](https://www.nytimes.com/2021/05/17/technology/apple-china-censorship-data.html) the privacy of Chinese users after pressure from the Chinese government. To operate in China, and get access to all that money, they have to do what the Chinese government says. The prime directive of all capitalists is their fiduciary responsibility to shareholders. 

I've decided to start divesting from being so heavily locked into the Apple-walled garden. This will most likely be a slow process and will take some time, but I'm starting with my phone. I just ordered a Pixel 4a and I plan to load [CalyxOS](https://calyxos.org/) on it. I plan to share my trials, tribulations, and thoughts on the whole process along the way. 

Wish me luck.