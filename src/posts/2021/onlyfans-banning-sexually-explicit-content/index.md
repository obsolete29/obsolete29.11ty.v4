---
title: OnlyFans Says It Is Banning Sexually Explicit Content -->
date: '2021-08-19T19:36:49.982-05:00'
lastmod: '2021-08-19T19:36:49.983-05:00'
tags:
  - fediverse
  - indie-web
  - mike-makes-comments
---
When your content is one someone else's platform, then they get to decide what's allowable. I bet there's a federated or some way to self host this sort of thing. I hope content creators are able to figure it out.

I only know of OnlyFans in the context of sexually explicity material. Seems like a dumb move.

--> Source: [NY Times](https://www.nytimes.com/2021/08/19/business/onlyfans-porn-ban.html)
