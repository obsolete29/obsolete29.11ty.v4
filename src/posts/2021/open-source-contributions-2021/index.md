---
title: My Open Source contributions for 2021 - Do you support any projects?
desc: I performed my year end audit of the OSs projects I use and support. I'm hoping to encourage others to support their projects
date: '2021-12-15T06:08:03.894-06:00'
tags:
  - FOSS
  - opensource
  - BeTheChange
slug: open-source-contributions-2021
---
I think most people reading these words probably use some open source software on a daily basis. [Log4j](https://arstechnica.com/information-technology/2021/12/hackers-launch-over-840000-attacks-through-log4j-flaw/) is shining a big ole bright light on some of the problems with the FOSS software model and that's many people use but few contribute. I've seen lots of vitriol aimed at the big corporations who run their platforms using FOSS without contributing to the projects.

What can we do as individuals though? As I [said yesterday](/posts/2021/12/13/log4j-challenge-your-work-place-to-support-oss-projects/), let's encourage our work places to contribute if they don't already. We can also contribute as individuals though! Do you contribute? Why or why not? Which projects do you support? I've had a haphazard approach to contributing so I wanted to do an audit and make sure I'm supporting the projects that I use on a daily basis. In general, I think $1/week is a pretty reasonable amount to pay for free, open source software that bring value to my computing experience. I'd like to try to establish a year-end giving routine for the projects that I want to support.

## Currently Contributing

* [KeePassXC](https://keepassxc.org/) ($5/month) - KeePass for Linux! It has a nice little browser extension and I use it daily on my Linux box.
* [KeePassDX](https://www.keepassdx.com/) ($52/year) - KeePass for Android! Works well. I like passwords!
* [Signal](https://signal.org/) ($10/month) - Secure messaging. I think it's the best option we have for secure messaging with non-technical people.
* [IndieWeb.social](https://indieweb.social) - ($20/month) - This is the [Mastodon](https://joinmastodon.org/) instance that I use and is my primary social media of choice. Running these instances seems like a thankless job so thanks [Tim](https://indieweb.social/web/@tchambers)!

## New Contributions

* [Aegis Authenticator](https://getaegis.app/) ($62.27/year) - Free, secure and open source 2-step authentication app. Odd number on the donation there but they use buymeacoffee.com and it was only in Euros. Ha.\
* [Eleventy](https://www.11ty.dev/) ($52/year) - The static site generator that I use to run my personal web sites.
* [Syncthing](https://syncthing.net/) ($52/year) - A really great sync tool. Works on my Linux box. Works on my CalyxOS Android phone. Free. Open Source. Awesome.
* [K-9 Mail](https://k9mail.app/) ($52/year) - Free, open source, email client for Android.
* [Dino](https://dino.im) ($52/year) - Free, open source Jabber/XMPP client for Linux.

## Considering

I'm considering supporting a couple more projects.

* [Firefox](https://www.mozilla.org/en-US/firefox/new/) - This is my browser of choice. I think having a non-Apple, non-Chromium web browser is important but I'm torn because I don't believe Firefox is totally open source. Additionally, I don't really agree with some of the choices from the Mozilla foundation on serving ads to their users. Plus, it's not available on F-Droid for mobile. Not great Bob.
* [CalyxOS](https://calyxos.org/) - This is my phone OS of choice. I'm not positive this is my forever OS. I'd like to try out LineageOS or maybe e.
* [PopOS](https://pop.system76.com/) - This is my Linux distro of choice atm. I'm not positive this is my forever Linux distro. I'm really wanting to try Debian.

So that's it! What projects do you support? What approach do you take in regards to your contributions?