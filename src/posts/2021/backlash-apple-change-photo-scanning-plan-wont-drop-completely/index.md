---
title: 'Amid backlash, Apple will change photo-scanning plan but won’t drop it completely'
desc: This annoucement doesn't change my mind about breaking up with Apple so I will continue the slow but methodical move in divesting myself from the Apple ecosystem.
date: '2021-09-03T10:01:00.184-05:00'
lastmod: '2021-09-03T10:01:00.184-05:00'
tags:
  - apple
  - privacy
  - mike-breaksup-with-apple
---
This annoucement doesn't change my mind about [breaking up with Apple](/topics/mike-breaksup-with-apple/) so I will continue the slow but methodical move in divesting myself from the Apple ecosystem.

> Apple said Friday that it will make some changes to its plan to have iPhones and other devices scan user photos for child-sexual-abuse images. But Apple said it still intends to implement the system after making "improvements" to address criticisms.
> 
> -- <cite>[Ars Technica](https://arstechnica.com/tech-policy/2021/09/apple-promises-to-change-iphone-photo-scanning-plans-to-address-criticisms/)</cite>

For me, it was never about the actual scanning or whatever Apple ultimately decides to do. Their CSAM scanning annoucement was just the catalyst that let me see clearly that Apple's commitment to privacy is just a marketing thing for them. I'm so skeptical about their claims that I wouldn't be surprised if it was ever revealed that [Apple's refusal to unlock iPhones for authorities](https://www.cnbc.com/2020/01/14/apple-refuses-barr-request-to-unlock-pensacola-shooters-iphones.html) was nothing more than a marketing stunt.

I'm interested in having more control over my data and the privacy of that data. I want to avoid vendor lock in because that increases the friction of choosing another device/platform/service.
