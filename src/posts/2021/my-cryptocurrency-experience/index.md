---
title: My Cryptocurrency experience
desc: To form some first person experiences with cryptocurrency I'm trying to buy some bitcoin and pay someone with it.
date: '2021-12-31T06:01:28.579-06:00'
tags:
  - cryptocurrency
  - bitcoin
  - mobilecoin
slug: my-cryptocurrency-experience
---
The popularity of web3 and NFTs in my timelines these last couple months has me thinking about cryptocurrency and block chain. I feel very "man yells at clouds" about the whole damn lot of it and my unexamined opinion is it's all solutions, looking for problems to solve.

That opinion is formed mostly by just reading the opinions of others though. I have no real world experience to base anything on so I set out to fix that. 

I recently had some people help me touch up my professional profile image. Being a good human bean, I offered to pay money for people's time and effort of having to look at my dumb face as they apply filters to my image. One person was open to receiving payment in a cryptocurrency so here was my chance to just try it out as a currency. Woohoo!

The first step was to figure out how to get my greasy fingers on some of this juicy cryptocurrency. I remember reading that [Signal was implementing MobileCoin](https://www.wired.com/story/signal-mobilecoin-payments-messaging-cryptocurrency/) on their platform so lets start there. If MobileCoin is good enough for Moxie Marlinspike then it's good enough for ole Mike Harley. So I start poking around trying to figure it out. After approximately 2 minutes of tirelessly searching the web, it turns out that people in the US are unable to buy MobileCoin directly from coin exchanges. [MobileCoin has a work around](https://mobilecoin.com/news/how-to-buy-mob-in-the-us) though! Here is a summary:

1. Buy Bitcoin.
2. Download the Mixin Messenger app on your phone. (??? wtf is Mixin Messenger)
3. Transfer your Bitcoins to the Mixin Messenger crypto wallet.
4. Trade Bitcoins for MobileCoins. 
5. Profit.

Ok. That's dumb and is a lot more work than I want to go thru to send some person in South Africa $20 worth of cryptocurrency. If I have to buy Bitcoin anyway, I'll just skip all trading and swapping and use the damn Bitcoin. So MobileCoin is out. Maybe there is an opportunity to use a VPN to avoid the "not available in the US" bit of MobileCoin but I don't know, I just didn't want to cross a line with the law without better understanding the risk I was taking by doing that.

So, let's get some Bitcoin! Lifehacker and reddit both suggested Coinbase so let's start there. I had this impression in my head that cryptocurrency was private and could be used anonymously... you know, being the currency of choice of the dark web and all. That seems to have changed because I was unable to find an easy way to acquire any cryptocurrency without having to verify my identity like I was opening a bank account. Coinbase makes you submit a photo of your ID and verify other info like last 4 of your social. Very invasive and not anonymous. Not great Bob. Let's check the other options.

I employed my hacking skills by searching ddg for "how to acquire bitcoin anonymously". This lead me to https://localbitcoins.com, which is supposed to be a peer-to-peer exchange. There were options to pay someone in US dollars using Apple Pay or Venmo. But as I searched for someone to trade with, every single person had this requirement that I send a photo of my ID to them to verify my person. The exchange site doesn't allow US based IP addresses to register so I in fact did connect to a European country this time using my VPN to register and try to do a trade. I suspected the ID requirement bit of everyone's advert was just boiler plate stuff they were using to shield themselves from liability and they would actually trade, even if I didn't give them a photo of my identification. Ultimately, I did not pull the trigger on trying to trade here because the site needed to verify identity as well. Damn it.

Next I considered one of the crypto atm things I've heard about. From the news piece I saw a few years ago, it's possible to walk up to an ATM, insert cash and receive a slip of paper with some numbers that represent your bitcoins. After searching around the Nashville area though, it turns out you still have to identify yourself with an ID at the main ATM provider. It looks like there was a possibility that another, smaller provider only required a phone number that could receive a code. I considered acquiring a throw-away number and trying that option but that was all a bit too much uncertainty to leave my house over. There's a pandemic out there! 

Reddit also suggested cash.app as an option but I couldn't find a way to trade for bitcoin using their website. It seems I must download their app to have the bitcoin option available. Their app is not available on the F-Droid store and I declined to pursue this line further.

Finally, I just went ahead and registered on Coinbase. I verified my identity and did all that shit. I hooked up my damn bank account... which btw uses some service called Plaid and it required me to type in my bank account credentials into the Plaid popup on the Coinbase website. Holy shit what an invasive thing and every fiber in my body was screaming not to do it but I realllly want some Bitcoin so I can form some first person experiences on *that* part of cryptocurrency experience as well. So, I spent $100 and as I'm typing this, I now have $94.86 of bitcoin sitting in my Coinbase account. It still will not allow me to transfer anything out though because it says the bank transfer hasn't cleared. I see the transaction cleared on my bank on the 28th. Today is the 31st. What a pain. As soon as I'm able to transfer my bitcoins, I'll close and delete the Coinbase account and disconnect the sharing permissions on my bank account.

At this point, I'm inclined to agree with [David Gerad](https://davidgerard.co.uk/blockchain/2021/05/18/cryptocurrency-in-2021-still-dysfunctional-nonsense-unusable-by-normal-humans/): 

> Amongst all the higher-level reasons crypto is rubbish, it’s important to remember the more basic ones: in 2021, all of this is still ridiculous garbage that doesn’t work properly, and is near-unusable by normal humans.

If you're reading this and you're up on crypto, what's your recommendation for trading for cryptocurrency to use as currency? What are you using cryptocurrency for? I'm interested in hearing about other's experiences using crypto.

Ps. Is anyone reading this willing to sell some crypto to me directly? If yes, email me and let's trade.
