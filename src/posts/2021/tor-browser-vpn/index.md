---
title: You want Tor Browser ... not a VPN
desc: Have I been overvaluing my VPN? Let's take a look at using Tor instead.
date: '2021-10-21T07:50:03.125-05:00'
tags:
  - vpn
  - privacy
  - tor
slug: you-want-tor-browser-not-a-vpn
---
As a privacy advocate, I'm really interested in VPNs and how they can help to protect our privacy. I [wrote about](/posts/2021/10/06/its-time-to-stop-paying-for-a-vpn/) the recent [NYTimes article](https://www.nytimes.com/2021/10/06/technology/personaltech/are-vpns-worth-it.html) and thought Brian was offering some straw man arguments. I use a VPN to mask my IP address as I that's a unique identifier on the web. It can be used to correlate my activity in datasets. I also use a VPN to mask my activity from my ISP. That means I'm just transferring trust from Comcast to Mullvad though. Have I been overvaluing VPN? Maybe.

[Matt Traudt](https://matt.traudt.xyz/) takes on the main reason I'm using a VPN and that's to *prevent tracking done by websites and big Internet companies*. 

> Untruth: VPNs prevent tracking done by websites and big Internet companies
>
> Ha. No.
>
> Yes, they change your IP as it appears to the websites that you visit. There is a hell of a lot more to being anonymous or preventing tracking than your IP address.
>
> First of all, perhaps the VPN gives you an IP address that no one else is using. Or perhaps you are tech savvy and set up your own private VPN server on a VPS for yourself. Cool. Now *that* IP address identifies you instead of your home one. You gained basically nothing.
>
> But that's not generally how it works. Generally you appear to have an IP address that many of the VPN's other customers are using at the same time as you. Now you're in a pool of users, so now you can't be tracked, right? Wrong.
>
> There is *so much* that websites and big Internet companies (e.g. Facebook, Google) are doing or can do to track us that do not rely on IP addresses. It's not funny. It's serious. It should stop. We should fight it.
>
> <cite>[Matt Traudt, You want Tor Browser... not a VPN](https://matt.traudt.xyz/posts/you-want-tor-24tFBCJV/#index3h1)</cite>

I do want to clarify a point before continuing as I've seen a few references to VPNs recently ([Ruben Schade](https://rubenerd.com/disparate-thoughts-about-vpns/) and [Lazybear)](https://lazybear.io/posts/have-you-tried-mullvad/) I do not think using only a VPN makes a person safe from tracking, malware, or hacking. My threat model is resisting surveillance capitalism and data collection. I try to use a [defense in depth](https://en.wikipedia.org/wiki/Defense_in_depth_(computing)) approach and feel a VPN can be part of that solution. I use the Strict setting on Enhanced Privacy Settings on Firefox. I also use uBlock Origin, Privacy Badger and Cookies AutoDelete plug-ins. I felt like I'm doing better than most. Here are the benefits of Tor according to Matt:

> - Tor Browser has intelligent first-party isolation of local state (i.e. cookies but other stuff too) so that state you get while logged in to facebook.com in tabs 1, 2, and 3 is not accessible from tabs 4 and 5 where you are browsing nytimes.com. Cookies cannot track Tor Browser users across sites. VPNs do nothing in this space.
>   - Side note: Tor Browser also intelligently isolates your traffic. With VPNs all of everything exits from the same IP address. With Tor Browser, all traffic from facebook.com tabs **regardless of the destination domain of the requests** uses one set of circuits through the Tor network while all traffic from nytimes.com uses a different set of circuits. Even if all the sites you visit use the same CDN for serving their images/videos/ads, neither the sites nor the CDN will be able to tell that it's you that is visiting facebook and nytimes at the same time.
> - Tor Browser defaults to the same window size for all users (1000x1000 if possible, but in increments of 100x100 if the former can't fit on your display). Window size cannot be used to track Tor Browser users. VPNs do nothing in this space.
> - Tor Browser takes additional measures to make all of its users have as similar of a browser fingerprint as possible. All users have the same supported fonts.  Graphic/audio fingerprinting is mitigated. All users have the same timezone. All users load ads and scripts by default. WebRTC is not supported. VPNs do nothing in this space.
>   - For users who do not wish to load scripts or in general desire higher levels of security, an easy-to-use security slider is provided for them in Tor Browser.  When many people use the slider, it allows them to achieve this higher security without having to manually change browser settings and potentially stand out as having done so slightly differently from other people.
> - Tor Browser attempts to mitigate behavior-based tracking. VPNs do nothing in this space.
> - Tor Browser prevents tracking across first party domains with the aforementioned intelligent isolation. VPNs do nothing in this space.
> - Tor Browser does better at preventing HSTS or alt-svc tracking. VPNs do nothing in this space.

OK damn Matt! I'm convinced. I've been test driving Tor Browser on the desktop for about a week now and here are my first impressions.

* Omg it's sometimes really ***slooooow***. I think this will be the main problem with nearly everyone, me included. Mostly, I can deal with it, especially on the desktop. On my phone, eh I'm quick to click over to Firefox.
* I have to log into all the things every time I close/open Tor. I tend to fire it up in the morning, log into Mastodon, Feedbin and Pinboard then leave it open through out the day. It makes sense but it's still kind of a pain. When I'm closing Tor, it's trashing all the session information so cookies and all that are deleted with the sessions. This is easier on the desktop. On mobile it's not great.
* As when using a VPN, website operators are suspicious of Tor traffic and either block the connection outright or present you with CAPTCHAs on hard level. Idk, with as many of these I've had to complete, maybe I'm not human anymore? Maybe I'm just a malfunctioning privacy AI. 
* There's a secret web of Tor Onion services that I didn't know existed. When you go to a website that offers an onion service, Tor browsers tells you it's available and offers to switch you. Neat. DuckDuckGo, Brave Search and the NYTimes are sites that come to mind.
* Since Tor is based on Firefox, I was able to install uBlock Origin. Plus, Tor disables sharing the browser plug-ins as part of the web headers when connecting to websites. This means that adding plug-ins doesn't make me more unique. *Updated note 11/25:* A reader reached out and asked that I clarify this and I don't mind to do so. Of note is that Tor only disables sharing the browser plug-ins as part of the web headers but that doesn't mean there could be other ways to be finger printed based on browser plug-ins. The reader was unable to provide any sources when asked about specifics.

Over the past week, I've used Tor about 85% of the time on the desktop. I think I'll continue my test drive. I definitely *feel* more secure! I'll still continue to use my VPN for everything else.

Have you used Tor? What are your thoughts?
