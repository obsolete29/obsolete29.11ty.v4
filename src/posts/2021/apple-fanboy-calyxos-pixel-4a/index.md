---
title: An Apple fanboy tries out Android on a Pixel 4a
desc: I am taking the initital step of divesting from Apple by trying out a Pixel 4a. Here are my first impressions
date: '2021-08-11T12:36:41.767-05:00'
lastmod: '2021-08-11T12:36:41.768-05:00'
tags:
  - mike-breaksup-with-apple
  - pixel4a
  - calyxos
  - android
---
I've used an iPhone since about 2012 when I bought the iPhone 5. Since then, I've been all-in on Apple products, buying just about every product they produce. Apple's announcement concerning CSAM scanning has made me feel that my phone isn't my phone. Instead, it seems that they feel like my phone is just an end point on their network. I don't want to feel that the mini-computer in my pocket is an endpoint for Apple or any tech company.

## Pixel 4a

I started with what is probably the most important device for most people, and that's my phone. The Pixel 4a was in the mid $300's to purchase outright, so I felt that was an OK price to pay for this experiment. I currently use a 2nd gen iPhone SE which is a mid-range phone. In descriptions on the web, the Pixel 4a is described as being in the same category.

Verizon does not require any sort of contract for a line if you own your own handset, so my plan is to activate the Pixel with another number for a true side-by-side test run. If testing goes well, then I'll migrate completely to the Pixel as my daily driver.

## CalyxOS

What's the point of moving off the Apple platform for privacy reasons only to walk around with Google in your pocket? For my requirements, Google is ***worse*** than Apple for privacy as they're literally vacuuming up every bit of data about you in order to better target you for ads. It's really important that any solution I'm trying out is as Google-free as I can make it.

I've read about several different flavors of Google-free Android, but I landed on [CalyxOS](https://calyxos.org/). Another big player in this scene is [GrapheneOS](https://grapheneos.org/), but my impression is that it's a hardened OS, and in my mind, that usually means there is less compatibility. I felt that in making this initial move, I just needed the maximum amount of capability with apps. It's going to be hard enough changing muscle memory to use Android instead of iOS; let's not do it on hard-level.

## Loading the ROM

I knew that loading a custom ROM was going to include some manual hacking, so I wasn't at all surprised or intimidated after reading the [installation instructions](https://calyxos.org/get/install/). My 2015 MacBook Pro doesn't have a USB-C, port so I had to purchase one. Once I had the cable, I plugged the phone in and followed the instructions.

Per the instructions, I did have to fiddle around to make the installation utility run on macOS. Then, as part of the installation instructions, I had to toggle a few things on the Pixel to allow the installation of the ROM. I think this would probably be a bit intimidating to tech muggles, but the instructions were 100% accurate, and I was able to get CalyxOS loaded.

{% myImage "apple-fanboy-calyxos-pixel-4a-01.jpeg", "Loading CalyxOS on an Android phone." %}

## Activating the phone

I called Verizon to activate the phone on a new number. The cost is $20/month plus taxes, and like I said, there's no contract. The person on the phone was unable to activate the Pixel, as there was some sort of problem with IMEI according to their system. She submitted a ticket to escalate, and I'm going to hear back from them soon, I hope. She said it was probably going to be tomorrow before we get any sort of answer. Since this is a secondary phone for me, it's no big deal, but this would be very irritating for someone just trying to activate their primary phone.

## Initial impressions

So far, I've been using CalyxOS for about 24 hours, and here are few impressions.

- The maximum length for the screen password is 17 characters. That seems dumb. My pass phrases can be much longer than that.
- Overriding 9 years of iOS muscle memory is hard and cognitively taxing, but I'm already starting to retrain myself. Swipe up to get the app drawer! SWIPE UP
- The finger print sensor is on the back. Ok cool... I'm still undecided about that. I do like that I'm able to configure the sensor to show the notification screen on down swipe. That's really cool! 
- Since the finger print sensor is on the back, I can't just lean over and push my finger to the TouchID button to unlock the phone. I either have to type the PIN or pick it up to touch the sensor.
- There's a back button‼️
- So far, I've been able to install the important apps I use a lot by either using F-Droid or Aurora. Firefox, 1Password, Mullvad VPN, Libro FM (audiobooks).
- My personal website (this site!) doesn't load when using DDG browser or Firefox on the device. It does work using the Onion browser on the same handset, so that's really weird, and I'm not sure why. I also wonder how many other people are getting the error message.
- Nearly 100% of my texting happens on iMessage. I'm not looking forward to asking my friends and family to download and install Signal just to text with me.

## Conclusion

So far there have been no show stoppers, and I'm still feeling pretty good about the decision. I need to figure out strategies for music, syncing contacts/calendar/photos, reminders, and notes.

I'm struck by how much of a hurdle there is for normies who are interested in privacy and want to opt out of Google and Apple. There doesn't seem to be an out of the box go-to that people can buy as a complete solution. I wish CalyxOS or someone else would offer a handset that comes preloaded as a complete solution.