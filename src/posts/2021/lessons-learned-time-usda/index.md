---
title: Lessons learned from my time at USDA
desc: I have accepted a new position at Asurion. Here is my retro from my time at USDA!
date: '2021-07-26T08:01:36.130-05:00'
lastmod: '2021-07-26T08:01:36.130-05:00'
tags:
  - career
  - retro
---
First, let me announcement that I have accepted a position as Senior SharePoint Developer, working at Asurion. I'm excited about the project and the opportunity. I'm sad to be leaving the team at USDA. They're a great team and I can't say enough nice things about them. I had the chance to experience Agile to the fullest, deep dive into Power Platform and work on some really cool and interesting projects. It's a great team and I'm going to miss working with them.

When I left Bridgestone, I reflected and came up with some [lessons learned from my time at Bridgestone](/posts/lessons-learned-from-bridgestone/). Let's see how well I've done at implementing these lessons as organizing principles for my professional development. Here is my retro from my time at USDA!

## Lesson 1 - Savings

Grade: C

When I left Bridgestone, I had a lot of debt and little savings. For me, being in such a situation felt limiting.

I've managed to keep about three months of expenses in savings but I wanted six. I give myself a C because I've not been as focused on this goal as I could have been. As I said in the other post, my ultimate goal is to have six months savings as I think that amount of cushion would truly give me the amount of freedom I'd want if I need to leave a role without having something else lined up or in the event of a short notice layoff. 

## Lesson 2 - Work Relationships

Grade: A

I give myself an A in this category as I feel that I have been successful at being mindful and genuine with my work relationships. It's important for me that I'm engaging and authentic with people on my team but also people outside of my team including my manager's manager.

## Lesson 3 - Professional Relationships outside of work

Grade: A

Again, I've tried pretty hard to be mindful, engaging, and genuine with these relationships as well. LinkedIn makes this easier than it would be otherwise.

## Lesson 4 - Resume

Grade: A

Giving myself an A on this even though my initial idea was to do an end-of-the-week type review. I did update my Resume at around the 6 month mark and I think that's a great time frame to reflect and update the resume and accomplishments.

## Lesson 5 - LinkedIn

Grade: B

I get slammed with recruiter emails/messages. Lots of them are from farming outfits and I get the same job from like 4 or 5 different recruiters in a short window. I generally ignore those but with messages from individuals, I try pretty hard to message people back though I've not been perfect.

## Lesson 6 - GitHub

Grade: C-

I was not successful at keeping samples of my script posted up on GitHub. I found that the sanitation process increased the friction of doing this so I kept putting it off. PowerShell scripts for doing SharePoint work will include site urls and such and I don't like posting those for the world to see. I did end up setting aside some time to do this and have now dumped my snippets to my GitHub PowerShell repository but I need to get better at this.

## New Lesson - Documentation

Once I gave my two week notice, I went straight into documentation mode and I kind of hate that. I wish I had been documenting processes and projects as if I was about to leave from the start.

## Conclusion

So overall, I'll give myself a B for implementing my lessons as professional organizing principles. Going forward, I will endeavor to double down and try harder to implementing each of these lessons learned.

- Lesson 1: Take savings goals seriously—the more, the better. Have six months of living expenses.
- Lesson 2: Be genuine and mindful about relationships at work.
- Lesson 3: Maintain professional relationships outside of work by being intentional and genuine.
- Lesson 4: Keep my resume current regardless of my current employment status.
- Lesson 5: Build and nurture my professional network with LinkedIn including recruiters.
- Lesson 6: Adjust development workflow to include GitHub.
- Lesson 7: Document processes and projects like I'm about to leave.
