---
title: Example of why I find it hard to move away from iPhone
desc: 'As a long time iOS user, I trust Apple much more than I trust Android to protect my privacy'
date: '2021-12-14T06:57:01.884-06:00'
tags:
  - mike-breaksup-with-apple
  - apple
  - privacy
  - pixel4a
  - calyxos
  - android
slug: why-its-hard-to-move-away-from-iphone
---
Recently, I've started to return to my Pixel, leaving my iPhone on it's charging station in my office. This week, I'm planning to see [Whitechapel](https://whitechapelband.com/) play in Knoxville, TN. That's about a 2.5 hour drive from my house so I'm planning to stay the night. The problem is, I plan to park at the motel then hail a Lyft to the venue and back, so I need to install the Lyft app.

Additionally, I'll need to reliably navigate to the hotel from my house so that means I need a reliable maps app. I want to love apps powered by OpenStreetMaps but I've found the map data not complete. I'm often unable to find things as simple as a street address using Organic Maps. That means, if I want to use my [CalyxOS phone](/posts/2021/08/16/impressions-of-calyxos-after-two-weeks/), I'm probably going to need to use Google Maps. Oof. I *hate* the idea of purposefully installing Google apps on my phone.

I've tried using the web site version of both and in both instances, I think they probably handicap that version on purpose so that people must to download the apps.

I feel stuck with this not great choice. On the one hand, I love that CalyxOS is a Google-less experience and I'm able to install F-Droid applications and feel like I'm in more control. On the other, I'm less confident that an Android OS is protecting me from applications harvesting the information from my phone and sending it back to their mother ship. I think I have this dogmatic view that iOS is better at protecting my privacy from 3rd parties but maybe that trust is somewhat misplaced?

What is a privacy minded person to do? I could call a cab like it's 2006, instead of using Lyft. I could also get myself a map of Knoxville and manually navigate to the hotel. I hate that I have to not use technology if I care about privacy. *Not using technology* is not the future that I imagine nor want.

I think I'm going to allow myself to use those apps for the edge cases. When I don't need to navigate out of town or hail a ride, then I'll uninstall those apps. Not allowing myself these types of exceptions is what [caused me to go back to my iPhone](/posts/2021/11/23/ok-i-give-up-im-back-on-my-iphone/) in irritated exacerbation at the state of surveillance capitalism and technology.
