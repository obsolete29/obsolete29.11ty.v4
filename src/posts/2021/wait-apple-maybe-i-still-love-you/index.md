---
title: 'Wait Apple, maybe I still love you'
desc: 'Last week I posted I''m breaking up with Apple in response to Apple''s plans to scan devices for banned content. I''ve had some time to sit with my feelings and process them, so I wanted to update my thoughts.'
date: '2021-08-09T11:00:17.999-05:00'
lastmod: '2021-08-09T11:00:18.000-05:00'
tags:
  - apple
  - privacy
  - mike-breaksup-with-apple
---
Last week I posted [I'm breaking up with Apple](https://obsolete29.com/posts/breaking-up-with-apple/) in response to [Apple's plans to scan devices for banned content](https://www.theregister.com/2021/08/05/apple_csam_scanning/). I've had some time to sit with my feelings and process them, so I wanted to update my thoughts.
 
I overreacted about the news that Apple is going to scan images on phones. It turns out, that Apple is only going to scan images that are being uploaded to iCloud. I've always known that iCloud photos were not end-to-end encrypted, and Apple could turn over my photos in the event they were presented with a warrant to do so. Theoretically, Apple could just scan my photos in the cloud on their servers. Apple engineers instead want to scan images locally, as they're being uploaded to the iCloud. I can see the logic of their decision here... if you're going to scan photos, do it locally on the user's phone to protect as much information as possible. Plus, maybe they're doing it this way in preparation for enabling end-to-end encryption on iCloud photos? I can still opt out of all this by simply not uploading my photos to iCloud, the same as always.
 
The iMessage part of the CSAM news is a bit more troubling. I know they're saying that this is only enabled in specific cases: for child accounts on a family iCloud account when the child is under 12. The problem in my mind is that they're building the hooks to analyze iMessages for content. The toggles are set a certain way by Apple but they could be set another way if they choose. I have no doubt in my mind that [APT](https://en.wikipedia.org/wiki/Advanced_persistent_threat) actors are chomping at the bit to hack into this framework. There's no such thing as a secure back door.
 
Overall, I think I probably had an emotional overreaction, but I'm still committed to divesting myself from being so heavily locked into one platform for my technology needs. My Pixel 4a is still on the way. I'm typing this on an instance of Ubuntu on VirtualBox on my MacBook Pro. I'm sorry, Apple; maybe we can still be friends for a while, but I think we should see other people to see what else is out there.