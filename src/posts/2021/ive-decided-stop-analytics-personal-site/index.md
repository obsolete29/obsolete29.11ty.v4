---
title: I've decided to stop using analytics on my personal site
desc: 'I obsess about the analytics in an unhealthy way, plus this aligns better with my values. Win/win!'
date: '2021-10-03T06:35:53.715-05:00'
tags:
  - obsolete29
  - privacy
---
I've decided to stop using [Fathom Analytics](https://usefathom.com/) on this site. I've not been unhappy with the service, the price seems fine, and their privacy policy lines up well with privacy minded people and organizations that have an analytics requirement.

I just want to not care about how many people are looking at my site. I can obsess about that metric in an unhealthy way in the same way I craved likes and such on social media. It's a bit unhealthy for me if I'm honest. I just want to write my posts, send them out into the universe and move on.

Plus, making this move aligns my actions closer with my values in regards to online privacy.
