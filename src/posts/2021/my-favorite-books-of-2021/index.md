---
title: My favorite books of 2021
desc: 'According to my notes, I''ve read 38 books in 2021. Here is my list of favorites for the year.'
date: '2021-12-29T08:21:12.054-06:00'
tags:
  - bookshelf
  - 2021-reading-goals
  - year-in-review
slug: my-favorite-books-of-2021
---
According to my notes, I've read 38 books in 2021. So without further ado, here is my list of favorite books for this year, in chronological order. 

*Note on ratings:  I use the Netflix scale. 1 hated, 2 didn't like, 3 liked, 4 really liked, 5 loved.*

## [The Palladium Wars Series](https://www.markokloos.com/?page_id=2105) by [Marko Kloos](https://www.markokloos.com/) 
My favorite genre is military science fiction. I love it when the ships, robots, cyborgs and aliens fight. The writing in this series feels pretty basic to me but I like it. There's no unique types of tech or ideas in the series, it's just a fun easy read and I cared about the characters, even if they were a bit on the shallow side. 4 stars.

## The Bobiverse Series by [Dennis E Taylor](http://dennisetaylor.org/)
I re-read the first three books in the series in preparation for reading the fourth book. This was the third time reading the first three and I still really like the series. While this isn't strict military science fiction, it's still lots of science fiction and a good bit of conflict. 5 stars.

## The Space Between Worlds by [Micaiah Johnson](https://www.micaiahjohnson.com/)
Science fiction but not space based or military based. Cool new ideas and concepts that I normally don't read. It's a great book and I hope Micaiah plans to write more in this series. 5 stars.

## The Murderbot Diaries by [Martha Wells](https://marthawells.com/)
Oh man. Yes, I love military science fiction but if you really want to get me going, give me some sentient AI characters and I'm in love. Again, these are pretty short easy reads but I laughed out loud quite a lot. Great books. 5 stars.

## The Soldier by [Neal Asher](https://www.nealasher.co.uk/)
Very far future military science fiction. Bigger book and was a bit harder to get into but once you do, it's great. Lots of aliens and sentient AIs. 5 stars.

## [Nomad](https://jswallow.com/book/nomad/) by [James Swallow](https://jswallow.com)
I occasionally have to take a break from the space fighting robots. Nomad is a special operations story, smiliar to Jason Bourne. I enjoyed it. 4 stars.

## [Atticus Priest Series](https://markjdawson.com/books/atticus-priest/) by [Mark Dawson](https://markjdawson.com)
These two books were fun reads for me. It's a detective/cop story that's fun to read. 4 stars.

## Vatta's War Series by [Elizabeth Moon](http://www.elizabethmoon.com/)
The first book in this series took me a loooong time to hook me but I'm glad I stuck with it. I saw someone else refer to them as *low stakes competnecy porn* and I thought that was an interesting way to describe it. No ground breaking or unique ideas here. Just a coming of age story of a young ship captain as she deals with adversity. 4 stars.

## The Scholomance Series by [Naomi Novik](https://www.naominovik.com/)
I think this genre is called urban fantasy which I take to mean modern magic. I'm sure Naomi hates it but I'd call this Harry Potter for adults. I really enjoyed reading the first two books and look forward to reading more. 5 stars.

