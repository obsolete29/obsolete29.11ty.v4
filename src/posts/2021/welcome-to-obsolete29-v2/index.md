---
title: Welcome to obsolete29.com v2.0!
desc: “I have decided to rebuild my personal site. I wanted a more minimlist look and feel. Here is what I did!”
date: 2021-07-11T00:00:00.000Z
tags:
  - obsolete29
  - obsolete29v2
  - eleventy
  - webdev
---
Back in December of 2020, I [built my site](/posts/welcome-to-obsolete29-v1/) using [Eleventy](https://www.11ty.dev/) as an exercise in teaching myself web development. I learned a lot during that process but I wanted to try out a more minimlist look and feel for the site. Additionally, the first time thru on a couple of the items felt a little sloppy and I felt I could do it better. Here are the things I'm trying to keep in mind as I rebuild my site:

1. I want a more minimilist look and feel for my site.
2. I still want the site to be lightning weight fast and light weight.
3. I think there's some built in friction when posting blog posts on a static site. Even though I'm writing the posts in markdown, I still have to use a specific template and very specific shortcodes. The template and shortcodes are complicated enough that I've had to build a little template notes file that I can reference and use when writing new blog posts. Anyhoo, I want to try to reduce that friction by simplying the shortcodes and templates.
4. I'm once again practicing my web development skills by rebuilding the site. The markup and styling is so minimlist this time around that it's giving me an opportunity to focus on node and javascript.

I won't have as large of a write up this time around as I basically followed my own guides on the topic of building v1 of my site with Eleventy. I have improved upon using [eleventy-img](https://github.com/11ty/eleventy-img) to serve optimized and responsive images. Additionally, I believe I have a cleaner, less hacky approach to generating the open graph social sharing images though. Expect write ups of both of those improved processes.

Ok, that's it for now. Thanks for reading!
