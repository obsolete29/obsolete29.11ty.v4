---
title: 'My online account creation workflow is very convoluted, for privacy!'
desc: Privacy is not convenient. Here is my workflow for creating new online accounts.
date: '2021-09-28T07:39:44.809-05:00'
lastmod: null
tags:
  - privacy
---
In my head there's a sliding scale. On one side is convienance and on the other is privacy and security. This morning, Rachelle came into my office and asked me to book an appointment for this thing we wanted to do. It required that I create a new account on Schedulicity. Doing this exercise while she stood there waiting for me to book it highlighted how convoluted my process actually is. Here is my workflow.

## Email address
Ok so the first thing that every online service wants is a dang email address. I use a different email address for nearly every online service I sign up for. I do this because I'm trying to resist the data aggregators and surveillance capitalism. I do not want my account at schedulicity associated with any other accounts.

My workflow and formula for generating an email address is this:

1. I have a bookmark to Random.org and it generates a 6 character random string of letters and numbers so I open that bookmark and grab the string.
2. I start with the website address, so in this instance schedulicity and I add a period plus the random string. Result: schedulicity.k0zd3u
3. I have registered a domain with a random'ish name that I won't share here. Instead, let's just substitute *randomdomain.org*. I've set it up to recieve emails at this domain. So, the email address I enter to register becomes *schedulicity.k0zd3u@randomdomain.org*.

Automated data aggregation will be a lot more difficult plus the unique email addresses act as a hacked canary. If I start receiving spam at one of my uinque email address, then I can know my email has been shared or hacked and I know the online service.

## Password
Ok I have to enter a password no problem. I just bring up my handy dandy password manager, KeePassDX and generate the password. Oops, don't forget to grab the email address that was generatered to plug it into the new entry.

## Phone
They also wanted a phone number. Ugh. Of course they do. Phone number is key for data aggregators so let's try to resist them at this step as well.

I have an account at [OpenPhone.co](https://www.openphone.co/) with a few numbers for myself. They market themselves as a business phone service but I use it as a google voice replacement. I picked one of the numbers at random and plugged that in.

## Credit card
I had to pay to reserve the appointment so they ask for a credit card number. I use [Privacy.com](https://privacy.com) to generate unique credit card numbers for each online vendor.

From a security perspective, I think we should avoid using debit cards online or really anywhere IMO. I no longer use mine to pay for anything, anywhere; I only use it as an ATM card. I know they're protected the same as other Visa/Mastercards but that's *your money* that's missing while you file claims and get the charges reversed. If fraud happens with a credit card, well that's not that big of a deal as that's the *credit card issuers* money and not yours. File the claims and report the issue and it's no skin off your teeth if they're slow about it as it's *their* money that's missing.

I've not done a lot of research on this topic but I feel that credit card numbers and purchase history is another data point for data aggregators. Plus, I think there's a security component to using seperate virtual credit card numbers. If a company has a security breach and my payment information is stolen, I only have to cancel the one card I used at that one vendor.

So, I open Privacy.com and generate a vendor specific card to credit card.

Done!

## Conclusion

It took me a few minutes to go through this process. Rachelle was kind of rolling her eyes as I flipped through screens and grabbed this bit of info and that bit of info and I do think it's pretty convoluted. I don't really bother to signup for anything on my phone because while it's possible, it's just harder on a touch screen.

I know there are online services that help with these types of things but I think I'd rather have an app on my computer that hooks into Privacy.com and can help me generate and display all the bits of information on a single screen. If I were a better developer, I'd build an application that could do this:

- Allow me configure my custom domain and email generator formula.
- Include a password generator.
- Include a username generator.
- Hook into the Privacy API and display a new credit card number if I told it to.
- Allow me to configure my availble phone numbers for use in account creation.

So when I open the app, maybe I paste in the URL of the site, check a few check boxes and click the generate button. That would then display all the info I needed to register and save on a single screen. I could then copy and paste the appropriate things into the appropriate places.

Do you do anything to try to protect your privacy when creating online accounts? I'd be curious to learn about other's workflows on this as well.
