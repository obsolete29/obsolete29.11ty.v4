---
title: My blog is now hosted on Capsul as a VPS
desc: I wasn't unhappy with the performance of GitHub Pages but I felt Capsul aligned better with my organizing principles.
date: '2021-09-05T15:15:11.459-05:00'
lastmod: '2021-09-05T15:15:11.464-05:00'
tags:
  - obsolete29
---
I wasn't unhappy with the performance of GitHub Pages, my previous host. It was free and well supported and documented-- lots of other developers used it for their personal sites. I just didn't feel the platform aligned with my organizing principles any longer. GitHub is owned by Microsoft, and it's an abstraction that runs on top of some big cloud providers cloud infrastructure. This move is certainly related to my [divestment from Apple](https://obsolete29.com/topics/mike-breaksup-with-apple/) in my mind.

My site now runs on a VPS hosted by [Capsul](https://capsul.org/). It's described as _simple, fast, private compute by [cyberia.club](https://cyberia.club)_. That's music to my ears and it fills my heart with joy. I was also hoping for some commitment to green, sustainable energy but I'll take simple, fast and private if I can't have sustainable.

This is my first experience with hosting a website using a VPS. Once I requested the VPS, I was able to get up and running within a day. Most of that time was just me figuring out how to do things in this environment. At a high level, I performed these tasks:

- Setup the firewall and allow OpenSSH
- Install Apache and allow access through the firewall
- Setup Apache for virtual hosts so I can host multiple sites on my VPS.
- Install SSL certificates using Let's Encrypt.
- Setup rsync so I can upload my site files.
- Configure custom 404 page.
- Configure rewrite rules in Apache to redirect http to https and to redirect www requests to non-www requests.

I've been [using Linux as my daily driver](https://obsolete29.com/posts/2021/08/31/macbook-pro-user-tries-linux-as-daily-driver/) now for almost three weeks so working in this environment seemed really straight forward for me.

I'm excited about Capsul and I feel like I'm living up to my principles, which is a nice feeling.