---
title: The Tech I Want in 2022
date: '2021-12-30T04:44:11.668-06:00'
tags:
  - big-tech
  - privacy
  - apple
  - google
slug: the-tech-i-want-in-2022
---
*I'm sitting at my computer at 3:45am after waking up thinking about work. Coffee is brewing and the cats are sitting on the corner of the desk.*

The NYTimes article, [The Tech I Want in 2022](https://www.nytimes.com/2021/12/21/technology/tech-innovations-2022.html) came across my feeds and it got me thinking about the tech I want. Ms Shira wants a robot to do her laundry and clear her clutter. That's cool because I'd like that too. The problem of course is that the stupid thing would be designed in such as a way so that it wouldn't work when there's a [hiccup with AWS](https://www.theverge.com/2021/12/22/22849780/amazon-aws-is-down-outage-slack-imgur-hulu-asana-epic).

But more than that, I want technology that enables users to have access to the internet that's free from the threat of corporate capture. I posted a [hot take](https://indieweb.social/web/@obsolete29/107456463188554064) about government grants to the non-profit Mozilla foundation over on Mastodon and that resulted in a lot of discussions. The nerds have strong opinions on such things it turns out. The web is too big and distributed for it to be owned by any single company. But what I observe big tech doing is trying to capture the portals to the web and becoming the gatekeeprs and the keyholders.

## Web browsers

Google, Apple and Microsoft account for 87% of all browsers used on the web. Firefox is fourth with 3.2%. It's vital that there's a browser option that isn't a big tech controlled browser, feeding their ad machines. Firefox is non-profit, free and open source but they still need money to run their operations. Recently, Mozilla has been reaching for the [tried and true methods of monetizing and that's ads](https://www.theverge.com/2021/10/7/22715179/firefox-suggest-search-ads-browser). They also take money from Google so that Google is set as the default search engine. Some government grants, in the name of public good and a free/open internet could go a long way to help.

## Search

Which brings us to another big problem: Search. DuckDuckGo has become more popular as the privacy search engine for some reason but [we can do better than DuckDuckGo](https://drewdevault.com/2020/11/17/Better-than-DuckDuckGo.html). I think there's an opportunity for a free and open source search engine, complete with it's own crawlers, run in the same manner as Wikipedia and funded by the public. It could be another brake check of big tech rushing to control all the entries to the web.

## Mobile

A mobile phone is the only way lots of people access the web these days. I'm only aware of a single, non-Apple or non-Google phone option that a non-technical user can buy and that's the phone from the [e Foundation](https://e.foundation/). There are other options if you're a technically minded person and want to mess around with loading custom ROMS on an Android phone but that's really the only one that I know of. The problem here is that nearly everyone who is interested in using a phone from a company other than Apple or Google probably can load a custom ROM. So who is this phone for? I think it's a great option for someone who doesn't care about social media and just wants a phone to use as a phone and to browse the web on occasion.

The problem is that no one cares about big tech capturing the web. They just want to download Facebook, TikTok, Instagram etc. Ok that's fine. Whatever. But, the fact that no one cares now is the main reason that the government could help. They could help by giving some grant money to the non-profit foundations and orgs that run, organize and advocate for free and open access to the web, free from corporate interests if a person so choose. They could also use the force of law to compel big tech. We already have laws against monopolies and anti-competitive and anti-consumer practices.

What I really want though is cool Augmented Reality glasses that would overlay a digital layer on top of the physical world, similar to the tech in [Daemon](https://en.wikipedia.org/wiki/Daemon_(novel_series)). I'm sure big tech is going to fuck it up.
