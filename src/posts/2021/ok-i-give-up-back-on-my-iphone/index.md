---
title: 'Ok, I give up - I''m back on my iPhone'
date: '2021-11-23T06:32:19.844-06:00'
tags:
  - mike-breaksup-with-apple
  - apple
  - privacy
---
I've written a lot about my decision to [break up with Apple](/topics/mike-breaksup-with-apple/) so it's only fair that I come back here to eat crow. 

As of October 26th, I switched the SIM card out of my Pixel and back into my little iPhone SE. I still plan to use Linux as my desktop, depending on how hard it is to make things sync between iOS and my Linux computer. I also reloaded macOS on my 2015 MacBook Pro.

After two months of trying to avoid Google and Apple, I'm waving the white flag in a temporary truce. I think I tried for a bridge too far, in that I was trying to change everything at once. I was trying to do so much to avoid big tech, ad tracking, data collectors that I found myself in a position that I couldn't use technology or the options I was trying to use simply did not work.

The pessimistic part of me feels big tech has successfully captured the Internet. If you want to participate, then you have to agree to be tracked, farmed and captured. Trying to take the third path, means you're going to be a second class citizen. 

I plan to write more on this topic once I've fully sorted out my thoughts and feelings on it. I feel like I'm failing but right now, I just need something that works. I reserve the right to rest, gather my strength and try again.
