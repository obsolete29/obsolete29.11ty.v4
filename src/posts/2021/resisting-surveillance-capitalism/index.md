---
title: On resisting surveillance capitalism and why it is important
desc: I believe resisting surveillance capitalism is important and here are some of the things I do.
date: '2021-07-30T08:57:20.851-05:00'
lastmod: '2021-07-30T08:57:20.852-05:00'
tags:
  - privacy
slug: resisting-surveillance-capitalism
---
I find that many of my friends don't care about privacy. For them, trading their information for access to an app or service is a fair trade. Google, Gmail, Chrome, Facebook, Instagram, otherwise "free" apps are worth it and they seem to have no qualms about using these products. I've seen this referred to as [surveillance capitalism](https://en.wikipedia.org/wiki/Surveillance_capitalism) and it's 100% fitting. For most people, I think it's hard to understand why surveillance capitalism is bad. That just means you get better ads on Instagram, right? Wrong. It can also have a real impact on your life and career when that data is used to reveal something that should be private.

Consider the [recent case of Jeffrey Burrill](https://www.washingtonpost.com/religion/2021/07/20/bishop-misconduct-resign-burrill/), a Catholic priest who had his use of Grindr publicly outed by [The Pillar](https://www.pillarcatholic.com/p/pillar-investigates-usccb-gen-sec).

> “A mobile device correlated to Burrill emitted app data signals from the  location-based hookup app Grindr on a near-daily basis during parts of  2018, 2019, and 2020 — at both his USCCB office and his USCCB-owned  residence, as well as during USCCB meetings and events in other cities,” the Pillar reported.
>
> “The data obtained and analyzed by The Pillar conveys mobile app date signals during two 26-week periods, the first in 2018  and the second in 2019 and 2020. The data was obtained from a data  vendor and authenticated by an independent data consulting firm  contracted by The Pillar,” the site reported. It did not identify who  the vendor was or if the site bought the information or got it from a  third party.
>
> The Pillar story says app data “correlated” to Burrill’s phone shows the priest visited gay bars, including while traveling for the USCCB.
>
> -- <cite>[The Washington Post](https://www.washingtonpost.com/religion/2021/07/20/bishop-misconduct-resign-burrill/)</cite>

Grindr is a "free" application that anyone can download and use so how do they make money? The most common way for these apps to make money is to show you ads. Another really common way is to [sell your data to data brokers](https://www.buzzfeednews.com/article/nicolenguyen/how-apps-take-your-data-and-sell-it-without-you-even). I'm 100% certain this is what happened with Jeffrey. Grindr sold his data to a data broker, which was then obtained and de-anonymized by The Pillar.

> The Grindr app and similar hookup apps use mobile device location  data to allow users to see a listing of other nearby users of the app,  to chat and exchange images with nearby users within the app, or to  arrange a meeting for the sake of an anonymous sexual encounter. 
>
> Commercially available app signal data does not identify the names of app users, but instead correlates a unique numerical identifier to each mobile device  using particular apps. Signal data, collected by apps after users  consent to data collection, is aggregated and sold by data vendors. It  can be analyzed to provide timestamped location data and usage  information for each numbered device. 
>
> The data obtained and analyzed by *The Pillar* conveys mobile app data signals during two 26-week periods, the first in 2018  and the second in 2019 and 2020. The data was obtained from a data  vendor and authenticated by an independent data consulting firm  contracted by *The Pillar*.
>
> *The Pillar* correlated a unique mobile device to Burrill when it was used consistently from 2018 until at least 2020 from the USCCB staff residence and headquarters,  from meetings at which Burrill was in attendance, and was also used on  numerous occasions at Burrill’s family lake house, near the residences  of Burrill’s family members, and at a Wisconsin apartment in Burrill’s  hometown, at which Burrill himself has been listed as a resident.
>
> -- <cite>[The Pillar](https://www.pillarcatholic.com/p/pillar-investigates-usccb-gen-sec)</cite>

Even closeted gay priests deserve privacy. Jeffery was using Grindr with the expectation of privacy and I don't think he was wrong to expect that.

These are some things I do to resist surveillance capitalism.

## Be the customer not the product

In general, I try very hard to pay for the products and services I use. Additionally, try to look for those products and services that are privacy minded. I use Fastmail and pay for the right to do so for example.

We pay for the products or services we use, one way or the other. Instagram doesn't charge you money for an account but you pay with your time and attention to the app, which serves ads and sells, who knows how much of your data.

## Don't use the app

Apps on our phones [can collect a lot information about us](https://www.nytimes.com/interactive/2018/12/10/business/location-data-privacy-apps.html). In Jeffery's case, Grindr was collecting location information and then selling that to data brokers. Our phones are able to uniquely identify us so even when the data is "anonymized", it's possible to reveal and identify individuals. Some applications were recently [caught collecting the contents of the clipboard](https://www.phonearena.com/news/more-iphone-clipboard-snoopers-surface_id125733) of users phones.

When possible we should use the services website instead of downloading their application. The amount of data they're able to collect about us is limited with the website. 

If you have to use the app, lock down it's permissions as much as possible or consider uninstalling it when you're not using it. Apps that I install when I need to are Walmart, Lyft, AirBnB.

## Use a VPN

A big piece of the information used to identify where we are and who we are is the IP address associated with our phones. A VPN allows you to connect to the Internet while trying to mask your real IP address. If I live in Chicago, but I connect to a VPN in Atlanta, many services will think I'm in Atlanta. This happens often to me when I'm shopping online at Lowes and Best Buy. Both websites try to pick your "local" store. Wrong assholes! I'm not in Kansas! Muahaha!

It also helps to mask your Internet activity from your service provider be it Verizon, Comcast or the hotel/coffee shop wifi so that *they* can't sell your data to data brokers.

I use Mullvad but check out [The Best VPN Service](https://www.nytimes.com/wirecutter/reviews/best-vpn-service/) at Wirecutter to read about all their recommendations.

## Use different email addresses

When signing up for services, use different email addresses. It's one less data point that can be used to cross reference and identify you as an individual in a data set. 

I have a pretty complicated and convoluted system to generate my email addresses but I don't expect non-tech people to try this. I've read a lot about [MySudo](https://mysudo.com/) so check that out if you're interested. They allow you to generate alternate emails and phone numbers.

## Use different phone numbers

The phone number field is optional for some services so opt out of providing a number if you can. When possible, try to use a virtual number instead of your actual number. I use [OpenPhone](https://www.openphone.co/) but there are other, non-google options (like MySudo!). I actually have two virtual numbers that I rotate through when providing login information. Sometimes services won't accept virtual numbers so then you have to decide if providing your actual number is worth it.

Again, I'm just trying to reduce the amount of data points that the automatic collation routines can use to identify me.

## Conclusion

There's no single approach to privacy unfortunately, short of being a luddite, eschewing all technology. I use a defense in depth approach to resist the automatic collation and identification of my personal records. I think there's some value in resisting and I encourage you to do so.

What are some things you do to resist surveillance capitalism? What do you think about my tips? Do you have any others? If you have any thoughts, shoot me an email and let me know!

Ok, that's it for now -- thanks for reading!
