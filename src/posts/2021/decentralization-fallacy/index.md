---
title: The Decentralization Fallacy
desc: 'When finding the path forward from our tech overlords, decentralization isn''t enough.'
date: '2021-10-20T07:25:21.096-05:00'
tags:
  - decentralization
  - mastodon
---
Even though [Alyssa Rosenzweig](https://rosenzweig.io/) wrote about [The Federation Fallacy](https://rosenzweig.io/blog/the-federation-fallacy.html) two years ago, I just read it recently and it's stuck with me because I think she's 100% correct. Even if you don't agree, please go over and read the whole post.

I'm new to breaking up with big tech and the related online communities interested in decentralization. I've noticed that there's a fervent belief in decentralized Internet systems as a way forward from our tech overlords. Just this morning in fact, someone posted *never have I appreciated more that Git is decentralized* [over on Mastodon](https://fosstodon.org/@be/107105137317042638). *Decentralization* seems to be a dogmatic mantra that true believers chant anytime one of the big tech companies have an outage. I've come to expect approximately 1 million different versions of the same snide comments about centralized systems when there's any sort of outage. 

Alyssa calls the decentralized model, *information anarchy*. The term that occurred to me was *information libertarianism* and as a progressive, I don't mean that in a good way. The dream being that we all run and own our own servers, services and information. If you don't have the know-how to setup web servers and such, then you'd rely on friends or family who do know how. Neat. Decentralization gives the whole, federated, decentralized system high availability because only a piece of the whole is down should any particular instance go down for some reason. Furthermore, decentralization makes it harder to tap or monitor the system as a whole. 

The problem is this model isn't accessible to everyone. I believe there are non-technical people in the world who are interested in not having their information farmed by big tech. What are they to do? Enter the flagship instances of the federation model. Alyssa says that online systems move towards centralization naturally, and I believe that to be true. The most well known decentralized, federated example is Mastodon. But even with Mastodon, 50.8% of the user base is on just three instances. I presume those are 2019 numbers but I wonder if that's changed much since then? 

Instead of information anarchy, Alyssa suggests *information democracy* for the masses. Her example of this as a working system is Wikipedia. I'm probably projecting here but I feel that when people say *decentralization* they actually mean *not a big tech company who is running their thing for maximum profits*. One way to resist and avoid big tech is to run you own instances of software. Another way is the Wikipedia path though. I think it makes a lot of sense for a combined model to exist. 

In the example of Mastodon, having flagship instances, that work as information democracies, while still allowing federation for those who want to run their own instances is pretty much *the* way forward. I don't think it's an argument of centralized vs decentralized. Systems tend to centralize so why not allow for that? Let's have *both*! The masses who are unable or unwilling to run or pay for their own instances can just have their accounts on the flagship instances, which are run in a manner consistent with information democracy. Decentralization for decentralization's sake isn't enough.
