---
title: Common pitfalls of low-code solutions with Power Platform
desc: My thoughts on what to look out for when enabling Citizen developers to use low-code solutions.
date: '2021-07-28T17:37:13.163-05:00'
lastmod: '2021-07-28T17:37:13.164-05:00'
tags:
  - power-platform
  - o365
  - sharepoint
---
Microsoft has really been pushing Power Platform as a no-code/low-code solution for businesses and the enterprise. The pitch to businesses being, the business users (ie., "[Citizen developers](https://www.microsoft.com/en-us/itshowcase/citizen-developers-use-microsoft-power-apps-to-build-an-intelligent-launch-assistant)") can build processes themselves and you don't need to hire those expensive developers and engineers to build it for you. 

Another benefit of no-code solutions is that it's quicker to build something. If you have a teammate who works with you in accounting, and they're smart enough to use a search engine, then they can probably figure out how to make Power Platform work -- it's a blessing and a curse.

Or to put it another way, the great thing about SharePoint/O365 is that business users can build their own solutions. The bad thing about Sh[arePoint/O365 is that business users are building their own solutions.

As a SharePoint developer/engineer and old school sysadmin, here are the common pitfalls I've seen with no-code/low-code solutions.

## Business continuity

If Nancy from accounting builds a cool little PowerApps form and a Power Automate Flow to go with it, those applications use Nancy's AD credentials to make those connections to the SharePoint sites, Outlook mailboxes, whatever. What happens when Nancy leaves the company? Inevitability her AD account will be disabled as part of the off-boarding process and the forms/workflows will stop working. Now, the business needs to engage with IT to come and figure out why the fiscal quarterly closing form and process are no longer working. Oh, and the business is having a critical outage so now engineers are scrambling to fix it.

IT professionals will know that these processes need to be setup to run with service accounts. Furthermore, they will understand the importance of securing the service account credentials and the proper way to do that.

## Support / Separation of concerns

Nancy, being an accountant and not an IT professional will probably just build her Flow as one giant flow that does everything she needs. I've seen this approximately 1 million times. This is a problem for Power Automate because Flows *can* be too big and they *can* take too long to run. Plus, it's just *harder* to make changes to one large Flow.

IT professionals will understand the importance of breaking up large processes into smaller, distinct processes. With Power Automate this would be to create multiple, smaller, and more manageable Flows instead of one large Flow.

## It's not really their job

Nancy has a job to do already... it involves spreadsheets and other accounting things. If Nancy is trying to build a form and workflow to handle some part of her workgroup's repetitive tasks, what happens if she runs into a problem and she can't figure it out? What if she get's stuck half way through building the Flow? How much time do we expect Nancy to spend on searching the web to figure out what "claims" is or the proper way to call a REST API using Power Automate Flow -- or even what a REST API is?

Nancy now has to engage with the IT department to help her with this especially complicated part and maybe that help isn't so forthcoming because she hasn't submitted the proper project request forms.

## Conclusion

I think the biggest draw for no-code/low-code solutions is that the business doesn't have to engage with an inefficient, slow IT apparatus to get help with doing something. I think it's critical that the business can get help quickly so that's on us, the IT professional to improve upon. We have to reduce the hurdles and the friction of getting help.

I think citizen developers building their own applications with Power Platform should probably be limited to workgroup level applications. I'm not against no-code/low-code in general, I just think IT professionals will know how to build these applications and processes to enable the business to be successful with the minimal amount of disruptions. 

Take a little more time up front to build the solution correctly and you'll end up with a lot less headache down the road.

Ok, that's all for now -- thanks for reading!

