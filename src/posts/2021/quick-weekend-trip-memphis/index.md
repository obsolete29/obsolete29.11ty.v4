---
title: Our quick weekend trip to Memphis
desc: 'To celebrate our two year anniversary, Rachelle and I decide to take a weekend trip to Memphis, in lieu of gifts. Here is my trip report.'
date: '2021-08-04T09:12:47.418-05:00'
lastmod: '2021-08-04T09:12:47.419-05:00'
tags:
  - trip-report
  - memphis
---
{{ desc }}

## Accommodations

We used AirBnB to rent a [1BR Apt](https://www.airbnb.com/rooms/43931227) located in the [historic Exchange Building](https://en.wikipedia.org/wiki/Exchange_Building_(Memphis)). The rate was affordable ($64 a night at the time of this writing) and it's centrally located.

The apartment didn't have central air conditioning. Instead, it has these little floor standing units that had a plastic hose sucking air from outside -- one in each, the living room and the bedroom. These units chugged at full blast the entire time we were there and it was still pretty stifling. I'm speculating, but since the building is a historic building, they haven't upgraded the windows so I felt there was almost no insulation to them. If the windows were updated, I believe the little AC units would have been fine. Hanging out in the living room wasn't very pleasant and the bedroom barely got cool enough to sleep comfortably.

I think this would be a great location to stay if it weren't near 100º F. Otherwise, avoid staying here during the hottest summer months!

## Day 0

We arrived and dumped our bags in the room by around 9pm and decided to find a bar to officially start our weekend. The reviews of our room on AirBnB recommended [Bardog Tavern](https://www.bardog.com/) which was just around the corner and we were pretty happy with the service. We drank, ate, watched some swimming events at the Olympics, and chatted up the bartender.

I think I'm just used to Nashville drinking prices because our bill was surprisingly cheap. This place was right up our alley and would recommend.

## Day 1

We had a big day planned, which including going to the civil rights and Stax museums. Since breakfast is the most important meal of the day, we started there!

### Sunrise Memphis

Our first stop was some breakfast at [Sunrise Memphis](https://sunrise901.com/). It's fast casual dining, insofar as you go up and order at the counter, take a number and they bring your food out. From there, you self-service water and coffee. I kind of prefer to have a waiter but the food was pretty good! The biscuits were huge and the coffee was strong enough. Win/win IMO!

<figure>
  {% myImage "day-one-sunrise-memphis.jpeg", "Mike at Sunrise diner, with a serious expression, shaking pepper on his breakfast plate." %}
  <figcaption>I'm about to tear this breakfast up!</figcaption>
</figure>
<figure>
  {% myImage "day-one-mural.jpeg", "Painted street mural on the side of a parking garage. The mural is 4 stories high is a scene of memphis and it's people." %}
  <figcaption>After breakfast, we had some time so we drove around the downtown area and just did a car tour of the downtown area.</figcaption>
</figure>

### Lorraine Motel

I didn't know what to expect prior to visiting the [Lorraine Motel](https://www.civilrightsmuseum.org/) other than it was going to be an emotionally taxing, heavy time. I don't think I've processed everything from the visit yet. There's so many thoughts and emotions swirling around and I'd like to write about them in their own post. For now, I'll just say the visit was powerful and informative. If you're visiting Memphis, I think carving out 3-4 hours to visit is well worth the time and emotion energy. Here are some photo impressions.

<figure>
  {% myImage "day-one-civil-rights-museum-01.jpeg", "Stone National Civil Rights Muesum sign in the foreground with a painted mural of important historical figures in the background." %}
  {% myImage "day-one-civil-rights-museum-02.jpeg", "Museum sign, A Fragile Truce describing the history of Birmingham boycott of 1964." %}
  {% myImage "day-one-civil-rights-museum-03.jpeg", "Museum display two statues, one an African woman holding her baby on a literal auction block while a white slaver displays her for sale. Informational sign is to the right." %}
  {% myImage "day-one-civil-rights-museum-04.jpeg", "Wall sized display of the continent of Africa depicting slaves, bound and walking across the map.." %}
  {% myImage "day-one-civil-rights-museum-05.jpeg", "Museum display of a 50s era bus and the bus boycott in Birmingham. Statue of a black woman walking instead of riding the bus is portrayed in the foreground." %}
  {% myImage "day-one-civil-rights-museum-06.jpeg", "Museum statue display of the 50s civil rights diner sit ins of Birmingham. African Americans sit at the counter wile white men stand nearby trying to intimidate." %}
  {% myImage "day-one-civil-rights-museum-07.jpeg", "Newspaper display from Feburary 1956. Headlines are about race relations." %}
  {% myImage "day-one-civil-rights-museum-08.jpeg", "Mike watches a video display the museum. In the forground a display with US Marshall protective equipment." %}
  {% myImage "day-one-civil-rights-museum-09.jpeg", "Statue display of protestors holding signs, marching for equality." %}
  {% myImage "day-one-civil-rights-museum-10.jpeg", "Black and white photo of an African American holding a large sign that says register to vote." %}
  {% myImage "day-one-civil-rights-museum-11.jpeg", "A large white wreath with white stripes is placed on the second level of the Lorraine Motel where Doctor King was killed. In the foreground, 1950s era cars are still parked in front of the preserved motel scene." %}
  <figcaption>Powerful.</figcaption>
</figure>

### The Blue Monkey

The original plan was to hit up Central BBQ after visiting the museum since it's right across the street. By the time we came out at around 1pm, there was a line and it was approximately 1 million degrees so we decided to get there early for lunch on Sunday instead.

We cruised around a few minutes and found [The Blue Monkey](https://bluemonkeymemphis.com/) and there weren't many cars out front so we decided to stop in. The AC and the beer were both cold. Exactly what the doctor ordered.

<figure>
  {% myImage "day-one-the-blue-monkey.jpeg", "Selfie of Rachelle and Mike." %}
  <figcaption>So glad to be out of the dang heat!</figcaption>
</figure>

### Bass Pro Shop

After taking in the civil rights museum, we just didn't have it in us to head over to Stax and take in more information so we skipped it this time. Note to self: Only plan one main attraction per day on future visits to places.

Did y'all know there was a *giant* [Bass Pro Shop](https://en.wikipedia.org/wiki/Memphis_Pyramid) in Memphis?! On a lark, we decided to stop in and walk around. It was nothing special inside or unexpected but it was just so huuuuge.

<figure>
  {% myImage "day-one-bass-pro-shop.jpeg", "The giant bass pro shop pyrimid. The building is glass and has the bass pro shop logo on the front." %}
  <figcaption>This dang thang was huge.</figcaption>
</figure>

### Beale Street

After an afternoon nap in the stifling apartment, we caught a Lyft down to Beale Street. I think the best analogy for anyone from Nashville, is that it's a less crowded and obnoxious version of Broadway. They close the street to car traffic and you can walk around with your beer -- neat. There were sidewalk stands to purchase alcohol, street performers doing acrobatics and live music. 

We had some BBQ nachos at [The Pig on Beale Street](http://pigonbeale.com/) and they were only OK. The portion was pretty small and it was expensive. Probably would not recommend.

<figure>
  {% myImage "day-one-beale-street-01.jpeg", "A busy Beale Street at sunset. People are walking in the street and on the sidewalks and bright neon signs advertise the establishments." %}
  {% myImage "day-one-beale-street-02.jpeg", "Building mural on the side of Kings Palace Cafe depicting a hand holding a bottle of budweiser with a shadow and 3d effect." %}
  {% myImage "day-one-beale-street-03.jpeg", "Mike and Rachelle selfie with the bright, late afternoon sun shining directly on them with the Beale Street home of the blues street sign in the background. Rachelle is wearing pink heart glasses." %}
  <figcaption>Beale Street was pretty cool. Def check it out.</figcaption>
</figure>

We had a slight snafu getting back to the apartment. Obviously, we both have been drinking all evening so we were pretty lit. Rachelle wanted to catch the trolly back to the apartment but I hadn't had a chance to review the trolly map before it came. She jumped on and I was slow and before I knew it, the doors were closed and she was gone on the trolly. Oops. The apartment was only a few blocks away and we met up just before getting there. No harm, no foul!

## Day 3

We only had two items on the agenda for Sunday: Breakfast and lunch!

### Sugar Grits

Sugar grits was pretty close to our apartment, in the bottom of the Peabody Hotel so we stopped in. Not only was this the worst experience of our trip, it was probably one of the worst dining experiences I've ever had if I'm honest. It just feels like they don't know what they're doing. Perhaps they lost all their staff during the pandemic and these new people are trying to figure out how to run a restaurant? I don't know but they need more training. Our waitress, while sweet and obviously trying hard just didn't know how to wait on people. The food was bad and took forever to come out and when it did come out, it was wrong. 1/10 would not recommend.

### Central BBQ

After our horrible breakfast experience, we were looking forward to [Central BBQ](https://eatcbq.com/) as it came highly recommended and it absolutely did not disappoint. We arrived promptly at 11am, ready to eat and get on the road heading home.

Like Edley's and Martin's BBQ, it's a fast casual style experience. You order at the counter, take a number, get your drink/sauce and they bring out the food. My pulled pork sandwich, Mac n cheese and potato salad were all off the chain. Rachelle got the beef brisket and we both really loved everything about the place. 10/10 would recommend.

<figure>
  {% myImage "day-three-central-bbq-02.jpeg", "A mostly yellow mural depicting the Memphis Blues Map with many famous African American musicians." %}
  {% myImage "day-three-central-bbq-03.jpeg", "A mural depicting a map of Memphis. It shows famous streets and locations." %}
  <figcaption>Murals inside Central BBQ.</figcaption>
</figure>
<figure>
  {% myImage "day-three-central-bbq-01.jpeg", "A close up view of Mikes plate with pulled pork sandwich, mac n cheese and potato salad sides." %}
  {% myImage "day-three-central-bbq-04.jpeg", "Mike with a content look on his face, sitting at the table with an empty plate in front of him." %}
  <figcaption>I did it! So delicious.</figcaption>
</figure>

## Conclusion

This was the first time either Rachelle or I have been to Memphis. Overall, I had a really great time and enjoyed visiting the city. Memphis feels gritty and sometimes a little unsafe because I felt there was a sense of oppression about the place. I don't know if that's just me projecting my emotions from visiting the civil rights museum or if it's the sense of neglect because there seems to be many abandoned buildings on the way to, and in the general area of our apartment. Plus, the bridge for Interstate 40 over the Mississippi River had been closed to traffic because the bridge was in disrepair and they were fixing it.

But there also seems to be a sense of renewal and effort as there were lots of area closed because of construction. I-40 over the Mississippi was due to open the following week. The crowds on Beale Street while thinner than I expected, were still lively and plentiful.

If you're thinking of visiting Memphis, I say go ahead and book your trip. It's a good time and there's lots of history in the city. Check out the museums. Check out Beale Street. Listen to some music. I bet you have a good time -- we did!




