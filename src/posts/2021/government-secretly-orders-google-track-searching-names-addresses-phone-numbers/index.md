---
title: 'Re: Government secretly orders Google to track anyone searching certain names, addresses, and phone numbers'
desc: My main threat model is focused on resisting surveillance capitalism but I am also interested in protecting myself from the police.
date: '2021-10-07T06:55:16.377-05:00'
tags:
  - privacy
  - google
---
My main threat model is focused on resisting surveillance capitalism but I am also interested in protecting myself from the police. As a white person in the US, I've always believed in the idea that our justice system works and the truth will prevail. However, I've watched enough documentaries, and listened to enough episodes of Serial to understand that people can be convicted on circumstantial evidence only. 

The police are people just like everyone else. They come with their own bias and motivation and I sincerely believe they can simply be wrong. They may have a hunch and have a bit of circumstantial evidence that leads them to believe in the guilt of a particular person, and then it becomes an exercise in the police making the evidence fit the person instead of allowing the evidence to lead to the guilty persons.

> The U.S. government is reportedly secretly issuing warrants for Google  to provide user data on anyone typing in certain search terms, raising  fears that innocent online users could get caught up in serious crime  investigations at a greater frequency than previously thought.
>
> --> <cite>[Government secretly orders Google to track anyone searching certain names, addresses, and phone numbers | Washington Examiner](https://www.washingtonexaminer.com/news/fed-govt-secretly-orders-google-track-anyone-searched-certain-names-addresses-phone-numbers)</cite>

So that's the context as I read about how the government is using keyword warrants to gather Internet search data from Google. The problem with surveillance capitalism is the surveillance part can be used for other things, not just extracting wealth from people. Lawyers advise us to not speak to the police:

>Almost every time I speak at a college or law school campus, there are  one or two audience members whose mother or father is a police officer or a prosecutor. I always ask them: What did your parents tell you about dealing with the police?
>
>Every one of them, without exception, has told me the same thing: My  parents in law enforcement taught me years ago that I should never talk  to the police, or agree to let them interview me about anything, or let them search my car or my apartment or my backpack without a warrant.
>
>--> <cite>[Advice from cops: Don’t talk to cops | Learn Libery](https://www.learnliberty.org/blog/advice-from-cops-dont-talk-to-cops/)</cite>

Not speaking to the police, and exercising our right to remain silent is the smart, responsible thing to do to protect ourselves. Using technology that doesn't track, compile, and collate our activity on the Internet is the smart, responsible thing to do to protect ourselves. Be smart.
