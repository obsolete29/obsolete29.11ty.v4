---
title: 'There’s no escape from Facebook, even if you don’t use it'
desc: The scale and scope of Facebook's internet surveillance is always shocking
date: '2021-09-09T11:55:03.209-05:00'
lastmod: '2021-09-09T11:55:03.212-05:00'
tags:
  - facebook
  - privacy
---
When I read stuff like that this, I come away feeling frustrated mostly because so many people just don't care.

> I tried my own version of Borovicka’s experience by cutting Facebook and Instagram out of my life for two weeks and then tallying who sent it my data. But unlike her, I left its apps on my phone — untouched for a while, but present. (Below I’ll show you how I unearthed what Facebook knows, and how you can see it for yourself.)
> 
> While I was gone, Facebook got a notice when I opened Hulu to watch TV. Facebook knew when I went shopping for paint, a rocking chair and fancy beans. Facebook learned I read the websites What To Expect, Lullaby Trust and Happiest Baby. There’s no surprising Facebook when you’re expecting a baby.
> 
> Over two weeks, Facebook tracked me on at least 95 different apps, websites and businesses, and those are just the ones I know about. It was as if Facebook had hired a private eye to prepare a dossier about my life.
> 
> -- <cite>[Washington Post](https://www.washingtonpost.com/technology/2021/08/29/facebook-privacy-monopoly/)</cite>

My girlfriend and her friends roll their eyes and have a good little chuckle when I'm talking about privacy. It's more than Facebook knowing that you watched a specific show on Hulu or [which songs you're listening to](https://obsolete29.com/posts/2021/08/17/opting-out-of-spotify/). There are two recent examples that are fresh in my mind about why this type of surveillance isn't just harmless data gathering so they can serve us better ads about pants.

I've talked about the [priest who was outted for using Grindr](/posts/2021/07/30/on-resisting-surveillance-capitalism-and-why-it-is-important/).

> “A mobile device correlated to Burrill emitted app data signals from the  location-based hookup app Grindr on a near-daily basis during parts of  2018, 2019, and 2020 — at both his USCCB office and his USCCB-owned  residence, as well as during USCCB meetings and events in other cities,” the Pillar reported.
>
> “The data obtained and analyzed by The Pillar conveys mobile app date signals during two 26-week periods, the first in 2018  and the second in 2019 and 2020. The data was obtained from a data  vendor and authenticated by an independent data consulting firm  contracted by The Pillar,” the site reported. It did not identify who  the vendor was or if the site bought the information or got it from a  third party.
>
> The Pillar story says app data “correlated” to Burrill’s phone shows the priest visited gay bars, including while traveling for the USCCB.
>
> -- <cite>[The Washington Post](https://www.washingtonpost.com/religion/2021/07/20/bishop-misconduct-resign-burrill/)</cite>

I also came across this while reading a wired article about Lyft/Uber:

> On December 25, 2020, I drove to pick up a woman and her mother. When passengers request Lyft rides, they can drag the “location pin” in the app to precisely where they want to be picked up. The pickup location for this ride wasn’t somewhere I could drive my car, although I was able to get within 25 feet. As the two got into my car, the daughter lit into me about not being where she had placed the pin. She called me every name in the book and demanded I “call Lyft” for a refund. The mother dramatically began to say her daughter’s name over and over, astonished that her daughter was doing this and pleading with her to apologize. The daughter wouldn’t let it go. I canceled the ride. I remembered how the mother had said her daughter’s name over and over. I retain vivid memories of the worst-behaved passengers. I assume retaining images of these faces increases my survival odds, and some people have suggested it’s a symptom of PTSD.
> 
> Two months later, I looked at my Facebook feed and saw the woman’s face in my “People You May Know” section. This spooked me. How’d she get there? What if she were stalking me, wanting to get in another rant about the location pin? I didn’t want to friend her on Facebook or befriend her in real life. I couldn’t help hearing her in the car, and it seems that Facebook was paying attention too. Google may have been tracking the ride too.
> 
> -- <cite>[Wired](https://www.wired.com/story/im-a-lyft-driver-my-passengers-act-like-im-part-of-the-app/)</cite>

Imagine the level of data collation that has to happen that would try to connect the Lyft driver with the irrate passenger. Seems probable to me that Facebook was matching some location data and the algorithm served up this people you may know suggestion.

These are real world examples of why I think all of this surveillance is bad and isn't just about serving better ads about pants.
