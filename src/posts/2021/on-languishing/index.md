---
title: On languishing and trying to see the positives
desc: 'I''ve struggled with my mental health the last few months. I thought I was depressed, but "languishing" seems to fit pretty well too.'
date: '2021-10-19T06:45:38.383-05:00'
tags:
  - life
---
Instead of shouting at the Internet about internet connected fridges, I want to write about something personal today. I've struggled with my mental health the last few months. I thought I was depressed, but "languishing" seems to fit pretty well too.

> Languishing is a sense of stagnation and emptiness. It feels as if  you’re muddling through your days, looking at your life through a foggy  windshield. And it might be the dominant emotion of 2021. 
>
> -- <cite>Adam Grant. "There’s a Name for the Blah You’re Feeling: It’s Called Languishing." [NY Times](https://www.nytimes.com/2021/04/19/well/mind/covid-mental-health-languishing.html)</cite>

This article resonated with me deeply when I first read it in April and I think about it quite often. I find myself focusing on the negative lately instead of looking for the positive. I consider myself to be a hopeful, optimistic person but I just feel like I'm experiencing the same day over and over. 

I feel like a few things are contributing to this feeling. I'm not doing enough to socialize with my friends for starters. I work from home so the only people I see are the ones living in this house. At least when I was going into the office at Bridgestone, I got to hang out with my work friends, and play foosball, and build some relationships that way. I need to be more purposeful about socializing. 

I took a contractor role a few months ago and it's really not for me. I think it's a solo contractor thing that's doing it... I prefer working on a team, helping to build the team, all rowing towards a common goal. I've already taken steps to resolve this problem.

I've gained a lot of weight lately. I've not been on my bike in months nor have I went on any hikes. I'm feeling pretty down about my health overall. I think I started gaining weight because I was becoming depressed about other things but now I'm depressed because I've gained weight. Good grief, talk about a negatively reinforcing cycle!

I'm searching for a way to break out of this cycle. I've resumed my sessions with my therapist and that feels good. I've also committed to participating in the [Coffeeneuring Challenge 2021](/posts/2021/10/06/coffeeneuring-challenge-2021/) and I still plan to do so. My first coffee ride will probably be this Saturday the 23rd. Rachelle and I are making plans for a weekend trip, and we've purposefully been having date nights. I'm starting to feel the tiniest bit of momentum. I hope that I can tend that tiny flicker and build something bigger.

I'm not really sure this post has a point, other than an exercise to help me sort my thoughts. Today is a good day at least. If you're feeling down, and reading these words, know you're not alone. Drop me an email and let's chat. We could both probably use the interaction. 
