---
title: Amazon-designed fridge will use the same tech as Amazon Go stores
desc: Anti-competitive and anti-consumer actions by big tech is bad mmkay?
date: '2021-10-08T07:01:23.441-05:00'
tags:
  - amazon
---
***Obviously***, I really hate this idea. 

> When you're ready for more food, the device will make ordering directly from Amazon-owned Whole Foods or Amazon Fresh a piece of cake. And as you might expect, the team working on the product may include Amazon Alexa support, but sources say that support is not the highest priority.

The problem isn't necessarily that Amazon will know that I'm out of corn dogs and then try to show me better ads about corn dogs. The problem is their fridge will *only* let me order more corn dogs from Amazon stores. To order from Kroger or Publix, do I have own a Kroger or Publix fridge? I hate this because it's another example of tech companies trying to capture it's customers in their ecosystem. They're not happy with just selling a connected fridge that can track inventory. They're also going to require that you only order new stuff from Amazon stores and that's anti-competitive and anti-consumer. Why can't this be an open standard that *any* store or connected fridge can connect to and participate in?

— Source: [Ars Technica](https://arstechnica.com/gadgets/2021/10/report-amazon-designed-fridge-will-use-the-same-tech-as-amazon-go-stores/)
