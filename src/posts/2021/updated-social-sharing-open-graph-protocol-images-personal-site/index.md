---
title: Updated social sharing (open graph protocol) images for my personal site
desc: Using the large social sharing images added nothing to the information being published so I've decided to stop using them.
date: '2021-10-18T06:26:30.167-05:00'
tags:
  - obsolete29
  - eleventy
  - eleventy-walkthrough
---
I've been inspired by [Jim Nielsen](https://blog.jim-nielsen.com/2021/quibbles-with-social-share-imagery/) to re-think my [approach to my social sharing previews](https://obsolete29.com/posts/2021/01/03/setup-social-sharing-previews-seo-and-favicons-on-eleventy/). I loved Jim's post about the topic and this bit really resonated with me:

> The way people talk about using social share imagery, it's like FOMO. If I don't use images, I'm missing out. I must do it. My agency in the matter is gone. Social share images are not my decision, it's the social platform’s decision—and that's just how they would like it. They compel us to build for them. They dangle this giant carrot in our faces, prime real estate we can fill with any attention-grabbing imagery we like. It’s like we have no choice but to say “yes, I will wield imagery for you and your platform” where otherwise we normally wouldn’t.
>

I am 100% guilty of the FOMO thing. My open graph images add little to nothing additional to the posts that the title doesn't already have. I've even had that independent thought a couple times but never took that next step and said *well to hell with that then, I'm just not gonna do it*. Everyone else was doing it, I felt I needed to participate to be part of the community.

Here is my updated social sharing code. Notice, I've updated the `twitter:card` tag to be summary and I've completely removed all the open graph image tags.

```html
{% raw %}
<!-- src/_includes/base.njk -->
<head>
  <!-- Open Graph -->
  {% if '/posts' in page.url %}
    {% set ogtype = "article" %}
  {% else %}
    {% set ogtype = "website" %}
  {% endif %}
  <meta property="og:title" content="{{ title or metadata.title }}">
  <meta property="og:type" content="{{ ogtype }}">
  <meta property="og:url" content="{{ metadata.url }}{{ page.url }}" />
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="@obsolete29">
  <meta name="twitter:creator" content="@obsolete29">
  <meta name="twitter:title" content="{{ title or metadata.title }}">
</head>
{% endraw %}
```
