---
title: MacBook Pro user tries Linux as daily driver
desc: I recently purchased a Meerkatfrom System76 with the intent of replacing my 2015 MacBook Pro as my daily driver. I wanted to share my initial impressions.
date: '2021-08-31T09:07:03.831-05:00'
lastmod: '2021-08-31T09:07:03.835-05:00'
tags:
  - apple
  - linux
  - system76
  - mike-breaksup-with-apple
---
I recently purchased a [Meerkat](https://system76.com/desktops/meerkat) from System76 with the intent of replacing my 2015 MacBook Pro as my daily driver. I've been using it for over two weeks now and I wanted to share my initial impressions.

The Meerkat comes with [PopOS](https://pop.system76.com/)which is based on Ubuntu. Since it comes from the manufacturer preinstalled, I thought that would be the best experience from a compatibility perspective. I like it pretty well, but without much experience with other distros, I just don't have much to compare it against. When I was testing distros with VirtualBox on my Mac, I also really liked Debian 11. I'm a little bit hesitant to install Debian but maybe that would be a good opportunity to start building my dot files for Linux.

I'm pleasantly surprised that all my peripherals have worked. I have an AKG desktop mic, Logi webcam, 27 inch HP HDMI monitor with speaker pass through, Keychron keyboard and an MX Master 3 mouse.

For work, I'm a SharePoint Developer which means I'm working on O365 every day and I've had no problems installing or doing anything work related. Zoom and Teams calls, with video, works great! I don't have any audio problems. I was able to install Visual Studio Code, the beta version of Microsoft Edge, and PowerShell along with the [PnP PowerShell](https://docs.microsoft.com/en-us/powershell/sharepoint/sharepoint-pnp/sharepoint-pnp-cmdlets?view=sharepoint-ps) module. I've had zero problems doing work stuff on my Linux system.

I have had some struggles but I think most of them is a combination of lack of experience and understanding with Linux, and coming from a tightly integrated and highly polished ecosystem (Apple).

## Calendar/Contacts Integration and sync
This was the very first thing I was stumbling on. As part of this process, I'm also test driving a [Pixel 4a](/posts/2021/08/11/an-apple-fanboy-tries-out-android-on-a-pixel-4a/) running [CalyxOS](https://calyxos.org/). I've had a hard time getting contacts and calendars to sync between the two without using some cloud service like Google. Lots of people seem to use NextCloud, and I may end up going that way as well but I've been trying to make the two devices sync via my new Synology NAS, which has CalDAV and CardDAV capabilities. I would love it, if I could just do away with all that and when I plug my phone up to the computer with a cable, all the things sync form my phone to my PC, which would then be backed up to my local NAS.

On my Apple devices, this just isn't a thing to think about. You sign into iCloud and all your things are on all your other Apple devices. While nice, I am prepared to adjust my expectations on this so it's not at all a deal breaker.

## Backups

As previously mentioned, I just purchased a new Synology NAS and reading the [Synology documentation](https://kb.synology.com/en-global/DSM/tutorial/How_to_back_up_Linux_computer_to_Synology_NAS) says to use rsync. I'm very comfortable in the command-line but this is just a whole different way of thinking about things. On my Mac, I'd just go into Time Machine and setup the backups to use the Synology and then stuff just happens automagically. With Linux, I will need to write bash scripts to use rsync, ssh keys and cron jobs to automate it.

## Screenshots

The Linux screenshot utility that comes with PopOS strikes me rudimentary. I capture a lot of screenshots where I want to select a certain area of the screen, then paste that somewhere. Doing that in macOS was very straight forward. There's a shortcut that brings up the selection tool, you select the area of the screen and when you let go of the mouse, that's sent to your clipboard for pasting. In Linux, there's a program you have to open and then there's multiple steps of mouse clicking to accomplish the same task. I plan to investigate and look for a better screenshot tool that I can bind to a keyboard shortcut.

## Conclusion
Overall, I'm very happy with PopOS and the System76 Meerkat. I've not experienced a single show stopper yet. I'm getting very used to switching Workspaces, bringing up the search box using the "super" key. Factorio works fine. All my work software works fine. The last main thing to tackle is the calendar situation. I want to sync between my pc and my phone and I want that backed up somewhere that isn't either one of those devices. I'd prefer my local Synology NAS.
