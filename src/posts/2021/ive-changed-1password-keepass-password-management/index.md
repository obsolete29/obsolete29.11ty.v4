---
title: I've changed from using 1Password to KeePass for password management
desc: I just finished my migration from 1Password to Keepass. Here's my reasons and setup.
date: '2021-10-04T14:05:06.449-05:00'
tags:
  - passwords
  - security
  - MikeYellsAtClouds
---
I've been a [1Password](https://1password.com/) user since 2011. I've been pretty happy with the service itself and have never had any issues accessing my passwords because of outages or h4x. I was glad to be able to pay to be a customer. Yet, I just completed migrating all 700+ entries from 1Password to Keepass.

## Removing dependency on cloud providers

My [breakup with Apple](/posts/2021/08/06/i-am-breaking-up-with-apple/) has evolved into a breakup with cloud providers in general. When possible, I want to host my own services and depend on things that I can access directly and/or put my hands on. There's some question about whether or not 1Password still allows local vaults. Local vaults was *the* reason I initially picked 1Password over [LastPass](https://www.lastpass.com/). Because they're doing capitalism, I think it's only a matter of time before they become cloud only if they haven't already.

My friend Chris recently had a problem that reinforced my decision to use Keepass:

> Currently using LastPass. Apple thought the renewal was suspicious so declined. Now I’m not able to get to my info on all devices. Site won’t let me try the renewal again. They don’t offer phone support. So waiting on an email.  Not happy.
>
> -- <cite>[Chris Green on Twitter 9:52 PM · Sep 29, 2021](https://twitter.com/Sethran/status/1443408485639729152)</cite>

I was [posting about this on Mastodon](https://indieweb.social/@obsolete29/107020398830241115) and several people had some good suggestions and operating procedures for dealing with these types of problems, which seemed to boil down to "have a local backup."

## Remove dependency on proprietary software

I'm not necessarily opposed to proprietary software and have no problems paying developers for the work they do. I think it's vital that developers are given an incentive to do the work they do. Password management is an important job though and I do not want to be locked into a vendor. Keepass is open source and there a several, legitimate Keepass clients (I'm using KeePassXC on Linux and KeePassDX on [CalyxOS](https://calyxos.org/)). Should KeePassXC [do something I really can't stand or don't like](https://initialcharge.net/2021/10/1password-share-sheet/), then I can simple change to regular ole Keepass. Migrating from a proprietary format to some other provider is pretty hard and I know because I just went through the process! I'd like to avoid that in the future if possible.

## 1Password is changing

This could just be my rose colored classes but I feel that 1Password is changing. When I first heard of them way back in 2011, they were a small shop that created cool software that worked great on Mac and iPhone and synced seamlessly. Now they're doing capitalism and growing. I think it's only a matter of time before their fiduciary responsibility forces them to do the typical shitty tech company things.

## My password setup

My requirements are very straight forward and I've purposefully engineered my solution to be simple. 

I have two computers running [Pop OS](https://pop.system76.com/) Linux and one Pixel 4a running [CalyxOS](https://calyxos.org/). I use [KeePassXC](https://keepassxc.org/) on my Linux computers and [KeePassDX](https://www.keepassdx.com/) on the Pixel. I use [Syncthing](https://syncthing.net/) to sync the password file between all three devices, using my local network. To follow the [3-2-1 backup rule](https://www.backblaze.com/blog/the-3-2-1-backup-strategy/), I do a nightly backup to my [Synology NAS](/posts/2021/09/06/how-im-using-my-synology-nas-to-own-my-content/). There's another nightly process that then syncs my NAS with my buddy [Dave's](https://twitter.com/dbyrge) NAS, 120 miles away.

There are some trade offs with my setup. Open source software will never be as slick and good as software created by a large team of paid developers. 1Password is very nice and slick and works great. They've created a great user experience. The KeePass user experience is good... like it works well but it's just not going to be as nice as 1Password nor have the nice integrations. That's a trade off I'm willing to accept.

My requirements are simple. If I were trying to share with a family, that might change my calculation and maybe something like [BitWarden](https://bitwarden.com/) would be a better solution.

I love the simplicity of the setup. I use my local network to sync things around to my local devices. That's the perfect solution in my mind. I now contribute the money I was spending on my 1Password subscription equally between the developers of KeePassDX, KeePassXC and Syncthing.

What do you use for password management?
