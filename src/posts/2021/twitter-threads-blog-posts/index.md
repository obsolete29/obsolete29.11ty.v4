---
title: Twitter threads are the new blog posts
desc: Which is more important to you? Reach and discoverbility or being the owner of your content and identity?
date: '2021-08-25T07:50:43.590-05:00'
lastmod: '2021-08-25T07:50:43.595-05:00'
tags:
  - twitter
  - blogging
  - indie-web
---
As a proponent of [IndieWeb](https://indieweb.org), I really dislike this idea.

> I believe threads are a compromised version of blog posts, and at the same time, an evolution.
> 
> In which ways are they worse?
> 
> - They're usually less readable — but Twitter is working on a solution, a reader mode for threads.
> - They're less discoverable — for example, if you visit a user profile, you don't instantly see their latest (or best) threads.
> 
> In which ways are the better?
> 
> - As you see these days, they're often more viral since they're native to the platform where you can instantly share them.
> - They're bite-sized, with each part becoming an independent atom that can be liked, commented and shared.

Perhaps Fabrizio is just using the topic as an excuse to link to some projects that he works on, but I think the question of blogging on platforms owned by large tech companies really boils down to your organizing principles and which of them you value more. Is audienace and reach more important? Or is owning your content and identity more important?

--> Source: [Twitter threads are the new blog posts](https://www.fabrizio.so/notes/threads)
