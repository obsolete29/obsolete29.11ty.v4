---
title: 'Analysts: Google to pay Apple $15 billion to remain default Safari search engine in 2021'
desc: 'I can''t believe I was every so naive to believe that Apple cares about privacy. This violates the core principle that I thought Apple had. '
date: '2021-09-04T05:31:15.142-05:00'
lastmod: '2021-09-04T05:31:15.146-05:00'
tags:
  - apple
  - privacy
  - mike-breaksup-with-apple
  - google
---
I can't believe I was every so naive to believe that Apple cares about privacy. This violates the core principle that I thought Apple had. People buying and using Apple devices and software are supposed to be the _customers_ not the ***product***.  Apple is selling access to it's customers.

Apple is a for profit company and they're _very good_ at the for profit part. They have a fiduciary responsibility to maximize profits. Whenever there is a conflict between Apple customers and Apple shareholders, Apple customers will always lose.

> It’s long been known that Google pays Apple a hefty sum every year to ensure that it remains the default search engine on iPhone, iPad, and Mac. Now, a new report from analysts at Bernstein suggests that the payment from Google to Apple may reach $15 billion in 2021, up from $10 billion in 2020.
> 
> In the investor note, seen by [_Ped30_](https://www.ped30.com/2021/08/25/apple-google-payments-15b/), Bernstein analysts are estimating that Google’s payment to Apple will increase to $15 billion in 2021, and to between $18 billion and $20 billion in 2022. The data is based on “disclosures in Apple’s public filings as well as a bottom-up analysis of Google’s TAC (traffic acquisition costs) payments.”
> 
> Bernstein analyst Toni Sacconaghi says that Google is likely “paying to ensure Microsoft doesn’t outbid it.”
> 
> -- <cite>[9To5Mac](https://9to5mac.com/2021/08/25/analysts-google-to-pay-apple-15-billion-to-remain-default-safari-search-engine-in-2021/)</cite>