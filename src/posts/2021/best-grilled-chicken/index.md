---
title: The best grilled chicken
desc: Sunday's usually mean grilling at our house! Today we're grilling some chicken breast.
date: '2021-08-15T10:24:07.930-05:00'
lastmod: '2021-08-15T10:24:07.931-05:00'
tags:
  - grilling
  - recipe
  - weber
---
Sunday's usually mean grilling at our house! Today we're grilling some chicken breast. Historically, grilling chicken breast has been hard to get right. In order to get the center done (ie, 160º), the ends were always overcooked. I stumbled across this method one day while randomly cruising YouTube.

## Prepare the grill

I usually start with getting the grill ready. I'll go ahead and fill up my charcoal chimney and get it lit. It usually takes about 15 minutes for it to get ready. That's plenty of time to prepare the chicken breasts.

<figure>
  {% myImage "best-grilled-chicken-03.jpeg", "Freshly lit coals in the chimney starter on the weber grill." %}
  <figcaption>Getting her ready!</figcaption>
</figure>

## Prepare the chicken

First, I take the time to cut off any obvious fat on the chicken breast.

### Beat your meat

Next, I beat it flat. Mmmhmm. That's right, beat it flat. I put the chicken into a 1 gallon zip lock bag, squirt a little olive oil in the bag then use a big rolling pin to beat the chicken flat. While inexact, I think they generally turn out to be about 1 inch or a smidge over. The important thing is that it's generally the same thickness all over so that the "middle" is done at the same time the edges are.

<figure>
  {% myImage "best-grilled-chicken-01.jpeg", "An unprepared chicken breast in a gallon ziplock bag with a large rolling pin laying next to it on the counter." %}
  {% myImage "best-grilled-chicken-02.jpeg", "The flattened chicken breast, on a plate after beating it with the rolling pin." %}
  <figcaption>Before and after</figcaption>
</figure>

### Season

Season to taste! I think some lemon pepper seasoning works great here or even just some salt and pepper. I personally prefer a rub. I usually use poultry rub from Kroger/Walmart but we were recently at a local farmers market and picked up a couple different rubs. 

To apply the rub, I sprinkle a liberal amount on the chicken, making sure it's covered all over then pat it in--not rub. I turn it over and repeat the process.

<figure>
  {% myImage "best-grilled-chicken-04.jpeg", "Bottles of Tuckers Classic Barbecue Rub and Tuckers Competition Rub." %}
  <figcaption>Use whatever rub or seasoning you like. These were really good.</figcaption>
</figure>

## Cook the chicken

Once the coals are ready and I've allowed the grill to heat up ~15 minutes, I'll throw the meat on.

When I'm grilling, I prefer to bank the coals on one side so I can have a hot side and a cool side. I use the charcoal bins that came with the grill but I place it to one side as in the photo below.

For the chicken breast, I put the prepared chicken breast directly over the hottest part of the heat, close the lid, and allow to cook for two minutes. 

<figure>
  {% myImage "best-grilled-chicken-05.jpeg", "Flattened and rub covered chicken breast on the grill with two foil packets to the side." %}
  <figcaption>Chicken on the barbie! I was meal prepping and trying out sweat potato packets.</figcaption>
</figure>

Next, flip it and allow it to cook for two more minutes, and check the temperature with an [instant thermometer](https://www.nytimes.com/wirecutter/reviews/the-best-instant-read-thermometer/). USDA says chicken should be cooked to 160º so that's what I do. If the chicken hasn't reach the desired temp yet, I move it off the direct heat and place it on the indirect side to allow it to continue to cook. The coals are really hot and if left on the direct heat, the chicken can burn.

<figure>
  {% myImage "best-grilled-chicken-06.jpeg", "Grilled chicken breast on the grill with a therometer stuck into one of the, the screen reads 162." %}
  <figcaption>Perfect.</figcaption>
</figure>

Once it reaches 160º, I'll take it off the grill, and allow it to rest for five minutes. Some of our favorite sides are grilled corn on the cob, zucchini/yellow squash kabobs, grilled carrots, and potato pouches.

<figure>
  {% myImage "best-grilled-chicken-07.jpeg", "Grilled and juicy chicken breast, cut up on a cutting board ready for plating." %}
  <figcaption>Bon appetit!</figcaption>
</figure>

How do you grill chicken?
