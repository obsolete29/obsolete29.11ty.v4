---
title: "Colorado Shooting"
date: "2012-07-20"
tags: 
  - "theater-shooting"
  - religion
  - shootings
  - this-is-america
---

PZ Myers [summed](http://freethoughtblogs.com/pharyngula/2012/07/20/monstrous/) up my thoughts on the gunman pretty well so I won't reiterate them here. While reading some news articles about it I came across this quote:

> The man pointed a gun at Jennifer Seeger, her mother said. The young woman slid onto the floor with a friend and crawled as bullets flew around her, including one that nearly grazed her forehead.
> 
> “He pointed a gun at her from five feet away,’’ Florita Seeger said. “She told me ‘Mom, God saved me, God still loves me.’ ’’

I really can't stand this type of thinking that people inevitably do after a crisis. Did God choose not to save the other 14? Did he not love the dead ones? There's no reason other than a crazy person had easy access to firearms.
