---
title: "I am an Atheist and I have many Christian friends"
date: "2012-09-23"
tags: 
  - "religion"
  - atheism
---

I'm an atheist. I have many Christian friends and family members. I used to have this internal conflict about how I'm supposed to feel towards my Christian friends and family. I would have this internal dialog wondering how can I respect people who believe some of the crazy things that they say they believe. The earth is 6k years old? People used to live to be 900 years old? There is an all loving and powerful being who answers prayers? Those types of ideas don't line up with what we observe in the world but I love my friends and family. They are kind, good people who are otherwise mostly logical and completely normal and rational. So how do I resolve that internal conflict?

I was listening to an [Oh No It's Ross and Carrie](http://www.ohnopodcast.com/) podcast where they were interviewing Phil Zuckerman and he said something that resonated with me. Paraphrasing, he said that it doesn't really matter what people _say_ they believe, it only matters how they act and what they do. If they're kind, good people it doesn't matter that they think the earth is only 6k years old. I backed it up and listened to that section a couple times and within minutes I felt more at peace about my atheism and all of the Christian people that I care about.

I try to apply that principle to my dealings with all people. It doesn't matter to me that Mitt Romney thinks magic underwear can protect him from evil. I don't judge him on his religion because he doesn't wear it on his shoulder. In that same vein, "it's my religion" is never a valid reason for bad behavior like trying to prevent people from having equal rights (marriage equality).
