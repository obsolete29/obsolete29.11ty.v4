---
title: "New Weekly Cook Up Menu"
date: "2012-11-26"
tags: 
  - "fitness"
  - "recipes"
  - meal-prep
---

I'm changing up my weekly cook up menu a little. I'm trying to get better about what I'm sticking in my mouth and we're not real consistent with our evening meals lately. I'll have planned to make fish and broccoli and perhaps some sweet potato fries but when the evening time rolls around, everybody is tired and not really feeling like cooking or cleaning up afterwards. When that happens, we resort to "eat what you can find" mode which leaves lots of room for me to make bad choices.

Below is my [myfitnesspal](http://www.myfitnesspal.com/obsolete29) breakdown.

[![2012-11-26_0715](images/8219868377_c17565abc0.jpg)](http://www.flickr.com/photos/pamperedjen76/8219868377/ "2012-11-26_0715 by brook181, on Flickr")

The Eggland's Best eggs are usually farm eggs we get from our egg lady. Sometimes I have to buy store eggs though and I do buy the EB eggs. I boil and peal these when I'm cooking up my batches on Sunday night.

The PB&J sandwiches are using Arnold Multi-Grain Sandwich Thins for the bread. I made 8 of these and [froze](http://amandascookin.com/2010/02/how-to-freeze-use-peanut-butter-jelly.html) them. This is the first week I've had these so I'm hoping that by the time lunch rolls around, these will be thawed out OK and ready to eat. I weigh the peanut butter and jelly to make sure I'm only getting what I'm intending to get.

The rice and beans entry for dinner is actually [quinoa and black beans](http://plantoeat.com/recipes/471548)... I'm just too lazy to change the title. :? I cook this up on Sunday night and store them in glass pyrex containers with the broccoli on top. I just nuke it for 2 minutes and it's ready.

All of these meals are relatively healthy (tho maybe the PB&J is debatable), frugal and convenient to make in batches. I can cook up everything in about 45 minutes on Sunday evening! It's also vegetarian, but I didn't intend to do that.

The only changes I'll probably make on a daily basis is having wine and baked sweet potato fries with dinner. If I add a lunch time walk around the block at work, then I'm still in calorie deficit on non-run days.
