---
title: "Why I am an iPhone Fanboy"
date: "2012-09-25"
tags: 
  - "technology"
  - apple
  - iphone
---
{% myImage "8009117847_86a4fd419e_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

I'm an Apple fanboy. I love their products and their philosophy about user experience. Recently, I've engaged in a couple of unsatisfactory conversations with my friends about why I prefer Apple products. It's also not too uncommon to get some smug [comments](https://twitter.com/riakin/status/250375261651095552) from Android users. Hey, I get it...  Apple fanboys are pretty smug and annoying too. I try real hard not to be one of those types but I do want to explain why _I_ like Apple products.

My first smart phone was a BlackBerry Curve. I thought it was a pretty great phone. I could surf the web (kind of), I could read ebooks, I could check Facebook , Twitter and Email all while on the go. It was the best thing since sliced bread and I was forever hooked on smartphones.

[Jen](http://jenharley.me/ "Jen Harley Crossville TN") and I upgraded to the [Droid X](http://en.wikipedia.org/wiki/Droid_X) on the day they launched and we were in geek heaven. The Curve seemed old and obsolete in comparison. The Droid was faster and had that awesome and large touch screen. We were both (and still are) Google fans as we used a lot of their services and we continue to do so today. I loved that we could customize the Droid until our heart was content. We could even load a custom ROM and _really_ customize everything. I had my Droid X setup exactly the way I wanted it.

Even though I was completely happy with my Droid X, I had never tried an iPhone. How could I say that the Droid was the best if I had never tried an iPhone? It was with that thought that I decided to get the iPhone 4 when my upgrade was available. I've now been using my iPhone for a year and I can say for me, it's the best phone. Here are the specific reason I prefer the iPhone:

1. It's been a little while since I used Android but the battery life on my Droid X wasn't very good. I understand that the newest Android phones are better in this regard than my Droid X.
2. The apps seem better and more stable. I have _much_ fewer crashes on my iPhone than I had on my Droid X. Additionally, when new, major versions of apps are published, they usually come out on iPhone first (Waze and foursquare come to mind). Additionally, there are just more and better apps for iPhone. I agree that more doesn't always mean better but in my experience, the apps _are_ better. Tweetbot is hands down the best Twitter application available on any platform and it's not available for Android.
3. User experience. When I'd hear Apple users say this, I'd kind of roll my eyes but I think it's true. The user experience is just... better. I think of it like a BWM or Mercedes compared to a Chevy or Dodge. On paper, a Chevy has 4 wheels, the same horse power and nice leather seats but there is just something about how the BMW/Mercedes is built that just makes for a nicer experience than driving the Chevy.
4. The aftermarket accessories market is much bigger and better for iPhone than OEM Android phones simply because there are more iPhone handsets than specific Android handsets. Maybe there are more Android users but how many different handsets are there? Motorola, Samsung, HTC and Google just to name the big players. How many iPhone 4/4s users are there compared to Nexus users are there? Is there anything even remotely like the [Magellan ToughCase](http://www.magellangps.com/lp/toughcase/index.html) for an Android device?

The transition to the iPhone wasn't painless but I was determined to make the transition so I just had to adjust my expectations to match how the iPhone worked. I couldn't automatically upload my photos to Flickr in the background with the iPhone for instance. This irritated me a great deal at first but I finally found an app that allowed me to sync by just opening the app. That app, CameraSync has since added the functionality to automatically upload photos based on a location (when you arrive at home for instance).

I still think the Android has a better notification system. I liked the indicator light and the persistent notification icons in the upper left corner. The iPhone notification center comes close to being as a good as Androids... good enough that it doesn't matter. I also miss the tighter integration  with Google Voice that the Android devices have. The Google Voice app on the iPhone is good enough that I can deal though.

So there you go. I like the iPhone because I think it works better, there are more and better apps and there are more and better accessories. Your mileage may vary.
