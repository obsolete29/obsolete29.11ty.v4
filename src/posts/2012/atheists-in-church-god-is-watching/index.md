---
title: "Atheists in Church: God is Watching"
desc: "As my friends know, my wife Jen is a Christian and goes to church pretty regular. Well today she invited me to church and I wanted to jot down my thoughts on part of the sermon."
date: "2012-04-29"
lastmod: "2012-04-29"
tags: 
  - "atheistinchurch"
  - "atheism"
---
As my friends know, my wife Jen is a Christian and goes to church pretty regular. Check out my initial post, [our Atheist-Christian marriage](/posts/our-atheist-christian-marriage/ "Our Atheist-Christian marriage") for some more background. Well today she invited me to church and I wanted to jot down my thoughts on part of the sermon.

I often say that we (humans) can be good for goodness' sake when I'm discussion religion and atheism with friends. We don't need gods to be good, moral people. If it could be proven beyond a doubt that there are no gods of any sort, I think you'd still have good people doing good things and bad people doing bad things. But today, Dennis said that God basically keeps a tally sheet and every time we do something good, we get a star beside our name (paraphrasing). He said, that we should never have to worry about trash in the parking lot because people would just pick it up on their own since God is keeping score. Why would a person setup and take down chairs at the TAAD center (local youth group thing) when hardly anybody was showing up? Well because he knew that God was keeping score. The impression I got from that bit of the sermon was that we should do good things because God is watching.

Does that mean that the people who think that God is keeping score would no longer pick up the trash from the parking lot or try to help teens if they no longer believed there was a God? Do they only do good things because they think God is watching?

I suspect that Dennis (the preacher) would say that no, people would still be good. If that's the case, why do we need to invoke this idea of a cosmic score keeper to get people to do good?

Maybe he would say yes though... Christians do believe in original sin after all and that people are inherently evil.

If you're a Christian, I'd really be interested in hearing your opinion on whether or not Christians are good because God is keeping score.
