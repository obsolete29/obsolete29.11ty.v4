---
title: "The Bible is Wrong About Sexuality"
desc: "Here are some of my thoughts based on Dan Savage's podcast."
date: "2012-04-30"
lastmod: "2012-04-30"
tags: 
  - "dansavage"
  - "religion"
  - "atheism"
---
Dan Savage is a sex advice columnist and creator of the [It Gets Better Project](http://www.itgetsbetter.org/ "It Gets Better Project"). Dan does a lot of work to fight against LGBT teen bullying. I'm a huge fan of Dan; especially his podcast.

Dan was invited to speak as a keynote speaker at NSPA / JEA's annual High School Journalism convention, Journalism on the Edge. His speech is getting a lot flak. Below is the speech. Warning, there is some minor language. NSFW, probably.

http://www.youtube.com/embed/ao0k9qDsOvs

I think Dan makes an excellent point. If we can ignore the bits of the Bible that pertains to shellfish, slavery, menstruation, virginity and masturbation, why can't we just ignore the bits about homosexuality?

Anyway, Dan made a bunch of people upset because he used the word "bullshit" to deride some of the crazy stuff in the Bible. They're also upset because he said the people walking out were "pansy-assed" for doing so:

> One thing I want to talk about is – ha, so you can tell the Bible guys in the hall that they can come back in because I’m done beating up the Bible. It’s funny that someone who’s on the receiving end of beatings that are justified by the Bible, how pansy-assed some people react to being pushed back. I apologize if I hurt anyone’s feelings but I have the right to defend myself, and to point out the hypocrisy of people who justify anti-gay bigotry by pointing to the Bible and insisting that we must live by the code of Leviticus on this one issue and no other.

[CitizenLink](http://www.citizenlink.com/2012/04/19/savage-incident-shows-why-we-need-the-day-of-dialogue/) posted this in response:

> Using profanity to deride the Bible—and then mocking the Christian students after they left the room—is obviously a form of bullying and name-calling. This illustrates perfectly what we’ve been saying all along: Too many times in the name of “tolerance,” Christian students find their faith being openly mocked and belittled in educational environments. Incidents like this one stand in stark contrast to the principles we’ve continually espoused on our Web sites, truetolerance.org and dayofdialogue.com, which call for a free exchange of ideas and respect for different viewpoints, including those that are faith-based and socially conservative.

People are worthy of respect, not their opinions. I respect everybody's right to free speech and free expression and I will do everything in my power to make sure we all, as free people, continue to have those rights. However, I reserve the right to criticize bad ideas and bad opinions.  As I said on Twitter a couple days ago, never conflate "freedom of speech" with "immunity from criticism."  If you're misogynistic, homophobic, racists or anti-science, don't expect to be able to hold up your "it's my religion" shield and think that imparts some sort of immunity from criticism; at least from me.

Edit 04/30, 1:01pm: I just wanted to point you guys to a much better write up than I could possibly think of doing: [Dan Savage, Bullying and the Truth about Gays and the Bible](http://www.huffingtonpost.com/john-shore/dan-savage-and-the-truth_b_1463390.html?m=false&icid=hp_religion_ftr_desktop)
