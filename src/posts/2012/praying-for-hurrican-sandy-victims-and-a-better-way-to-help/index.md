---
title: "Praying for Hurricane Sandy Victims and a Better Way to Help"
date: "2012-10-30"
tags: 
  - "religion"
---

Anytime there is a big natural disaster like Hurrican Sandy, I see people in my timelines saying that they're praying for the people in the path of destruction. I'm always reminded of one of my favoriate quotes:

> Two hands working can do more than a thousand clasped in prayer.

I understand why people do this. It makes them feel like they're doing something. Or maybe they're using it in the same sense as "I'm sending happy thoughts/karma/vibes your way" but praying is literally the least that a person can do to help victims of natural disasters.

If you really want to help, donate to the Red Cross: http://www.redcross.org

You can also text REDCROSS to 90999 to donate $10 on your cell phone bill.
