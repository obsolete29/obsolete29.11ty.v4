---
title: "Cathy Samford Lawsuit"
desc: "Cathy Samford was fired for being pregnant out of wedlock."
date: "2012-04-27"
lastmod: "2012-04-27"
tags: 
  - "cathysamford"
  - "atheism"
---
<figure>
  {% myImage "6967490426_e9b68d7459_m.jpg", "Photo of Cathy Samford." %}
  <figcaption>Cathy Samford was fired for being pregnant out of wedlock.</figcaption>
</figure>

Cathy Samford was fired by Heritage Christian Academy for being pregnant while single. Ms Samford says that what Heritage has done is against the discrimination laws of our country. Heritage says being able to discriminate against Ms Samford is part of their religious freedom.

Does religious freedom shield organizations from the discrimination laws of our country? Should it?

**My Cathy Samford Lawsuit Posts**

- [Cathy Samford Fired For Being Unwed Mother](/posts/cathy-samford-fired-for-being-unwed-mother/ "Cathy Samford Lawsuit")
- [RE: Cathy Samford Fired For Being Unwed Mother](/posts/cathy-samford-fired-for-being-unwed-mother2/ "Cathy Samford Lawsuit")

**Other Cathy Samford Lawsuit Sources**

- [Texas Teacher Fired for Unwed Pregnancy Offered to Get Married](http://abcnews.go.com/US/texas-teacher-cathy-samford-fired-unwed-pregnancy-offered/story?id=16115051#.T5rLVbNvbQV) (abcnews)
- [Hosanna-Tabor Evangelical Lutheran Church and School v. EEOC](http://en.wikipedia.org/wiki/Hosanna-Tabor_Evangelical_Lutheran_Church_and_School_v._EEOC "Cathy Samford Lawsuit") (wikipedia)
