---
title: "Darwin Sleeve Tattoo"
date: "2012-05-02"
tags: 
  - "tattoos"
---
Today was part one of my Darwin sleeve tattoo. Ian White at [Safe House Tattoo](https://www.safehousetattoo.com/artists/ian-white) in Nashville is doing it and I couldn't be more happy with the results so far.

I started planning this tattoo last fall and reached out to Ian with my vision. We had an in-person consult at the end of January and talked everything over. He liked my idea and thought it would be a fun project so we made my first appointment. He's super booked and my first appointment wasn't until May 1st!

I told Ian that I wanted a sleeve tattoo as a tribute to Charles Darwin and human evolution. It would be a "skull sleeve" but with a twist. At the very top of my arm would be a Charles Darwin portrait. For the rest of the sleeve, we'd have the skulls of actual humanoid fossils: [Australopithecus afarensis](http://en.wikipedia.org/wiki/File:Mrs_Ples_Face.jpg), [Homo habilis](http://en.wikipedia.org/wiki/File:Homo_habilis-KNM_ER_1813.jpg), [Homo ergaster](http://en.wikipedia.org/wiki/File:Homo_ergaster.jpg), [Homo erectus](http://en.wikipedia.org/wiki/File:Homo_erectus.jpg) and Homo sapien. The skulls would be arranged in such a way so that the oldest fossil would be at the bottom of my arm, nearest the wrist and the youngest (Homo sapien) would be up nearest the shoulder and the Charles Darwin portrait.

Today we did the Charles Darwin portrait and as I said, I really couldn't be more happy with the results. We also went over the exact skulls and laid out the plan for how the rest of the tattoo will go; my next session is June 5th. Needless to say, I'm pretty excited about the project. Ian is awesome and is great with black and grey stuff and he loves doing skulls so if you're in the market, give him a shout.

<figure>
  {% myImage "darwin-01.jpeg", "Black and gray portrait tattoo of the famous Charles Darwin photo." %}
  <figcaption>I heart you Charles Darwin</figcaption>
</figure>

Update 07/13/2012: You can read about my progress [here](http://michaelharley.me/blog/2012/darwin-sleeve-tattoo-progress/).
