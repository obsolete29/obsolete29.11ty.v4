---
title: "On Being Content"
date: "2012-05-07"
tags: 
  - "life"
---
<figure>
  {% myImage "4876544161_9dd0036bda_m.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>TRAIN in Concert 2 / Early Summer Concert Series 2010 - Apple Plaza, Manhattan NYC - 08/09/10</figcaption>
</figure>

I leave the job of being a Train fan to my wife, Jen and she does a pretty good job. Over the weekend she was reading [a blog post by Pat Monahan](http://patblogahan.com/post/22460499921/confessions-of-the-baby-of-the-family "CONFESSIONS OF THE BABY OF THE FAMILY"):

> How do I become okay with my life results?! How do I look at my body and say, “Not bad.”? How do I listen to my records and say, “Dude, you fucking did it!”? When will I sit in a dressing room in Birmingham, England, knowing that I am about to perform to a beautiful group of people in a sold out O2 venue and look in the mirror and think, “Congratulation! You’ve come a long way.” Because all I do now is want more. I want to look better. I want to be better. I want to have more and more and more love till I explode. I read 10,000 things from people that say “I LOVE YOU, Pat” and then one that says,”Hey Pat You suck!” and I am filled with sadness to my bones.

If Pat Monahan can't be happy with the things that he has in his life then who can? The guy is in great shape, he's successful, wealthy and has the adoration of many fans yet he still has these same urges that I have. Knowing that he feels that way helps me to realize that I need to enjoy the moment more... give myself permission. I sometimes get caught up in the things I don't have and that negatively affects my ability to enjoy the things I _do_ have.

Yes [my commute is pretty killer](http://michaelharley.me/blog/2012/commuting-why-am-i-doing-this/ "Commuting: Why am I doing this?") but I have a job that allows me to leave my work at work and it's pretty flexible. I'm not living where I want to right now but I have a great family that appreciates the sacrifices I'm making. Yes, we have a crazy, chaotic house  (10, 12 & 15 year old boys, my 20 year old daughter and her almost 2 year boy plus 2 cats and 2 dogs!) but these are memories that I already cherish. I figure I'll get plenty of peace and quite after I'm dead. No, we're not able to travel as much or hang out in Nashville as much or just go out as much as we want but we're making huge strides with our finances so that we can.

I know there are people who are in a lot worse shape than I that would gladly trade me... people who have made mistakes and are in prison, people who are unemployed or who no longer have their health.

So if there's anything Pat has shown me it's that I should focus on what I do have instead of what I don't. Maybe he should write a song about it.
