---
title: "How-to be a good driver"
date: "2012-11-11"
tags: 
  - "life"
  - "travel"
---

My [commute](http://michaelharley.me/blog/2012/commuting-why-am-i-doing-this/ "Commuting: Why am I doing this?") to work is 120 miles one way, four days a week so I consider myself to be an advanced level commuter. With so much time behind the wheel I see a lot of bad driving so I wanted to list a few of my biggest pet peeves. If you follow these tips, you're sure not to piss me off if I'm behind you. ;)

1. **Slow traffic keep right** - It is surprisingly common for people to drive slow in the fast lane and it can cause some impatient people behind you to try to pass on the right. I notice a lot more people breaking this basic rule of the road around holiday weekends. I chalk it up to all the [n00b](http://www.urbandictionary.com/define.php?term=n00b "n00b") drivers who rarely drive on the interstate. How about we look in the rear view mirror and notice all the people behind you?
2. **Speed up when passing** - This is related to the first item but when you get over into the fast lane to pass, speed up, pass the car you're passing and get back over into the slow lane if there are people behind you. There are few things more irritating than to see a person who has set his cruise control on 71 and is passing a line of big rigs going 70. Do you not notice the line of 10 cars queued up behind you? I realize you're doing the speed limit but be considerate, dick.
3. **Learn how to merge** - [Zipper merge](http://www.dot.state.mn.us/zippermerge/ "zipper merge"). It's a pretty easy concept. Do it.

Items number 1 & 2 basically boil down to being aware of what's happening around you (i.e., look in your review mirror once in a while) and be considerate of other drivers (how about we give a shit that you're holding up 20 other people?). I'll boil it down even further: Awareness & Empathy.
