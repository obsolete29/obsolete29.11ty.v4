---
title: "Infastructure Investment In Nahsville"
date: "2012-11-19"
tags: 
  - "life"
---

I'm probably the only person in my circle of friends and Internet acquaintances interested in this topic... and by development, I don't mean programming. ;)

I recently read a few articles about development and infrastructure investment in the Nashville area. I read about the full opening of [State Route 840](http://www.greenvilleonline.com/article/DN/20121104/BUSINESS01/311040090/State-Route-840-opens-fertile-region-commerce-development?odyssey=nav%7Chead) and how that area is a _fertile region of commerce and development._ The two main things 840 is supposed to do is divert a small portion of traffic and spur development.

> The 14-mile uncompleted portion of 840 was located in Williamson County, and the unprecedented accessibility to Thompson’s Station and Spring Hill is expected to keep development buzzing.
> 
> In Thompson’s Station, Mars Petcare is building an $87.9 million innovation center, where pet food will be produced, and Shelter Insurance recently broke ground on its state headquarters, two additions that wouldn’t have happened without 840 being finished, said Town Administrator Greg Langeliers. “The future, going forward, with the final section of 840 complete,” he said, “it’s a little harder to say. But I think it’s going to have a huge impact.”

I'm honestly curious why Mars Petcare and Shelter Insurance choose this area to build their new developments instead of choosing to build closer or in Nashville where the density and resources of the city could help them. I wonder if Nashville tried to entice them to build closer?

And this:

> One the most interesting areas to watch in coming years might be the rural, far western portions of Williamson County, particularly Fairview.
> 
> “This is the last frontier of Williamson County,” said Andrew Hyatt, Fairview’s city manager. People want to live in the county because of its schools, he said, and 840 makes Fairview — which is now much more accessible from areas to the east — a more appealing option.
> 
> Already, the city has 1,700 lots ready for residential development, he said.

Fairview? Again, seems like we should be enticing people and businesses to Nashville proper because we can't have good effective transit until we're dense enough as a metro area.

I also saw [this](http://www.tennessean.com/article/20121107/WILSON/311070021/Mt-Juliet-home-means-closer-commute-closer-family) piece about a family that moved from Pulaski to Mt Juliet. I get why they choose to live where they did... people want their yard and big ole house and all that. Plus, they're a nice white family, living in a nice white area, going to nice white schools. At least Mt Juliet has the commuter rail option to take them into downtown Nashville.  But good luck if they're going anywhere besides Broadway as I understand the bus service in Nashville isn't the most efficient. I don't mean to pick on the Yap's but I wonder what other people consider when they're moving into Nashville? Do they want walk-able communities? Do they want to be able to bike to work? Do they want to get rid of one of their cars and use transit more? Am I the only weirdo?

Finally, I read about [Hamilton Springs](http://www.tennessean.com/article/20121107/WILSON01/311070015/Commuter-rail-built-U-S-70-development) which will be a transit oriented community, billed as _New Urbanism_. Not much criticism here... I like the idea but it'll just be a really small island and I think we need more of this in Nashville.

I think we should be investing in existing infrastructure. We should be making the core of Nashville proper better and enticing businesses and people to move in closer, not live out 30+ miles away. Being so spread out really hurts our ability to serve people with transit. As long as we're spread out, we'll continue to have traffic problems here in Nashville. Did you know that Nashville has the [worst traffic](http://abcnews.go.com/Travel/cities-worst-traffic-nightmares/story?id=11756947#.UKpoW-TXaAk) in the nation?
