---
title: "Apple and Amazon security flaws, changes I made to protect myself"
date: "2012-08-07"
tags: 
  - "security"
  - info-sec
---

Update: [Amazon Quietly Closes Security Hole After Journalist’s Devastating Hack](http://www.wired.com/gadgetlab/2012/08/amazon-changes-policy-wont-add-new-credit-cards-to-accounts-over-the-phone/)

Last night, I read in horror Mat Honan's account of [how he got hacked](http://www.wired.com/gadgetlab/2012/08/apple-amazon-mat-honan-hacking/all/) and how a large part of his digital existence ceased to exist. The scarest part of this "hack" was that it didn't take advantage of any sort of software bug or security vulnerability. It wasn't even really somebody doing any sort of social engineering. Some clever people just used the available account recovery processes of Amazon, Apple and Google in order to gain unauthorized access to multiple accounts and delete everything.

Apple only requires a person validate the account holder name, billing address and the last 4 of the credit card on file. The first two are pretty trivial to get. If a person has their own domain name, usually a dns lookup will reveal the billing address. So how did the people involved in this get the last 4 of a credit card #? Enter Amazon.

The Amazon part is a two part process but is pretty trivial:

> First you call Amazon and tell them you are the account holder, and want to add a credit card number to the account. All you need is the name on the account, an associated e-mail address, and the billing address. Amazon then allows you to input a new credit card. (Wired used a bogus credit card number from a website that generates fake card numbers that conform with the industry’s published self-check algorithm.) Then you hang up.
> 
> Next you call back, and tell Amazon that you’ve lost access to your account. Upon providing a name, billing address, and the new credit card number you gave the company on the prior call, Amazon will allow you to add a new e-mail address to the account. From here, you go to the Amazon website, and send a password reset to the new e-mail account. This allows you to see all the credit cards on file for the account — not the complete numbers, just the last four digits. But, as we know, Apple only needs those last four digits.

The Amazon bit of this hack is astoundingly easy! Yikes! So now with the last 4 of the CC in hand, the hackers called up Apple and was issued a temporary password.

Gmail also played a role in this hack because the target of the hack was actually Mat's Twitter account. Mat was using his @me.com email address as his backup account with Google thus the hackers targeted the Apple account.

After I read the article I was horrified to realize that my Apple ID was vulnerable to the same hack. Somebody could access my Amazon account using the process outlined above (assuming Amazon hasn't already changed their security processes), call Apple Care and get access to my Apple ID. Since I had recently switched most of my online accounts to use my Apple email address, it was trivial to get the keys to my digital kingdom: Twitter, Facebook, etc. Here are the actions I took to harden myself against these types of hacks:

- I changed the CC on file with Apple (iTunes) to a different CC from the one I have on file with Amazon. In fact, I've never used this new CC with any online site at all. That by itself would have prevented the hack described by Mat from working. I actually deleted the CC's I had on file with Amazon but a look at the order history reveals the last 4 of the CC used on each order.
- I verified that I didn't have my me.com email address as my backup with gmail.
- I enabled 2-step authentication on gmail.
- I'm switching my online accounts back to my gmail account instead of using my me.com account.

My trust in Apple security has been shattered. I feel that the two factor authentication with gmail better protects me from account recovery attacks. Not just that but having used the icloud solution for a while, gmail is just a better, more full featured email solution.

Please review your accounts to make sure you're not vulnerable to this hack.
