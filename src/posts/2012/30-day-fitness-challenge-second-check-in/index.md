---
title: "30 Day Fitness Challenge, Second Check-in"
date: "2012-08-06"
tags: 
  - "fitness"
---

My daughter [Jessie](https://twitter.com/jharleyxo "Jessica Harley"), my friend [David](https://twitter.com/dbyrge "David Byrge") and myself are doing a [fitness challenge](http://michaelharley.me/blog/2012/30-day-fitness-challenge/ "30 Day Fitness Challenge"). This is the second check in.

Jess/Dave, please comment on this post with your updates.

- Myfitnesspal - I was over my calorie goal Friday, Saturday & Sunday which brings me to being over my calorie goal 4 out of 14 days.
- Distance - 6.6 miles for a total of 28.6 miles
- Weight Loss % - Bleh, the weekend binge means I gained a couple pounds so I'm 1.5% down overall. This is the formula I used: \[new weight\]/\[old weight\] \* 100 - 100.

Had a pretty bad weekend from a fitness perspective. I'm determined to do better this week.
