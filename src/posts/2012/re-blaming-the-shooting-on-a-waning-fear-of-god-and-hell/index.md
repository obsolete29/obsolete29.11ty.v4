---
title: "RE: blaming the shooting on a waning fear of God and Hell"
date: "2012-07-24"
tags: 
  - "religion"
---

I've fallen behind on my RSS reader a bit and I came across [this](http://www.rightwingwatch.org/content/afa-news-director-says-liberal-churches-media-share-responsibility-colorado-shooting) nice little bit of reporting.

> blamed the American Civil Liberties Union for destroying the public school system by supposedly forbidding students from reading the Bible. “You wonder why all these terrible things are happening to us when there is no fear of God,” Newcombe said.

I'd like to ask Mr Newcombe if fear of God is the only thing keeping him from committing mass murders, raping pretty girls he sees or killing his wife when she pisses him off? I'd say the answer is no for most people. Everybody else should check with their psychiatrist.

(via [PZ Myers](http://freethoughtblogs.com/pharyngula/2012/07/21/in-case-youve-been-wondering-who-to-blame/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+freethoughtblogs%2Fpharyngula+%28FTB%3A+Pharyngula%29))
