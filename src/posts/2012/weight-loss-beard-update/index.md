---
title: "Weight loss beard update"
date: "2012-11-03"
tags: 
  - "fitness"
---

\[caption id="" align="alignnone" width="320"\][![The Weight Loss Beard!](images/8151107581_2581218294_n.jpg "The Weight Loss Beard!")](http://www.flickr.com/photos/pamperedjen76/8151107581/ "2012-11-03 at 12.34.20 by brook181, on Flickr") It's also no shave November!\[/caption\]

The [weight loss beard](http://michaelharley.me/blog/2012/the-weight-loss-beard/ "The Weight Loss Beard") is getting bigger! Withings says I only have 9 lbs to go. I'm thinking a November 30th goal is within my grasp! Muahaha.

PS- I'm not wearing pants in this photo, please try to contain yourselves.
