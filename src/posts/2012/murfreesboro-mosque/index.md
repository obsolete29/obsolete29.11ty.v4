---
title: "Murfreesboro mosque"
date: "2012-06-01"
tags: 
  - "religion"
---

I read that a Judge [ordered](http://www.wkrn.com/story/18646563/murfreesboro-mosque-construction "Judge: Murfreesboro mosque construction must stop") that the construction on the Murfressboro mosque has to stop because of a technicality on the approval process. I'm no friend of religion or Islam and I think that our society would be better off without either but I just can't believe how little respect for the basic ideas of our country the opponents of the mosque seem to have.


{% myImage "7314457400_d62592f3ea_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

I thought that it was well understood by everybody in our country that we have freedom of religion. That means _any_ religion, not just picking a flavor of Christianity. Apparently the mosque opponents thought of that!

> Mosque opponents have fought construction for two years, arguing that Islam is not a real religion deserving First Amendment protections and that the Islamic Center of Murfreesboro has terrorist ties.

What constitutes a real religion? Why is Islam disqualified while the opponent's version of Christianity approved?

I'd bet $20 that the same people who have their panties in a wad over the mosque are also the same people hiding behind _their_ "it's my religion" shield when they [discriminate against women](http://michaelharley.me/blog/2012/cathy-samford-lawsuit/ "Cathy Samford Lawsuit"), [allow their children to die](http://www.huffingtonpost.com/2012/04/17/brandi-and-russel-bellew-lose-children-after-faith-healing_n_1432922.html), deny women contraceptives and deny people marriage equality.

The scarey part is that it's not just the dumb rednecks but also politians:


  {% myImage "7314567866_669909fc21_n.jpg", "IMG imported from an old site. Sorry no alt text." %}

Although, maybe Lou Ann Zelenik _is_ just a dumb redneck.
