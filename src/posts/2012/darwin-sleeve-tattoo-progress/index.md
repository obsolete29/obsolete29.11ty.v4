---
title: "Darwin Sleeve Tattoo Progress"
date: "2012-07-13"
tags: 
  - "tattoos"
---
<figure>
  {% myImage "darwin-01.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Progress on the Darwin Sleeve Tattoo</figcaption>
</figure>

If you've followed my blog for a while then you know that I've been working on a [Darwin Sleeve Tattoo](http://michaelharley.me/blog/2012/darwin-sleeve-tattoo/ "Darwin Sleeve Tattoo"). Last Tuesday, I had my second appointment and [Ian](http://ianwhitetattoos.com/) was really productive. You can click on the image to get a better view of it.

At the top is [Charles Darwin](http://www.topnews.in/files/charles-darwin335.jpg), then next is a Homo sapien skull on my tricep, then [Homo erectus](http://en.wikipedia.org/wiki/File:Homo_erectus.jpg) on the inside of my bicep, then [Homo ergaster](http://en.wikipedia.org/wiki/File:Homo_ergaster.jpg) in the ditch, then [Homo habilis](http://en.wikipedia.org/wiki/File:Homo_habilis-KNM_ER_1813.jpg) on the inside of my forearm and finally [Australopithecus africanus](http://en.wikipedia.org/wiki/File:Mrs_Ples_Face.jpg) on the lowest part of my arm on my inside wrist.

Even though I have two more sessions scheduled, Ian said I'd probably only need one more to finish up. We're going to add a pre-modern human spear or arrow tip and some touch up and filling in around my elbow area and then we're done.

I'm really happy with it and I think it looks great. I'm a bit apprehensive about what the in-laws will think but I have to live my life for me and not what other people think of me. [Carpe diem](http://en.wikipedia.org/wiki/Carpe_diem).
