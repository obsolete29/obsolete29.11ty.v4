---
title: "Can someone please explain prayer to me?"
date: "2012-12-28"
tags: 
  - "religion"
---

It seems most people think prayer works this way. Lady prays, is involved in an accident but God saved her; presumably because she just prayed.

\[caption id="" align="alignnone" width="500"\][![From Facebook...](images/8317574223_8bdeba2776.jpg)](http://www.flickr.com/photos/michael_harley/8317574223/ "Untitled by Michael.Harley, on Flickr") From Facebook...\[/caption\]

Most people seem to believe that things happen according to God's plan and that everything happens for a reason. If everything happens according to God's plan then why bother praying about it? You're going to be protected no matter since it's part of The Plan. I really have a problem with the _God's plan_ line of thought anyway because that means that 6 millions Jews went into the oven because of God's plan but that's another post I suppose.

If God gets the credit for saving the woman in the Facebook post because she prayed, who gets the blame for allowing stuff like [this](http://www.wate.com/story/19918492/families-of-church-van-crash-victims-file-suit-against-driver)? If you subscribe to the idea that God saves people then you have to conclude that God choose not to intervene.

I get slightly annoyed by the overly simplistic worldview of people like Mrs Miller. She was involved in a minor accident in slippery conditions. She was "saved" by the engineering that went into her car and luck, no super natural explanation required.
