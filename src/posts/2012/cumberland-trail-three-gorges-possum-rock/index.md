---
title: "Trip Report: Cumberland Trail, Three Gorges, Possum &amp; Rock Sections"
date: "2012-10-28"
tags: 
  - "hiking"
  - trip-report
  - cumberland-trail
---

[![Cumberland Trail](images/8131907919_5f1f47ba45_m.jpg "Cumberland Trail")](http://www.flickr.com/photos/pamperedjen76/8131907919/ "Cumberland Trail by brook181, on Flickr")

On Saturday I hiked some more of the [Cumberland Trail](http://www.cumberlandtrail.org). I wanted to do a longer hike so I decided to do both the [Possum](http://cumberlandtrail.org/three_gorges_possum.html) and [Rock](http://cumberlandtrail.org/three_gorges_rock.html) sections which ended up being 15.2 miles.

Title:    Cumberland Trail, Three Gorges, Possum & Rock Sections Date:    10/27/12, 7:34:28 AM CDT Distance:    15.24 mi Time:    11:55:32 Average Speed:    1.3 mph Ascent:    4,197.75 ft Min/Max Altitude:    984.25 ft, 1,942.26 ft Started:    10/27/12, 7:34:50 AM CDT - (35.34610, -85.17503) Ended:    10/27/12, 5:39:16 PM CDT - (35.40995, -85.13085)

### Directions

The directions on the Cumberland Trail web site were good for the Heiss Mountain trailhead (where I started). I was familiar with the area tho so perhaps that helped.

### The Hike

The day promised to be a good day for hiking with a weather forecast in the mid and upper 40's (not too hot!). I had planned to hike 17 miles, from the Heiss Mountain trailhead (Possum section) to the Lower Leggett trailhead (Rock section). I knew I had an option to shorten the hike to 15.2 miles by coming out at the Upper Leggett trailhead instead and I ended up taking the wimpy option and shorten the hike to 15.2 miles.

It had rained the night before and autumn is in full effect now so there were lots of wet leaves on the trail. This really drove home the point that I need some gortex boots and gaiters for fall/winter hiking because my feet were wet after about an hour. My feet weren't cold but the last time I hiked with wet feet, I got some pretty bad blisters. This time was no exception but I was able to keep the blisters to a minimum by changing into fresh socks when I stopped for lunch.

The first half of the Possum section is a pretty nice hike. Lots of scenery as you hike up and down the gorges and cross over scenic creeks and rivers. I really enjoyed this part of the hike quite a bit and it's probably my favorite part of the Cumberland Trail to date.

The last half of Possum isn't so bad either but it's just walking through the woods and there wasn't much to take my mind from the blisters forming from wet socks.

The trail instructions from the Cumberland Trail website says you have to ford two creeks but I only had to ford one and I was able to rock hop using my trekking poles for balance without having to just walk through water.

I took a lunch break at the Rock section trailhead (after 10 miles) then continued, determined to finish. The first 1/3rd of the Rock section was pretty flat and featureless and I was glad of that honestly. I was tired and was glad not to have to go up or down for a while. The last half of the Rock section had some nice features but I was too tired to appreciate them and I was dreading the climb out of the gorge up to Leggett Rd. The trail was very rocky in this section and covered in leaves. Additionally, I was dog tired so the combination of a rocky trail, lots of leaves hiding the rocks and a very tired hiker made for a stumbling, bumbling climb.

I did see some people on this hike. I saw some photographers at the beginning of the Possum section and some trail runners about half way through Possum. Right before finishing the Rock section, I passed a Cub Scout pack (these kids must have only been 5-6 years old?) going to camping for the night.

Finally, having access to the "trail section" features from the Cumberland Trail website is _really_ nice. I can verify my own GPS distance vs what's on the website and I can easily see where I am on the trail map plus see what's coming up next and how soon. I really love having it available so I've been downloading it on my iPhone before getting on the trail.

### Lessons Learned

I need water proof boots and gaiters for fall/winter hiking. Wet leaves = wet feet and that's been giving me blisters recently. I do wonder if the smart wool socks are causing the blisters because all of mine are pretty worn now. Perhaps some new socks will help with the blister problem with wet feet but wet feet will not be desirable the colder it gets.

I still need a system for fording. My plan was to cross in bare feet if it wasn't too rocky. I also brought an extra pair of socks in case it was rocky with the expectation that my soaked boats would eventually just soak through the fresh socks anyway but it was better than nothing. Luckily, I didn't have to wade through any water but my feet still got wet from wet leaves on the trail.

I need a head lamp. It was late in the afternoon when I came out at the Upper Leggett trailhead. Even if I had enough energy to continue onto the Lower Leggett trailhead, I didn't feel like I could make it before the light started to fail. If I would have had a headlamp, this wouldn't have been a concern. Additionally, I can use the headlamp for running in the evenings after work. When I ran on Thursday, it was completely dark when I got home.

I need to be able to start a fire. In the event that I get stuck out in the woods, I need to be able to start a fire. This means I need to acquire the materials and knowledge needed. It was completely dark by the time Jen picked me up and it wasn't hard to imagine how sucky a night in the wet woods would have been.

I'm thinking about switching to a dedicated hiking gps device. I've been trying to make the iPhone and an app work but I've been struggling to find a good app. I use RunKeeper for the tracks but it doesn't have any sort of navigation. I use Gaia GPS on the iPhone and that is the best app I've used but it has zero back end. It hooks into Everytrails.com but that's not a very good site either. I want to be able to see detailed statistics like on RunKeeper and then easily share my hikes. I'm thinking about getting a Garmin of some sort but they're expensive ($300 range).

### Conclusion

I really enjoyed this hike, even though I had wet feet, some blisters and was bone tired at the end. I was fantasizing about what it would be like to be a thru hiker and having to get up and do this same distance and effort again. It would be hard to roll out of the bag to get started but I think once I got going again, it would be ok. I was really sore this morning.

I would rate this hike as strenuous because of distance (15.2 miles) and elevation (4197 ft).

I would definitely recommend these sections of the Cumberland Trail to anybody wanting to see some cool Tennessee scenery.

### Resources

- [RunKeeper Activity Report](http://runkeeper.com/user/obsolete29/activity/127711873)
- [Flickr Set](http://www.flickr.com/photos/pamperedjen76/sets/72157631874735279/)

<iframe src="http://www.youtube.com/embed/qpFz3hGQ2Jg" frameborder="0" width="560" height="315"></iframe>

[Cumberland Trail, Three Gorges, Possum & Rock Sections at EveryTrail](http://www.everytrail.com/view_trip.php?trip_id=1851019) 

<iframe src="http://www.everytrail.com/iframe2.php?trip_id=1851019&amp;width=400&amp;height=300" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" width="400" height="300"></iframe>
