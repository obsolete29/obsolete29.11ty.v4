---
title: "Weight Loss Beard Update"
date: "2012-11-13"
tags: 
  - "fitness"
  - "life"
---

[![Weight loss beard!](images/8182009687_0e3c3c0a6f_m.jpg)](http://www.flickr.com/photos/pamperedjen76/8182009687/ "Weight loss beard! by brook181, on Flickr")

The weight loss beard is going strong but my weight loss stalled last week. I wasn't very focused as I didn't do well on diet or exercise. I'm refocusing this week and had a real good Monday with a new [running route](http://runkeeper.com/user/obsolete29/activity/130816064) of 5.4 miles and [good eating](http://www.myfitnesspal.com/food/diary/obsolete29?date=2012-11-12).

I'm determined to get a good week of running in as I'm running the [Mayor's 5k Challenge](http://www.mayorschallenge5k.nashville.gov/) on Sunday the 18th and I'd really like to get a good benchmark on my 5k time.

Into the breach!
