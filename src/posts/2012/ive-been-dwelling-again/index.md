---
title: "I have been dwelling again..."
date: "2012-05-29"
tags: 
  - "life"
---

I spend too much money. I've known this for a long time but I'm feeling it acutely after reading [this](http://www.mrmoneymustache.com/2012/05/26/guest-posting-financial-independence-23-years-later). I've purchased too many Walking Dead comics over the weekend. I'm in the process of getting my [Darwin sleeve tattoo](/posts/2012/05/02/darwin-sleeve-tattoo/ "Darwin Sleeve Tattoo"). We're going to DC in a couple weeks for a visit. The money is there... I always scrape it together but I wish I could _not_ spend money with the same intensity that I tend to scrape it together. There are more _things_ that I want. I want an iPad. I want more tattoos. I want camping gear. The first official day of summer is coming in a few weeks and that always makes me think about the downhill rush to Christmas and all the crazy spending that comes with it.

I know that the key to all of this is having some net worth... after this DC trip I'm going to do better, I promise.
