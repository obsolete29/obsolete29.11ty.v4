---
title: "Trip Report: Virgin Falls"
date: "2012-08-22"
tags: 
  - "hiking"
  - trip-report
  - virgin-falls
---
<figure>
  {% myImage "7811788072_76c36fed3a_m.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Virgin Falls</figcaption>
</figure>

In preparation for our [Mt LeConte](http://www.lecontelodge.com/) hike on August 25th, we hiked to **Virgin Falls** on Saturday.

**Directions**

[Here](https://maps.google.com/maps?q=virgin+falls&hl=en&ll=35.854327,-85.280964&spn=0.007174,0.013937&sll=71.773941,-133.945312&sspn=11.486626,57.084961&gl=us&hq=virgin+falls&radius=15000&t=m&z=17) is the Google maps link. It's about 20 minutes from our house!

**The Hike**

This a pretty fun hike with two falls and a lot of interesting trail. It's downhill mostly from the trailhead to Big Laurel falls (about 1/2 way) then it's up hill on the way back of course. RunKeeper clocked it at 7.9 miles with 1512" of elevation gain.

I'm not sure when the last rain was in the area but the first part of the trail was pretty wet and muddy. [Jen](http://jenharley.me/ "Jen Harley Crossville TN") slipped and banged [her elbow](http://www.flickr.com/photos/pamperedjen76/7811675036/) pretty good on a rock. It was the first time I used my first aid kit for something other than ibuprofen!

{% myImage "7811659490_dab9149ca5_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

There was only one serious [blow down](http://www.flickr.com/photos/pamperedjen76/7811206568/) on the trail and it wasn't too hard to navigate although [Tyler](https://www.facebook.com/tylerharv "Tyler Harvey") managed to scrape his legs up show boating on it. This was the second time I got to use my first aid kit for something than ibuprofen!

Overall, we set a pretty leisurely pace and spent a lot of time hanging around at both falls so we didn't make great time but we _had_ a great time.

**Conclusion**

This is a fun, interesting hike real close to our house. I'd rate this hike on the lower end of moderate on the difficulty scale. LeConte is next week and I'm pretty stoked!

Resources:

- [RunKeeper Activity Report](http://runkeeper.com/user/obsolete29/activity/110798253)
- [Flickr Photo Set](http://www.flickr.com/photos/pamperedjen76/sets/72157631167298892/)
