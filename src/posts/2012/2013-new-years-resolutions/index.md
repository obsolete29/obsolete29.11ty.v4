---
title: "2013 New Years Resolutions"
date: "2012-12-17"
tags: 
  - "40b440"
  - "life"
---

My only resolution is to not make any more resolutions. I already have my [40 before 40](http://michaelharley.me/blog/40-before-40/ "40 Before 40") list so I'm just going to focus on that. Of particular interest are the fitness goals and monetary goal.

I have 7 more sections of the Cumberland Trail to hike, 4 more "races" to complete (1 of which I think will be a 10K), I have to get 8 minutes faster on my 5K time, I have to get 3.5 minutes faster on my 1 mile time, I have 60 lbs to go on my weight loss (think I might have bitten off more than I can chew on that one) and the 100 push up/sit up goal. That's a lot of fitness crammed into 9 months.

The monetary goal is to simply become debt free except for the mortgage. We ratcheted up the difficulty level as we borrowed some money for Christmas which means our hole is a little deeper at $17K and we only have 9 months to pay off that much money.

Wish me luck guys, I'm going to need some! What are some of your goals?
