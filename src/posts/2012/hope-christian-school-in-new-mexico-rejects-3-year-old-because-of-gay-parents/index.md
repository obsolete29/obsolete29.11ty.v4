---
title: "Hope Christian School In New Mexico Rejects 3-Year-Old Because Of Gay Parents"
date: "2012-08-10"
tags: 
  - "religion"
---

[Awesome](http://www.huffingtonpost.com/2012/07/26/hope-christian-school-rejects-3-year-old-because-parents-are-gay_n_1706283.html)!

> Hope Christian School, a **partially publicly funded religion-based institution** in Albuquerque, N.M., has garnered local media attention after denying a 3-year-old admittance because he has two gay fathers, KOAT-TV reports.

Emphasis mine. Am I missing something here? How can a school that receives part of it's funding from tax payer dollars, discriminate against a 3  year old kid because of his dads' lifestyle?
