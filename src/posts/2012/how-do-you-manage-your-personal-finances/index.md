---
title: "How do you manage your personal finances?"
date: "2012-10-09"
tags: 
  - "finances"
---

[![2012-08-07_1701](images/7735840450_858791cffd_m.jpg)](http://www.flickr.com/photos/pamperedjen76/7735840450/ "2012-08-07_1701 by brook181, on Flickr")

I asked today on Twitter how people manage their personal finances. I got a couple responses of "Mint". I use Microsoft Money but since so many people seem to use Mint, I often think that perhaps I'm doing something wrong or I'm thinking about finances wrong. Here is my process... feel free to chime in at the end.

I don't use a monthly budget. I find that the idea of a monthly budget is just too vague and isn't useful from a cash flow perspective. So if I have $200 budgeted for donuts in October, what happens if I blow my $200 budget in the first week of October and now my rent check bounces? I like to look at personal finances at a more detailed level. If I make $1000 this payday, I want to know how much I owe in bills before next payday  to make sure I _can_ buy donuts. I also want to know how much is coming out _next_ payday so I can know how much, if any, of this paycheck to save. As far as I can tell, you can't look ahead like that with Mint. Mint doesn't have any sort of automatic reminders that are auto entered into your check register nor do they have any sort of cash flow projections based on said reminders.

I've actually level set my bills by using a "bill" account at ING Direct. This is just a normal electronic orange checking account but I've calculated my monthly expenses, calculated at a bi-weekly level (my pay period) and then I transfer that amount into the bill account (automatic electronic transfer of course). For example, lets consider the following monthly bills:

Cell phone: $200/month ISP Bill: $45/month Electric: $80/month Water: $40 /month

In order to get the bi-weekly amounts on those, you just add everything up then divide by two. So that turns into this:

Cell phone: $100/bi-weekly ISP Bill: $22.50/bi-weekly Electric: $40/bi-weekly Water: $20 /bi-weekly Total: $182.50

Every payday, $182.50 gets transferred from our "main" checking account over to the bill account automatically and all of those bills are paid automatically with bill pay on their due dates. This helps me to easily keep up with it. If every payday I get paid $1000, I pay $100 on the gas/grocery CC and $182.50 goes to the "bill" account then that avoids having a lean pay period and a rich pay period as the expenses on the main account are almost always the same from paycheck to paycheck.

I think maybe everybody has a lot more money than I so they don't have to think in terms of short term cash flow.

So how do you manage your personal finances?
