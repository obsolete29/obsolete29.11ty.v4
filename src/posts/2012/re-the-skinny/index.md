---
title: "RE: The Skinny..."
date: "2012-06-24"
tags: 
  - "fitness"
---

Melissa Ellis is doing a [public weight loss program](http://mymidlifedoover.wordpress.com/2012/06/18/the-skinny/). I too would like to lose weight and I'll be doing it along with her. Additionally, I'll be knocking out 3 of my [40 before 40](http://michaelharley.me/blog/2012/my-40-before-40/ "My 40 Before 40") items (8, 9 and 19). During the spring, I was down to 272 but I've gained 10 lbs and I was 282 as I weighed in this morning. My goal is that just by being aware of the number of calories going into my pie hole and exercising at least 30 minutes a day for 30 days, I'll see some weight loss... I'm hoping to get below 270 before August 1st.
