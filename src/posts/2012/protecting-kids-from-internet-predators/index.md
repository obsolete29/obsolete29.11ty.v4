---
title: "Protecting Kids From Internet Predators"
date: "2012-05-02"
tags: 
  - "life"
---

I saw this article, [How a fake Justin Bieber "sextorted" hundreds of girls through Facebook](http://arstechnica.com/tech-policy/news/2012/05/how-a-fake-justin-bieber-sextorted-hundreds-of-girls-through-facebook.ars), while going through my RSS feeds this morning. I have a 20 year old daughter and we have three boys in the house plus we have lots of friends with kids so I've thought about this type of stuff quite a bit.

{% myImage "20120502-103804.jpg", "IMG imported from an old site. Sorry no alt text." %}

When my daughter was growing up, I made a pretty big mistake: We allowed her to have a computer in her bedroom. In retrospect, I think this was a mistake because we couldn't see what websites she was surfing or who she was chatting with. AFAIK, there was only one time that she was searching for porn on the web. I'd like to think that I handled that incident pretty well. I.e., I didn't freak out on her and explained how porn can set unrealistic expectations about what sex is etc.

With Jen's boys, we've always had a policy that all computer usage must happen in the public area of the house so that we can glance over and see what they're doing at any time. This has seemed to work well but times are a changing. Now everybody has these little pocket computers called smart phones. T has an activated Droid 1 and both of the younger boys have unactivated Droid 1's (they can only use them on WIFI), although neither are permitted to have social media accounts until they're 13. The problem with having smart phones is that it's a lot harder to monitor the kids using smart phones compared to monitoring kids using laptop computers on the kitchen table.

Our strategy so far has just been the threat of mutually ensured destruction. I.e., we review browsing history and need to have access to the phones at a moments notice and if anything turns up, the phones are taken away. We've only had one incident where the middle kid was searching for "boobs". But an after the fact review of logs and such won't protect kids from stories like one I linked to at the beginning of my post. Pretty scary thought.

What do you guys do with your kids? Technology is here and I think we all want to equip our kids by exposing them to technology so that it's second nature but there are dangers.
