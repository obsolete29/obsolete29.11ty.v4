---
title: "Titans vs Colts"
date: "2012-12-09"
tags: 
  - "titans"
---

Yes the Titans are bad and we're in the lame duck phase of our season but I'm still looking forward to today's game vs the Colts. I'm hoping to see the offense look better under new coordinator Dowell Loggains and assistant Tom Moore. I've tempered my expectations so just an incremental improvement would be great. The offense doesn't have to look like the Manning Colts but hopefully we won't seem completely outclassed and out of sync like last week. Hopefully we can get the run game going and WR's won't drop passes that hit them in the hands.

My brother has influenced my thinking on the Titans defense in that I don't think it's as bad as I initially thought. I'm open to being influenced on this but I feel that the defense has been better that past few weeks and it's the offense that's really the problem. I still don't have a good feel for what Grey tries to do on defense. At least with Fishers defense I knew that the defensive line was pinning it's ears back and coming for the QB. I actually think that philosophy makes a lot of sense in this day and age of passing offenses. The defensive line in this defense doesn't make a lot of plays. I'm not sure if that's by design insofar as that they're just supposed to stay in their lanes and fill gaps. The philosophy for the secondary seems to be just keeping people in front of them as it's rare to see our DB's making a play other than tackling people after the catch. Again, maybe that's by design and maybe Grey doesn't have the talent he wants (who does?).

I'm expecting this game to be more competitive than last week. Here are my keys for success for the Titans:

- WR's have to catch balls that hit them in the hands. Never mind making any sort of extraordinary plays afterwards. If they just catch the ball and fall down before they fumble or injury themselves, that would be an improvement over what we've seen lately.
- CJ has to have a good game. Get him going early and often IMO. I don't think CJ is worth what we've paid him but he's still a weapon and we have to keep teams honest. If they put 8 in the box to stop CJ that _should_ give Lock more passing opportunities.
- Locker has to continue to try to make plays but he shouldn't force things, especially early. I realize that's a tricky line to walk but all of the deflected passes last week in the 1st half was pretty frustrating for me so I'm sure it was frustrating for him.
- The patchwork offensive line has to give Jack enough time. I expect more rollouts and quick passes. Maybe the Titans can figure out how to run a screen too?

[![Posted with Blogsy](images/blogsy_footer_icon.png)Posted with Blogsy](http://blogsyapp.com)
