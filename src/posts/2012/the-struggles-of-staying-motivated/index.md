---
title: "The Struggles of Staying Motivated"
date: "2012-11-01"
tags: 
  - "life"
---

I've been working on losing weight and being a healthier and better person but I've been struggling with my motivation recently.

I motivate myself by using silly tricks like having [contests with my daughter](http://michaelharley.me/blog/2012/30-day-fitness-challenge/ "30 Day Fitness Challenge") or growing a "[weight loss beard](http://michaelharley.me/blog/2012/the-weight-loss-beard/ "The Weight Loss Beard")". These tricks generally work pretty good for me as I respond well to competition and gamification. I've had some success as I've gone from 284.7 lbs on July 23rd to 268.5 lbs on Oct 27th. That's a good feeling.

I've also had some success with being healthier by getting into running. I'm running farther and faster than I ever have. I've had lots of encouragement on Twitter from my friends [Sally](https://twitter.com/sallaboutme) and [Freaky](https://twitter.com/FreakyWeasel). My wife has also been instrumental in helping me stay motivated as she's very supportive and encouraging. It's hard to explain how much the encouragement from my friends and family have helped me but it helps _soooo_ much. That's a good feeling.

I used to have a tendency to completely give up and crash back down to my old self when I'd have a bad day or two. The thinking was "well, I've already screwed this up so f' it..." and I'd just slip back into my old habits and go on an eating bender. I haven't ran since last Thursday. Ugh... I hate that. I've had a few bad days of eating too. When I'm at home, my day is a lot less structured than when I go to work. At work, I have my lunch box with all of my prepared food and I have a schedule of when I'm going to eat it. Easy peazy!

I hiked 15.2 miles Saturday and that's really when this latest bender started. I felt that I had hiked so far that I could pretty much eat whatever I wanted and it would be no big deal... so I ate all the things. Then on Sunday I used the same excuse to cram my fat face with food. Monday I actually went to work but I gave myself another pass and one trip to the snack machine turned into two then a double cheese burger (and milk shake!) sounded really good for the trip home. Ugh. (/self loathing)  Tuesday I telecommuted so it was an unstructured day and I had already slipped up Saturday, Sunday and Monday... lets get Cracker Barrel and then Subway and I don't even remember what I had for dinner but I bet it wasn't good (maybe Blind Zebra?). Wednesday I didn't go to work so another unstructured day so more of the same: Triple D Diner for breakfast, Taco Bell for lunch and Krystal's for dinner (plus lots of Halloween candy!). Never mind the war on my waistline, none of this was budgeted spending. :|

So why has this happened? Recently, I've had some stress at work, mostly related to a project that I've been heavily involved with. When I'm stressed, it's easy to allow myself eating bad and it kind of snowballs. At least I think it's the work stress but maybe it's a few things. I've felt melancholy for a few weeks... the drive has been wearing on me and it takes more of an effort to keep up with the chores at home. Plus the holidays are coming up and this time of year always stresses me out. But those are just excuses, how do I fix it?

So here I am, trying to dig in my heels to stop the slide into the abyss. I just have to remind myself that four days of slippage doesn't undo the last three months of doing well. Even if I've gained a couple lbs, it takes just a couple days of doing well again to get that back off. I'll probably be a little slower on my run this evening but that's not the end of the world and it will only take a few runs to get that back. I'm hiking again on Saturday and I've had a good, structured day today and I'll do that again tomorrow. I already feel like I've mostly regained my footing (it's nearly all mental) but I'm afraid of what can happen if I'm not mindful of my bad tendencies.

I'm reminded of several quotes that basically say (paraphrasing): _It doesn't matter that you fail. It only matters that you try again._ This is how I've approached my goals with fitness and finances. I'm going to make mistakes and I'm going to fail but if I just keep taking swings at it, I know I can be successful.
