---
title: "Two great articles on password security"
date: "2012-08-22"
tags: 
  - "security"
  - info-sec
  - passwords
---

[Why passwords have never been weaker—and crackers have never been stronger](http://arstechnica.com/security/2012/08/passwords-under-assault/) (Ars)

> The average Web user maintains 25 separate accounts but uses just 6.5 passwords to protect them, according to a [landmark study (PDF)](https://research.microsoft.com/pubs/74164/www2007.pdf) from 2007. As the Gawker breach demonstrated, such password reuse, combined with the frequent use of e-mail addresses as user names, means that once hackers have plucked login credentials from one site, they often have the means to compromise dozens of other accounts, too.

[More than just one password: Lessons from an epic hack](http://blog.agilebits.com/2012/08/19/more-than-just-one-password-lessons-from-an-epic-hack/) (1password)

> [Mat Honan](http://www.wired.com/gadgetlab/author/mathonan/), a 1Password user and writer for [Wired](http://www.wired.com/), did everything right. He had strong, unique passwords everywhere. Yet he was the [victim of an “epic hack”](http://www.wired.com/gadgetlab/2012/08/apple-amazon-mat-honan-hacking/), and had to put a great deal of effort into[getting his digital life back](http://www.wired.com/gadgetlab/2012/08/mat-honan-data-recovery/).
> 
> A very brief account of this [Homer](http://en.wikipedia.org/wiki/Homer)\-worthy hack is that someone talking to Amazon customer service got into Mat’s Amazon account, from which they were able to learn enough about him to then call Apple’s customer service to get a new password set. Once they had Mat Honan’s Apple ID and password they used Remote Wipe to erase his iOS devices and his Macs.  This has been written about extensively elsewhere, but I would like to talk about this in the context of whether there are useful lessons for 1Password users.
