---
title: "Quote of The Day - John Greenleaf Whittier"
desc: "Quote of the day for 4/30/2012"
date: "2012-04-30"
lastmod: "2012-04-30"
tags:
  - "qotd"
---
“For all sad words of tongue and pen, The saddest are these, 'It might have been'.” — John Greenleaf Whittier
