---
title: "Trip Report: Cumberland Trail, Three Gorges Segment, Soddy Creek Section"
date: "2012-09-10"
tags: 
  - "40b440"
  - "hiking"
  - trip-report
  - cumberland-trail
---
{% myImage "7959136824_6d5fe8f975_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

On Saturday I hiked another section of the [Cumberland Trail](http://www.cumberlandtrail.org/); [Three Gorges Segment, Soddy Creek Section](http://www.cumberlandtrail.org/three_gorges_soddy.html).

**Directions**

I hiked the trail northbound from Mowbray Pike to Heiss Mountain Rd. The directions on the Cumberland Trail website (quoted below) for the Mowbray Pike trailhead were pretty decent with a couple minor differences. Mountain Rd is unmarked and was 1.0 miles after turning onto Dayton Pike (instead of 1.1). You'll turn right before the Dollar Store. The trailhead was 2.4 miles according to my odometer instead of the 1.8 listed in the directions after turning onto Mountain Rd. I was about to turn around thinking I had missed it when I finally saw it.

> From Chattanooga take US-27 north to Sequoyah Road in Daisy.  Turn right on Sequoyah Road and travel one-half mile to the intersection with Dayton Pike.  Turn left on Dayton Pike and proceed 1.1 mile to the intersection with Mountain Road.  Turn right on Mountain Road and proceed 1.8 miles to the trailhead on the right. (Note: Mountain Road becomes Mowbray Pike)

**The Hike**

The hike is listed as 16.1 miles and moderately strenuous so I was expecting a hard hike and I was not disappointed.  Plus, I was feeling a bit under the weather with a sore throat and the tickling in my chest. Not to be deterred, I decided to go ahead on the hike.

The fun started within the first part of the hike when I slipped hard on a slippery rock. It didn't _look_ slippery but the thing was coated in some sort of slime, I swear. My backpack caught part of the fall but my booty caught the majority of it. After lying there for 10-15 seconds, the systems check came back and said that nothing was seriously wrong. After checking myself out at home, I had a pretty angry looking, deep dark purple bruise on my booty!

About halfway through the hike, there are two creek crossings with no bridge ([map](http://www.flickr.com/photos/pamperedjen76/7959125064/)) and it was right before deep creek that I got a bit turned around. The trail came to a spot and it split into a V. The trail to the left was marked with a sign with the word TRAIL on it. The trail to the right had the normal white blaze and went downhill. I opted for the blazed trail (partly because it went downhill and not up) and hiked down a few hundred feet to Deep Creek. Once I got down to the creek, it was very rocky and slippery and I opted to have part of my lunch and to weigh my options. I was a bit concerned because I couldn't see any white blazes on the other side of the trail (only the pink surveyors' ribbon) and the crossing seemed pretty [treacherous.](http://www.flickr.com/photos/pamperedjen76/7959252504/) There was a swimming hole so I thought maybe this was just a spur trail to the swimming hole or that this was going to be a future part of the trail once the bridge was built. So after weighing my options, I convinced myself that the big giant sign with the word TRAIL on it must be the right way, climbed back up the few hundred feet and took the TRAIL trail. It turns out that this was a sign for the rock climbers... there is apparently a parking lot in that general direction and they access the rock bluffs across the creek. Gah. So after exploring that bit, turning back around and starting _back_ down the hill, I met the only other people I saw on the trail. Three old men were hiking together and I was able to query them about where to cross and if that was the right way. By the time I got back down to the creek it had started to rain. I was a bit gun-shy of slippery rocks so I butt-scooted on a couple sections and managed to soak my pants in the process but my feet were perfectly fine. Better a wet butt and pants instead of taking another shot on the rear-end by falling. Plus, hiking with wet, cool pants was actually pretty nice after the humidity of the morning.

Next up was crossing Big Soddy Creek. There wasn't a bridge here and no way to cross by rock hopping... even if there were, I'm not sure I'd trust the slippery rocks so I decided to just wade across. Luckily, the [spot I crossed](http://www.flickr.com/photos/pamperedjen76/7959253646/) wasn't very deep or wide but I did end up getting some wet feet and that turned out to be pretty significant. I've only had to ford a creek two other times and never with so many miles left to hike. After I crossed, I took off my boots and socks and tried to wring out my socks and pour the water out of my boots. Apparently, wet socks move a lot more than dry socks do. I managed to give myself at least 4 blisters including [this massive thing](http://www.flickr.com/photos/pamperedjen76/7959279138/).

The rest of the hike was pretty uneventful. I was progressively more miserable as my feet got worse and it continued to rain and I got more tired. There was a lot of mining evidence on the hike as well as lots of old trash dumps when the trail got close to a road. One other thing to note was the big wood spiders. On other trails, I've walked through a lot of spider webs and even if there was a spider on the web, I'd just brush it off without too big of a deal but these spiders were big! Bodies as big as my whole thumb! I managed to avoid walking directly into any with big spiders in them, although I did brush a few. \*shivers\*

**Conclusion**

There were some neat features along the trail, but nothing worth the effort that a 16 mile, moderately strenuous hike takes. I gained almost the same elevation as we did when we hiked Mount LeConte and with nothing even _close_ to the views that LeConte has. I just remember thinking that the trail was meandering up and down mountains for no good reason. After getting home, I'd felt like I'd been in a fight and lost. Badly.

I tried a new lunch for hiking on this trip. I made peanut butter, chocolate and dried apricot wraps that I saw in the Backpacker magazine. I made myself two and they were pretty delicious and filling. I will for sure make these again for hiking.

**Lessons Learned**: I need a system for creek crossing. Hiking long miles in wet shoes isn't good, period. I'm not sure if that means I need to get some water shoes or just bring extra socks but I'll be doing some research. Additionally, I need to get my own water filter... my friend Dave let me borrow his and I had to use it about 3/4th of the way through. Next, I need bring a head lamp... it was getting dark towards the end of the trail and I was getting a little worried about making it out in time. Finally, I just feel like I need to be better prepared for an overnight stay if the need arises. If my feet would have gotten bad enough or if I ran out of day light I wouldn't have been able to make a fire or anything. I wouldn't have been in _serious_ trouble because I had the water filter for water and I had a long sleeve base layer shirt and my rain jacket but not being able to build a fire would have been bad.

Resources:

- [RunKeeper Activity Report](http://runkeeper.com/user/obsolete29/activity/116365103)
- [Flickr Photo Set](http://www.flickr.com/photos/pamperedjen76/sets/72157631490069900/)
