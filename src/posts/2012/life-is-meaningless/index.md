---
title: "Life is Meaningless..."
date: "2012-11-05"
tags: 
  - "titans"
---

\[caption id="" align="alignright" width="240"\][![By Bruce A Stockwell](images/2706305147_df83e23e91_m.jpg "By Bruce A Stockwell")](http://www.flickr.com/photos/bas68/2706305147/ "titans by Bruce A Stockwell, on Flickr") By Bruce A Stockwell\[/caption\]

... and full of pain.

The Titans were embarrassed by the Bears on Sunday 51-20 but it wasn't even that close. I struggle with trying to figure out what exactly is wrong with our team and who's to blame. I'm just a layman, watching it on TV.

Paul Kuharsky [reported](http://espn.go.com/blog/afcsouth/post/_/id/43145/fraying-titans-overmatched-by-bears):

> Rookie receiver [Kendall Wright](http://espn.go.com/nfl/player/_/id/14909/kendall-wright) said he thought he was responsible for at least one of the calls.
> 
> “It hurt the team a lot,” he said. “But what I did at practice all week, I thought I was on the ball. I screwed it up. It’s my fault all the way.”
> 
> He thought he was on the ball all week, but he was supposed to be off the ball and no one spotted it or corrected it until the officiating crew got a look on Sunday? Sorry, but that is some major evidence in a case against the people running things for this team right now.

Ouch. WTF coaches?

Also:

> [Jordan Babineaux](http://espn.go.com/nfl/player/_/id/5840/jordan-babineaux) was the one Titans player I talked to who didn’t offer an immediate defense of the coaches and the plan.
> 
> “You got any questions, you’ve got to ask the defensive coordinator,” he said, referring to Jerry Gray.
> 
> I asked about the blocked punt, where he was lined up as the personal protector, but where he didn’t offer protection, running to the right and cutting out of the backfield entirely. He said I’d need to ask the special-teams coach, Alan Lowry.

Babs is saying he did exactly what the game plan called for... double ouch.

But then you read something like this (also from Paul's article):

> Titans coaches warned players all week about how [Charles Tillman](http://espn.go.com/nfl/player/_/id/4493/charles-tillman)strips the ball, offering specifics of his techniques.
> 
> Then four Titans went out and got stripped by the Bears cornerback, including [Kenny Britt](http://espn.go.com/nfl/player/_/id/12556/kenny-britt)on the game’s first play from scrimmage.
> 
> What does that say about the quality of players on Tennessee’s roster and their ability to absorb and execute a coaching message?

If the players are being coached on something but they're not getting it... what the hell is the coach supposed to do?

It sucks that Munchak is having to experience this adversity but it seems to me that he's not an effective coach right now. He and his staff are either not coaching them correctly or the players aren't listening. If he's lost his team... that's not good. So I hope Munchak gets it all figured out.
