---
title: "30 Day Fitness Challenge, First Check-in"
date: "2012-07-30"
tags: 
  - "fitness"
---

My daughter [Jessie](https://twitter.com/jharleyxo "Jessica Harley"), my friend [David](https://twitter.com/dbyrge "David Byrge") and myself are doing a [fitness challenge](http://michaelharley.me/blog/2012/30-day-fitness-challenge/ "30 Day Fitness Challenge"). This is the first check in.

Jess/Dave, please comment on this post with your updates.

- Myfitnesspal - I was only over my calorie goal Sunday by 430 calories. Stupid baby shower and their stupid cup cakes. So, I was only over my calorie goal 1 day.
- Distance - 22 miles.
- Weight Loss % - 1.76% This is the formula I used: \[new weight\]/\[old weight\] \* 100 - 100.
