---
title: "Mayors 5K Challenge - My First 5k!"
date: "2012-11-21"
tags: 
  - "40b440"
  - "fitness"
---

Completing 5 "races" is the second item on [My 40 Before 40 list](http://michaelharley.me/blog/2012/my-40-before-40/). On Sunday the 17th, I ran my first ever real 5K, the [Mayor's 5K Challenge](http://www.mayorschallenge5k.nashville.gov/) in Nashville.

Going into it I really had no idea what to expect. There were lots of people -- the website says 5000! -- doing this thing and I was a little surprised at how many of us there were.

Seeing how the corrals worked was informative. I got into the main corral; the 21-35 minute one. That seemed like a wide range so I got into the very back. I usually do a 12 minute mile so that means I should be able to finish a 5k right around 36 minutes. The course was hilly and I was a little surprised about that. You don't really notice the hills when you're driving downtown but that's quite another story when you're running.

I hadn't really ran any the week before which was a huge mistake... I should have figured out some way to make the time because I struggled on the whole run to find my zone and feel good with my pace. I'm not sure if it was all the people I was running with or the fact that I hadn't run any that was throwing me off but I was pretty miserable the whole time. Plus, headphones were "discouraged" on the run so I didn't bring mine. I'm a newbie runner and it's hard to run without the music; weird I know.

[RunKeeper](http://runkeeper.com/user/obsolete29/activity/131993300) says I finished in 38:38. I thought I would be faster and was hoping for 35 minutes. Another one of my 40 before 40 goals is to finish a 5k in under 30 minutes so that means I have a lot of work to do.

In conclusion, I'm happy I got this first one under my belt. I learned a lot about how these things work so I won't have that deer in the high beams look next time. I should have probably done some recon on the course so the hills wouldn't have been a big surprise and I should probably have made sure I was training enough the week leading up to the event. I hope to do another 5K in December. If you guys know of anything fun, let me know. I posted a couple photos below. You can check out the others [here](http://www.flickr.com/photos/pamperedjen76/sets/72157632059162370/with/8202167281/).

 

\[caption id="" align="alignnone" width="500"\][![Nervous energy! Darwin is not impressed.](images/8197485117_7b0975fd3a.jpg "Nervous energy! Darwin is not impressed.")](http://www.flickr.com/photos/pamperedjen76/8197485117/ "2012-11-18 at 14.10.14 by brook181, on Flickr") Nervous energy! Darwin is not impressed.\[/caption\]

\[caption id="" align="alignnone" width="500"\][![And the finish.. ](images/8202167837_57d44dcdf4.jpg "And the finish.. ")](http://www.flickr.com/photos/pamperedjen76/8202167837/ "IMG_4964 by brook181, on Flickr") And the finish..\[/caption\]
