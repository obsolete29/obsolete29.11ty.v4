---
title: "Prometheus spoiler alert!"
date: "2012-06-11"
tags: 
  - "prometheus"
  - movie-review
---
{% myImage "7176412575_cd2eb18132_o.jpg", "IMG imported from an old site. Sorry no alt text." %}

I watched Prometheus opening weekend and I enjoyed it pretty well. I didn't realize that it was an Alien franchise prequel so that little bit was a complete surprise to me!

Overall, I think the science fiction bits were interesting and well done (nice ship!). I enjoyed the actors a great deal and thought all of that was peachy.  I do have some complaints tho.

## Major Issues

- Intelligent Design - Ugggghhh... the whole intelligent design/faith based angle of the movie was kind of a bummer to me. I rewatched Alien yesterday with the intention of watching the rest of the series but I don't recall the other movies having such a large part (or any really) of the plot revolving around that topic. It felt like it was focused grouped into the movie or something. Just didn't care for it.
- Characters acting stupidly - Specifically, the first guy in the cave to be attacked by the alien larvae. **_Who would act that way?!?_** It's cute? Lets try to pet it? What. The. Fuck. Man. The utter craziness of it was super jarring to me and bolted me out of my suspension of disbelief during the movie.

## Other Issues

- I didn't quite understand the motivation of the human aliens intent to destroy the Earth. Why seed the Earth with their DNA, then change their minds and attempt to destroy it with their bio weapons? More detail [here](http://enchantedmitten.blogspot.com/2003/12/reading-previous-entries-in-this-series.html) (Thanks [Dave](https://twitter.com/dbyrge/status/211950774077505536)).
- Why did David infect Peter with the substance from the bio weapon containers? _Why_ do that?
- David infected Peter with the substance from the bio weapon containers. Does the bio weapon stuff cause the Alien aliens? After David infected Peter with the biomass, Peter had sex with Elizabeth and she became pregnant with an Alien alien. What?
- Why were larvae/transitional forms of the Alien aliens so different in Prometheus than in the Alien movies? The aliens from the Alien movie were very small when they came out of the pods... they attached to a person's face. The Prometheus Alien was a big bad ass mofo.
- Considering that we have military drones in our day and age, it seems logical to me that any ship of the Prometheus' caliber would also have drones and they would be super sophisticated. Why wouldn't they deploy scouting drones to explore the planet before getting out of their ship or even _landing_ for that matter? Why wouldn't they send robotic missions to this planet ahead of the ship to have all points of interest identified and all of those buildings mapped out?
- It also seems logical to me that a ship going to basically make first contact with unknown aliens would have a dedicated military component not just geologists with flame throwers. Where are the ex-marines with the Mk V, military grade, power assisted suits and military grade weaponry?

Like I said, I did enjoy the movie despite all of these annoyances. I'd give it a 3.5 stars out of 5 (Netflix scale) and allow it to round up to a 4 (Really Liked it!). How did you like it?
