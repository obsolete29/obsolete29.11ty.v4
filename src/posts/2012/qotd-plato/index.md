---
title: "QOTD - Plato"
date: "2012-05-04"
tags: 
  - "qotd"
---

Music gives soul to the universe, wings to the mind, flight to the imagination, a charm to sadness, gaiety & life to everything. - Plato
