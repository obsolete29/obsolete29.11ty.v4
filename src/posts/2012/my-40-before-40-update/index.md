---
title: "My 40 Before 40 Update"
date: "2012-07-24"
tags: 
  - "life"
---

I updated my [40B440 list](http://michaelharley.me/blog/2012/my-40-before-40/ "My 40 Before 40") go check it out. I've hiked some trails, read a bunch of books, drank a bunch of beer, went on vacation, visited some museums and got some more of my tattoo done.

I'm anxious to work on my fitness goals and our [fitness competition](http://michaelharley.me/blog/2012/30-day-fitness-challenge/ "30 Day Fitness Challenge") is going to address that, I hope!
