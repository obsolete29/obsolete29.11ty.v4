---
title: "40B440 Update"
date: "2012-05-14"
tags: 
  - "40b440"
---

I'm working a few of [My 40 Before 40](http://michaelharley.me/blog/2012/my-40-before-40/ "My 40 Before 40") items. I'm going to do my exercise,  myfitnesspal logs and no caffeine for 30 days items simultaneously as they seem to go together in my mind. On Saturday I purchased myself a bottle of American Seynal Blanc from [Stonehaus Winery](http://stonehauswinery.com/) and had a glass. I didn't want to just go in for a tasting and consider my challenge met; seems like cheating. So even though I'm not a big wine drinker, I thought the wine was pretty good.

8\. Exercise at least 30 minutes a day for 30 days straight (1/30) 9. Complete myfitnesspal logs for 30 days straight (1/30) 13. Try 10 new wines (1/10) 19. Go a month without caffeine (1/30)
