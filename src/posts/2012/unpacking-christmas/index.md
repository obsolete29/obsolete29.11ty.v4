---
title: "Unpacking Christmas"
date: "2012-11-21"
tags: 
  - "finances"
---

\[caption id="" align="alignright" width="192"\][![Santa and Mark -- Vintage 1959 Santa](images/5864910729_14f8eafbed_m.jpg "Santa and Mark -- Vintage 1959 Santa")](http://www.flickr.com/photos/mutrock/5864910729/ "Santa and Mark -- Vintage 1959 Santa by mutrock, on Flickr") Photo by Mark Kortum\[/caption\]

Christmas is a stressful time of year for me. The problem is that my own expectations of what I want to buy people doesn't always match up to what our Christmas savings account can afford. That sets up an internal struggle and I can't admit to myself that I've internally over promised the gift giving. So I spend about 6 weeks wrestling with it and it just stresses me out. I wish I had all the money to get everybody all the perfect gifts.

So, how do I fix this so that Christmas doesn't stress me out? Here are my actions items and I'll be applying these to next year.

**Emergency Savings** 

The lynch pin to the whole thing is having some money in a savings account for when _things come up_. Maybe I can call this saving account _emergency savings_. Ingenious right? That I should be doing the thing that I've been blogging about for months now? Believe me... it's as frustrating to keep typing that as it is for you guys to keep reading about it. So here I am again, resolving (again) to try harder. I'm swearing a blood oath, swearing on my ancestors graves and binding myself to the gods of personal finance but the emergency savings account will fully funded before the end of January... I swear, cross my heart and hope to die if it's not. ;)

**Christmas Club**

The frustrating thing is that we _have_ a savings account for our Christmas money. If I just let the auto-debits transfer over there and leave it alone, we'd have about $1,000 to spend on Christmas gifts. Something always comes up though so we have to dip into the Christmas club and I leave an IOU with every intention of paying it back but then something else comes up and then something else... /sigh. With the emergency savings in place though, we won't have to dip into this pot of money.

So that's my  master plan. If we have a fully funded emergency savings in place, then the Christmas club money can build up and it'll be there when Christmas rolls around again.

How do you save for Christmas? What kind of Christmas budget do you guys have?
