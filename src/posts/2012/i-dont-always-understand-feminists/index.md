---
title: "I do not always understand feminists..."
date: "2012-05-10"
tags: 
  - "feminism"
---

I'm a feminist. I'm a father with a daughter so _of course_ I'm a feminist. Even if I only had boys I think I'd still be a feminist but maybe I wouldn't be as actively trying to understand feminist issues as I am now.

I follow several prominent feminists including [Jessica Valenti](http://jessicavalenti.tumblr.com/ "Jessica Valenti"), [Amanda Marcotte](http://pandagon.net/index.php/site/index/ "Pandagon"), [Rebecca Watson](http://en.wikipedia.org/wiki/Elevatorgate "Rebecca Watson") and [Greta Christina](http://freethoughtblogs.com/greta "Greta Christina"). It took a little thinking for my male privileged brain to fully understand the problem with [Elevatorgate](http://en.wikipedia.org/wiki/Elevatorgate#Elevator_incident) but I finally did.

<figure>
  {% myImage "7172389494_4387706024_m.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Too much cleavage!</figcaption>
</figure>

I explain all of that to say that I'm having another failure to launch moment... I'm not quite understanding Betsy Rothstein when she says Michelle Fields was showing too much cleavage on FoxNews and that's sexist [(source](http://www.mediabistro.com/fishbowldc/theyre-real-and-theyre-spectacular_b73051)):

> Anyone watching Fox News’s **[Neil Cavuto](http://www.mediabistro.com/Neil-Cavuto-profile.html)** Wednesday afternoon was in for a real treat of exposed breasts. Free of charge. The star of the soft porn film, _er_ show, was _The Daily Caller_‘s**[Michelle Fields](http://www.mediabistro.com/Michelle-Fields-profile.html)**, who decided to go on national TV with a large portion of her breasts jiggling out of her shirt.
> 
> This isn’t the first time Cavuto’s program and others have been criticized for having on scantily clad female guests. Media Matters [wrote about](http://mediamatters.org/iphone/research/201101310001) the rash of sexism emanating from the network’s programming in January 2011.

The implication is that the act of Michelle Fields showing too much cleavage is sexists. Without some qualifications, I don't think it is. I thought a big part of the feminist movement is to allow women to dress sexy (if they want) without men seeing that as an invitation to rape and harass said women? From a sex positive perspective, where is that line of a woman dressing sexy in order to feel sexy and men objectifying her? Am I a sexist jerk for thinking that Michelle is good looking and for appreciating her beauty?

I've always thought that it's possible for women to dress sexy and for men to appreciate her. Men just have to treat her like a person (a sexy person!) and not just some sex object. We shouldn't street call women, we shouldn't make rude sexual gestures and we should just act as decent fucking human beings. A woman dressy sexy isn't an invitation to treat her like a sex object.

Maybe Betsy is suggesting that Cavuto's show is creating a hostile work environment for Michelle? Maybe she's suggesting that Cavuto's show is objectifying women?
