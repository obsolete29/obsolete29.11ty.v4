---
title: "QOTD - John Ruskin"
date: "2012-05-05"
tags: 
  - "qotd"
---

What we think, or what we know, or what we believe is, in the end, of little consequence. The only consequence is what we do.—John Ruskin
