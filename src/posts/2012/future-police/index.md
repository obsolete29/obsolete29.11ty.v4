---
title: "Future Police"
date: "2012-06-14"
tags: 
  - "ideas"
---
{% myImage "4926477031_af295c93df_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

So I have this [killer commute](http://michaelharley.me/blog/2012/commuting-why-am-i-doing-this/ "Commuting: Why am I doing this?") which affords me the chance to listen to lots of talk radio, audio books and to day dream about stuff. My imagination has been cultivating this idea about/for the police department. What if each squad car was this passive sensor out in the world? By that I mean, what if each police car was equipped with cameras and were sophisticated enough to automatically read every license plate that the cameras could see and to log those plates with a time and location? If you take this idea a step further, you could even have the system perform a quick check to see if that particular plate or car is reported as stolen or if the registered owner of the vehicle is wanted for anything. This system would work without any intervention from the officer in the car. If the system came up with a stolen car based on a license plate it scanned, the system would alert the officer and he could proceed from there.

I wonder if this could also help in investigations? Imagine a suspected killer being investigated... an investigator could look up the suspect in the system and it's possible that the killers car was scanned by a police officer on routine patrol just a block from a crime scene on the night of the murder.

I realize that this has all sorts of problems from a privacy perspective and is pretty big brotherish but that's a detail; I'm an idea guy. ;)
