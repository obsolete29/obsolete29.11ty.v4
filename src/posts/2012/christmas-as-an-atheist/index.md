---
title: "Christmas as an Atheist"
date: "2012-12-02"
tags: 
  - "life"
  - "religion"
  - atheism
---

Christmas has never been a religious holiday for me. My dad was an atheist, though not a secular humanist type atheist. He was the type of atheist that didn't want to be bothered by the people coming around on Saturday. If I'm honest, I don't remember hardly anything about Christmas with my dad. I don't remember us having Christmas trees nor do I remember really opening presents.

When I was 12, my dad passed away and I came to live with my mom. My mom is really religious now but she wasn't back then. We never said grace at meals and we rarely went to church on Sunday. By rarely, I mean I can count the number of times I remember on a single hand. The only religious thing I can remember doing while growing up with my mom was going to a vacation bible school thing one summer; that's it.

Christmas for me is about spending time with loved ones, friends and family. It's a time of rest and relaxation and again being thankful for our lives and the people in them.

It's just been in the last 10-12 years that I've realized that Christmas is a religious holiday for other people. I realize that some people could have been raised celebrating Christmas as a birthday party for Jesus just like I was raised without a single mention of Christmas and Jesus in the same sentence.

We live in the US and that still means Christmas can be what we make it. If you want to celebrate Christmas with birthday cake for Jesus, a crucifix as your tree topper and little Jesus ornaments on your tree then that's completely fine with me. If I'm invited to your house and this is the way you celebrate the holiday, I won't even be offended. I'm not offended if you say Merry Christmas (or not), Happy Holidays, Happy Kwanzaa or any other type of greeting you want to use.

I'd be curious to read about the experiences of people who were raised in religious families where Christmas and Jesus are closely tied together. What is your Christmas Day like?
