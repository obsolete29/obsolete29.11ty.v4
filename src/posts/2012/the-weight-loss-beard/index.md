---
title: "The Weight Loss Beard"
date: "2012-09-24"
tags: 
  - "fitness"
---
{% myImage "8021548365_0843c45013_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

As I [tweeted](https://twitter.com/obsolete29/status/250197633640050688) yesterday morning, I've decided to give myself another personal weight loss challenge. I've decided that I won't shave my facial hair until I've reached 260 lbs. That's 15 lbs from my current weight.

My target date is December 1st. Wish me luck. :)
