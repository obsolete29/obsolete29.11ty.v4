---
title: "Trip Report: Piney River Segment"
date: "2012-07-28"
tags:
  - "hiking"
  - trip-report
  - cumberland-trail
---
<figure>
  {% myImage "7663582904_2d6f6435a1_m.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>White Pine Cascades </figcaption>
</figure>

As part of my [40 Before 40](http://michaelharley.me/blog/2012/my-40-before-40/ "My 40 Before 40"), I’m trying to hike every section of the [Cumberland Trail](http://www.cumberlandtrail.org "Cumberland Trail"). Today’s hike was the [Piney River segment](http://www.cumberlandtrail.org/piney_river.html "Piney River") down near Spring City, TN. I actually started at the Newby Forest Branch Camp and Trailhead and hiked back to the East Piney River Trail Picnic Area Trailhead. I found the Newby Forest Branch Trailhead OK but I was a little unsure if I was on the right track as I was looking for it. Here are the directions from the website to the Newby Forest Branch Trailhead:

> Drive past the East Piney River Picnic Area Trailhead. Continue to the top of the plateau and until you see the sign on the left pointing to Newby Branch Forest Camp. Follow the signs to the campsite and parking area.

I would revise this to say:

> Drive past the East Piney River Picnic Area Trailhead. Continue to the top of the plateau and turn left onto Forest Camp Rd (1.2. miles past the Stinging Fork Falls trailhead). Note: Both Waze and RunKeeper identify this road as Forest Camp Rd. Look for a [brown sign post](http://farm9.staticflickr.com/8168/7663627588_c7bef9101d_c.jpg) on the left saying **Newby Duskin**. Next, stay right (another brown sign posts marks the way to Newby) at the fork to stay on Forest Camp Rd. Next, take the 2nd left into the camp ground and you'll arrive at the [trailhead](http://farm9.staticflickr.com/8013/7663631662_9a313c195f_b.jpg) on your right. Note: Forest Camp Rd is gravel and pretty rough with some washouts and such. A passenger car can make it but just be careful.

I got a real early start because I knew it was going to be a long hike. RunKeeper says I hiked 9.45 miles and climbed 2946 feet, although the elevation was generally down if you look at the RunKeeper profile. I enjoyed the hike quite a bit as the trail follows the river for a good portion of the hike and none of the elevation changes were very steep. There were a few sections of the trail that weren't that well blazed and I had to back track at least once to find my missed turn. There were a few blow downs but they weren't much of a problem.

This trail seemed to have a lot more spider webs across the trail than normal. I'm not sure if it's just the right time of year for spiders or if this trail isn't hiked very often but I must have walked through 25 webs (shudder). I saw a small garter snake (first snake of the year), several toads, some squirrels and lots of millipede.

I would rate this hike as moderate, mostly for distance and even the sloping elevation gains are hard after 7-8 miles. Lots of interesting features to see along Piney River and I didn't see anybody until I was finished with my hike. On an enjoyment scale of 1-10, I'd rate this hike as a 7.

Resources:

- [Flickr](http://www.flickr.com/photos/pamperedjen76/sets/72157630792476258/)
- [RunKeeper](http://runkeeper.com/user/obsolete29/activity/105256190)
