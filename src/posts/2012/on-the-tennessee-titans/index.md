---
title: "On The Tennessee Titans"
date: "2012-09-23"
tags: 
  - "titans"
---
{% myImage "20120923-115959.jpg", "IMG imported from an old site. Sorry no alt text." %}

After 2 weeks, it's pretty obvious that the Titans are a pretty bad team. We have some _talent_ on offense but that hasn't translated to points. CJ continues to have problems running the ball. It's not obvious to me from watching the games if the problem is with CJ's effort/ability, our offensive line or coaching schemes and play calling. I love Jake Locker but I just don't ever see him being a franchise pocket passer QB. I hope he proves me wrong but I think problems with his accuracy will mean he can be a pretty good QB but he'll never be a Brady, Manning, Rogers or even a Luck. It's obvious that our offense is tooling up to be a pass first team. But with Locker's accuracy issues, I'm not sure that's going to be as successful as everybody hopes. Combine that with our inability to run the ball and you have teams teeing off on Locker.

Our defense is _terrible_ and I have no idea what the problem is. I've read some articles this past week and the players are talking the way they talked when we were having such problems on defense when Chuck Cecil was defensive coordinator. My brother, who I respect quite a bit as a football mind, sings the praises of Jerry Gray but I just get the feeling there is something seriously wrong with Gray's plans or ideas. I'm not sure if we just don't have personnel to run Gray's scheme or if our personnel are just not executing it. With either scenario, Gray has to adjust his game plan to match the people he has on the field and right now that whole unit is failing utterly.

We have no where to go but up and I've resigned myself to a terrible season and looking forward to small victories... like maybe CJ will get over 30 yards in a game or maybe the Titans defense can hold a team under 30; baby steps.
