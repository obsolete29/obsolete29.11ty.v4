---
title: "Our Washington DC Trip"
date: "2012-06-25"
tags: 
  - "travel"
  - "washington-dc"
  - trip-report
---

Jen and I went to Washington DC last week for our first ever  non-family, adult vacation. We left at mid-night after I had a quick 2 hour nap and we drove the 8 hour drive all the way through to Manassas, VA where our motel was located. Once we arrived, the boys' dad picked them up and our vacation officially started. A few pictures from the drive up are located [here](http://www.flickr.com/photos/pamperedjen76/sets/72157630265320214/), and [here](http://www.flickr.com/photos/pamperedjen76/collections/72157630284243048/) is the full set of photos from the trip.

## Day 1

  {% myImage "7385312792_d560067f70_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

Once the boys were picked up, our friend Kristen took us around some of the main monuments and attractions. We saw the White House, The Washington Monument and the WWII Memorial. We also ate some overpriced hot dogs and ice cream and drank some overpriced water for lunch.

Another point of interest for me was that I was able to ride public transit... _real_ public transit. It was a lot of fun figuring out the Metro. We only got yelled at a couple of times by locals for standing on the wrong side of the escalator or not operating the exit gates fast enough.

For dinner, Kristen and her friend Tommy took us to [Freddie's Beach Bar](http://www.freddiesbeachbar.com/) for some karaoke and drag. The food and company was excellent.

{% myImage "7385421220_9f2d327a1e_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

As we were sitting there, a gay guy asked me if I knew what this place was. He said that it's not uncommon for tourists with kids and such to come there for the food and then they're surprised to see people in drag being all gay and stuff. After I explained the situation to him, he asked me if I ever get hit on. This was an excellent question! I've never been hit on in a gay bar... I'm not sure if I'm just not gay guy material or if it's painfully obvious that I'm straight.

Anyhoo, a couple more things to note about our trip to Freddie's. First, Jen was really digging our bartender. He was eastern European and fit and had a good personality. I forget the name of the dessert that Jen and Kristen ordered but it looked pretty perverted when it arrived. Finally, there was a guy there and we  _think_ he had two prosthetic hooks for hands (henceforth known as the gay pirate). We know he had one, we're just not sure if he had both. At one point in the evening, some guy raised his shirt up and the gay pirate pinched his nipple with one of his hooks. We wondered if he had other accessories. Once the dessert came, he raised his beer and was making suggestive faces and such to Jen and Kristen as they started eating their perverted dessert. You can see the gay pirate and his gay pirate friends across the bar and to the left in the screenshot below. If you look close, you can even see the gay pirate's nipple pinching prosthetic accessory.

{% myImage "7385433488_66410febce_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

They were wanting to hit up another bar down the street, but by this point I had only 2 hours sleep in the last 24 hours and I was needing to crash so we went back. I felt like a party pooper but I was having a hard time keeping my eyes from crossing with sleep. Here are the [photos from day 1](http://www.flickr.com/photos/pamperedjen76/sets/72157630265533546/).

## Day 2

We had two things on the itenerary for day 2: [Mount Vernon](http://www.mountvernon.org/) (at Kristen's suggestion) and [Arlington National Cemetery](http://www.arlingtoncemetery.mil/).

<figure>
  {% myImage "7403191324_e65e6497b1_m.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Mount Vernon</figcaption>
</figure>

In case you didn't know, Mount Vernon was George Washington's personal estate on the banks of the Potomac river. There are some pretty nice gardens on the grounds as well as a working smithy where a smith was making nails for the construction crews who restore and repair the various buildings and such. George Washington and his wife are even buried here.

There was also slave quarters on the grounds which really drove the point home of how the founding fathers of our nation were wrong on the biggest moral question of their time.

Because of time constraints, we had to skip the tour of the manor itself. I think this would be better as a full day destination... especially if you were wanting to tour the manor and the distillery.

They also had big trees here and I liked hugging them. Here are the [photos from Mount Vernon](http://www.flickr.com/photos/pamperedjen76/sets/72157630266230074/).

<figure>
  {% myImage "7403202456_99691c06bc_m.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Arlington National Cemetery</figcaption>
</figure>

This was a pretty awesome experience. The gravity of what went into our country is present and palatable as you're walking through the cemetery. I don't really have a lot to say about it other than we both had a deep and emotional reaction to all of the sacrifices. Seeing the changing of the guard (we stayed for two cycles) at the Tomb of the Unknowns is spectacular and moving. It was one of my favorite things on our trip. Here are our [photos from Arlington](http://www.flickr.com/photos/pamperedjen76/sets/72157630266563604/).

<figure>
  {% myImage "7419351100_1ff1788b99_m.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Ray's Hell Burger</figcaption>
</figure>

At the recommendation of [FreakyWeasel](https://twitter.com/FreakyWeasel), we searched out Ray's Hell Burger in Arlington (the city, not the cemetery). We actually went to Ray's 3 across the street because they had seating, took credit cards and had steak & alcohol whereas the original(?) Ray's was a cash only operation and seating was a bit sketchy. Same burger menu and recipes and the food was actually pretty great. We were pretty proud of ourselves as this was the first time of riding the metro on our own and finding our way around.

After dinner, we were a little worried that we were going to miss the last Metro train out of the city so we had to eat quickly and walk back to the Metro station with a purpose as it was Sunday and the last train left at 9:30pm. We made it without any problems tho!

## Day 3

On day three, we went up to Maryland to visit the Inner Harbor, to see some of Jen's old life and to eat dinner with some of her friends from the area.

<figure>
  {% myImage "7419412928_c3bec46962_m.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Inner Harbor</figcaption>
</figure>

We drove our own car up and I have to say that Washington DC traffic **_really_** sucks. I mean wow!

The first stop was for lunch at the [Rusty Scupper](http://www.selectrestaurants.com/rusty/) on the Inner Harbor. The food was fantastic (I had fresh salmon) but the food was pretty damned expensive... we ended up paying $78 with tip for lunch. As a friend noted however, we had dinner for lunch. They do have the best crab cakes I've ever tasted though!

After lunch we caught the water taxi across the harbor. It turns out we accidently caught the ass end of [Sailabration](http://www.starspangled200.com/). As you can read from the link, there were all of these tall sailing ships from around the world in port. It was pretty crowded and touristy feeling so we just did a quick walk around and went to find the local micro-brewery, [Pratt Street Ale House](http://www.prattstreetalehouse.com/)! I'm not a very sophisticated beer drinker so I tend to like the light beers or wheat beers better. Our bartender was pretty sassy and I'm pretty sure she was making fun of me for wanting light beer. Here are all of our [Inner Harbor photos](http://www.flickr.com/photos/pamperedjen76/sets/72157630272255800/).

<figure>
  {% myImage "7419485684_7dff947fec_m.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Glen Burnie</figcaption>
</figure>

After walking off our slight buzz from the beer, Jen showed me where they used to live and where the boys went to school. Fun walk (drive?) down memory lane. The area seemed somewhat close to the sketchy parts of town so I'm sure she'll have no problems moving to East Nashville when we're ready to move. ;)

We had a nice long dinner with some of her old friends. I had lobster and wish I would have gotten some crab instead... how does a person go to Baltimore and not have the crab??? [Photos](http://www.flickr.com/photos/pamperedjen76/sets/72157630272463946/).

## Day 4

Again at the recommendation of FreakyWeasel, we decided to attempt to follow the [Frommer's day 1 itinerary](http://www.frommers.com/destinations/washingtondc/0035020766.html). By now we're starting to feel like old pros at riding the metro so we had no problem finding our way about.

<figure>
  {% myImage "7410229574_7be847eed4_m.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>The Capitol</figcaption>
</figure>

We signed up for the guided tour of The Capitol building. There were a lot of people and a few large groups so that was a little bit of a bummer but the tour was pretty informative. The artwork (and all of the history and significance) inside of the rotunda is pretty amazing. As with Arlington, I really felt the significance of what our country means and what it stands for.

Both houses were in session when we were there although we didn't get to see any congressmen walking around. I'm not sure if there is such a thing in DC but I think I would have preferred to go with a smaller group or during the "off season" when there aren't so many people. The guided tour part was only like 25 minutes. One thing of note: I had to throw away my pocket knife because it wasn't allowed into the building.

We decided to just walk by the Supreme Court building and skipped the tour because of time constraints.

<figure>
  {% myImage "7419745852_fd27d0189e_m.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>The Library of Congress</figcaption>
</figure>

We arrived just in time for the guided public tour of the library. This one lasted about 40 minutes and we were hustled around to the various sections of the library. Before going on this tour, I didn't really understand or appreciate the significance of The Library of Congress. I particularly enjoyed seeing the [Gutenberg Bible](http://en.wikipedia.org/wiki/Gutenberg_Bible) and the [Giant Bible of Mainz](http://en.wikipedia.org/wiki/Giant_Bible_of_Mainz). We weren't allowed to take photos of any of the books. I suppose it's because of the flash but they were pretty clear: no photos, with or without flash. I got the sense that it was a copyright type issue.

The artwork and symbolism is magnificent... a lot of the artwork would be pretty damned cool tattoos too.

_Le Bon Cafe_

We got lunch at this little cafe recommended by Frommers called [Le Bon Cafe](http://www.leboncafedc.com/). I really enjoyed it because there seemed to be mostly DC locals and not much in the way of tourists, like us. It must have been very obvious that we were tourists because as we were sitting there, looking at our guide book (hah) an apparent local came up and asked us where we were from, how long we were in town etc. She recommended a local bar around the corner although as I type this, I can't remember the name of the establishment.

Frommers recommends going to the Folger Shakespeare Library and Garden and it was a bit of a walk in the heat of the noon day sun. Maybe I'm just missing something but I'm not sure non-Shakespeare nerds would really enjoy this. Admittedly, we just sat in the garden and didn't go inside but the energy spent going there wasn't worth the payoff for me. We should have just went straight to the Botanic Garden after lunch.

{% myImage "7419844742_f56cea4965_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

_U.S. Botanic Garden_

At this point, it was getting hotter and it was a little bit of a walk from the Folger Library/Garden to the Botanic Garden (we had to back track to Le Bon Cafe for water). Sorry about the repetitive language, but the garden was pretty spectacular and we both enjoyed it. I think my favorite part was the carnivorous plant display, Savage Gardens. Wouldn't a sleeve of carnivorous plants be pretty damned cool?

We were _supposed_ to go to the National Gallery of Art and Sculpture Garden next but both us and our phones were running out of power. Instead, we went in search of ice cream and somewhere to drink beer and charge our phones. I would have enjoyed seeing the sculptures; I guess we'll just save that one for next time.

The ice cream was served up courtesy of Pitango Gelato... it was ok but I was in the mood for butter pecan waffle cone.

Beer and a power outlet was served up by Penn Quarter Sports Tavern around the corner from the gelato place. The AC and chairs were so pleasant that we cancelled our reservations at Johnny Half Shell's and just stayed there and ate pizza/wings and drank more beer. If there had been a tattoo shop close enough, I'm pretty sure we would have gotten tattoos that day but alas, it was too hot to hike across town.

By the time we headed out for Union Station, we had a nice buzz going.

{% myImage "7411598314_a6a24eaa0e_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

_Monuments by Moonlight_

At the recommendation of Frommers, we purchased tickets for the [Old Town Trolley](http://www.trolleytours.com/washington-dc/) Monuments by Moonlight tour. Our tour guide was knowledgeable and funny and I'd recommend Victor for anybody else wanting to do the tour. The tour itself is over 2 hours long and there are 3 stops at various monuments. We didn't get many photos after dark because both phones ran out of battery before the tour did. Note to self: invest in some [mophie](http://www.mophie.com/) battery packs. I'm glad we did this as we were able to see things we couldn't have otherwise had time for. Vince also dropped some bus driver wisdom on us: _We have to keep doing what we know is the right thing no matter what people say otherwise._ -Vince

A funny thing happened after the tour. When we got back to Union Station, it wasn't readily apparent which way we had to go in order to get to the Metro. We walked in a little bit and found a map that wasn't useful and we got separated at this point. While she was studying the map, I said "let's go" and started back towards the entrance with the intention of asking one of the security people for directions. I found a security person then waited for Jen to catch up but she never came out where she should have... 30 minutes of hilarity at 11:30pm in a mostly empty Union Station followed. We kept missing each other as we walked around looking for each other and since our phones were dead, we couldn't call or text the other. To add a little extra tension to the mix, the last train was leaving the station shortly. We finally found each other down at the Metro station. We were both exhausted after a long, hot day and we actually caught the train before last!

So ended an 18 hour fun filled day. Photos for Day 4 is [here](http://www.flickr.com/photos/pamperedjen76/sets/72157630282338538/).

**Day 5**

By a stroke of luck, we were able to get Tyler earlier than we had planned... they passed us on the street in Manassas, ha! So he got to hang out with us for the day and catch the Natural History Museum.

The original plan for our last day was to follow [Frommer's day 2 itinerary](http://www.frommers.com/destinations/washingtondc/0035020790.html) but the previous day taught us that Frommer is a very ambitious person. We wanted to take it easier on our last day to we culled the itinerary down to just visiting the Natural History Museum. So instead of getting up at the butt crack of dawn in order to be on time for our Capitol tour, we just had to be sure to eat breakfast at the motel before they put it away at 9:30... much better. ;)

{% myImage "7423601258_b8d9888e9e_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

_National Museum of Natural History_

This was the destination that I was most looking forward to on this trip and it didn't disappoint. I got to be in charge of the pace and I got to see every part of the museum that I wanted and spend as much time as I wanted there. Jen didn't rush me at all and for that I was super happy.

I especially enjoyed the Human Origins exhibits. I'm getting a [human evolution tattoo](http://michaelharley.me/blog/2012/darwin-sleeve-tattoo/ "Darwin Sleeve Tattoo") on my right arm and I was able to get a lot of photos for Ian.

{% myImage "7411983108_c03876f119_m.jpg", "IMG imported from an old site. Sorry no alt text." %}

[Here](http://www.flickr.com/photos/pamperedjen76/sets/72157630283700760/) are the photos from Day 5.

## Conclusion

This was the first adult vacation that I've ever been on and I have to say it's the best vacation I've ever been on. It's fun to take your kids to places and see them experience places and have fun. It's also fun to go visit the family on vacation but I really enjoyed not having to worry about any of that. We got up and we got to do what we wanted when we wanted. That's a weird experience for a person who's always doing things because of other people.

We found the people in DC to be an odd mix of personality types... they were friendly and helpful people like the lady from the cafe, there are the impatient ones yelling at people for standing on the wrong side of the escalator and then there are the drivers who are quick to flip you the bird (we wave with all of our fingers in TN).

On our first day we picked up hats and a backpack from target because we were planning on being on foot for most of the time. On the 2nd day, we started bringing the charging cable. I think now I'd have put both charging cables in the bag and also brought a [mobile battery pack](http://www.mophie.com/ProductDetails.asp?ProductCode=2029_JPU-PWRSTION-DUO) for when we don't have time to sit for an hour in a cafe or bar. We were mostly using Google Maps on the browser to get around but I wish there were a better app that could orient the based on which way the phone is pointing (like a compass) and give walking or transit directions. We were able to get around fine but we were using several different apps to get the job done. I'm quite proud that we didn't get sun burned btw... we packed sun screen and I was pretty religious about trying to protect my tattoos.

After visiting all of the sites, I have a much better appreciation for the history of our country and the principles it stands for. [E pluribus unum](http://en.wikipedia.org/wiki/E_pluribus_unum).
