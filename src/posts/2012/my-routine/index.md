---
title: "My Routine"
date: "2012-11-15"
tags: 
  - "life"
---

On days that I go to Nashville (Tue-Fri), I get up at 4:20 am and am usually on the road by 4:35. A quick stop to fill up the car and off to 1 hour 45 minutes of audio book or NPR news. I arrive at work around 6:30, warm up my breakfast (3 boiled eggs, orange, green tea and a glass of water lately) and scroll through my Google reader feed while I consume breakfast. Once finished eating, I'll reconcile the bank accounts with MS Money, check Flickr for new photos that need to be tagged and organized, check Evernote for new notes and then finally open up the work email. Easy tasks are completed on the spot; harder things are put on The List.

My most productive hours at work are between 7:30 am and 11 and I usually try to spend this time tackling big, hard things because my mind is the most focused. After lunch, I'll tackle things that are less hard but still need more than five minutes. At 3 pm, I'm usually looking to hit the road and will be on the road by 3:10. Cue 2 hours of audio book or NPR news. I'm very current on my news. ;)

I arrive home about 5:15, change into running clothes on running days or change into comfy clothes on non-running day. I usually run for about an hour, hit the shower and help start dinner if it isn't already going. Or if we're doing easy dinner, make myself something from the fridge. By now, it's 6 or 7 and I'm trying to sit on the sofa with my dinner to catch last night's The Daily Show. Most of the time I can make it through an episode before it gets too busy with the kids, pets and people.

I'm usually retreating to the bedroom no later than 8 -- sometimes before -- from over stimulation. I'll scroll through social media timelines and try to read a bit before falling asleep.

Rinse, repeat.
