---
title: "Why I am still going to use Flickr"
date: "2012-12-21"
tags: 
  - "technology"
---

I saw today that Instagram has a [new new updated](http://mashable.com/2012/12/20/instagram-updates-its-terms-of-service/), TOS and now they have no intention of selling your photos. We can all rest easy and keep on posting pictures of our sushi on Instagram, right?

I realize in the grand scheme of things this is a pretty trivial issue. They provide a free service and in exchange they get to show us ads and make money. It's a pattern we've seen a few times now including Google, Facebook, Twitter, etc.

I'm growing to really dislike this model. When we submit ourselves to this type of model we're giving up control of our content. If you've been using Facebook for years, they have a lot of your information and they're just one TOS change away from using that content however they like or making it all public by default or any of the other annoying things people on the Internet have rage about.

I'm no longer going to post pictures to Instagram. I'd rather pay Flickr to get rid of the ads if I'm honest (the new Flickr app is pretty good too). Plus, I like the model where I'm the customer and not the product. It's the same reason I have a self hosted blog and why I'm trying to use [App.net.](https://join.app.net/)
