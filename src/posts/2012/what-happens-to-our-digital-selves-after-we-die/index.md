---
title: "What happens to our digital presence when we die?"
date: "2012-07-27"
tags: 
  - "life"
---

[Dave Winer](http://scripting.com/stories/2012/07/21/mantonReeceOnPermanence.html) has been talking a lot about archiving our digital presence lately and it's a topic I'm very interested in. What happens to our digital presence after we die? Nobody cares about cat photos I post or stupid jokes that I retweet on Twitter but my family may care about the things I'm writing on my blog. It's basically a digital journal that gets lost forever if I miss a hosting payment. What about all of my photos on [Flickr](http://www.flickr.com/photos/pamperedjen76/)? Twenty-five years ago my photos would be in a big shoe box in my closet and that would pass on to my daughter. Now I have to make sure my daughter gets my user info and there's no way to make sure she can get the photos out of Flickr as a batch.

I wonder if many online services have processes in place for this type of thing? Or do I just have to make sure I have a very detailed document for all my stuff for the technically minded people in my family? It's topic that causes me a bit of anxiety if I'm honest.

Digital is great because it's so convenient but it also makes it very easy for our memories and documents to be erased.

Have you thought about this topic at all? How can people plan for this?
