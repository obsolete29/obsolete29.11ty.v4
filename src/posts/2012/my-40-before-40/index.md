---
title: "My 40 Before 40"
date: "2012-05-11"
tags: 
  - "40b440"
  - "life"
---

I turn 39 in August. This will be a list of 40 things that I will try to accomplish before I'm 40 years old. Wish me luck. ;)

1. Hike every section of the [Cumberland Trail](http://www.cumberlandtrail.org/)(6/13)
    1. Grassy Cove Segment
    2. Stinging Fork Falls
    3. [Piney River Segment](http://michaelharley.me/blog/2012/piney-river-segment/ "Trip Report: Piney River Segment")
    4. [Three Gorges Segment - Soddy Creek Section](http://michaelharley.me/blog/2012/cumberland-trail-three-gorges-segement-soddy-creek-section/ "Trip Report: Cumberland Trail, Three Gorges Segment, Soddy Creek Section")
    5. [Three Gorges Segment - Possum & Rock Sections](http://michaelharley.me/blog/2012/cumberland-trail-three-gorges-possum-rock/ "Trip Report: Cumberland Trail, Three Gorges, Possum & Rock Sections") (hiked two sections here!)
2. Complete 5 Races (0/5)
3. Complete a 5k in under 30 minutes
4. Run an 8 minute mile (11:51 is best time to date but I haven't run a timed [mile](http://runkeeper.com/user/obsolete29/route/1497294) since August 29th)
5. Read one book/fortnight (18/34)
    1. [Goodreads List](http://www.goodreads.com/review/list/1009942-michael-harley?format=html&shelf=read&sort=date_read)
6. Become debt free except for the mortgage ($13,501.70 to go!)
7. Purchase a [bike](http://www.mrmoneymustache.com/2012/05/07/what-do-you-mean-you-dont-have-a-bike/ "What Do You Mean “You Don’t Have a Bike”?!").
8. Exercise at least 30 minutes a day for 30 days straight (30/30) Complete!
9. Complete myfitnesspal logs for 30 days straight (30/30) Complete!
10. Get my passport
11. Go on an [art crawl](http://www.nashvilledowntown.com/play/first-saturday-art-crawl "Nashville Art Crawl")
12. Try 10 new beers (8/10)
13. Try 10 new wines (3/10)
14. Attend 1 networking event per month (I'm failing miseribly at this)
15. Get personal business cards
16. Visit 5 museums (2/5)
17. Host a Sunday grilling for the neighbors
18. Complete Code Year Javascript course
19. Go a month without caffeine (30/30)
20. Kayak a river
21. Go on a 2 night backpacking trip
22. Hike [Mt LeConte](http://www.leconte-lodge.com/ "Mt LeConte") again Complete!
23. Go to my first professional hockey game
24. Go to my first professional basketball game
25. Go to my first professional baseball game
26. Learn to swim... really swim.
27. Complete 100 push ups in a single session
28. Complete 100 sit ups in a single session
29. Complete 5 pull ups
30. Go on an adult vacation with my wife, [Jen Harley](http://jenharley.me "Jen Harley Crossville TN") Complete!
31. Attain goal weight of 210 lbs (12lbs/70lbs)
32. Go to a shooting range
33. Go to Titans training camp Complete!
34. Go to a Titans home game
35. Complete my [Darwin Sleeve Tattoo](/posts/2012/05/02/darwin-sleeve-tattoo/ "Darwin Sleeve Tattoo") (1 session to go!)
36. Go to see a symphony play
37. Go to a play
38. Learn to ballroom dance
39. Go to an atheist/skeptic conference
40. Call 1 family member a week (0/64) (I'm _really_ failing at this)

What are some things that would be on your list?

I saw this idea of on Samantha's blog, [faint gray lines](http://www.samantha-y.com/30-before-30/).
