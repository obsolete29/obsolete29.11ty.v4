---
title: "2012 in Photos"
date: "2012-12-31"
tags: 
  - "year-in-review"
---

Here is our 2012 in photos.

## January

<figure>
  {% myImage "6613022465_ed57be4660.jpg", "Grandy, Grammy & the boys by _harleysangel_, on Flickr" %}
  <figcaption>We started the New Year in FL visiting Tim and Deb!</figcaption>
</figure>

<figure>
  {% myImage "6864492511_ceae224bdc.jpg", "Hunter got baptized." %}
  <figcaption>Hunter got baptized.</figcaption>
</figure>

<figure>
  {% myImage "6905072340_23142f5eee.jpg", "IMG_0829 by _harleysangel_, on Flickr" %}
  <figcaption>This happened</figcaption>
</figure>

<figure>
  {% myImage "7165766509_1e7235d438.jpg", "2012-01-08 at 20.43.01 by _harleysangel_, on Flickr" %}
  <figcaption>Tyler started shaving</figcaption>
</figure>

## February

<figure>
  {% myImage "6823417261_f47d875242.jpg", "photo.JPG by _harleysangel_, on Flickr" %}
  <figcaption>Jen and I got matching tattoos</figcaption>
</figure>

<figure>
  {% myImage "6905095548_fa5b4773bb.jpg", "IMG_0954 by _harleysangel_, on Flickr" %}
  <figcaption>Jen got baptized too</figcaption>
</figure>

<figure>
  {% myImage "6905100054_c030a8623a.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>First haircut</figcaption>
</figure>

## March

<figure>
  {% myImage "6824971634_82bae01679.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>I hiked Stone Door</figcaption>
</figure>

<figure>
  {% myImage "7020776051_d869522654.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>Trayvon Martin happened.</figcaption>
</figure>

<figure>
  {% myImage "6905102948_84247233f8.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>There was a tornado. We hid in the closet.</figcaption>
</figure>

<figure>
  {% myImage "7167496385_5b54c4e55e.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>This kid...</figcaption>
</figure>

<figure>
  {% myImage "7167508031_9ea8e79d8f.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>Tyler and Hunter promised to keep it in their pants </figcaption>
</figure>

## April

<figure>
  {% myImage "7116366601_4199da449b.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>We saw Butch Walker at 12th and Porter</figcaption>
</figure>

<figure>
  {% myImage "7165341877_3574c8ef80.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>I won something at podcamp</figcaption>
</figure>

<figure>
  {% myImage "7167563879_85b1aba621.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>@Analogwalrus came to Crossville. He was afraid.</figcaption>
</figure>

<figure>
  {% myImage "7167521901_4cf6c90aa1.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>Jen had a birthday!</figcaption>
</figure>

<figure>
  {% myImage "7169035321_c303e34bce.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>Jen's mom Cyndi came to visit and we took her to see the tree house (which has since been closed.. boo</figcaption>
</figure>

## May

<figure>
  {% myImage "7354272936_32f0d94607.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>Started my tattoo</figcaption>
</figure>

<figure>
  {% myImage "7169101187_6cc7f3b37b.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>WILL YOU GO TO PROM WITH ME?!?</figcaption>
</figure>

<figure>
  {% myImage "7354322092_8a2acdc31d.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>We helped celebrate Britt's birthday</figcaption>
</figure>

<figure>
  {% myImage "7384408684_74e5c146d1.jpg", "IMG by _harleysangel_, on Flickr" %}
  <figcaption>We hiked Black Mountain</figcaption>
</figure>

## June

<figure>
  {% myImage "7350621508_a618b2be74.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>... and Brady Mountain</figcaption>
</figure>

<figure>
  {% myImage "7165408301_ccbf209e0b.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Mitt Romney misspelled a word on his app - we laughed</figcaption>
</figure>

<figure>
  {% myImage "7384845860_ecdd74523b.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Jen visited her sister in FL; baby shower and birthday.</figcaption>
</figure>

<figure>
  {% myImage "7385035948_724aecd494.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>We attended the Ziegle family reunion as honorary family members</figcaption>
</figure>

<figure>
  {% myImage "7385262106_16c1e87df5.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>We went to DC...</figcaption>
</figure>

<figure>
  {% myImage "7411633022_999b3ce62d.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>... and saw lots of cool shit</figcaption>
</figure>

<figure>
  {% myImage "7561241226_b6fdb5645c.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>We celebrated Jessie's 21st birthday in Nashville.</figcaption>
</figure>

## July

<figure>
  {% myImage "7561265502_7c691eb607.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>We celebrated the 4th with friends in Nashville\</figcaption>
</figure>

<figure>
  {% myImage "7561333484_37deea6ecf.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Carter went swimming</figcaption>
</figure>

<figure>
  {% myImage "7561358482_2cce93c41b.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>MOAR TATTOO</figcaption>
</figure>

<figure>
  {% myImage "7663608794_90be6de5e1.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>I hiked the Piney River section of the Cumberland Trail</figcaption>
</figure>

<figure>
  {% myImage "7762136332_7b1c56e498.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>This happened...</figcaption>
</figure>

<figure>
  {% myImage "7768131172_24bd5f0f5a.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>... and this.</figcaption>
</figure>

August

<figure>
  {% myImage "7768402794_af55d78fca.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Jen saw Train!</figcaption>
</figure>

<figure>
  {% myImage "7770399168_2d0eff4c64.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>First day of school</figcaption>
</figure>

<figure>
  {% myImage "7770426818_5d793648cf.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>We hiked Savage Gulf, again.</figcaption>
</figure>

<figure>
  {% myImage "7805749102_eea74b234d.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Big man had a birthday</figcaption>
</figure>

<figure>
  {% myImage "7811763430_0c30ed7a9d.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>We hiked Virgin Falls</figcaption>
</figure>

<figure>
  {% myImage "7861490988_7d728b3b6b.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>... and we hiked to Le Conte in the Smokies...</figcaption>
</figure>

<figure>
  {% myImage "7867506616_30f8af15f9.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>... it was cool</figcaption>
</figure>

<figure>
  {% myImage "7940063848_223ced93b5.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>We went to Pretty Lake!</figcaption>
</figure>

## September

<figure>
  {% myImage "7940084660_9a9b917edd.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>And it's really pretty, see?</figcaption>
</figure>

<figure>
  {% myImage "7975561123_300435eda4.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>:)</figcaption>
</figure>

<figure>
  {% myImage "7975618188_cc00f62b65.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Really tho</figcaption>
</figure>

<figure>
  {% myImage "7959135142_2c16298407.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>I hiked the Soddy Creek section of the Cumberland Trail</figcaption>
</figure>

<figure>
  {% myImage "7994870372_290b37c766.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>We saw Butch again in concert!</figcaption>
</figure>

<figure>
  {% myImage "7994873105_aca25a7094.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>We went to Made in Nashville</figcaption>
</figure>

## October

<figure>
  {% myImage "8097405832_2ef9e833ce.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Tim and Deb rented a cabin in the Smokies and we hiked some of the AT</figcaption>
</figure>

<figure>
  {% myImage "8097561184_fbc3cd6b09.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>And Jen saw Train again</figcaption>
</figure>

<figure>
  {% myImage "8132175152_2d33f67b45.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>I hiked the Rock and Possum sections of the Cumberland Trail</figcaption>
</figure>

<figure>
  {% myImage "8146140535_33bde306b4.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Halloween!</figcaption>
</figure>

## November

<figure>
  {% myImage "8186431369_79895db745.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>We helped celebrate James' birthday</figcaption>
</figure>

<figure>
  {% myImage "8197485117_7b0975fd3a.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>I ran my first 5K</figcaption>
</figure>

<figure>
  {% myImage "8211972475_4348eb8d30.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Thanksgiving</figcaption>
</figure>

<figure>
  {% myImage "8221076219_194891c3e3.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>This happened too</figcaption>
</figure>

<figure>
  {% myImage "8224753577_1721551f71.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Tyler was in The Nutcracker with MaKayla, and we went to see them perform.</figcaption>
</figure>

## December

<figure>
  {% myImage "8249329689_f196839ea7.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>I got my iPad</figcaption>
</figure>

<figure>
  {% myImage "8256822372_3aa2601f35.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Help Portrait happened</figcaption>
</figure>

<figure>
  {% myImage "8281488832_3874808f0e.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Cyndi and John came to visit</figcaption>
</figure>

<figure>
  {% myImage "8300615590_1ae8510188.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Finished my tattoo</figcaption>
</figure>

<figure>
  {% myImage "8308851606_8d898323ae.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>We played board games on Christmas eve</figcaption>
</figure>

<figure>
  {% myImage "8299643239_d97a77b5d0.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Big man enjoyed Christmas</figcaption>
</figure>

<figure>
  {% myImage "8330227693_89cfa0cc66.jpg", "IMG imported from an old site. Sorry no alt text." %}
  <figcaption>Cya 2012!</figcaption>
</figure>
