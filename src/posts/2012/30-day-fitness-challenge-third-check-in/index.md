---
title: "30 Day Fitness Challenge, Third Check-in"
date: "2012-08-13"
tags: 
  - "fitness"
---

My daughter [Jessie](https://twitter.com/jharleyxo "Jessica Harley"), my friend [David](https://twitter.com/dbyrge "David Byrge") and myself are doing a [fitness challenge](http://michaelharley.me/blog/2012/30-day-fitness-challenge/ "30 Day Fitness Challenge"). This is the third check in.

Jess/Dave, please comment on this post with your updates.

- Myfitnesspal – I was over my calorie goal one day which brings me to being over my calorie goal 5 out of 21 days.
- Distance – 24.1 miles for a total of 52.7 miles
- Weight Loss % – I’m 2.4% down overall. This is the formula I used: \[new weight\]/\[old weight\] \* 100 – 100.

These are the totals I have for Jess & Dave.

Jess

- Myfitnesspal - Over 6 days total
- Distance - 15.66 miles total
- Weight Loss % - 2.01% overall

David

- Myfitnesspal - DNF
- Distance - DNF
- Weight Loss % - DNF
- :|

So with one week to go, the distance part of our challenge is in the bag for me. the Myfitnesspal & weight loss % categories are both in the air.
