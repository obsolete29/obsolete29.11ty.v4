---
title: "Quote Of the Day Douglas Adams"
date: "2012-05-20"
tags: 
  - "qotd"
---

"Isn’t it enough to see that a garden is beautiful without having to believe that there are fairies at the bottom of it too?" -Douglas Adams
