---
title: "Cycling the C&O and GAP Trails (Reloaded): Day 3 Hancock to Cumberland"
date: "2016-10-05"
tags: 
  - co-and-gap-2016
  - "cycling"
  - trip-report
---

Todays Distance: 65.2 miles Total Distance 192.9 miles

[{% myImage "strava-track.png", "Screenshot of Strava track" %}](https://www.strava.com/activities/c-o-day-3-hancock-to-cumberland-734697737)

Day 3 - I dominated myself. Well, I've now gone further than I did last year. It feels pretty good to just go over that mental hump. According to Strava, I'm way faster than last year too.

Today was mostly uneventful. Saw some deer, squirrels, rabbits, turkeys, turtles, snakes and lots of cyclists.

At one point later in the afternoon, I was feeling really tired. I was really struggling to turn the pedals over, even in the lowest gear. For a while, I just thought I was just getitng super tired... until I took my headphones out so that I could hear the rubbing come from the front. It turns out, that mud from the muddy trail had built up in the front fender and was really compacted in there. I pulled over, used my little screwdriver things from my multitool and got it all cleared out. The rest of the miles sailed right along... I mean other than a tender booty and hands. Owie.

Tonight's camp is at the YMCA in Cumberland. $12 to camp in the field across the street and to use the showers. Sweet deal IMO.

Tomorrow is a relatively short day of 44 miles but it's also the hardest climbing day of the trip. Wish me luck as I'm not very good at going up.. I'm much better at going down. B)

{% myImage "day-03-1.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-03-2.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-03-3.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-03-4.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-03-5.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-03-6.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-03-7.jpg", "This post was migrated from my old post. Sorry about no alt text." %}