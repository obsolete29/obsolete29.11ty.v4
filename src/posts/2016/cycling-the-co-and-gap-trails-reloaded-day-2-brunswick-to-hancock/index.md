---
title: "Cycling the C&O and GAP Trails (Reloaded): Day 2 Brunswick to Hancock"
date: "2016-10-04"
tags: 
  - co-and-gap-2016
  - "cycling"
  - trip-report
---

Todays Distance - 72 miles Total Distance - 127.7 miles

[{% myImage "strava-track.png", "Screenshot of Strava track" %}](https://www.strava.com/activities/c-o-day-2-brunswick-to-hancock-733577858)

Day 2 - Butt rope and technical problems.

{% myImage "IMG_3139.jpg", "Butt rope." %}

I usually start my days off by FaceTiming with Jen for the first bit of my ride; today was no different. The problem with being the first rider out of camp is that you get to be the person clearing the spider webs from the trail. Anybody who knows me knows that spiders kind of freak me out. So, I'm riding along and hit the first web... I do that flailing thing that people do when they run into spider webs, trying desperately to knock the spider off my head. Jen of course thinks this is very funny and is laughing hysterically over FaceTime. :D

So anyhoo quick summary because I'm exhausted - will flesh this out more when I have time.

Today was a much harder day than yesterday. Not just the longer distance but the riding part seemed harder. I went 40 miles before breaking for lunch in Williamsburg.

After lunch, it was 30 more miles to the camp site which was supposed to be the camp shelter thing that C&O Bicycle Shop has. Unfortunately, I arrived late so ended up getting a room. Finally, my stupid iphone cable stopped working so my phone died about 5 miles short of finishing. I picked up a cheapie from an Exxon that's working fine so far.

[Photos from today](https://www.flickr.com/photos/michael_harley/albums/72157671310569084).

Tomorrow's destination is Cumberland MD which is only 60 miles. I'll pass the spot where I quit my ride last year so wish me luck.
