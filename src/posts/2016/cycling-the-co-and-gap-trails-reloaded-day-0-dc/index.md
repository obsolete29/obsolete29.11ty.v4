---
title: "Cycling the C&O and GAP Trails (Reloaded): Day 0 - DC"
date: "2016-10-01"
tags: 
  - co-and-gap-2016
  - "cycling"
  - trip-report
---

Editors Note: This post was written on Saturday Oct 1st but post Sunday Oct 2nd.

Day 0. I'm sitting in my room in Washington DC. I've shipped my traveling bags ahead, secured Tank Bicycle and I'm about ready to get things packed up.

I used a Lyft to get over to BicycleSPACE to pick up Tank Bicycle then I just rode the short ride back to the hotel. The route takes you right through a lot of the big sites in DC like the Washington monument and the Whitehouse etc... it's pretty nice.

{% myImage "day-00-01.jpg", "This post was migrated from my old post. Sorry about no alt text." %}

After that my brother and his wife came to take me to lunch and shuttle me around to Target and Dicks to pick up a few odds and ends that I needed. It was super nice being able to visit with them for a bit.

Flying is much better than riding the bus and the plane ticket was only $113 to get myself here 1 way. That's a solid win in my book. The North Face basecamp duffle held all my camping and biking gear just fine. I checked it and everything showed up on the other side in perfect condition.

I gave myself a whole day of being in DC, to get things gathered up and ready and it's been real nice not having to rush around.

I think I'll be using this basic template for all future bicycle trips. Fly, use my duffle to carry my stuff, ship the bike ahead and give myself plenty of time in the starting city to not feel rushed.

It's been raining the last 3-4 days in DC so I'm expecting the C&O Trail to be muddy again this year. Such is life but I'm ready for it.
