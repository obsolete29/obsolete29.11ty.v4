---
title: "Cycling the C&O and GAP Trails (Reloaded): Day 1 DC - Brunswick Family Campground"
date: "2016-10-02"
tags: 
  - co-and-gap-2016
  - "cycling"
  - trip-report
---

Today’s Distance – 55.7 miles Total Distance 55.7 miles

[{% myImage "strava-track.png", "Screenshot of Strava track" %}](https://www.strava.com/activities/dc-to-brunswick-family-campground-732494519)

Day 1 - No butt stuff. That is, I forgot to pack my chamois cream. For those who are uninitiated in the ways of cyclists who ride their bicycles for long periods of time at once, the cream helps to prevent saddle sores and abrasions in general down in the nether regions. As you can imagine it's NOT GOOD to forget this piece of gear. I'm pretty stunned that I forgot it if I'm honest as I had 3 dang lists to keep me straight. I was planning to call ahead to a bike shop near my camp site to find a replacement but at lunch, I ran into a couple cycle tourists who were finishing their tour today and they offered to give me theirs after hearing my tale of woe and need to find a bicycle shop. Yay kindness of strangers!!

{% myImage "day-01-01.jpg", "This post was migrated from my old post. Sorry about no alt text." %}

The trail was as muddy as I was expecting honestly and the conditions were close to what they were last year. I knew fully what to expect though so having the appropriate expectations set helps.

{% myImage "day-01-02.jpg", "This post was migrated from my old post. Sorry about no alt text." %}

Really, the only other notable thing that happened today was that I finished the ride 1.5 hours faster than last years. Part of it was that I wasn't stopping as often to look at things (been there, done that last year). I feel like most of it was that I'm in better shape this year than last - the ride certainly felt easier today than I remember from last year.

The plan for tomorrow is 70 miles - it'll be the longest ride of this trip.

The camp site tonight is near a train transfer area so there's lots of train noises and stuff happening. I brought ear plugs.

{% myImage "day-01-03.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-01-04.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-01-05.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
