---
title: "Cycling the C&O and GAP Trails (Reloaded): Planning"
date: "2016-06-12"
tags: 
  - "co-and-gap-2016"
  - "cycling"
  - trip-report
---

Last year I attempted to ride the C&O and GAP trails from DC to Pittsburgh. I ended up straining/pulling a quad muscle on day 3 and had to [abandon my ride](/posts/2015/10/05/cycling-the-cando-and-gap-trails-2015/). I've decided to attempt the ride again this fall because I'm not going out like no punk.

I'll be making these changes.

- Instead of doing the Greyhound thing, I'm going to fly up. I've been looking at tickets already and they're $233; so worth it.
- Instead of shipping my gear up ahead, I'm going to purchase a [packable duffle](http://thewirecutter.com/reviews/travel-guide/#packable). I'll use the duffle to carry all my gear up on the plane with me. Once I've arrived in DC, I'll then ship the duffle ahead to where I'm staying in Pittsburgh.
- I'll still have [Green Fleet Bicycles](http://greenfleetbikes.com) ship my bike up to [BicycleSPACE](http://www.bicyclespacedc.com) in DC.

I'm really looking forward to the trip. I'm confident that I'll be much better prepared than I was last year.
