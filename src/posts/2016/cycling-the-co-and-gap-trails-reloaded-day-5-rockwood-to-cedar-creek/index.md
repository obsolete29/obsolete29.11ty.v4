---
title: "Cycling the C&O and GAP Trails (Reloaded): Day 5 Rockwood to Cedar Creek"
date: "2016-10-06"
tags: 
  - co-and-gap-2016
  - "cycling"
  - trip-report
---

Todays Distance: 69.6 miles Total Distance: 306.2 miles

[{% myImage "strava-track.png", "Screenshot of Strava track" %}](https://www.strava.com/activities/736676553)

Day 5 - CODE NAME: Animal

Today was really a great day. I can't really explain how nice the GAP is after riding on the C&O. Silky, smooth as butter... so nice. I even aired my tires up to road levels at lunch time and boy did the miles just click by.

Today at lunch time, I figured out my trail name: Animal. I was standing near the path in Ohiopyle (Ohiowhat??!?), about ready to set off on the rest of my day when a lady said "your legend precedes you. It's nice to meet you". I'm like "what??" Let me back up. The past few days, I've been passing these guys that were heading in the same direction. They'd pass me, I'd pass them, we'd have a nice friendly chat while doing so, etc. It turns out, they actually left DC on Saturday while I left on Sunday. Most of the time with these guys was spent yesterday, on the ascent up from Cumberland to the contentitnal divide. I'm a bad climber in cycling terms because I'm big but I just keep turning over the pedals, complaining and grumbling but just click, click, click... one pedal stroke closer every second. So we kept passing each other on the climb. I'd pass, then they'd pass. I'm running a fully loaded touring bicycle while these guys were credit card touring (i.e., staying in motels and eating out most meals). So imagine, here is a big burly, tattooed, bearded dude with fully loading touring rig climbin up the mountain with you while you have a couple bags only. I dunno... I don't think I'm some sort of monster cyclist but they apparently thought so because they were talking to this lady who spoke to me in Ohiopyle. They'd told her "you can't miss him, he's a big guy with a beard and a fully loaded touring bike. The guy's an animal.

I got a big kick out of it. ;)

The other interesting thing that happened to me today was that I ran across this guy pulling the cart pictured below. Like, imagine you're riding your bicycle in the woods and it's been a while since you've seen another living soul then this. At first, I thought it was some guy trying to run a pot stand in the woods. Turns out, he's pulling the thing to Washington DC to protest pot being illegal or whatever. If you notice in the rest of the photos at the bottom, his cart is just a few inches too wide to fit through the baracades and he was so mad. He was talking about overthrowing the government for violating his rights of freedom of movement and he was starting a revolution right there; I suppose he was using Twitter. At this point, I asked if I could take photos of his trailer then promptly set off. So strange.

{% myImage "day-5-01.jpg", "Alt text should go here." %}

38 miles to Pittsburgh so that means I don't have to feel rushed to get going in the morning. I might actually sleep in a little bit later than I have been on my trip. Yay!

{% myImage "day-5-02.jpg", "this photo was migrated without alt text. Sorry about that." %}
{% myImage "day-5-03.jpg", "this photo was migrated without alt text. Sorry about that." %}
{% myImage "day-5-04.jpg", "this photo was migrated without alt text. Sorry about that." %}
{% myImage "day-5-05.jpg", "this photo was migrated without alt text. Sorry about that." %}
{% myImage "day-5-06.jpg", "this photo was migrated without alt text. Sorry about that." %}
{% myImage "day-5-07.jpg", "this photo was migrated without alt text. Sorry about that." %}
{% myImage "day-5-08.jpg", "this photo was migrated without alt text. Sorry about that." %}
{% myImage "day-5-09.jpg", "this photo was migrated without alt text. Sorry about that." %}
{% myImage "day-5-10.jpg", "this photo was migrated without alt text. Sorry about that." %}
