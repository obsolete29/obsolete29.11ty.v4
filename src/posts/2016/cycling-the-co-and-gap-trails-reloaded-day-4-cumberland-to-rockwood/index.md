---
title: "Cycling the C&O and GAP Trails (Reloaded): Day 4 Cumberland to Rockwood"
date: "2016-10-05"
tags: 
  - co-and-gap-2016
  - "cycling"
  - trip-report
---

Todays Distance: 45.7 miles Total Distance 238.6 miles

[{% myImage "strava-track.png", "Screenshot of Strava track" %}](https://www.strava.com/activities/c-o-day-4-cumberland-to-rockwood-735575930)

Day 4 - Fuck hills. I knew today was going to be a lot of climbing but seriously, fuck hills. The first part of the day turned out to be about 22 miles of steady climbing. I think I was averaging between 6-8 mph during that section.

I did probably see some of the prettiest scenery of the trip though so that was cool. Additionally, the GAP trail is much better than the C&O... it's just much nicer to pedal on. It's this compact, fine gravel stuff; not mud like the C&O.

Tonight's camp ground is husky haven in Rockwood, PA. The camp ground itself isn't so bad - I just wish the shower houses were closer. You have to basically bike 3/4th of a mile, across the river to use the shower house; not ideal IMO.

Tomorrow is 59 miles to Perryopolis, PA.

{% myImage "day-04-01.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-04-02.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-04-03.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-04-04.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-04-05.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-04-06.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-04-07.jpg", "This post was migrated from my old post. Sorry about no alt text." %}
{% myImage "day-04-08.jpg", "This post was migrated from my old post. Sorry about no alt text." %}