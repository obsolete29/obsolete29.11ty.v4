---
title: "Cycling the C&O and GAP Trails (Reloaded): Pre-trip anxiety"
date: "2016-09-29"
tags: 
  - "co-and-gap-2016"
  - "cycling"
  - trip-report
---

As I type this, I leave out on my trip tomorrow night. I've heard from BicycleSPACE in DC that my bike has arrived and is ready for me to pick it up on Saturday. I have plane ticket in hand. I have a motel reservation in DC. I've test packed my new duffle like 5 times. I have 3 lists I'm using to try to keep myself straight: Pre-Trip To-DO, Important things not to forget and DC To-Do. The lists help my brain be at ease.

{% myImage "IMG_3082-300x190.jpg", "This image was migrated from the old blog - sorry about no alt text" %}

At this point I'm re-reading my own journal entries from my previous trips, especially last years entries so I can be better prepared. I think I've done everything I can do to be prepared and to feel prepared.

Maybe I should just test pack my bag one more time and go over my lists again to make sure I'm not forgetting anything though....
