---
title: "Cycling the C&O and GAP Trails (Reloaded): Conclusion"
date: "2016-10-13"
tags: 
  - co-and-gap-2016
  - "cycling"
  - trip-report
---

<figure>
  {% myImage "30114915181_36efc24211_o.jpg", "Alt text should go here." %}
  <figcaption>Selfie from Point State Park in Pittsburgh. #Success</figcaption>
</figure>

My bicycle tour from DC to Pittsburgh is in the books and it was a very successful trip. I'll be using this trip as a template for future trips. After each trip, I like to reflect and identify what worked well and what did not work so well so that I might review this next year as I'm preparing for future trips.

### What didn't work so well

Despite having three dang lists to work from, I managed to forget my chamois cream. Rashes on the booty is a lot like pain management in that you really have to stay ahead of it. Once you're in pain, well it's already too late. A kind stranger gave me some stuff to use - although it wasn't chamois cream. It would have been much worse otherwise but it would have been much better if I was slathering on the [button hole](https://www.amazon.com/Enzos-Button-Hole-Chamois-Cream/dp/B00BA1EPK4/ref=sr_1_1?ie=UTF8&qid=1476376460&sr=8-1&keywords=button+hole) straight from the get go. Lesson learned: DON'T FORGET THE DANG CHAMOIS CREAM

I also had some difficulties keeping my phone charged. I was using a non-Apple charging cable and the thing failed on the evening of day 2. I managed to find a gas station and get another little cheapy but that one started to fail a few days later. Additionally, I over estimated how bad ass my new [external battery charger](https://www.amazon.com/gp/product/B012NIQG5E/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1) was so by the 5th night, it was drained. I should have taken more care to charge the external battery. Lessons learned: Go ahead and invest in the [dyno hub](http://www.peterwhitecycles.com/schmidt.php) so that I can charge while pedaling. Additionally, stick with official Apple charging cables and maybe even pack a spare; they're small and light.

I want to reduce my kit. I had a whole bag full of stuff that I barely even broke into - GoPro mounts and odds and ends. I think I can probably fit my kit into the two rear bags if I strapped my tent to the top of the rear rack. I also carried too much food. I ended up eating a fair amount along the way. There's something really nice about pulling up to a diner or pub at the end of a long day of pedaling.  In the future, I'll probably carry less food while planning to eat along the way.

### Trip template going forward

- Ship bike ahead to a bike shop near starting point using bikeflights.com. $49 for shipping plus whatever the bike shops charge ya to box and reassemble. Probably close to $200 total.
- Fly to the destination. I was able to get a ticket to DC for under $150. More than the bus ticket I purchased last year but way more convenient.
- Use my new [North Face base camp duffle](https://www.thenorthface.com/shop/base-camp-duffellarge-cww1?variationId=EPT) to check all my gear on the flight. Last year I shipped my gear ahead to the motel. Checking my stuff was way easier and less stressful.
- Give myself at least one whole day in the starting city/town to get things together. Plus it'll be nice to have a little extra time visiting a new place.

Since Pittsburgh is relatively close to Nashville, I was able to use hotwire to rent a car to drive myself and all my gear back to Nashville. After fees and such, it was only $38 from Thrifty. Tank bicycle fit perfectly into the trunk after the laying the seats down on the Chevy Cruz.

Finally, I wanted to give a huge shoutout to our friend [Eve](https://www.facebook.com/eve.kupersanin) in Pittsburgh. She let me couch surf while in Pittsburgh so I saved a lot of money on motel costs. She was an excellent tour guide and showed me around the city Saturday.
