---
title: "Cycling the C&O and GAP Trails (Reloaded): Day 6 Cedar Creek to Pittsburgh"
date: "2016-10-08"
tags: 
  - co-and-gap-2016
  - "cycling"
  - trip-report
---

Today's Distance: 41 miles Total Distance: 347.2 miles

Day 6 - The big finish

I made it. I rode my bike all the way from Washington DC to Pittsburgh PA. That's a pretty nice feeling.

The ride itself was pretty easy - only 41 miles so I took my time breaking camp and getting on the road. During the night, my external battery and phone battery both died so I felt kind of discombobulated not being connected to friends or family or having a way to really navigate. Additionally, I woke up to the second flat of my trip. This time I actually found the tiny metal splinter and removed it.

I had breakfast at a diner in West Newton and I tried to charge up my devices.

The rest of the ride was uneventful. I spent most of the time in silence (dead phone) and that was actually pretty nice in retrospect.

{% myImage "30114915181_36efc24211_o.jpg", "Alt text should go here." %}

{% myImage "30114927191_e7e58191ee_o.jpg", "Alt text should go here." %}
