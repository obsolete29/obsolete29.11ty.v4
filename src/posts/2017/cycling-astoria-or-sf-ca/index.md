---
title: Cycling Astoria OR to SF CA
desc: In the fall of 2017 I attempted to ride my bicycle from Astoria OR to San Franciso CA. I only made it three days.
date: '2017-10-10'
lastmod: null
tags:
  - cycling
  - cycle-touring
  - trip-report
  - pacific-coast-highway
---
In the fall of 2017, I attempted to ride my bicycle from Astoria OR to San Franciso CA. I ended up quitting my ride after three days. Here is my write up!

- [Planning](#planning)
- [Day 0 Astoria OR](#day-0-astoria-or)
  - [Panic attack](#panic-attack)
  - [Plot Twist](#plot-twist)
  - [The Rest](#the-rest)
- [Day 1 Astoria to Barview campground](#day-1-astoria-to-barview-campground)
- [Day 2 Barview campground to Lincoln City](#day-2-barview-campground-to-lincoln-city)

## Planning

> Originally published 08/16/2017 on my old blog. Migrated here 07/19/2021.

This year, I'm planning what will be my most epic bicycle trip to date. I plan to ride from Astoria OR, along the Pacific coast down to SF CA. It's roughly 800 miles and I'm giving myself 2 weeks to complete the trip. I'll be following the same basic plan I used for [Cycling the C&O and GAP Trails Reloaded](http://michaelharley.me/blog/cycling/cycling-the-co-and-gap-trails-reloaded/).

- I'll use [BikeFlights.com](https://www.bikeflights.com) to have my bike shipped ahead to a local bike shop, [Bikes & Beyond](http://biz179.inmotionhosting.com/~bikesa5/) in Astoria, OR in this case the week before I leave for my trip.
- Book AirBnBs in Portland, Astoria and SF.
- I'll use my North Face Base Camp duffle to carry my gear with me on the flight.
- I'll fly to Portland OR and stay at the AirBnB on Day -1.
- On Day 0, I'll catch the North West Point bus to Astoria OR. Google maps says it's an almost 3 hour trip from Portland to Astoria and it leaves at 9:30am.
- Arrive in Astoria and pick up my bicycle and check into my AirBnB
- Day 1, start pedaling.

If everything goes to plan, I should arrive in SF on a Friday afternoon/evening. I plan to spend the weekend in SF and leave out on Sunday.

## Day 0 Astoria OR

> Originally published 10/08/2017 on my old blog. Migrated here 07/19/2021.

As I type this, I’m sitting in my AirBnb in Astoria, OR. I’ve collected my bicycle, sorted and packed my gear and I’m pretty much ready to start pedaling first thing in the morning.

Astoria is a pretty cool little town. The view from the Astoria Column is breathtaking. But first, let me detail my journey to get here because there was a #PlotTwist.

<figure>
  {% myImage "mike-at-bna.jpg", "Mike standing with his bag packs, sun glasses and hat at the Nashville airport." %}
  <figcaption>Ok, let's do it!</figcaption>
</figure>

### Panic attack

I caught my flight as expected Saturday afternoon. I had a two hour layover in Chicago then caught my flight to Portland. I opted for a window seat because I thought I could more easily sleep on the 4 hour flight from Chicago to Portland by using my camp pillow and leaning against the wall/window.

That was working perfectly but about an hour and half into the flight I woke up suddenly feeling claustrophobic. I don’t know if I was having a bad dream or something but I really felt panicked and I was struggling to breath. I asked the other two in my row to please excuse me and they let me out. I walked slowly to the bathroom to just be out of that little bitty seat for minute. After some calm, deep breathing I felt good enough to get back into my seat. I kind of struggled the rest fo the flight it stay calm. I had to distract myself with the magazines and talking to my seat neighbors. Anyhoo, I think I understand a little bit now what a panic attack is. It sucked pretty hard core!

### Plot Twist

After the longest flight in the history of the world, my plane finally arrived in Portland and it felt so good to be off the plane. I hussled down to the rental car area to pick up my car and for reasons that I won’t get into here, my reservation wasn’t available. 😑

Ok fine... \*deep breaths\* I’ll just go to plan B... there’s bus/shuttle service that runs between Portland and Astoria. From my research on the internet, I know that departure point for the bus was Union station. To make a long story short, I ended up sleeping on a bench in front of Union station in Portland - the station closes at 10pm. I had my camping gear so I setup my sleeping pad and sleeping bag and camp pillow so I was pretty comfortable if somewhat nervous as there were other people waiting for the station to open. In addition, there was a homeless camp across the street and there was so much activity. Every little sound had me waking up to check my bag.

{% myImage "shuttle.jpg", "A shuttle bus in the predawn morning." %}

### The Rest

Once I was on the bus at 6:30am, everything else was pretty routine. It took about 3 hours to get to Astoria - the driver even let me off right at the bike shop ([Bikes and Beyond](http://biz179.inmotionhosting.com/~bikesa5)) as they were opening. I transferred my gear from my duffle into my bike panniers at the shop. Unfortunately, the only pack and mail place in Astoria is closed on the weekends so I’ll have to pack my duffle and clothes that I was going to send ahead to SF, until I can find a pack and ship place.

{% myImage "mike-hoodie-waiting-for-shuttle.jpg", "A selfie of Mike wearing a hoodie with the hood up." %}

My [AirBnb](https://abnb.me/EVmg/SRedHmw23G) is pretty nice... especially compared to a bench in front of a train station in downtown Portland. ;)

I did the tourist thing a bit and went up to the Astoria Column. I walked the entire way there and back, found some dinner and am now relaxing in my room.

Tomorrow 79 miles to Cape Lookout State Park. #ShutUpLegs

{% myImage "tank-bicycle-at-the-bike-shop.jpg", "Clean and unloaded Surly Disc Trucker bicycle sitting at the bike shop." %}
{% myImage "tank-bicycle-front-of-bike-shop.jpg", "Tank bicycle in front of the bike shop fully loaded and waiting to leave." %}
{% myImage "bay-with-ships.jpg", "Photo of the bay from a dock with container ships sitting anchored." %}
{% myImage "mike-selfie-from-astoria-column.jpg", "Selfie of Mike from the top of Astoria Column with the bay in the back ground." %}
{% myImage "mike-selfie-with-astoria-column.jpg", "Selfie of Mike from the bottom of Astoria Column with the column in the back ground." %}
{% myImage "mike-selfie-from-astoria-column2.jpg", "Selfie of Mike from the top of Astoria Column with the bay in the back ground." %}
{% myImage "astoria-column-pano.jpg", "Panoramic view of the bay from the top of Astoria Column." %}
{% myImage "bay-from-astoria-column.jpg", "The bay from the top of Astoria Column. Weather is dark and stormy." %}

## Day 1 Astoria to Barview campground

> Originally published 10/09/2017 on my old blog. Migrated here 07/19/2021.

Todays Distance – 59 miles Total Distance – 59 miles

[Strava Link](https://www.strava.com/activities/1221854461)

Well, today was an interesting first day. I had a flat, I underestimated the climbing challenges today and I didn’t get to my planned destination.

I’m super exhausted so I’ll come back and clean this post up a bit but here are some photo impressions from today's ride.

<figure>
  {% myImage "day1-cannon-beach-1.jpg", "Sunny beach photo of the Goonies rock on Cannon Beach with a wind swept evergreen tree in the foreground." %}
  {% myImage "day1-oregon-beach-from-cliff.jpg", "Photo from a cliff with the picturesque beach view in the distance." %}
  {% myImage "day1-pano-cannon-beach.jpg", "Panorama photo of a sunny Cannon beach." %}
  <figcaption>The Pacific coast is GORGEOUS. Wow, for reals.</figcaption>
</figure>

<figure>
  {% myImage "day1-tank-bicycle-gets-flat.jpg", "Tank bicycle on the side of the road with the back wheel off and panniers unloaded." %}
  <figcaption>I caught a flat tire within the first 20 miles of day 1. I even just put new, expedition grade tires on too. Bleh.</figcaption>
</figure>

There were 3 serious climbs today. I’ve now calibrated what the little lines on the map elevation profile actually MEANS.

<figure>
  {% myImage "day1-sketchy-bridge.jpg", "Photo of tunnel enterance from the side of the road. Small sign says to push button before entering." %}
  <figcaption>During the 2nd climb, there was a super sketchy bridge situation. For those who saw my Facebook live, you know what’s up. For everybody else, go check out my Facebook live video.</figcaption>
</figure>

With all the climbing, I failed to reach my mileage goals for today so I camped at a different camp ground than I planned. They had hot showers (coin operated and no lights!!) so that was good enough for me. I can hear the surf in the distance and it’s a very brisk night. I expect to sleep really great tonight.

I’m aiming for Lincoln City tomorrow and is about 65 miles or there abouts with two big climbs. Wish me luck.

Day one photos are [here](https://www.flickr.com/gp/michael_harley/rL7p3Q).

## Day 2 Barview campground to Lincoln City

> Originally published 10/10/2017 on my old blog. Migrated here 07/19/2021.


Today’s Distance: 63 miles Total Distance: 122 miles

[Strava Link](https://www.strava.com/activities/astoria-to-sf-day-2-1223262271?utm_campaign=ride_share&utm_content=3132582&utm_medium=widget&utm_source=obsolete29.com)

Day two! Honestly, I woke up feeling somewhat disappointed that I didn’t really hit my mileage goal for day one. As you can see from my Strava track, a lot of the route today was inland a bit so I was riding through farmland and I swear I kept seeing trucks of hops going by. Also, the aroma of manure was thick in the air. I actually saw a pump truck, pumping liquified cow manure on a field at one point so that explained that.

Around mid-morning I finally made it to Tillimook and was able to ship some stuff ahead to myself. Turns out Astoria is closed on the weekends! So I was toting like 8-10 lbs of extra weight on my bike. Additionally, I tossed my u-lock because I brought the wrong key; worthless. I’ll need to pickup a new u-lock soon though I’ve not found much opportunity to lock up my bike. Total weight off the bike: ~15 lbs.

Lunch today was veggie pizza in Pacific City. The pizza was delicious but I ended up eating too much and was sick a few miles down the road. Ugh.

The rest of the day passed pretty uneventful. The big climb of the day was saved for the very last and boy was it a doozy. Probably the hardest, longest climb I’ve ever done; can’t remember a harder or longer one. I arrived at camp after dark, quickly setup and zonked out.

I don’t have an exact spot picked out for a target today. Just going to pedal until I’m too tired.

{% myImage "day2-coast-view1.jpg", "A view of the picturesque beach from the side of the road." %}
{% myImage "day2-coast-view2.jpg", "A large boulder in the distance on the beach." %}
{% myImage "day2-view-from-the-road.jpg", "Photo from the side of the road with a long straight road disappearing into the distance with mountains in the background." %}
{% myImage "day2-bay-rock-formations.jpg", "Interesting rock formations in the bay in the morning twilight." %}
