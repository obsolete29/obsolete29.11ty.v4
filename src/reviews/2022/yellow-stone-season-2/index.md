---
title: "Watched Yellow Stone Season 2"
date: "2022-03-13T07:32:36.850-05:00"
rating: "★★★★"
tags:
  - "watched"
  - "series"
  - "yellowstone"
  - "drama"
---
{% myImage "cover.jpg", "Title cover for {{ Yellow Stone Season 2 }}." %}

- [Yellow Stone Season 2](https://www.paramountnetwork.com/shows/yellowstone)
- ★★★★

I don't know if I just got used to the writing and acting or got more into the characters but the show definitely gets better in season 2.

Liked it a lot! 4 stars.