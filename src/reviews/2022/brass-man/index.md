---
title: "Read Brass Man"
date: "2022-02-19T11:00:47.992-06:00"
tags:
  - "read"
  - "militaryscifi"
  - "agentcormac"
  - "nealasher"
  - "polityuniverse"
rating: "★★★★"
---
{% myImage "cover.jpg", "Title cover for {{ Brass Man }}." %}

- [Brass Man](https://www.nealasher.co.uk/book/brass-man/) (Agent Cormac #3)
- [Neal Asher](https://www.nealasher.co.uk)
- ★★★★

Some of my favorite sci-fi has AI characters as main characters and Neal does this about as well as anyone. Pretty enjoyable even though some of tech seems nearly magical and Skellor is annoying as an evil character. Anyhoo, really enjoyed it. Thinking I'll take a break from the military sci-fi for a minute.