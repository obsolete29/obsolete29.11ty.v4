---
title: "Read Project Hail Mary"
date: "2022-02-25T18:18:46.757-06:00"
rating: "★★★★★"
tags:
  - "read"
  - "andyweir"
  - "sciencefiction"
---
{% myImage "cover.jpg", "Title cover for {{ Project Hail Mary }}." %}

- [Project Hail Mary](https://www.andyweirauthor.com/books/project-hail-mary-hc/project-hail-mary-el)
- [Andy Weir](https://www.andyweirauthor.com/)
- ★★★★★

Really loved this book. Funny and smart.