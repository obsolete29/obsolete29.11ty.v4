---
title: "Watched My Salinger Year"
date: '2022-02-09T20:29:54.492-06:00'
rating: "★★★★"
tags:
  - "watched"
  - "movie"
  - "drama"
---
{% myImage "cover.jpg", "Title cover for {{ Watched My Salinger Year }}." %}

- [Watched My Salinger Year](https://en.wikipedia.org/wiki/My_Salinger_Year)
- ★★★★

What a lovely coming of age story. We watched Margaret Qualley previously in [Maid](https://www.netflix.com/title/81166770) and she gives a great performance here. Four stars.