---
title: "Watched The Tinder Swindler"
date: "2022-02-25T05:51:25.399-06:00"
rating: "★★★½"
tags:
  - "watched"
  - "movie"
  - "netflix"
  - "documentary"
---
{% myImage "cover.jpg", "Title cover for {{ The Tinder Swindler }}." %}

- [The Tinder Swindler](https://en.wikipedia.org/wiki/The_Tinder_Swindler)
- ★★★½

Rachelle was hearing good things about this true crime documentary in her feeds so we decided to give it a go.

My main take away from the movie is that money/wealth is a bad way to determine if a person is good or not, yet so many women were swept off their feet by the illusion of a young wealthy, attractive man seducing them. This guy is a piece of shit but people are blinded by nice cars and private jets and summer vacations to Greece. It's just so shallow.