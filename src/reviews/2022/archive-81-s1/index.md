---
title: "Watched Archive 81 S1"
date: '2022-02-06T17:47:27.116-06:00'
rating: "★★★★"
tags:
  - "watched"
  - "series"
  - "netflix"
  - "archive-81"
  - "horror"
---
{% myImage "cover.jpg", "Title cover for {{ Archive 81 S1 }}." %}

- [Archive 81 S1](https://en.wikipedia.org/wiki/Archive_81)
- ★★★★

What a fun series, especially for a Netflix one. It was spooky with super natural themes and had some jump scares but not so many to be annoying. It was well acted and the pace of the series was good for me. Both [Mamoudou Athie](https://en.wikipedia.org/wiki/Mamoudou_Athie) and [Dina Shihabi](https://en.wikipedia.org/wiki/Dina_Shihabi) were great for me and I enjoyed their performances quite a lot. 

I really liked it and am looking forward to S2. Four stars.