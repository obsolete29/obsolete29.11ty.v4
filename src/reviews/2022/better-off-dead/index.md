---
title: "Read Better Off Dead"
date: "2022-03-06T13:57:59.555-06:00"
rating: "★★★½"
tags:
  - "read"
  - "leechilds"
  - "jackreacher"
  - "fiction"
  - "thriller"
---
{% myImage "cover.jpg", "Title cover for {{ Better Off Dead }}." %}

- [Better Off Dead](https://www.jackreacher.com/us/books/better-off-dead/) (Jack Reacher #26)
- [Lee Childs](https://www.jackreacher.com/us/authors/)
- ★★★½

I'm pretty late to the Jack Reacher series but I've read the past four or five and I have to say, I've really enjoyed them. They're fun, easy reads and if you like a big manly man, kicking ass and taking names for the cause of good, then maybe you'd enjoy this.

3.5 stars