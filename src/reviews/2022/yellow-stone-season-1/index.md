---
title: "Watched Yellow Stone Season 1"
date: "2022-03-13T07:23:08.669-05:00"
rating: "★★★"
tags:
  - "watched"
  - "series"
  - "yellowstone"
  - "drama"
---
{% myImage "cover.jpg", "Title cover for {{ Yellow Stone Season 1 }}." %}

- [Yellow Stone Season 1](https://www.paramountnetwork.com/shows/yellowstone)
- ★★★

We started watching Season 1 of Yellow Stone after a recommendation from one of Rachelle's friends. She described it as Succession for cowboys. Plus, I know it was popular but I personally wasn't really that interested in watching it. The previews I watched and blurbs I read, it seemed kind of a stereotypical trope. The big evil banker people are trying to take a ranch from cowboys.

In retrospect, I think I probably need to do some internal examination about my feelings here. I watch many other types of shows with tropes but it's tropes I'm into or find interesting...  Or perhaps on topics that I like, the tropes are not as glaring? Idk.

The show did seem to have lots of tropes that kind of annoyed me but the story was good enough to keep me interested. 3 stars overall for season 1.