---
title: "Watched Spider-Man: No Way Home"
date: "2022-02-19T15:10:49.682-06:00"
rating: "★★★"
tags:
  - "watched"
  - "movie"
  - "marvel"
  - "action"
  - "superhero"
  - "spider-man"
---
{% myImage "cover.jpg", "Title cover for {{ Watched Spider-Man: No Way Home }}." %}

- [Watched Spider-Man: No Way Home](https://en.wikipedia.org/wiki/Spider-Man%3A_No_Way_Home)
- ★★★

Rachelle and I had some time to burn on Sunday so we decided to take in Spider-Man: No Way Home.

I've never really been a huge Spider-Man fan. Of the Spider-Mans (Spider-Men?), Tom Holland has been my favorite. I did really enjoy the crossover from the other Spider-Man movies. Overall, the movie wasn't really that great. Tom Holland, Zendaya and Benedict Cumberbatch carried the movie for me. Some of the characters were boring and unexamined, like Sandman and Lizard. Their motivations in the movie just didn't make much sense.

The main plot mechanism of this Spider-Man was the same as Spider-Man: Into the Spider-Verse so that was a little disappointing. Furthermore, it just wasn't believable to me that Dr Strange would agree to cast a spell that would break the multi-verse just because a few teenagers didn't get into MIT.

I liked it ok but I'd say it was closer to 2.75 and I'll let it round up to 3 stars.