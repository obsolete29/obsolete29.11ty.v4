---
title: "Watched Too Close"
date: "2022-02-20T05:47:59.119-06:00"
rating: "★★★★½"
tags:
  - "watched"
  - "series"
  - "drama"
---
{% myImage "cover.jpg", "Title cover for {{ Too Close }}." %}

- [Too Close](https://en.wikipedia.org/wiki/Too_Close_(TV_series))
- ★★★★½

I enjoyed this little 3 part mini-series more than I expected. It's a drama and we watched it on the Sundance app. [Emily Watson](https://en.wikipedia.org/wiki/Emily_Watson) was great. [Denis Gough](https://en.wikipedia.org/wiki/Denise_Gough) is new to me but I really enjoyed her performance.