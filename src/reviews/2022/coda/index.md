---
title: "Watched CODA"
date: "2022-02-17T06:46:55.165-06:00"
rating: "★★★★"
tags:
  - "watched"
  - "movie"
  - "appletv"
  - "drama"
---
{% myImage "cover.jpg", "Title cover for {{ CODA }}." %}

- [CODA](https://en.wikipedia.org/wiki/CODA_(2021_film))
- ★★★★

Great movie. Rachelle and I both really enjoyed it.