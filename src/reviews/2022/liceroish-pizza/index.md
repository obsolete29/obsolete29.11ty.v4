---
title: "Watched Liceroish Pizza"
date: "2022-03-17T06:17:58.454-05:00"
rating: "★★★½"
tags:
  - "watched"
  - "movie"
  - "drama"
  - "comingofage"
---
{% myImage "cover.jpg", "Title cover for {{ Liceroish Pizza }}." %}

- [Liceroish Pizza](https://www.unitedartistsreleasing.com/licorice-pizza/)
- ★★★½

This movie was highly rated and was nominated or won an award lately so it makes me feel kind of dumb I didn't like it as much as critics. It was a coming of age story of two young people in 70s. It was good but about 30 minutes too long as it kid of lost the thread for me towards the end. 